<?php

/* AdventureLoginBundle:Member:home.html.twig */
class __TwigTemplate_45f47aad17c6753a51a2dc4708346227066fa5c74047edc8ce497cd526c59fd7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::headerbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::headerbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class=\"fixed-top\">

   <!-- BEGIN CONTAINER -->
   <div id=\"container\" class=\"row-fluid\">
     
      <!-- BEGIN PAGE -->  
      <div id=\"main-content\" class=\"maincon\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE CONTENT-->
           
                <div class=\"row-fluid\">
                \t<div class=\"span6 pull-left \">
                    <span class=\"span12 text-center page-title headerbg text-info\"><b>Adventure photo, video, trails</b></span>
                        <div class=\"span4 overall\" style=\"margin-left:0px\">\t
                            <div class=\"circle-container\">
                                <div class=\"outer-ring\"></div>
                                <div class=\"circle\">
                                   <a class=\"\" href=\"http://adventureclicknblog.com/trails.php\"> 
                                    <div class=\"front\">
                                        <p>10</p>
                                    </div>
                                    <div class=\"back\">
                                       
                                    </div>
                                    </a>
                                </div>
                                <h3 class=\"text-center\">Adventure Trails/ Trips / Tours</h3>
                            </div>
                         </div>
                         
                         <div class=\"span4 overall\">\t
                  \t\t<div class=\"circle-container\">
                            <div class=\"outer-ring\"></div>
                            <div class=\"circle\">
                               <a class=\"\" href=\"http://adventureclicknblog.com/allAlbums.php\"> 
                                <div class=\"front\">
                                    <p>73</p>
                                </div>
                                <div class=\"back\">
                                   
                                </div>
                                </a>
                            </div>
                            <h3 class=\"text-center\">Adventure Photos</h3>
                        </div>
                     </div>
                     
                     <div class=\"span4 overall\">\t
                  \t\t<div class=\"circle-container\">
                            <div class=\"outer-ring\"></div>
                            <div class=\"circle\">
                               <a class=\"\" href=\"http://adventureclicknblog.com/allVideos.php\"> 
                                <div class=\"front\">
                                    <p>120</p>
                                </div>
                                <div class=\"back\">
                                   
                                </div>
                                </a>
                            </div>
                            <h3 class=\"text-center\">Adventure Videos</h3>
                        </div>
                     </div>
                     
                     <div class=\"span4 overall\" style=\"margin-left:0px\">\t
                  \t\t<div class=\"circle-container\">
                            <div class=\"outer-ring\"></div>
                            <div class=\"circle\" >
                               <a class=\"\" href=\"http://adventureclicknblog.com/travelogues.php\"> 
                                <div class=\"front\">
                                    <p>100</p>
                                </div>
                                <div class=\"back\">
                                   
                                </div>
                                </a>
                            </div>
                            <h3 class=\"text-center\">Adventure Travelogues</h3>
                        </div>
                     </div>
                     
                     <div class=\"span4 overall\">\t
                  \t\t<div class=\"circle-container\">
                            <div class=\"outer-ring\"></div>
                            <div class=\"circle\">
                               <a class=\"\" href=\"http://adventureclicknblog.com/specials.php\"> 
                                <div class=\"front\">
                                    <p>89</p>
                                </div>
                                <div class=\"back\">
                                   
                                </div>
                                </a>
                            </div>
                            <h3 class=\"text-center\">Adventure Specials</h3>
                        </div>
                     </div>
                     
                     <div class=\"span4 overall\">\t
                  \t\t<div class=\"circle-container\">
                            <div class=\"outer-ring\"></div>
                            <div class=\"circle\">
                               <a class=\"\" href=\"http://adventureclicknblog.com/photologues.php\"> 
                                <div class=\"front\">
                                    <p>32</p>
                                </div>
                                <div class=\"back\">
                                   
                                </div>
                                </a>
                            </div>
                            <h3 class=\"text-center\">Adventure photolouges</h3>
                        </div>
                     </div>
     
                    
                \t</div>
                    <div class=\"span6\">
                    <span class=\" text-center page-title headerbg text-info\"><b>Request list</b></span>
                    <!--marquee start here-->
                    <article class=\"marquee page  pull-right\">
 \t\t\t\t\t\t  
                          <ul class=\"timeline\">
                           <!--Display list of request start-->
                           ";
        // line 138
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "request"));
        foreach ($context['_seq'] as $context["_key"] => $context["request"]) {
            // line 139
            echo "                                <li class=\"timeline-milestone is-current\">
                                  <div class=\"timeline-action is-expandable expanded\">
                                    <h2 class=\"title\">";
            // line 141
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "name"), "html", null, true);
            echo "</h2>
                                        ";
            // line 142
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "updatedAt"), "d-M-Y"), "html", null, true);
            echo "
                                    <div class=\"content\">
                                      <ul class=\"file-list\">
                                        <li>Zone : ";
            // line 145
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "zone"), "html", null, true);
            echo "</li>
                                        <li>Location : ";
            // line 146
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "location"), "html", null, true);
            echo "</li>
                                        <li>Day : ";
            // line 147
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "day"), "html", null, true);
            echo "</li>
                                        <li>Description : ";
            // line 148
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "description"), "html", null, true);
            echo "</li>
                                      </ul>
                                    </div>
                                  </div>
                                </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['request'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 154
        echo "                           <!--Display list of request end--> 
                          </ul>
                        </article>
                         <!--marquee End here-->
                   </div>
                </div>
                
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   ";
    }

    // line 170
    public function block_javascripts($context, array $blocks = array())
    {
        // line 171
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

  <script type=\"text/javascript\">
  \$('.marquee').marquee({
    //speed in milliseconds of the marquee
    duration: 11000,
    //gap in pixels between the tickers
    gap: 50,
    //time in milliseconds before the marquee will start animating
    delayBeforeStart: 1000,
    //'left' or 'right'
    direction: 'up',
    //true or false - should the marquee be duplicated to show an effect of continues flow
    duplicated: true,
\t
\tpauseOnHover:true,
\t//pauseOnCycle:'resume'

});

  </script>

 ";
    }

    public function getTemplateName()
    {
        return "AdventureLoginBundle:Member:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 170,  207 => 148,  195 => 145,  185 => 141,  344 => 190,  336 => 158,  333 => 157,  328 => 26,  325 => 25,  318 => 193,  316 => 189,  308 => 187,  304 => 186,  300 => 185,  296 => 184,  292 => 183,  288 => 182,  284 => 181,  272 => 178,  260 => 175,  256 => 174,  248 => 172,  216 => 147,  200 => 136,  190 => 122,  170 => 108,  126 => 63,  211 => 127,  181 => 139,  129 => 66,  279 => 148,  275 => 147,  265 => 143,  261 => 142,  257 => 141,  222 => 115,  188 => 93,  167 => 107,  178 => 82,  175 => 84,  161 => 31,  150 => 81,  114 => 54,  110 => 53,  90 => 41,  127 => 87,  124 => 86,  97 => 61,  290 => 151,  286 => 150,  280 => 180,  276 => 179,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 171,  236 => 133,  231 => 160,  197 => 103,  191 => 99,  180 => 115,  165 => 75,  146 => 65,  65 => 21,  53 => 18,  152 => 85,  148 => 68,  134 => 73,  76 => 26,  70 => 33,  34 => 4,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 189,  337 => 103,  322 => 101,  314 => 99,  312 => 188,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 177,  264 => 176,  258 => 140,  252 => 173,  247 => 78,  241 => 77,  229 => 157,  220 => 70,  214 => 128,  177 => 138,  169 => 91,  140 => 55,  132 => 67,  128 => 49,  107 => 36,  61 => 20,  273 => 96,  269 => 144,  254 => 92,  243 => 88,  240 => 171,  238 => 85,  235 => 74,  230 => 82,  227 => 117,  224 => 71,  221 => 77,  219 => 154,  217 => 75,  208 => 68,  204 => 109,  179 => 69,  159 => 91,  143 => 64,  135 => 68,  119 => 62,  102 => 46,  71 => 21,  67 => 20,  63 => 19,  59 => 18,  94 => 28,  89 => 28,  85 => 40,  75 => 22,  68 => 24,  56 => 18,  87 => 25,  28 => 5,  93 => 28,  88 => 48,  78 => 21,  27 => 7,  46 => 10,  44 => 15,  31 => 3,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 82,  171 => 81,  166 => 37,  163 => 62,  158 => 30,  156 => 66,  151 => 77,  142 => 66,  138 => 69,  136 => 70,  121 => 57,  117 => 44,  105 => 51,  91 => 27,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 23,  72 => 16,  69 => 25,  47 => 15,  40 => 7,  37 => 14,  22 => 1,  246 => 133,  157 => 73,  145 => 67,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 57,  111 => 53,  108 => 57,  101 => 48,  98 => 53,  96 => 43,  83 => 24,  74 => 14,  66 => 34,  55 => 17,  52 => 17,  50 => 24,  43 => 14,  41 => 15,  35 => 12,  32 => 4,  29 => 6,  209 => 82,  203 => 147,  199 => 146,  193 => 73,  189 => 142,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 76,  164 => 77,  162 => 57,  154 => 78,  149 => 66,  147 => 58,  144 => 73,  141 => 48,  133 => 69,  130 => 41,  125 => 44,  122 => 55,  116 => 41,  112 => 51,  109 => 54,  106 => 36,  103 => 32,  99 => 31,  95 => 45,  92 => 44,  86 => 28,  82 => 33,  80 => 30,  73 => 26,  64 => 20,  60 => 19,  57 => 19,  54 => 10,  51 => 16,  48 => 11,  45 => 10,  42 => 7,  39 => 7,  36 => 13,  33 => 13,  30 => 2,);
    }
}
