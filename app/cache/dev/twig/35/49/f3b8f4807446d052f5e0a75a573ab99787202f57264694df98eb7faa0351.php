<?php

/* AdventureLoginBundle:Login:login.html.twig */
class __TwigTemplate_3549f3b8f4807446d052f5e0a75a573ab99787202f57264694df98eb7faa0351 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>

<head>

  <meta charset=\"UTF-8\">

  <title>Adventure Bidding System | Login</title>
    
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
</head>

<body>
\t<div id=\"flatix\">
\t\t<header class=\"headertop\">
        \t<h3>Adventure Bidding System</h3>
\t\t\t<span class=\"title\"><strong>Member Sign In</strong></span>
\t\t</header>
       
\t\t<section> <div class=\"\" title=\"login notification massege\"></div>
\t\t\t<form class=\"loginform\" action=\"#\">
\t\t\t\t<input type=\"text\" name=\"email\" class=\"maill\" placeholder=\"E-Mail Address\">
\t\t\t\t<input type=\"password\" name=\"password\" class=\"passwordd\" placeholder=\"Password\">
\t\t\t\t<a href=\"#myModal\" role=\"button\" data-toggle=\"modal\" title=\"Lost your password?\">Lost your password?</a>
\t\t\t\t";
        // line 27
        echo "                                <a href=\"";
        echo $this->env->getExtension('routing')->getPath("dashboard");
        echo "\" class=\"login\" title=\"Sign In\">SIGN IN</a>
\t\t\t</form>
\t\t\t<div class=\"or\">
\t\t\t\t<div class=\"facebook\">
\t\t\t\t\t<a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("usersignup");
        echo "\" title=\"Sign up\">SignUp as User</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"twitter\">
\t\t\t\t\t<a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("orgsignup");
        echo "\" title=\"Sign up\">SignUp as Organizer</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</section>
\t\t<footer>
\t\t\t
            Copyright &copy; 2006 - 2015
            
\t\t</footer>
\t</div>
    

<!--model for forgate password-->
    <div id=\"myModal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-header\">
    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
    <h4 id=\"myModalLabel\">Enter your email id to get a link</h4>
    </div>
    <div class=\"modal-body\">
        <form action=\"#\" method=\"post\">
            <input type=\"email\" class=\"span6\" placeholder=\"Enter your email id\">
            <p>After click on submit, please check your mail</p>
    </div>
    <div class=\"modal-footer\">
    <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>
    <button class=\"btn btn-primary\">Submit</button>
    </div>
    </div>
    
 <!--End model for forgate password-->   

    
    

  
  <script src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>  
  <script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>


</body>

</html>

";
    }

    public function getTemplateName()
    {
        return "AdventureLoginBundle:Login:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 11,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 51,  128 => 49,  107 => 36,  61 => 13,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 56,  135 => 53,  119 => 42,  102 => 32,  71 => 17,  67 => 15,  63 => 15,  59 => 14,  94 => 28,  89 => 20,  85 => 25,  75 => 17,  68 => 14,  56 => 9,  87 => 25,  28 => 5,  93 => 28,  88 => 6,  78 => 21,  27 => 7,  46 => 7,  44 => 12,  31 => 8,  38 => 6,  26 => 6,  24 => 3,  25 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 56,  121 => 46,  117 => 44,  105 => 70,  91 => 27,  62 => 23,  49 => 19,  21 => 2,  19 => 1,  79 => 18,  72 => 16,  69 => 25,  47 => 9,  40 => 8,  37 => 10,  22 => 3,  246 => 90,  157 => 56,  145 => 46,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 43,  111 => 37,  108 => 36,  101 => 32,  98 => 31,  96 => 31,  83 => 25,  74 => 14,  66 => 34,  55 => 15,  52 => 27,  50 => 10,  43 => 6,  41 => 7,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 57,  154 => 58,  149 => 51,  147 => 58,  144 => 49,  141 => 48,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 41,  112 => 42,  109 => 71,  106 => 36,  103 => 32,  99 => 31,  95 => 28,  92 => 21,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 14,  60 => 31,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 10,);
    }
}
