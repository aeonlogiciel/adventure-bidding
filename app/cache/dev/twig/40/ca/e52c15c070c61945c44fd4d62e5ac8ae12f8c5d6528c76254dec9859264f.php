<?php

/* AdventureBiddingBundle:event:show.html.twig */
class __TwigTemplate_40cae52c15c070c61945c44fd4d62e5ac8ae12f8c5d6528c76254dec9859264f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>event</h1>

    <table class=\"record_properties\">
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "id"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Ename</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "ename"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Eactivity</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eActivity"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Eorganizer</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eOrganizer"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Budget</th>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "budget"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Econtactdetail</th>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eContactDetail"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Duration</th>
                <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "duration"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Edate</th>
                <td>";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eDate"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Emonth</th>
                <td>";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eMonth"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Eyear</th>
                <td>";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eYear"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Edifficulty</th>
                <td>";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eDifficulty"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Ezone</th>
                <td>";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eZone"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Continent</th>
                <td>";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "continent"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Country</th>
                <td>";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "country"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Estate</th>
                <td>";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eState"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Eregion</th>
                <td>";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eRegion"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Edestination</th>
                <td>";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eDestination"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Ebrief</th>
                <td>";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eBrief"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Logo</th>
                <td>";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "logo"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Status</th>
                <td>";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "status"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Eregistrationdate</th>
                <td>";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "eRegistrationDate"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Flag</th>
                <td>";
        // line 94
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "flag"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "email"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Uniquee</th>
                <td>";
        // line 102
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "uniquee"), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 109
        echo $this->env->getExtension('routing')->getPath("event");
        echo "\">
            Back to the list
        </a>
    </li>
    <li>
        <a href=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_edit", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
        echo "\">
            Edit
        </a>
    </li>
    <li>";
        // line 118
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "delete_form"), 'form');
        echo "</li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:event:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  218 => 114,  210 => 109,  186 => 94,  172 => 86,  81 => 34,  192 => 75,  174 => 66,  155 => 59,  23 => 1,  233 => 113,  184 => 76,  137 => 66,  58 => 11,  225 => 118,  213 => 78,  205 => 21,  198 => 101,  104 => 49,  237 => 170,  207 => 148,  195 => 145,  185 => 97,  344 => 190,  336 => 158,  333 => 157,  328 => 26,  325 => 25,  318 => 193,  316 => 189,  308 => 187,  304 => 186,  300 => 185,  296 => 184,  292 => 183,  288 => 182,  284 => 181,  272 => 178,  260 => 175,  256 => 174,  248 => 172,  216 => 79,  200 => 102,  190 => 122,  170 => 108,  126 => 63,  211 => 127,  181 => 96,  129 => 66,  279 => 148,  275 => 147,  265 => 143,  261 => 142,  257 => 141,  222 => 101,  188 => 93,  167 => 62,  178 => 73,  175 => 84,  161 => 91,  150 => 82,  114 => 42,  110 => 53,  90 => 30,  127 => 52,  124 => 86,  97 => 61,  290 => 151,  286 => 150,  280 => 180,  276 => 179,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 171,  236 => 133,  231 => 160,  197 => 103,  191 => 99,  180 => 69,  165 => 82,  146 => 65,  65 => 21,  53 => 18,  152 => 60,  148 => 78,  134 => 73,  76 => 26,  70 => 38,  34 => 3,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 189,  337 => 103,  322 => 101,  314 => 99,  312 => 188,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 177,  264 => 176,  258 => 140,  252 => 173,  247 => 78,  241 => 77,  229 => 112,  220 => 70,  214 => 128,  177 => 95,  169 => 93,  140 => 55,  132 => 50,  128 => 49,  107 => 47,  61 => 20,  273 => 96,  269 => 144,  254 => 92,  243 => 88,  240 => 171,  238 => 85,  235 => 74,  230 => 82,  227 => 117,  224 => 110,  221 => 109,  219 => 154,  217 => 75,  208 => 22,  204 => 109,  179 => 90,  159 => 60,  143 => 56,  135 => 54,  119 => 50,  102 => 46,  71 => 21,  67 => 26,  63 => 19,  59 => 6,  94 => 28,  89 => 28,  85 => 40,  75 => 22,  68 => 24,  56 => 18,  87 => 42,  28 => 3,  93 => 31,  88 => 38,  78 => 21,  27 => 7,  46 => 14,  44 => 11,  31 => 4,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 82,  171 => 69,  166 => 67,  163 => 61,  158 => 78,  156 => 66,  151 => 74,  142 => 55,  138 => 70,  136 => 70,  121 => 59,  117 => 43,  105 => 51,  91 => 43,  62 => 23,  49 => 8,  21 => 2,  19 => 1,  79 => 40,  72 => 16,  69 => 11,  47 => 12,  40 => 7,  37 => 14,  22 => 1,  246 => 133,  157 => 63,  145 => 67,  139 => 55,  131 => 53,  123 => 58,  120 => 44,  115 => 49,  111 => 48,  108 => 57,  101 => 34,  98 => 33,  96 => 43,  83 => 41,  74 => 30,  66 => 37,  55 => 10,  52 => 17,  50 => 24,  43 => 6,  41 => 15,  35 => 6,  32 => 4,  29 => 5,  209 => 82,  203 => 147,  199 => 80,  193 => 98,  189 => 98,  187 => 84,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 65,  154 => 78,  149 => 66,  147 => 57,  144 => 70,  141 => 48,  133 => 69,  130 => 62,  125 => 60,  122 => 55,  116 => 54,  112 => 51,  109 => 50,  106 => 36,  103 => 46,  99 => 45,  95 => 42,  92 => 41,  86 => 28,  82 => 33,  80 => 30,  73 => 39,  64 => 10,  60 => 22,  57 => 19,  54 => 10,  51 => 16,  48 => 11,  45 => 10,  42 => 10,  39 => 10,  36 => 13,  33 => 6,  30 => 2,);
    }
}
