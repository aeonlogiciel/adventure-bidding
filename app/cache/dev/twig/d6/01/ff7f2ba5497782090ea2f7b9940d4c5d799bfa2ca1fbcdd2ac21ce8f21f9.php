<?php

/* AdventureBiddingBundle:User:viewOpenRequestUser.html.twig */
class __TwigTemplate_d601ff7f2ba5497782090ea2f7b9940d4c5d799bfa2ca1fbcdd2ac21ce8f21f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::userbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::userbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "<!-- BEGIN BODY -->
<body class=\"fixed-top\">
<!-- BEGIN CONTAINER -->
    <div id=\"container\" class=\"row-fluid\">
    <!-- BEGIN PAGE -->  
    <div id=\"main-content\">
        <!-- BEGIN PAGE CONTAINER-->
        <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
                <div class=\"span12\">

                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class=\"page-title\">
                        User Request and Comments
                    </h3> 
                    <ul class=\"breadcrumb\">
                        <li>
                            <a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a>
                            <span class=\"divider\">/</span>
                        </li>
                        <li>
                            <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_list_request");
        echo "\">Request List</a>
                            <span class=\"divider\">/</span>
                        </li>
                        <li class=\"active\">
                            User Request and Comments
                        </li>

                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class=\"row-fluid\">
                <div class=\"span12\">
                    <!-- BEGIN BLANK PAGE PORTLET-->
                    <div class=\"widget blue\">
                        <div class=\"widget-title\">
                            <h4><i class=\"icon-question-sign\"></i> Request and Comments </h4>

                        </div>
                        <div class=\"widget-body\">
                            <div class=\"row-fluid\">
                                <div class=\"span6\">
                                    <h4 class=\"title grey\" style=\"font-weight:bold; color:#000;\">User Request Details</h4>

                                    <!-- BEGIN HORIZONTAL DESCRIPTION LISTS PORTLET-->
                                    <div class=\"widget blue\">
                                        <div class=\"widget-title\">
                                            <h4><i class=\"icon-reorder\"></i> Your Request Details </h4>

                                        </div>
                                        <div class=\"widget-body\">
                                            <div class=\"row-fluid\">
                                                <div class= \"span12\">
                                                    <h3>Trip Info:</h3>
                                                    <ul class=\" unstyled amounts\">
                                                        <li class=\"text-bold\">Trip Name\t: ";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "name"), "html", null, true);
        echo "</li>
                                                        <li  class=\"text-bold\">No of Males: ";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "male"), "html", null, true);
        echo "</li>
                                                        <li  class=\"text-bold\">No Of Females\t: ";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "female"), "html", null, true);
        echo "</li>
                                                        <li  class=\"text-bold\">No Of Travellers\t: ";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "traveler"), "html", null, true);
        echo " </li>
                                                        <li  class=\"text-bold\">Pick-Up Location\t: ";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "pick"), "html", null, true);
        echo "</li>
                                                        <li  class=\"text-bold\">Drop Location\t: ";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "down"), "html", null, true);
        echo "</li>
                                                        <li  class=\"text-bold\">Trip Tentative (in days)\t: ";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "day"), "html", null, true);
        echo "</li>
\t\t\t\t\t\t\t";
        // line 77
        if (($this->getAttribute($this->getContext($context, "request"), "team") == 0)) {
            // line 78
            echo "                                                        <li  class=\"text-bold\">Combine Group\t: No</li>
                                                     ";
        } else {
            // line 80
            echo "                                                        <li  class=\"text-bold\">Combine Group\t: Yes</li>
                                                     ";
        }
        // line 82
        echo "                                                </div>
                                            </div>
                                            <div class=\"row-fluid\">
                                                <div class= \"span6\">
                                                    <h3>Activities:</h3>
                                                     
                                                    <ul class=\" unstyled amounts\">
                                                       ";
        // line 89
        $context["category"] = "";
        // line 90
        echo "                                                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "request"), "activity"));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 91
            echo "                                                        <li class=\"text-bold\">    
                                                            ";
            // line 92
            $context["category"] = $this->getAttribute($this->getAttribute($this->getContext($context, "activity"), "bidCategory"), "name");
            echo "                                                        
                                                            ";
            // line 93
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "activity"), "bidCategory"), "name") == $this->getContext($context, "category"))) {
                // line 94
                echo "                                                        
                                                          ";
                // line 95
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "activity"), "typeOfActivity"), "html", null, true);
                echo " 
                                                         ";
            }
            // line 97
            echo "                                                           ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "activity"), "bidCategory"), "name"), "html", null, true);
            echo " :";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "activity"), "typeOfActivity"), "html", null, true);
            echo "
                                                       </li>
                                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "                                                       
                                                    </ul>
                                                </div>
                                                <div class= \"span6\">
                                                    <h3>Location:</h3>
                                                    <ul class=\" unstyled amounts\">

                                                        <li  class=\"text-bold\">Zone\t: ";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "zone"), "html", null, true);
        echo "</li>
                                                        <li  class=\"text-bold\">Country\t: ";
        // line 108
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "country"), "html", null, true);
        echo "</li>


                                                    </ul>
                                                </div>

                                            </div>
                                            <div class=\"row-fluid\">
                                                <div class= \"span12\">
                                                    <h3>Special Requirements:</h3>
                                                    <ul class=\" unstyled amounts\">
                                                        <li class=\"text-bold\"><p>";
        // line 119
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "description"), "html", null, true);
        echo " </p></li>


                                                    </ul>
                                                </div>
                                            </div>
                                        </div>   <!-- END row-fluid -->
                                    </div>
                                    <!-- END HORIZONTAL DESCRIPTION LISTS PORTLET-->
                                </div>
                                <div class=\"span6\">
                                    <h4 style=\"font-weight:bold; color:#000;\">Organiser Participated</h4>
                                    <div id=\"accordion\" class=\"accordion in collapse\" style=\"height: auto;\">
                                                ";
        // line 132
        $context["orgid"] = "";
        // line 133
        echo "                                        ";
        $context["desp"] = "";
        // line 134
        echo "                                        ";
        $context["pick"] = "";
        // line 135
        echo "                                        ";
        $context["down"] = "";
        // line 136
        echo "                                        ";
        $context["dateMin"] = "";
        // line 137
        echo "                                        ";
        $context["dateMax"] = "";
        // line 138
        echo "                                        ";
        $context["includeCost"] = "";
        // line 139
        echo "                                        ";
        $context["excludeCost"] = "";
        // line 140
        echo "                                        ";
        $context["totalCost"] = "";
        // line 141
        echo "                                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "listOrganiser"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["listOrganiser"]) {
            // line 142
            echo "                                                                          
                                     <div class=\"accordion-group\">
                                            
                                            
                                           
                                            <div class=\"accordion-heading\">
                                                <a href=\"#collapseOne";
            // line 148
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "loop"), "index"), "html", null, true);
            echo "\" data-parent=\"#accordion\" data-toggle=\"collapse\" class=\"accordion-toggle\">
                                                 ";
            // line 149
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "listOrganiser"));
            foreach ($context['_seq'] as $context["_key"] => $context["listname"]) {
                echo twig_escape_filter($this->env, $this->getContext($context, "listname"), "html", null, true);
                echo " ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listname'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 150
            echo "                                                </a>
                                            </div>
                                            
                                            <div class=\"accordion-body collapse \" id=\"collapseOne";
            // line 153
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "loop"), "index"), "html", null, true);
            echo "\">
                                                <div class=\"accordion-inner\">
                                                    <div class=\"widget red\">
                                                        <div class=\"widget-title\">
                                                            <h4><i class=\"icon-comments-alt\"></i> Conversation</h4>
                                                            
                                                        </div>
                                                        <div class=\"widget-body widgetoverflow\">
                                              
                                                            ";
            // line 162
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "comment"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
                // line 163
                echo "                                                            
                                                            <div class=\"timeline-messages\">
                                                                
                                                                ";
                // line 166
                if ((($this->getAttribute($this->getContext($context, "comment"), "user") != null) && ($this->getAttribute($this->getContext($context, "comment"), "role") == "ROLE_USER"))) {
                    // line 167
                    echo "                                                                <!-- Comment -->
                                                                <div class=\"msg-time-chat\">
                                                                    <a class=\"message-img\" href=\"#\"><img alt=\"\" src=\"";
                    // line 169
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getAttribute($this->getContext($context, "comment"), "user"), "webpath")), "html", null, true);
                    echo "\" class=\"avatar\"></a>
                                                                    <div class=\"message-body msg-in\">
                                                                        <span class=\"arrow\"></span>
                                                                        <div class=\"text\">
                                                                            <p class=\"attribution\"><a href=\"#\">";
                    // line 173
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "user"), "html", null, true);
                    echo "</a> at ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "createdAt")), "html", null, true);
                    echo "</p>
                                                                            <p>";
                    // line 174
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "description"), "html", null, true);
                    echo "</p>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- /comment -->
                                                                ";
                } elseif ((($this->getAttribute($this->getContext($context, "comment"), "organiser") != null) && ($this->getAttribute($this->getContext($context, "comment"), "role") == "ROLE_ORGANISER"))) {
                    // line 181
                    echo "                                                                <!-- Comment -->
                                                                    <div class=\"msg-time-chat\">
                                                                        <a class=\"message-img\" href=\"#\"><img alt=\"\" src=\"";
                    // line 183
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getAttribute($this->getContext($context, "comment"), "organiser"), "webpath")), "html", null, true);
                    echo "\" class=\"avatar\"></a>
                                                                        <div class=\"message-body msg-out\">
                                                                            <span class=\"arrow\"></span>
                                                                            <div class=\"text\">
                                                                                <p class=\"attribution\"> <a href=\"#\">";
                    // line 187
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "organiser"), "html", null, true);
                    echo "</a> at ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "createdAt")), "html", null, true);
                    echo "</p>
                                                                                <p>";
                    // line 188
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "description"), "html", null, true);
                    echo "</p>
                                                                                ";
                    // line 189
                    if ($this->getAttribute($this->getContext($context, "loop"), "length")) {
                        // line 190
                        echo "                                                                                ";
                        $context["orgid"] = $this->getAttribute($this->getAttribute($this->getContext($context, "comment"), "organiser"), "id");
                        // line 191
                        echo "                                                                                ";
                        $context["desp"] = $this->getAttribute($this->getContext($context, "comment"), "description");
                        // line 192
                        echo "                                                                                ";
                        $context["down"] = $this->getAttribute($this->getContext($context, "comment"), "down");
                        // line 193
                        echo "                                                                                ";
                        $context["pick"] = $this->getAttribute($this->getContext($context, "comment"), "pick");
                        // line 194
                        echo "                                                                                 ";
                        $context["dateMin"] = $this->getAttribute($this->getContext($context, "comment"), "dateMin");
                        // line 195
                        echo "                                                                                 ";
                        $context["dateMax"] = $this->getAttribute($this->getContext($context, "comment"), "dateMax");
                        // line 196
                        echo "                                                                                 ";
                        $context["includeCost"] = $this->getAttribute($this->getContext($context, "comment"), "includeCost");
                        // line 197
                        echo "                                                                                 ";
                        $context["excludeCost"] = $this->getAttribute($this->getContext($context, "comment"), "excludeCost");
                        // line 198
                        echo "                                                                                 ";
                        $context["totalCost"] = $this->getAttribute($this->getContext($context, "comment"), "totalCost");
                        // line 199
                        echo "                                                                                ";
                    }
                    // line 200
                    echo "                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /comment -->
                                                                ";
                }
                // line 205
                echo "                                                                                                 
                                                   
                                                            </div>
                                                            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 209
            echo "                                                       
                                                        </div>
                                                    </div>
                                                      <div class=\"accordion-block\">
                                                    <div class=\"row-fluid\">
                                                        ";
            // line 214
            echo             $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_start');
            echo "
                                                        <div class=\"span5\">
                                                            <div class=\"control-group\">
                                                                <div class=\"controls\">
                                                                    ";
            // line 218
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "description"), 'widget', array("attr" => array("class" => "span12")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "description"), 'errors');
            echo "
                                                                    <input type=\"hidden\" name=\"orgid\" value=\"";
            // line 219
            echo twig_escape_filter($this->env, $this->getContext($context, "orgid"), "html", null, true);
            echo "\">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class=\"span7\">
                                                        ";
            // line 225
            echo "                                                        <div class=\"span4\">
                                                            <button class=\"btn btn-small btn-success\" id=\"save\" type=\"submit\"> <i class=\"icon-ok\"></i>Reply</button>
                                                            </div>
                                                        ";
            // line 228
            echo             $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_end');
            echo "
                                                        ";
            // line 232
            echo "                                                            <div class=\"span4\">
                                                                <button class=\"btn btn-small btn-default\"><i class=\"icon-ban-circle\"></i> Cancel</button>
                                                            </div>

                                                        </div>

                                                    </div>\t\t
                                                </div>
                                                     <div class=\"row\">
                                           
<!--- organiser response form start here-->
    <div class=\"form-actions\" style=\"margin-left:50px;\">
                                        <!-- Close form  organiser -->
                                        <form action=\"";
            // line 245
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_user_request_confirm_deal", array("reqId" => $this->getAttribute($this->getContext($context, "request"), "id"))), "html", null, true);
            echo "\" method=\"post\" >       
                                        <div id=\"collapse_2\" class=\"accordion-body collapse in\">
                      <div class=\"\">
                        
                        <div class=\"well\">
                          <h3>Organiser Quatation Details:-</h3>
                          <hr>
                          <div class =\"row-fluid\">
                            <div class=\"span8\">
                              <div class=\"form-group\">
                                <label >Proposed Date </label>
                                
                                <input type=\"text\"  value=\"";
            // line 257
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getContext($context, "dateMin"), "m-d-Y"), "html", null, true);
            echo "\" readonly='readonly' class=\"span5\"  >
                                To
                                
                                <input type=\"text\"  value=\"";
            // line 260
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getContext($context, "dateMax"), "m-d-Y"), "html", null, true);
            echo "\" readonly='readonly' class=\"span5\" >
                              </div>
                            </div>
                               </div>
                          <div class =\"row-fluid\">
                            <div class=\"span4\">
                              <div class=\"control-group\">
                                <label class=\"control-label\" >Pick-Up Point </label>
                               <input class='input-block-level' type=\"text\" readonly='readonly'value=\"";
            // line 268
            echo twig_escape_filter($this->env, $this->getContext($context, "pick"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                            <div class=\"span4\">
                              <div class=\"control-group\">
                                <label class=\"control-label\" >Drop off point </label>
                               <input class='input-block-level'type=\"text\" readonly='readonly' value=\"";
            // line 274
            echo twig_escape_filter($this->env, $this->getContext($context, "down"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                          </div>
                          <div class=\"row-fluid\">
                            <div class=\"span4\">
                              <div class=\"control-group\">
                                  <label class=\"control-label\" >Total Estimate</label>
                                <input class='input-block-level' type=\"text\" readonly='readonly' value=\"";
            // line 282
            echo twig_escape_filter($this->env, $this->getContext($context, "totalCost"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                            <div class=\"span4\">
                              <div class=\"form-group\">
                                <label class=\"control-label\">Cost include</label>
                               <input class='input-block-level'type=\"text\" readonly='readonly' value=\"";
            // line 288
            echo twig_escape_filter($this->env, $this->getContext($context, "includeCost"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                            <div class=\"span4\">
                              <div class=\"form-group\">
                                <label class=\"control-label\">Cost Exclude</label>
                               <input class='input-block-level' type=\"text\" readonly='readonly' value=\"";
            // line 294
            echo twig_escape_filter($this->env, $this->getContext($context, "excludeCost"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                                  </div>
                          <div class=\"row-fluid\">
                             
                                  
                            <div class=\"span5\">
                              <div class=\"control-group\">
                                <label class=\"input-block-level\"> Short itinerary and Details: </label>
                                <div class=\"controls\">
                                    <textarea readonly='readonly'> ";
            // line 305
            echo twig_escape_filter($this->env, $this->getContext($context, "desp"), "html", null, true);
            echo "</textarea>
";
            // line 307
            echo "                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                              
                        </div>
                   
                 <button class=\"btn btn-small btn-primary\"><i class=\"icon-check\"></i>  Accept Deal</button>
                                    
                                        <!-- close form final deal -->
                                                   </div>
                                        <!--  from ends here -->
                                        </form>
                                                 </div>
                                                </div>
                                            </div>
                                            ";
            // line 325
            echo "                                        </div>
                                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listOrganiser'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 327
        echo "                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END BLANK PAGE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->  
    </div>
    <!-- END CONTAINER -->
    </div>

</body>
<!-- END BODY -->
";
    }

    // line 348
    public function block_javascripts($context, array $blocks = array())
    {
        // line 349
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:User:viewOpenRequestUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  653 => 349,  650 => 348,  627 => 327,  612 => 325,  593 => 307,  589 => 305,  575 => 294,  557 => 282,  546 => 274,  537 => 268,  520 => 257,  505 => 245,  486 => 228,  481 => 225,  467 => 218,  460 => 214,  429 => 200,  426 => 199,  411 => 194,  405 => 192,  399 => 190,  397 => 189,  380 => 183,  376 => 181,  212 => 108,  500 => 323,  497 => 322,  456 => 284,  351 => 182,  339 => 179,  335 => 178,  311 => 164,  226 => 119,  417 => 196,  386 => 219,  367 => 208,  320 => 176,  223 => 147,  373 => 147,  370 => 146,  357 => 186,  301 => 167,  289 => 158,  281 => 152,  277 => 154,  504 => 354,  501 => 353,  434 => 293,  412 => 281,  404 => 275,  390 => 254,  354 => 228,  343 => 180,  329 => 174,  295 => 160,  118 => 72,  369 => 241,  313 => 153,  297 => 166,  293 => 165,  287 => 173,  234 => 140,  271 => 147,  259 => 138,  232 => 128,  473 => 219,  470 => 332,  349 => 167,  324 => 205,  160 => 86,  239 => 155,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 288,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 232,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 197,  408 => 193,  403 => 241,  382 => 183,  377 => 216,  372 => 215,  346 => 200,  342 => 163,  299 => 162,  263 => 155,  255 => 152,  250 => 135,  215 => 141,  153 => 82,  503 => 355,  455 => 310,  446 => 305,  436 => 205,  425 => 289,  414 => 195,  406 => 277,  392 => 222,  356 => 230,  345 => 178,  331 => 177,  363 => 178,  358 => 204,  350 => 196,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 153,  274 => 153,  251 => 146,  551 => 325,  548 => 324,  526 => 260,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 240,  394 => 217,  378 => 214,  366 => 174,  353 => 169,  347 => 166,  340 => 197,  338 => 178,  334 => 177,  321 => 172,  317 => 171,  315 => 171,  307 => 169,  245 => 125,  242 => 132,  228 => 138,  206 => 117,  202 => 103,  194 => 100,  113 => 64,  218 => 114,  210 => 115,  186 => 94,  172 => 104,  81 => 38,  192 => 114,  174 => 126,  155 => 74,  23 => 3,  233 => 146,  184 => 94,  137 => 69,  58 => 13,  225 => 144,  213 => 144,  205 => 21,  198 => 117,  104 => 49,  237 => 141,  207 => 141,  195 => 103,  185 => 107,  344 => 190,  336 => 212,  333 => 175,  328 => 188,  325 => 162,  318 => 207,  316 => 205,  308 => 150,  304 => 198,  300 => 185,  296 => 184,  292 => 159,  288 => 166,  284 => 165,  272 => 152,  260 => 141,  256 => 137,  248 => 154,  216 => 79,  200 => 102,  190 => 96,  170 => 91,  126 => 60,  211 => 125,  181 => 128,  129 => 61,  279 => 148,  275 => 147,  265 => 140,  261 => 157,  257 => 147,  222 => 121,  188 => 93,  167 => 124,  178 => 73,  175 => 91,  161 => 80,  150 => 80,  114 => 71,  110 => 53,  90 => 30,  127 => 59,  124 => 72,  97 => 57,  290 => 166,  286 => 142,  280 => 155,  276 => 178,  270 => 146,  266 => 160,  262 => 139,  253 => 136,  249 => 145,  244 => 133,  236 => 150,  231 => 139,  197 => 114,  191 => 99,  180 => 69,  165 => 90,  146 => 78,  65 => 18,  53 => 19,  152 => 91,  148 => 90,  134 => 76,  76 => 33,  70 => 15,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 209,  444 => 303,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 198,  413 => 234,  409 => 132,  407 => 131,  402 => 191,  398 => 129,  393 => 188,  387 => 187,  384 => 121,  381 => 120,  379 => 182,  374 => 116,  368 => 237,  365 => 27,  362 => 26,  360 => 173,  355 => 182,  341 => 177,  337 => 176,  322 => 178,  314 => 172,  312 => 170,  309 => 170,  305 => 163,  298 => 149,  294 => 148,  285 => 163,  283 => 156,  278 => 149,  268 => 141,  264 => 164,  258 => 150,  252 => 146,  247 => 134,  241 => 77,  229 => 120,  220 => 142,  214 => 116,  177 => 93,  169 => 125,  140 => 76,  132 => 74,  128 => 73,  107 => 62,  61 => 20,  273 => 177,  269 => 151,  254 => 92,  243 => 143,  240 => 142,  238 => 154,  235 => 74,  230 => 121,  227 => 182,  224 => 181,  221 => 115,  219 => 115,  217 => 75,  208 => 107,  204 => 109,  179 => 94,  159 => 75,  143 => 71,  135 => 63,  119 => 59,  102 => 46,  71 => 31,  67 => 20,  63 => 19,  59 => 18,  94 => 30,  89 => 28,  85 => 42,  75 => 22,  68 => 28,  56 => 15,  87 => 25,  28 => 3,  93 => 29,  88 => 45,  78 => 18,  27 => 3,  46 => 10,  44 => 12,  31 => 3,  38 => 6,  26 => 5,  24 => 4,  25 => 4,  201 => 102,  196 => 90,  183 => 94,  171 => 69,  166 => 67,  163 => 89,  158 => 94,  156 => 79,  151 => 72,  142 => 78,  138 => 66,  136 => 75,  121 => 66,  117 => 65,  105 => 51,  91 => 26,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 24,  72 => 17,  69 => 29,  47 => 15,  40 => 7,  37 => 10,  22 => 1,  246 => 144,  157 => 63,  145 => 67,  139 => 65,  131 => 59,  123 => 58,  120 => 71,  115 => 49,  111 => 42,  108 => 54,  101 => 47,  98 => 50,  96 => 23,  83 => 24,  74 => 21,  66 => 26,  55 => 17,  52 => 14,  50 => 17,  43 => 14,  41 => 15,  35 => 12,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 100,  193 => 113,  189 => 132,  187 => 97,  182 => 95,  176 => 105,  173 => 92,  168 => 76,  164 => 77,  162 => 96,  154 => 82,  149 => 75,  147 => 68,  144 => 77,  141 => 67,  133 => 61,  130 => 60,  125 => 67,  122 => 57,  116 => 70,  112 => 52,  109 => 63,  106 => 66,  103 => 55,  99 => 32,  95 => 47,  92 => 29,  86 => 28,  82 => 17,  80 => 30,  73 => 29,  64 => 14,  60 => 16,  57 => 19,  54 => 18,  51 => 16,  48 => 13,  45 => 9,  42 => 12,  39 => 13,  36 => 10,  33 => 13,  30 => 2,);
    }
}
