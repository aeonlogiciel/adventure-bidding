<?php

/* AdventureBiddingBundle:Organiser:viewTrailsOrganiser.html.twig */
class __TwigTemplate_c487d9bd8d53ddc7688a90f6bd2a32bc87dd9c10debd82858cb6117accecc064 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::organiserbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::organiserbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<style>
    .thumbnail:hover img{
            opacity:0.5;
    }


    </style>

";
    }

    // line 15
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 18
    public function block_body($context, array $blocks = array())
    {
        // line 19
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                    Trails
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"";
        // line 36
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           All Trails
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <!-- Acknowledgement message start -->
\t\t\t";
        // line 50
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 55
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 64
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 69
            echo "                        ";
        } else {
            // line 70
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 71
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 75
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 79
            echo "                        ";
        }
        // line 80
        echo "                        <!-- Acknowledgement message end --> 
           <div id=\"page-wraper\">
               <div class=\"row-fluid\">
                <div class=\"span12\">
                <!-- BEGIN EXAMPLE TABLE widget-->
                
                <div class=\"widget blue\">
                    <div class=\"widget-title\">
                        <h4><i class=\"icon-search\"></i> Filter Your Search</h4>
                         
                    </div>
                    <div class=\"widget-body\">
                         <form class=\"form-inline\" method=\"post\">
                         <div class=\"row-fluid\">
                         
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Filter Search</label>
                          <div class=\"controls\">
                                    <select class=\"span8 \" data-placeholder=\"Choose a Category\" tabindex=\"1\">
                                        <option value=\"\">Select...</option>
                                        <option value=\"Activities\">Activities</option>
                                        <option value=\"Zones\">Zones</option>
                                        <option value=\"Category 3\">Category 5</option>
                                        <option value=\"Category 4\">Category 4</option>
                                    </select>
                                </div>
                          
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Search</label>
                           <div class=\"controls\">
                          <input type=\"text\" class=\"span8\"/>   
                          </div>
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          
                          <label class=\"control-label\"></label>
                          <div class=\"controls\">
                <button type=\"submit\" class=\"btn btn-primary\"><i class=\"icon-ok\"></i> Submit</button>
                         <button type=\"button\" class=\"btn\">Cancel</button>
                
               </div>
                          </div>
                         </div>
                         
                         
                         </div>
                         
                         </form>
                        
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
          
            <div class=\"widget blue\">
                        <div class=\"widget-title\">
                            <h4><i class=\"icon-user\"></i>Adventure Trails List </h4>
\t\t\t\t\t\t</div>
                        <div class=\"widget-body\">
                            <div class=\"accordion\" id=\"accordion1\">
                                <div class=\"accordion-group\">
                                   
                                    <div id=\"tab1\" class=\"accordion-body collapse in\">
                                        <div class=\"accordion-inner\">
                                           
                                                <div class=\"row text-center\">
                                       ";
        // line 153
        $context["count"] = "1";
        // line 154
        echo "                                                    ";
        $context["image"] = "";
        // line 155
        echo "                                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "trails"));
        foreach ($context['_seq'] as $context["_key"] => $context["trails"]) {
            // line 156
            echo "                                                    ";
            $context["image"] = twig_split_filter($this->getAttribute($this->getContext($context, "trails"), "coverImage"), ".");
            // line 157
            echo "                                      <div class=\"span3 hero-feature\">
                <div class=\"thumbnail\">

                    <img src=\"http://www.adventureclicknblog.com/home/article/";
            // line 160
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "image"), 0, array(), "array"), "html", null, true);
            echo "/cover/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "coverImage"), "html", null, true);
            echo "\" alt=\"Adventure\" class=\"img-responsive\" style=\"height:185px !important \" >
                    
                    <div class=\"caption\">
                        <h3>";
            // line 163
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "state"), "html", null, true);
            echo "</h3>
                        <p>";
            // line 164
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "articleTitle"), "html", null, true);
            echo "</p>
                        <div class='row-fluid'><div class='span6' style='border-right:1px #000 solid ; '><h4>Duration</h4>
                                <p> ";
            // line 166
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "durationMin"), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "durationMax"), "html", null, true);
            echo " days</p>   
                         </div>
                        <div class='span6' style=' line-height: 4px;'><h4>Difficulty</h4><p> ";
            // line 168
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "difficulty"), "html", null, true);
            echo "</p>   </div>
                        </div>
                       
                         <form action=\"";
            // line 171
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_add_trails", array("id" => $this->getAttribute($this->getContext($context, "trails"), "id"))), "html", null, true);
            echo "\" method='POST' name='trails'>
                        <p>
                           <a href=\"http://www.adventureclicknblog.com/articlepage.php?title=";
            // line 173
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "uniquee"), "html", null, true);
            echo "\" target='_blank'class=\"btn btn-primary\">Read More</a>
                       
                        <input class=\"btn btn-danger\"  type='submit' name='submit' value='Add Trail'/>
                          <input type='hidden' name='orgid' value='";
            // line 176
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "security"), "getToken", array(), "method"), "getUser", array(), "method"), "getId", array(), "method"), "html", null, true);
            echo "' />
                        <input type='hidden' name='trailid' value='";
            // line 177
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "id"), "html", null, true);
            echo "' />
                        </p>
                         </form>

                    </div>
                </div>
            </div>       
                                                   

                              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trails'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 187
        echo " 

                                                    
                                                    
        </div>
                                                 
                                             
                                           
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                
                                       <!-- END ACCORDION PORTLET-->
                </div>
            
                
                
            </div>

            <!-- END PAGE CONTENT-->         

      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
 <!--model for feedback-->  
      <div id=\"myModal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">All Trails</h3>
            </div>
            <div class=\"modal-body\">
            <p>Phasellus fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo.
             Sed ut quam at magna porttitor hendrerit. Maecenas quis erat fringilla augue feugiat vulputate a eu 
            sem.Vivamus ut diam at turpis varius tempor. Aliquam dictum sagittis erat, vehicula adipiscing
             diam condimentum id. </p>
            </div>
            <div class=\"modal-footer\">
            <button class=\"btn btn-primary\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>
           
            </div>
      </div>
               
 <!--end model for feedback-->    
    
</body>
<!-- END BODY -->
";
    }

    // line 241
    public function block_javascripts($context, array $blocks = array())
    {
        // line 242
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:viewTrailsOrganiser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  369 => 241,  313 => 187,  297 => 177,  293 => 176,  287 => 173,  234 => 153,  271 => 145,  259 => 138,  232 => 121,  473 => 333,  470 => 332,  349 => 220,  324 => 205,  160 => 86,  239 => 155,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 318,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 286,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 238,  408 => 232,  403 => 230,  382 => 218,  377 => 216,  372 => 242,  346 => 200,  342 => 198,  299 => 171,  263 => 155,  255 => 152,  250 => 150,  215 => 141,  153 => 82,  503 => 355,  455 => 310,  446 => 305,  436 => 251,  425 => 289,  414 => 283,  406 => 277,  392 => 222,  356 => 230,  345 => 224,  331 => 215,  363 => 178,  358 => 147,  350 => 201,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 171,  274 => 161,  251 => 146,  551 => 325,  548 => 324,  526 => 304,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 219,  394 => 217,  378 => 204,  366 => 179,  353 => 190,  347 => 25,  340 => 197,  338 => 178,  334 => 177,  321 => 174,  317 => 172,  315 => 171,  307 => 169,  245 => 125,  242 => 124,  228 => 144,  206 => 117,  202 => 106,  194 => 110,  113 => 59,  218 => 110,  210 => 140,  186 => 94,  172 => 124,  81 => 38,  192 => 114,  174 => 96,  155 => 59,  23 => 1,  233 => 146,  184 => 94,  137 => 69,  58 => 11,  225 => 126,  213 => 78,  205 => 21,  198 => 117,  104 => 49,  237 => 170,  207 => 120,  195 => 145,  185 => 104,  344 => 190,  336 => 212,  333 => 157,  328 => 188,  325 => 25,  318 => 207,  316 => 189,  308 => 175,  304 => 173,  300 => 185,  296 => 184,  292 => 183,  288 => 166,  284 => 165,  272 => 178,  260 => 163,  256 => 174,  248 => 149,  216 => 79,  200 => 102,  190 => 96,  170 => 108,  126 => 75,  211 => 127,  181 => 93,  129 => 66,  279 => 148,  275 => 147,  265 => 171,  261 => 157,  257 => 141,  222 => 128,  188 => 93,  167 => 123,  178 => 73,  175 => 91,  161 => 91,  150 => 85,  114 => 54,  110 => 53,  90 => 30,  127 => 59,  124 => 64,  97 => 57,  290 => 166,  286 => 165,  280 => 146,  276 => 168,  270 => 161,  266 => 160,  262 => 134,  253 => 149,  249 => 137,  244 => 156,  236 => 154,  231 => 145,  197 => 101,  191 => 99,  180 => 69,  165 => 122,  146 => 69,  65 => 21,  53 => 19,  152 => 60,  148 => 78,  134 => 68,  76 => 36,  70 => 38,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 234,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 220,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 237,  365 => 111,  362 => 110,  360 => 225,  355 => 223,  341 => 189,  337 => 103,  322 => 185,  314 => 172,  312 => 170,  309 => 175,  305 => 174,  298 => 168,  294 => 169,  285 => 171,  283 => 88,  278 => 149,  268 => 172,  264 => 164,  258 => 140,  252 => 160,  247 => 157,  241 => 77,  229 => 112,  220 => 142,  214 => 128,  177 => 95,  169 => 91,  140 => 70,  132 => 67,  128 => 58,  107 => 62,  61 => 20,  273 => 164,  269 => 166,  254 => 92,  243 => 127,  240 => 171,  238 => 154,  235 => 74,  230 => 82,  227 => 182,  224 => 181,  221 => 115,  219 => 123,  217 => 75,  208 => 107,  204 => 109,  179 => 126,  159 => 80,  143 => 71,  135 => 68,  119 => 62,  102 => 46,  71 => 21,  67 => 26,  63 => 24,  59 => 21,  94 => 30,  89 => 28,  85 => 33,  75 => 22,  68 => 28,  56 => 20,  87 => 25,  28 => 3,  93 => 50,  88 => 48,  78 => 18,  27 => 7,  46 => 10,  44 => 11,  31 => 3,  38 => 6,  26 => 5,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 127,  171 => 69,  166 => 67,  163 => 82,  158 => 78,  156 => 79,  151 => 77,  142 => 67,  138 => 69,  136 => 70,  121 => 59,  117 => 66,  105 => 51,  91 => 43,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 32,  72 => 17,  69 => 33,  47 => 17,  40 => 7,  37 => 10,  22 => 1,  246 => 133,  157 => 63,  145 => 67,  139 => 65,  131 => 59,  123 => 58,  120 => 44,  115 => 49,  111 => 48,  108 => 57,  101 => 47,  98 => 53,  96 => 23,  83 => 40,  74 => 30,  66 => 26,  55 => 17,  52 => 17,  50 => 17,  43 => 16,  41 => 15,  35 => 14,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 134,  193 => 131,  189 => 129,  187 => 128,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 91,  154 => 78,  149 => 75,  147 => 68,  144 => 67,  141 => 76,  133 => 61,  130 => 60,  125 => 57,  122 => 56,  116 => 71,  112 => 52,  109 => 43,  106 => 66,  103 => 55,  99 => 45,  95 => 42,  92 => 29,  86 => 28,  82 => 33,  80 => 30,  73 => 29,  64 => 10,  60 => 15,  57 => 19,  54 => 18,  51 => 18,  48 => 15,  45 => 10,  42 => 12,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
