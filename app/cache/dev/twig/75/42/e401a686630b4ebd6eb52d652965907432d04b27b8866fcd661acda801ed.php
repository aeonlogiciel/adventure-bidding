<?php

/* AdventureBiddingBundle:Organiser:changePassword.html.twig */
class __TwigTemplate_7542e401a686630b4ebd6eb52d652965907432d04b27b8866fcd661acda801ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::organiserbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::organiserbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System | User Profile";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

   <!-- BEGIN CONTAINER -->
   <div id=\"container\" class=\"row-fluid\">

      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                     Change Password
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"index.html\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           Change Password
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
                <div class=\"row-fluid\">
                <div class=\"span12\">
                    <!-- Acknowledgement message start -->
\t\t\t";
        // line 48
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 53
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 62
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 67
            echo "                        ";
        } else {
            // line 68
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 69
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 77
            echo "                        ";
        }
        // line 78
        echo "                        <!-- Acknowledgement message end -->  
                    <!-- BEGIN ACCORDION PORTLET-->
                    <div class=\"widget blue\">
                        <div class=\"widget-title\">
                            <h4><i class=\"icon-user\"></i>Change Password</h4>
\t\t\t\t\t\t</div>
                        <div class=\"widget-body\">
                            <div class=\"accordion\" id=\"accordion1\">
\t\t\t\t\t\t\t\t<div class=\"accordion-group\">
                                    <div id=\"collapse_4\" class=\"accordion-body collapse in\">
                                        <div class=\"accordion-inner\">
                                        \t<!--Submit massege-->
                                        \t
                                            <form  id=\"change_password\" method=\"POST\" action=\"";
        // line 91
        echo $this->env->getExtension('routing')->getPath("adventure_update_organiser_password");
        echo "\" name=\"changepassword\" enctype=\"multipart/form-data\">      
\t\t\t\t\t\t<label class=\"control-label\">Current Password</label>
\t\t\t\t\t\t<input type=\"password\" name=\"currentPassword\" class=\"m-wrap span8\" required/>
\t\t\t\t\t\t<label class=\"control-label\">New Password</label>
\t\t\t\t\t\t<input type=\"password\" class=\"m-wrap span8\" name=\"newPassword\" required/>
\t\t\t\t\t\t<label class=\"control-label\">Re-type New Password</label>
\t\t\t\t\t\t<input type=\"password\" class=\"m-wrap span8\" name=\"confirmPassword\" required/>
\t\t\t\t\t\t<div class=\"submit-btn\">
                                                    <button class=\"btn btn-primary\" type=\"submit\">Save</button>
                                                    <a href=\"";
        // line 100
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_changepassword");
        echo "\"><button class=\"btn btn-default\" type=\"button\">Cancel</button></a>
                                            </div>
\t\t\t\t\t</form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- END ACCORDION PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->         
         </div>

            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   </div>
</body>
<!-- END BODY -->
";
    }

    // line 127
    public function block_javascripts($context, array $blocks = array())
    {
        // line 128
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  473 => 333,  470 => 332,  349 => 220,  324 => 205,  160 => 86,  239 => 147,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 318,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 286,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 238,  408 => 232,  403 => 230,  382 => 218,  377 => 216,  372 => 214,  346 => 200,  342 => 198,  299 => 171,  263 => 155,  255 => 152,  250 => 150,  215 => 141,  153 => 82,  503 => 355,  455 => 310,  446 => 305,  436 => 251,  425 => 289,  414 => 283,  406 => 277,  392 => 222,  356 => 230,  345 => 224,  331 => 215,  363 => 178,  358 => 147,  350 => 201,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 164,  274 => 161,  251 => 146,  551 => 325,  548 => 324,  526 => 304,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 219,  394 => 217,  378 => 204,  366 => 179,  353 => 190,  347 => 25,  340 => 197,  338 => 178,  334 => 177,  321 => 174,  317 => 172,  315 => 171,  307 => 169,  245 => 125,  242 => 124,  228 => 144,  206 => 117,  202 => 106,  194 => 110,  113 => 56,  218 => 110,  210 => 140,  186 => 94,  172 => 124,  81 => 34,  192 => 114,  174 => 96,  155 => 59,  23 => 1,  233 => 146,  184 => 76,  137 => 80,  58 => 11,  225 => 126,  213 => 78,  205 => 21,  198 => 117,  104 => 49,  237 => 170,  207 => 120,  195 => 145,  185 => 104,  344 => 190,  336 => 212,  333 => 157,  328 => 188,  325 => 25,  318 => 207,  316 => 189,  308 => 187,  304 => 173,  300 => 185,  296 => 184,  292 => 183,  288 => 166,  284 => 165,  272 => 178,  260 => 153,  256 => 174,  248 => 149,  216 => 79,  200 => 102,  190 => 103,  170 => 108,  126 => 75,  211 => 127,  181 => 100,  129 => 66,  279 => 148,  275 => 147,  265 => 171,  261 => 157,  257 => 141,  222 => 128,  188 => 93,  167 => 123,  178 => 73,  175 => 125,  161 => 91,  150 => 85,  114 => 54,  110 => 53,  90 => 30,  127 => 59,  124 => 58,  97 => 57,  290 => 166,  286 => 165,  280 => 146,  276 => 179,  270 => 161,  266 => 160,  262 => 134,  253 => 149,  249 => 137,  244 => 171,  236 => 133,  231 => 145,  197 => 103,  191 => 99,  180 => 69,  165 => 122,  146 => 69,  65 => 21,  53 => 19,  152 => 60,  148 => 78,  134 => 65,  76 => 33,  70 => 38,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 234,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 220,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 237,  365 => 111,  362 => 110,  360 => 225,  355 => 223,  341 => 189,  337 => 103,  322 => 185,  314 => 172,  312 => 170,  309 => 175,  305 => 168,  298 => 168,  294 => 169,  285 => 171,  283 => 88,  278 => 162,  268 => 172,  264 => 176,  258 => 140,  252 => 151,  247 => 78,  241 => 77,  229 => 112,  220 => 142,  214 => 128,  177 => 95,  169 => 91,  140 => 67,  132 => 67,  128 => 71,  107 => 62,  61 => 20,  273 => 164,  269 => 159,  254 => 92,  243 => 144,  240 => 171,  238 => 154,  235 => 74,  230 => 82,  227 => 182,  224 => 181,  221 => 109,  219 => 123,  217 => 75,  208 => 22,  204 => 109,  179 => 126,  159 => 77,  143 => 68,  135 => 68,  119 => 62,  102 => 46,  71 => 21,  67 => 26,  63 => 24,  59 => 21,  94 => 30,  89 => 28,  85 => 33,  75 => 22,  68 => 28,  56 => 20,  87 => 25,  28 => 3,  93 => 45,  88 => 48,  78 => 18,  27 => 7,  46 => 10,  44 => 11,  31 => 3,  38 => 6,  26 => 5,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 127,  171 => 69,  166 => 67,  163 => 87,  158 => 78,  156 => 87,  151 => 77,  142 => 67,  138 => 69,  136 => 70,  121 => 59,  117 => 66,  105 => 51,  91 => 21,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 32,  72 => 17,  69 => 33,  47 => 17,  40 => 7,  37 => 10,  22 => 1,  246 => 133,  157 => 63,  145 => 67,  139 => 65,  131 => 62,  123 => 58,  120 => 44,  115 => 49,  111 => 48,  108 => 57,  101 => 34,  98 => 53,  96 => 23,  83 => 40,  74 => 30,  66 => 16,  55 => 17,  52 => 17,  50 => 17,  43 => 16,  41 => 15,  35 => 14,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 134,  193 => 131,  189 => 129,  187 => 128,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 91,  154 => 78,  149 => 70,  147 => 78,  144 => 73,  141 => 76,  133 => 61,  130 => 60,  125 => 60,  122 => 55,  116 => 71,  112 => 51,  109 => 43,  106 => 66,  103 => 49,  99 => 45,  95 => 42,  92 => 29,  86 => 28,  82 => 33,  80 => 30,  73 => 29,  64 => 10,  60 => 15,  57 => 21,  54 => 14,  51 => 18,  48 => 13,  45 => 10,  42 => 12,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
