<?php

/* AdventureBiddingBundle:User:viewProfileUser.html.twig */
class __TwigTemplate_2cd0232bc20ce4867a46b8c26780fe888fdf06868cce3b0c6c5ddf1342e32e6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::userbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::userbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System | User Profile";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "<body class=\"fixed-top\">
  
   <!-- BEGIN CONTAINER -->
   <div id=\"container\" class=\"row-fluid\">
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                     
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"index.html\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           My Profile <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           View Profile
                       </li>
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
                <div class=\"row-fluid\">
                <div class=\"span12\">
                    <!-- BEGIN ACCORDION PORTLET-->
                    <div class=\"widget blue\">
                        <div class=\"widget-title\">
                            <h4><i class=\"icon-user\"></i>View User Inforation</h4>
\t\t\t\t\t\t</div>
                        <div class=\"widget-body\">
                            <div class=\"accordion\" id=\"accordion1\">
                                <div class=\"accordion-group\">
                                   
                                    <div id=\"tab1\" class=\"accordion-body collapse in\">
                                        <div class=\"accordion-inner\">
                                           <div class=\"span3\">
                                                 <img src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getContext($context, "member"), "webPath")), "html", null, true);
        echo "\" alt=\"\">
                                             </div>
                                             <div class=\"span9\">
                                                <h4><b>Full Name : Aeon Logiciel</b></h4>
                                                 <ul class=\" unstyled amounts\">
                                                     <li>User Name\t: ";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "name"), "html", null, true);
        echo "</li>
                                                     <li>Email\t\t: ";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "email"), "html", null, true);
        echo "</li>
                                                     <li>Phone No.\t: ";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "phone"), "html", null, true);
        echo "</li>
                                                     <li>Address\t: ";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "address"), "html", null, true);
        echo "</li>
                                                     <li>Area Code\t: ";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "zip"), "html", null, true);
        echo "</li>
                                                     <li>Country\t: ";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "country"), "html", null, true);
        echo "</li>
                                                    
                                                 </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                    <!-- END ACCORDION PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->         
         </div>

            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   </div>
</body>
";
    }

    // line 97
    public function block_javascripts($context, array $blocks = array())
    {
        // line 98
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:User:viewProfileUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 98,  158 => 97,  125 => 67,  121 => 66,  117 => 65,  113 => 64,  109 => 63,  105 => 62,  97 => 57,  49 => 11,  46 => 10,  40 => 7,  34 => 4,  31 => 3,);
    }
}
