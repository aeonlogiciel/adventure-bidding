<?php

/* AdventureBiddingBundle:Organiser:viewProfileOrganiser.html.twig */
class __TwigTemplate_3ccc4495d23edbfb20134ae406c077c444127791b32b229d9b17af7fe6a3e802 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::organiserbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::organiserbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

<style>
    .thumbnail:hover img{
            opacity:0.5;
    }
    
    .thumbnail:hover button {
    display: block;
                            }
    .thumbnail button {
    position:absolute;
    display:none;
                      }
    .a{ top:auto;
        left:auto;
       position: relative;
        
    }
    .dl-horizontal > dd {
        text-align: left;
    }
     
    </style>
";
    }

    // line 30
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        // line 34
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                     
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"#\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           My Profile <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           View Profile
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
             <!-- Acknowledgement message start -->
\t\t\t";
        // line 66
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 71
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 80
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 84
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 85
            echo "                        ";
        } else {
            // line 86
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 87
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 91
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 95
            echo "                        ";
        }
        // line 96
        echo "                        <!-- Acknowledgement message end -->  
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
                <div class=\"row-fluid\">
                <div class=\"span12\">
                    <!-- BEGIN ACCORDION PORTLET-->
                    <div class=\"widget blue\">
                        <div class=\"widget-title\">
                            <h4><i class=\"icon-user\"></i>View Organizer Inforation</h4>
\t\t\t\t\t\t</div>
                        <div class=\"widget-body\">
                            <div class=\"accordion\" id=\"accordion1\">
                                <div class=\"accordion-group\">
                                   
                                    <div id=\"tab1\" class=\"accordion-body collapse in\">
                                        <div class=\"accordion-inner\">
                                           <div class=\"span3\">
                                                 <img src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getContext($context, "member"), "webPath")), "html", null, true);
        echo "\" alt=\"\">
                                             </div>
                                             <div class=\"span9\">
                                                <h4><b>Full Name : ";
        // line 117
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "name"), "html", null, true);
        echo "</b></h4>
                                                 <ul class=\" unstyled amounts\">
                                                     <li>Email\t\t: ";
        // line 119
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "email"), "html", null, true);
        echo "</li>
                                                     <li>Phone No.\t: ";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "phone"), "html", null, true);
        echo "</li>
                                                     <li>Address\t: ";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "address"), "html", null, true);
        echo "</li>
                                                     <li>Area Code\t: ";
        // line 122
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "zip"), "html", null, true);
        echo "</li>
                                                     <li>Country\t: ";
        // line 123
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "country"), "html", null, true);
        echo "</li>
                                                    
                                                 </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                    <!-- END ACCORDION PORTLET-->
                </div>
                      <div class=\"widget blue\">
                        <div class=\"widget-title\">
                            <h4><i class=\"icon-user\"></i>Your Events List </h4>
\t\t\t\t\t\t</div>
                        <div class=\"widget-body\">
                            <div class=\"accordion\" id=\"accordion1\">
                                <div class=\"accordion-group\">
                                   
                                    <div id=\"tab1\" class=\"accordion-body collapse in\">
                                        <div class=\"accordion-inner\">
                                           
                                                <div class=\"row text-center\" style='margin-left:0px;'>
                                                      ";
        // line 149
        $context["count"] = "1";
        // line 150
        echo "                                                 ";
        // line 151
        echo "                                                    ";
        $context["activity"] = "";
        // line 152
        echo "                                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "event"));
        foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
            // line 153
            echo "                                                   
                                                  ";
            // line 155
            echo "
            <div class=\"span3 hero-feature\">
                <div class=\"thumbnail\">
                    <img src=\"http://blog.lionbrand.com/wp-content/uploads/2014/11/traveltips2.jpg  \" alt=\"\">
                       <form action=\"";
            // line 159
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_event_delete", array("id" => $this->getAttribute($this->getContext($context, "event"), "id"))), "html", null, true);
            echo "\" method='POST' name='trails'>
                     <button class=\"btn-danger a\" onclick=\"return confirm('Are you sure you want to delete this member ?');\">X</button>
                       <input type='hidden' name='orgid' value='";
            // line 161
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "security"), "getToken", array(), "method"), "getUser", array(), "method"), "getId", array(), "method"), "html", null, true);
            echo "' />
                        <input type='hidden' name='trailid' value='";
            // line 162
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "id"), "html", null, true);
            echo "' />
                       </form>
                    <div class=\"caption\">
                       <h3>";
            // line 165
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "eState"), "html", null, true);
            echo "</h3>
                        <p>";
            // line 166
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "ename"), "html", null, true);
            echo "</p>
                        <div class='row-fluid'>
                            <div class='span6' style='border-right:1px #000 solid ; '><h4>Duration</h4>
                                <p> ";
            // line 169
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "duration"), "html", null, true);
            echo " days</p>   
                         </div>
                            <div class='span6' style='line-height: 4px;'><h4>Difficulty</h4><p> ";
            // line 171
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "eDifficulty"), "html", null, true);
            echo "</p></div>
                             </div>
                         <form action=\"";
            // line 173
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_edit_event", array("id" => $this->getAttribute($this->getContext($context, "event"), "id"))), "html", null, true);
            echo "\" method='POST' name='trails'>
                        <p>
                           <a href=\"#myModal";
            // line 175
            echo twig_escape_filter($this->env, $this->getContext($context, "count"), "html", null, true);
            echo "\" role=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\">Read More</a>
                       
                        <input class=\"btn btn-danger\"  type='submit' name='submit' value='Edit'/>
                       
                        </p>
                         </form>
                    </div>
                </div>
            </div>
                                             <!--model for feedback-->  
      <div id=\"myModal";
            // line 185
            echo twig_escape_filter($this->env, $this->getContext($context, "count"), "html", null, true);
            echo "\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
            // line 188
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "ename"), "html", null, true);
            echo "</h3>
            </div>
            <div class=\"modal-body\">
            <div class=\"row-fluid\">
                    <div class=\"span4\">
                    <img src=\"http://alumni.brandeis.edu/images/LeelaPalace.jpg\" alt=\"\" width='100%'/>
                    
                       </div><br/><br/>
                    <div class=\"span8\">
                        ";
            // line 197
            $context["activity"] = twig_split_filter($this->getAttribute($this->getContext($context, "event"), "eActivity"), "***");
            // line 198
            echo "                       <dl class=\"dl-horizontal\" style='margin-top:-30px;   '>
                            <dt>Activities :</dt>
                             ";
            // line 200
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "activity"));
            foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
                // line 201
                echo "                            <dd>";
                echo twig_escape_filter($this->env, $this->getContext($context, "activity"), "html", null, true);
                echo "</dd>
                            </dl> 
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 204
            echo "                    </div>
                    </div>
                    <div class=\"row-fluid\">
                    
                    <div class=\"span12 well\">
                    
                         <div class='row-fluid'>
                            <div class='span6' style='border-right:1px #000 solid ; '><h4>Location</h4>
                                <dl class=\"dl-horizontal\">
                            <dt>State</dt>
                            <dd>";
            // line 214
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "eState"), "html", null, true);
            echo "</dd>
                            <dt>Budget</dt>
                            <dd>";
            // line 216
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "budget"), "html", null, true);
            echo "</dd>
                            <dt>Zone</dt>
                            <dd>";
            // line 218
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "eZone"), "html", null, true);
            echo "</dd>
                            <dt>Region</dt>
                            <dd>";
            // line 220
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "eRegion"), "html", null, true);
            echo "</dd>
                            <dt>Destination</dt>
                            <dd>";
            // line 222
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "eDestination"), "html", null, true);
            echo "</dd>
                            
                        </dl> 
                         </div>
                            <div class='span6' style='line-height: 4px;'><h4>Details</h4>
                                
                                <p> <dl class=\"dl-horizontal\">
                            <dt>Date</dt>
                            <dd>";
            // line 230
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "eDate"), "d-M-Y"), "html", null, true);
            echo "</dd>
                            <dt>Duration</dt>
                            <dd>";
            // line 232
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "duration"), "html", null, true);
            echo "</dd>
                            <dt>Difficulty</dt>
                            <dd>";
            // line 234
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "eDifficulty"), "html", null, true);
            echo "</dd>
                            <dt>Activities</dt>
                            <dd>Comming soon</dd>
                            <dt>About Event</dt>
                            <dd>";
            // line 238
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "eBrief"), "html", null, true);
            echo "</dd>
                        </dl></p></div>
                             </div>
                    </div>
                    </div>
            </div>
            <div class=\"modal-footer\">
            <button class=\"btn btn-primary\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>
           
            </div>
      </div>
               
 <!--end model for feedback-->   
      ";
            // line 251
            $context["count"] = ($this->getContext($context, "count") + "1");
            // line 252
            echo "                                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "              
               </div>                               
                                         <ul class=\"pager\">
  <li><a href=\"#\">Previous</a></li>
  <li><a href=\"#\">Next</a></li>
</ul>  
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                    <!-- END ACCORDION PORTLET-->
                </div>
                    
                      <div class=\"widget blue\">
                        <div class=\"widget-title\">
                            <h4><i class=\"icon-user\"></i>Your Trail List</h4>
\t\t\t\t\t\t</div>
                        <div class=\"widget-body\">
                            <div class=\"accordion\" id=\"accordion1\">
                                <div class=\"accordion-group\">
                                   
                                    <div id=\"tab1\" class=\"accordion-body collapse in\">
                                        <div class=\"accordion-inner\">
                                           
                                                <div class=\"row text-center\">

                                                    ";
        // line 282
        $context["count"] = "1";
        // line 283
        echo "                                                  ";
        $context["image"] = "";
        // line 284
        echo "                                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "trails"));
        foreach ($context['_seq'] as $context["_key"] => $context["trails"]) {
            // line 285
            echo "                                                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "trails"));
            foreach ($context['_seq'] as $context["_key"] => $context["trails"]) {
                // line 286
                echo "                                                   ";
                $context["image"] = twig_split_filter($this->getAttribute($this->getContext($context, "trails"), "coverImage"), ".");
                // line 287
                echo "                                      <div class=\"span3 hero-feature\">
                <div class=\"thumbnail\">

                    <img src=\"http://www.adventureclicknblog.com/home/article/";
                // line 290
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "image"), 0, array(), "array"), "html", null, true);
                echo "/cover/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "coverImage"), "html", null, true);
                echo "\" alt=\"Adventure\" class=\"img-responsive\" style=\"height:185px !important \" >
                    
                    <div class=\"caption\">
                        <h3>";
                // line 293
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "state"), "html", null, true);
                echo "</h3>
                        <p>";
                // line 294
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "articleTitle"), "html", null, true);
                echo "</p>
                        <div class='row-fluid'>
                            <div class='span6' style='border-right:1px #000 solid ; '><h4>Duration</h4>
                                <p> ";
                // line 297
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "durationMin"), "html", null, true);
                echo " to ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "durationMax"), "html", null, true);
                echo " days</p>   
                            </div>
                        <div class='span6' style='line-height: 4px;'><h4>Difficulty</h4><p> ";
                // line 299
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "difficulty"), "html", null, true);
                echo "</p>   </div>
                        </div>
                       
                         <form action=\"";
                // line 302
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_remove_trails", array("id" => $this->getAttribute($this->getContext($context, "trails"), "id"))), "html", null, true);
                echo "\" method='POST' name='trails'>
                        <p>
                           <a href=\"http://www.adventureclicknblog.com/articlepage.php?title=";
                // line 304
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "uniquee"), "html", null, true);
                echo "\" target='_blank'class=\"btn btn-primary\">Read More</a>
                       
                        <input class=\"btn btn-danger\"  type='submit' name='submit' value='Remove Trail'/>
                          <input type='hidden' name='orgid' value='";
                // line 307
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "security"), "getToken", array(), "method"), "getUser", array(), "method"), "getId", array(), "method"), "html", null, true);
                echo "' />
                        <input type='hidden' name='trailid' value='";
                // line 308
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trails"), "id"), "html", null, true);
                echo "' />
                        </p>
                         </form>

                    </div>
                </div>
            </div>       
                    

                              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trails'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 317
            echo "  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trails'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 318
        echo "      
        </div>
                                     </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                    <!-- END ACCORDION PORTLET-->
                </div>
                    
            </div>

            <!-- END PAGE CONTENT-->         
         </div>

            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   
</body>
<!-- END BODY -->
";
    }

    // line 349
    public function block_javascripts($context, array $blocks = array())
    {
        // line 350
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:viewProfileOrganiser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  602 => 350,  599 => 349,  566 => 318,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  506 => 293,  498 => 290,  493 => 287,  490 => 286,  485 => 285,  480 => 284,  477 => 283,  475 => 282,  438 => 252,  436 => 251,  420 => 238,  413 => 234,  408 => 232,  403 => 230,  392 => 222,  387 => 220,  382 => 218,  377 => 216,  372 => 214,  360 => 204,  350 => 201,  346 => 200,  342 => 198,  340 => 197,  328 => 188,  322 => 185,  309 => 175,  304 => 173,  299 => 171,  294 => 169,  288 => 166,  284 => 165,  278 => 162,  274 => 161,  269 => 159,  263 => 155,  260 => 153,  255 => 152,  252 => 151,  250 => 150,  248 => 149,  219 => 123,  215 => 122,  211 => 121,  207 => 120,  203 => 119,  198 => 117,  192 => 114,  172 => 96,  169 => 95,  162 => 91,  156 => 87,  153 => 86,  150 => 85,  147 => 84,  137 => 80,  126 => 75,  116 => 71,  106 => 66,  72 => 34,  69 => 33,  63 => 30,  34 => 4,  31 => 3,);
    }
}
