<?php

/* AdventureLoginBundle:Member:registerjhuma.html.twig */
class __TwigTemplate_37ccec04862449c58feb56cb12227243e358d3f32c4f87e6d73a16b99eaf5c44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
";
    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        echo "Adventure Bidding System | Login";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "
\t<div id=\"flatix\">
\t\t<header class=\"headertop\">
        \t<h3>Adventure Bidding System</h3>
\t\t\t<span class=\"title\"><strong>Member Sign In</strong></span>
\t\t</header>
       
            <section> 
              
                 ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                     <div class=\"alert alert-success fade in\">
                              <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                              <h5 class=\"alert-heading\">Success!</h5>
                              <p>
                                  ";
            // line 25
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                              </p>
                          </div>
                    
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "                    
                    ";
        // line 31
        if ((!array_key_exists("error", $context))) {
            // line 32
            echo "                    ";
        } else {
            // line 33
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 34
                echo "                            <div class=\"alert alert-danger fade in\">
                              <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                              <h4 class=\"alert-heading\">Message!</h4>
                              <p>
                                  ";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                              </p>
                          </div>
                        ";
            }
            // line 42
            echo "                    ";
        }
        // line 43
        echo "               
                ";
        // line 44
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_start', array("attr" => array("class" => "loginform")));
        echo "
                    
\t\t\t<div class='row'>
\t\t\t\t<label for=\"username\">Username</label>
                                ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "username"), 'widget', array("attr" => array("class" => "maill", "placeholder" => "Username ")));
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "username"), 'errors');
        echo "
\t\t\t\t";
        // line 50
        echo "\t\t\t</div>
                        <div class='row'>
\t\t\t\t<label for=\"email\">E-mail</label>
                                ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'widget', array("attr" => array("class" => "maill", "placeholder" => "E-mail ")));
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'errors');
        echo "
\t\t\t\t";
        // line 55
        echo "\t\t\t</div>
\t\t\t<div class='row'>
\t\t\t\t<label for=\"cemail\">Password</label>
                                ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "password"), 'widget', array("attr" => array("class" => "passwordd", "placeholder" => "Password ")));
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "password"), 'errors');
        echo "
\t\t\t\t";
        // line 60
        echo "\t\t\t</div>
                        <div class='row'>
\t\t\t\t<label for=\"cemail\">Confirm Password</label>
                                ";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "confirmpassword"), 'widget', array("attr" => array("class" => "passwordd", "placeholder" => "Confirm Password ")));
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "confirmpassword"), 'errors');
        echo "
\t\t\t\t";
        // line 65
        echo "\t\t\t</div>
                        
                    ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "Form"), 'rest', array("attr" => array("class" => "login")));
        echo "
                        <input type=\"submit\" value=\"Register\" id=\"save\">
                ";
        // line 69
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_end');
        echo "
                    
\t\t\t<div class=\"or\">
\t\t\t\t<div class=\"facebook\">
\t\t\t\t\t<a href=\"";
        // line 73
        echo $this->env->getExtension('routing')->getPath("adventure_login_user_register");
        echo "\" title=\"Sign up\">SignUp as User</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"twitter\">
\t\t\t\t\t<a href=\"";
        // line 76
        echo $this->env->getExtension('routing')->getPath("adventure_login_organiser_register");
        echo "\" title=\"Sign up\">SignUp as Organizer</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</section>
\t\t<footer>
\t\t\t
            Copyright &copy; 2006 - 2015
            
\t\t</footer>
\t</div>
    

<!--model for forgate password-->
    <div id=\"myModal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-header\">
    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
    <h4 id=\"myModalLabel\">Enter your email id to get a link</h4>
    </div>
    <div class=\"modal-body\">
        <form action=\"#\" method=\"post\">
            <input type=\"email\" class=\"span6\" placeholder=\"Enter your email id\">
            <p>After click on submit, please check your mail</p>
    </div>
    <div class=\"modal-footer\">
    <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>
    <button class=\"btn btn-primary\">Submit</button>
    </div>
    </div>
    
 <!--End model for forgate password-->   

 ";
    }

    // line 109
    public function block_javascripts($context, array $blocks = array())
    {
        // line 110
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

 <script src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>  
  <script src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "AdventureLoginBundle:Member:registerjhuma.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 113,  184 => 76,  137 => 53,  58 => 11,  225 => 102,  213 => 78,  205 => 21,  198 => 101,  104 => 49,  237 => 170,  207 => 148,  195 => 145,  185 => 97,  344 => 190,  336 => 158,  333 => 157,  328 => 26,  325 => 25,  318 => 193,  316 => 189,  308 => 187,  304 => 186,  300 => 185,  296 => 184,  292 => 183,  288 => 182,  284 => 181,  272 => 178,  260 => 175,  256 => 174,  248 => 172,  216 => 79,  200 => 105,  190 => 122,  170 => 108,  126 => 63,  211 => 127,  181 => 96,  129 => 66,  279 => 148,  275 => 147,  265 => 143,  261 => 142,  257 => 141,  222 => 101,  188 => 93,  167 => 107,  178 => 73,  175 => 84,  161 => 91,  150 => 82,  114 => 42,  110 => 53,  90 => 30,  127 => 48,  124 => 86,  97 => 61,  290 => 151,  286 => 150,  280 => 180,  276 => 179,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 171,  236 => 133,  231 => 160,  197 => 103,  191 => 99,  180 => 115,  165 => 92,  146 => 65,  65 => 21,  53 => 18,  152 => 60,  148 => 78,  134 => 73,  76 => 26,  70 => 33,  34 => 3,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 189,  337 => 103,  322 => 101,  314 => 99,  312 => 188,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 177,  264 => 176,  258 => 140,  252 => 173,  247 => 78,  241 => 77,  229 => 112,  220 => 70,  214 => 128,  177 => 95,  169 => 93,  140 => 55,  132 => 50,  128 => 49,  107 => 38,  61 => 20,  273 => 96,  269 => 144,  254 => 92,  243 => 88,  240 => 171,  238 => 85,  235 => 74,  230 => 82,  227 => 117,  224 => 110,  221 => 109,  219 => 154,  217 => 75,  208 => 22,  204 => 109,  179 => 69,  159 => 91,  143 => 64,  135 => 68,  119 => 62,  102 => 48,  71 => 21,  67 => 20,  63 => 19,  59 => 18,  94 => 28,  89 => 28,  85 => 40,  75 => 22,  68 => 24,  56 => 18,  87 => 25,  28 => 5,  93 => 31,  88 => 48,  78 => 21,  27 => 7,  46 => 10,  44 => 15,  31 => 2,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 82,  171 => 69,  166 => 67,  163 => 62,  158 => 30,  156 => 66,  151 => 77,  142 => 55,  138 => 70,  136 => 70,  121 => 59,  117 => 43,  105 => 51,  91 => 27,  62 => 23,  49 => 8,  21 => 2,  19 => 1,  79 => 25,  72 => 16,  69 => 20,  47 => 15,  40 => 7,  37 => 14,  22 => 1,  246 => 133,  157 => 63,  145 => 67,  139 => 45,  131 => 52,  123 => 47,  120 => 44,  115 => 57,  111 => 53,  108 => 57,  101 => 34,  98 => 33,  96 => 43,  83 => 24,  74 => 14,  66 => 34,  55 => 10,  52 => 17,  50 => 24,  43 => 6,  41 => 15,  35 => 12,  32 => 4,  29 => 6,  209 => 82,  203 => 147,  199 => 146,  193 => 99,  189 => 98,  187 => 84,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 65,  154 => 78,  149 => 66,  147 => 58,  144 => 73,  141 => 48,  133 => 69,  130 => 41,  125 => 60,  122 => 55,  116 => 41,  112 => 51,  109 => 54,  106 => 36,  103 => 32,  99 => 31,  95 => 32,  92 => 41,  86 => 28,  82 => 33,  80 => 30,  73 => 24,  64 => 20,  60 => 19,  57 => 19,  54 => 10,  51 => 16,  48 => 11,  45 => 10,  42 => 7,  39 => 5,  36 => 13,  33 => 13,  30 => 2,);
    }
}
