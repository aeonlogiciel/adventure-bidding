<?php

/* ::headerbase.html.twig */
class __TwigTemplate_d7adbef81cb274ac51f1fa918b110f29e6f0ffb093a9e27cc2b448b9d307bc6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 8]> <html lang=\"en\" class=\"ie8\"> <![endif]-->
<!--[if IE 9]> <html lang=\"en\" class=\"ie9\"> <![endif]-->
<!--[if !IE]><!--> <html lang=\"en\"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset=\"utf-8\" />
   <title>Welcome to Adventure Bidding System</title>
   <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\" />
   <meta content=\"\" name=\"description\" />
   <meta content=\"\" name=\"author\" />
   <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap-fileupload.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/font-awesome/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style-responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style-default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"style_color\" />
   <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\"/>
   ";
        // line 21
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 24
        echo "
</head>
<div id=\"header\" class=\"navbar navbar-inverse navbar-fixed-top\">
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class=\"navbar-inner\">
           <div class=\"container-fluid\">
              
               <!-- BEGIN LOGO -->
               <a class=\"brand\" href=\"index.html\">
                   <h2 class=\"hreaderbrand\">Adventure Bidding System</h2>
               </a>
               <!-- END LOGO -->
               
               <!-- END  NOTIFICATION -->
               <div class=\"top-nav \">
                   <ul class=\"nav pull-right top-menu\" >
                   \t\t<li class=\"dropdown\">
                           <a href=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath("adventure_login_index");
        echo "\" class=\"dropdown-toggle\">
                               <i class=\"icon-angle-up\"></i>
                               <span class=\"username\">Join Now</span>
                               <!--     <b class=\"caret\"></b>-->
                           </a>
                       </li>
                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       ";
        // line 48
        if ((!$this->getAttribute($this->getContext($context, "app"), "user"))) {
            // line 49
            echo "                       <li class=\"dropdown\">
                           <a href=\"";
            // line 50
            echo $this->env->getExtension('routing')->getPath("adventure_login_index");
            echo "\" class=\"dropdown-toggle\">
                               <i class=\"icon-user\"></i>
                               <span class=\"username\">Login</span>
                           </a>
                          
                       </li>
                       ";
        } else {
            // line 57
            echo "                       <li class=\"dropdown\">
                           <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                               <img src=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home"), "html", null, true);
            echo "/img/avatar-mini.png\" alt=\"\">
                               <span class=\"username\">";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "email"), "html", null, true);
            echo "</span>
                               <b class=\"caret\"></b>
                           </a>
                           <ul class=\"dropdown-menu extended logout\">
                               <li><a href=\"viewprofile.html\"><i class=\"icon-user\"></i> My Profile</a></li>
                               <li><a href=\"editprofile.html\"><i class=\"icon-cog\"></i> Edit Profile</a></li>
                               <li><a href=\"login.html\"><i class=\"icon-key\"></i> Log Out</a></li>
                           </ul>
                       </li>
                       ";
        }
        // line 70
        echo "                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                   <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
";
        // line 78
        $this->displayBlock('body', $context, $blocks);
        // line 82
        echo "<!-- START FOOTER -->
   <div id=\"footer\">
       2014 &copy; Adventure Bidding System
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   
   <script src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/marquee.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery.marquee.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery.nicescroll.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
   <script src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
   <script src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-slimscroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
   <script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/common-scripts.js"), "html", null, true);
        echo "\"></script>
    
";
        // line 101
        $this->displayBlock('javascripts', $context, $blocks);
        // line 105
        echo "    </body>
</html>";
    }

    // line 21
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 22
        echo " 
";
    }

    // line 78
    public function block_body($context, array $blocks = array())
    {
        // line 79
        echo "

";
    }

    // line 101
    public function block_javascripts($context, array $blocks = array())
    {
        // line 102
        echo "

";
    }

    public function getTemplateName()
    {
        return "::headerbase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 102,  213 => 78,  205 => 21,  198 => 101,  104 => 49,  237 => 170,  207 => 148,  195 => 145,  185 => 97,  344 => 190,  336 => 158,  333 => 157,  328 => 26,  325 => 25,  318 => 193,  316 => 189,  308 => 187,  304 => 186,  300 => 185,  296 => 184,  292 => 183,  288 => 182,  284 => 181,  272 => 178,  260 => 175,  256 => 174,  248 => 172,  216 => 79,  200 => 105,  190 => 122,  170 => 108,  126 => 63,  211 => 127,  181 => 96,  129 => 66,  279 => 148,  275 => 147,  265 => 143,  261 => 142,  257 => 141,  222 => 101,  188 => 93,  167 => 107,  178 => 82,  175 => 84,  161 => 91,  150 => 82,  114 => 54,  110 => 53,  90 => 41,  127 => 87,  124 => 86,  97 => 61,  290 => 151,  286 => 150,  280 => 180,  276 => 179,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 171,  236 => 133,  231 => 160,  197 => 103,  191 => 99,  180 => 115,  165 => 92,  146 => 65,  65 => 21,  53 => 18,  152 => 85,  148 => 78,  134 => 73,  76 => 26,  70 => 33,  34 => 4,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 189,  337 => 103,  322 => 101,  314 => 99,  312 => 188,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 177,  264 => 176,  258 => 140,  252 => 173,  247 => 78,  241 => 77,  229 => 157,  220 => 70,  214 => 128,  177 => 95,  169 => 93,  140 => 55,  132 => 67,  128 => 49,  107 => 50,  61 => 20,  273 => 96,  269 => 144,  254 => 92,  243 => 88,  240 => 171,  238 => 85,  235 => 74,  230 => 82,  227 => 117,  224 => 71,  221 => 77,  219 => 154,  217 => 75,  208 => 22,  204 => 109,  179 => 69,  159 => 91,  143 => 64,  135 => 68,  119 => 62,  102 => 48,  71 => 21,  67 => 20,  63 => 19,  59 => 18,  94 => 28,  89 => 28,  85 => 40,  75 => 22,  68 => 24,  56 => 18,  87 => 25,  28 => 5,  93 => 28,  88 => 48,  78 => 21,  27 => 7,  46 => 10,  44 => 15,  31 => 3,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 82,  171 => 81,  166 => 37,  163 => 62,  158 => 30,  156 => 66,  151 => 77,  142 => 66,  138 => 70,  136 => 70,  121 => 59,  117 => 57,  105 => 51,  91 => 27,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 23,  72 => 16,  69 => 25,  47 => 15,  40 => 7,  37 => 14,  22 => 1,  246 => 133,  157 => 73,  145 => 67,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 57,  111 => 53,  108 => 57,  101 => 48,  98 => 53,  96 => 43,  83 => 24,  74 => 14,  66 => 34,  55 => 17,  52 => 17,  50 => 24,  43 => 14,  41 => 15,  35 => 12,  32 => 4,  29 => 6,  209 => 82,  203 => 147,  199 => 146,  193 => 99,  189 => 98,  187 => 84,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 57,  154 => 78,  149 => 66,  147 => 58,  144 => 73,  141 => 48,  133 => 69,  130 => 41,  125 => 60,  122 => 55,  116 => 41,  112 => 51,  109 => 54,  106 => 36,  103 => 32,  99 => 31,  95 => 45,  92 => 41,  86 => 28,  82 => 33,  80 => 30,  73 => 24,  64 => 20,  60 => 19,  57 => 19,  54 => 10,  51 => 16,  48 => 11,  45 => 10,  42 => 7,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
