<?php

/* AdventureLoginBundle:Member:verifyEmailMessage.html.twig */
class __TwigTemplate_e4c033062c823f2e7dfbdf43f189202dee4b6a1981a960b4e477578df8c02607 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
Dear User,

        Welcome to www.adventureclicknblog.com, the adventure hub of India.
        
        We again, thank you for registering with us.
        
        We need to make sure you are human. Please verify your email and get started using your Website account.
        
        Click on this link to verify your email address 
        
        Localhost :  
        
        http://localhost/dev/Bid_Adventure/web/app_dev.php/verify-email?memId=";
        // line 14
        echo twig_escape_filter($this->env, $this->getContext($context, "memberId"), "html", null, true);
        echo "&activation=";
        echo twig_escape_filter($this->env, $this->getContext($context, "code"), "html", null, true);
        echo "&role=";
        echo twig_escape_filter($this->env, $this->getContext($context, "role"), "html", null, true);
        echo "

        http://www.localhost.com/dev/Bid_Adventure/web/app_dev.php/verify-email?memId=";
        // line 16
        echo twig_escape_filter($this->env, $this->getContext($context, "memberId"), "html", null, true);
        echo "&activation=";
        echo twig_escape_filter($this->env, $this->getContext($context, "code"), "html", null, true);
        echo "&role=";
        echo twig_escape_filter($this->env, $this->getContext($context, "role"), "html", null, true);
        echo "
        
        You can follow us in facebook at https://www.facebook.com/adventureclicknblog?ref=hl

        Warm Regards,
        Admin,
        www.adventureclicknblog.com
";
    }

    public function getTemplateName()
    {
        return "AdventureLoginBundle:Member:verifyEmailMessage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 102,  213 => 78,  205 => 21,  198 => 101,  104 => 49,  237 => 170,  207 => 148,  195 => 145,  185 => 97,  344 => 190,  336 => 158,  333 => 157,  328 => 26,  325 => 25,  318 => 193,  316 => 189,  308 => 187,  304 => 186,  300 => 185,  296 => 184,  292 => 183,  288 => 182,  284 => 181,  272 => 178,  260 => 175,  256 => 174,  248 => 172,  216 => 79,  200 => 105,  190 => 122,  170 => 108,  126 => 63,  211 => 127,  181 => 96,  129 => 66,  279 => 148,  275 => 147,  265 => 143,  261 => 142,  257 => 141,  222 => 101,  188 => 93,  167 => 107,  178 => 82,  175 => 84,  161 => 91,  150 => 82,  114 => 54,  110 => 53,  90 => 41,  127 => 87,  124 => 86,  97 => 61,  290 => 151,  286 => 150,  280 => 180,  276 => 179,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 171,  236 => 133,  231 => 160,  197 => 103,  191 => 99,  180 => 115,  165 => 92,  146 => 65,  65 => 21,  53 => 18,  152 => 85,  148 => 78,  134 => 73,  76 => 26,  70 => 33,  34 => 14,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 189,  337 => 103,  322 => 101,  314 => 99,  312 => 188,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 177,  264 => 176,  258 => 140,  252 => 173,  247 => 78,  241 => 77,  229 => 157,  220 => 70,  214 => 128,  177 => 95,  169 => 93,  140 => 55,  132 => 67,  128 => 49,  107 => 50,  61 => 20,  273 => 96,  269 => 144,  254 => 92,  243 => 88,  240 => 171,  238 => 85,  235 => 74,  230 => 82,  227 => 117,  224 => 71,  221 => 77,  219 => 154,  217 => 75,  208 => 22,  204 => 109,  179 => 69,  159 => 91,  143 => 64,  135 => 68,  119 => 62,  102 => 48,  71 => 21,  67 => 20,  63 => 19,  59 => 18,  94 => 28,  89 => 28,  85 => 40,  75 => 22,  68 => 24,  56 => 18,  87 => 25,  28 => 5,  93 => 28,  88 => 48,  78 => 21,  27 => 7,  46 => 10,  44 => 15,  31 => 3,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 82,  171 => 81,  166 => 37,  163 => 62,  158 => 30,  156 => 66,  151 => 77,  142 => 66,  138 => 70,  136 => 70,  121 => 59,  117 => 57,  105 => 51,  91 => 27,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 23,  72 => 16,  69 => 25,  47 => 15,  40 => 7,  37 => 14,  22 => 1,  246 => 133,  157 => 73,  145 => 67,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 57,  111 => 53,  108 => 57,  101 => 48,  98 => 53,  96 => 43,  83 => 24,  74 => 14,  66 => 34,  55 => 17,  52 => 17,  50 => 24,  43 => 16,  41 => 15,  35 => 12,  32 => 4,  29 => 6,  209 => 82,  203 => 147,  199 => 146,  193 => 99,  189 => 98,  187 => 84,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 57,  154 => 78,  149 => 66,  147 => 58,  144 => 73,  141 => 48,  133 => 69,  130 => 41,  125 => 60,  122 => 55,  116 => 41,  112 => 51,  109 => 54,  106 => 36,  103 => 32,  99 => 31,  95 => 45,  92 => 41,  86 => 28,  82 => 33,  80 => 30,  73 => 24,  64 => 20,  60 => 19,  57 => 19,  54 => 10,  51 => 16,  48 => 11,  45 => 10,  42 => 7,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
