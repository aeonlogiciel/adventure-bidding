<?php

/* AcmeDemoBundle:Secured:hello.html.twig */
class __TwigTemplate_89472cd3b763103acd150c4b8ae7b96843df5998ff6f64514489d242cff5eaec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AcmeDemoBundle:Secured:layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle:Secured:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 11
        $context["code"] = $this->env->getExtension('demo')->getCode($this);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ("Hello " . $this->getContext($context, "name")), "html", null, true);
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h1 class=\"title\">Hello ";
        echo twig_escape_filter($this->env, $this->getContext($context, "name"), "html", null, true);
        echo "!</h1>

    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_demo_secured_hello_admin", array("name" => $this->getContext($context, "name"))), "html", null, true);
        echo "\">Hello resource secured for <strong>admin</strong> only.</a>
";
    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Secured:hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  458 => 253,  375 => 201,  364 => 195,  332 => 209,  690 => 610,  687 => 609,  100 => 32,  653 => 349,  650 => 348,  627 => 327,  612 => 325,  593 => 307,  589 => 305,  575 => 294,  557 => 282,  546 => 274,  537 => 268,  520 => 257,  505 => 245,  486 => 228,  481 => 225,  467 => 218,  460 => 214,  429 => 229,  426 => 199,  411 => 194,  405 => 192,  399 => 190,  397 => 189,  380 => 183,  376 => 181,  212 => 134,  500 => 323,  497 => 322,  456 => 284,  351 => 182,  339 => 179,  335 => 210,  311 => 164,  226 => 119,  417 => 196,  386 => 219,  367 => 208,  320 => 176,  223 => 147,  373 => 147,  370 => 146,  357 => 186,  301 => 181,  289 => 158,  281 => 160,  277 => 154,  504 => 354,  501 => 353,  434 => 293,  412 => 281,  404 => 275,  390 => 208,  354 => 228,  343 => 180,  329 => 174,  295 => 160,  118 => 72,  369 => 241,  313 => 166,  297 => 200,  293 => 165,  287 => 173,  234 => 142,  271 => 162,  259 => 169,  232 => 133,  473 => 219,  470 => 332,  349 => 188,  324 => 205,  160 => 86,  239 => 155,  84 => 29,  77 => 31,  602 => 350,  599 => 349,  566 => 288,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 232,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 197,  408 => 193,  403 => 241,  382 => 183,  377 => 216,  372 => 215,  346 => 200,  342 => 184,  299 => 162,  263 => 155,  255 => 152,  250 => 135,  215 => 110,  153 => 81,  503 => 355,  455 => 252,  446 => 305,  436 => 205,  425 => 289,  414 => 195,  406 => 277,  392 => 222,  356 => 230,  345 => 178,  331 => 177,  363 => 178,  358 => 204,  350 => 196,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 179,  274 => 180,  251 => 136,  551 => 325,  548 => 324,  526 => 260,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 230,  416 => 227,  410 => 226,  400 => 240,  394 => 217,  378 => 214,  366 => 174,  353 => 169,  347 => 166,  340 => 197,  338 => 182,  334 => 177,  321 => 172,  317 => 171,  315 => 171,  307 => 163,  245 => 141,  242 => 132,  228 => 140,  206 => 132,  202 => 110,  194 => 129,  113 => 62,  218 => 136,  210 => 133,  186 => 127,  172 => 104,  81 => 38,  192 => 114,  174 => 126,  155 => 74,  23 => 3,  233 => 145,  184 => 98,  137 => 72,  58 => 13,  225 => 115,  213 => 144,  205 => 133,  198 => 130,  104 => 49,  237 => 141,  207 => 108,  195 => 105,  185 => 107,  344 => 190,  336 => 181,  333 => 175,  328 => 178,  325 => 162,  318 => 207,  316 => 205,  308 => 150,  304 => 198,  300 => 185,  296 => 179,  292 => 159,  288 => 166,  284 => 165,  272 => 152,  260 => 141,  256 => 137,  248 => 154,  216 => 135,  200 => 130,  190 => 128,  170 => 92,  126 => 60,  211 => 109,  181 => 100,  129 => 66,  279 => 148,  275 => 147,  265 => 167,  261 => 157,  257 => 147,  222 => 137,  188 => 99,  167 => 124,  178 => 125,  175 => 91,  161 => 98,  150 => 81,  114 => 71,  110 => 22,  90 => 32,  127 => 28,  124 => 67,  97 => 57,  290 => 176,  286 => 142,  280 => 155,  276 => 178,  270 => 178,  266 => 167,  262 => 166,  253 => 136,  249 => 145,  244 => 133,  236 => 142,  231 => 139,  197 => 114,  191 => 107,  180 => 69,  165 => 90,  146 => 78,  65 => 18,  53 => 10,  152 => 91,  148 => 90,  134 => 71,  76 => 17,  70 => 15,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 209,  444 => 303,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 198,  413 => 234,  409 => 132,  407 => 131,  402 => 191,  398 => 211,  393 => 188,  387 => 187,  384 => 121,  381 => 202,  379 => 182,  374 => 116,  368 => 197,  365 => 27,  362 => 26,  360 => 173,  355 => 189,  341 => 177,  337 => 176,  322 => 178,  314 => 172,  312 => 170,  309 => 170,  305 => 163,  298 => 161,  294 => 199,  285 => 163,  283 => 156,  278 => 178,  268 => 167,  264 => 164,  258 => 150,  252 => 146,  247 => 135,  241 => 77,  229 => 120,  220 => 126,  214 => 134,  177 => 124,  169 => 91,  140 => 73,  132 => 40,  128 => 73,  107 => 62,  61 => 12,  273 => 179,  269 => 168,  254 => 92,  243 => 143,  240 => 143,  238 => 154,  235 => 74,  230 => 141,  227 => 182,  224 => 138,  221 => 113,  219 => 115,  217 => 111,  208 => 107,  204 => 109,  179 => 94,  159 => 83,  143 => 76,  135 => 74,  119 => 70,  102 => 17,  71 => 31,  67 => 20,  63 => 19,  59 => 18,  94 => 34,  89 => 28,  85 => 42,  75 => 32,  68 => 28,  56 => 11,  87 => 29,  28 => 3,  93 => 53,  88 => 31,  78 => 26,  27 => 3,  46 => 8,  44 => 12,  31 => 3,  38 => 6,  26 => 11,  24 => 4,  25 => 4,  201 => 102,  196 => 90,  183 => 126,  171 => 69,  166 => 89,  163 => 89,  158 => 97,  156 => 82,  151 => 77,  142 => 78,  138 => 42,  136 => 75,  121 => 66,  117 => 19,  105 => 18,  91 => 30,  62 => 15,  49 => 11,  21 => 2,  19 => 1,  79 => 24,  72 => 17,  69 => 29,  47 => 8,  40 => 6,  37 => 5,  22 => 1,  246 => 144,  157 => 63,  145 => 77,  139 => 75,  131 => 73,  123 => 71,  120 => 20,  115 => 69,  111 => 42,  108 => 19,  101 => 47,  98 => 53,  96 => 31,  83 => 24,  74 => 21,  66 => 26,  55 => 17,  52 => 14,  50 => 17,  43 => 14,  41 => 5,  35 => 12,  32 => 7,  29 => 5,  209 => 82,  203 => 107,  199 => 106,  193 => 113,  189 => 132,  187 => 103,  182 => 125,  176 => 124,  173 => 92,  168 => 76,  164 => 77,  162 => 88,  154 => 78,  149 => 79,  147 => 80,  144 => 43,  141 => 78,  133 => 61,  130 => 60,  125 => 67,  122 => 57,  116 => 70,  112 => 52,  109 => 63,  106 => 66,  103 => 58,  99 => 32,  95 => 47,  92 => 29,  86 => 28,  82 => 28,  80 => 30,  73 => 16,  64 => 13,  60 => 16,  57 => 19,  54 => 18,  51 => 14,  48 => 13,  45 => 10,  42 => 9,  39 => 13,  36 => 4,  33 => 3,  30 => 2,);
    }
}
