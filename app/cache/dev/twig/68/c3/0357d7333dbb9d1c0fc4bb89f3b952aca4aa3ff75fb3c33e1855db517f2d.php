<?php

/* AdventureBiddingBundle:User:editRequestUser.html.twig */
class __TwigTemplate_68c30357d7333dbb9d1c0fc4bb89f3b952aca4aa3ff75fb3c33e1855db517f2d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::userbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::userbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System | User Request";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

<!-- BEGIN CONTAINER -->
<div id=\"container\" class=\"row-fluid\">
 
<!-- BEGIN PAGE -->
<div id=\"main-content\"> 
  <!-- BEGIN PAGE CONTAINER-->
  <div class=\"container-fluid\"> 
    <!-- BEGIN PAGE HEADER-->
    <div class=\"row-fluid\">
      <div class=\"span12\"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class=\"page-title\"> </h3>
        <ul class=\"breadcrumb\">
          <li> <a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a> <span class=\"divider\">/</span> </li>
          <li class=\"active\"> Post Request </li>
         
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER--> 
    <!-- BEGIN PAGE CONTENT-->
    <div id=\"page-wraper\">
      <div class=\"row-fluid\">
        <div class=\"span12\"> 
          <!-- BEGIN ACCORDION PORTLET-->
          <!-- Acknowledgement message start -->
\t\t\t";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 47
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 56
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 61
            echo "                        ";
        } else {
            // line 62
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 63
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 71
            echo "                        ";
        }
        // line 72
        echo "                        <!-- Acknowledgement message end -->  
          <div class=\"widget blue\">
            <div class=\"widget-title\">
              <h4><i class=\"icon-user\"></i>Edit User Inforation</h4>
            </div>
            <div class=\"widget-body\">
              <div class =\"row-fluid\">
                <div class=\"span12\">
                    ";
        // line 80
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "editForm"), 'form_start');
        echo "
                    <div class=\"well\">
                      <h3>Trip Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Trip Name</label>
                            ";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "name"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "name"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Duration Of Trip</label>
                          ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "day"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "day"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label >Journey Date </label>
                            ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "dateMin"), 'widget', array("attr" => array("class" => "datepicker span5")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "dateMin"), 'errors');
        echo "
                            
                            To
                            ";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "dateMax"), 'widget', array("attr" => array("class" => "datepicker span5")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "dateMax"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class=\"well\">
                      <h3>Location Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Country</label>
                            ";
        // line 115
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "country"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "country"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\" style='display: none;'>
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Location</label>
                            ";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "location"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "location"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Zones</label>
                            <div class=\"controls\">
                                ";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "zone"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "zone"), 'errors');
        echo "
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class=\"well\">
                      <h3> Groups Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Number Of Males</label>
                            ";
        // line 141
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "male"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "male"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Number Of Females</label>
                            ";
        // line 147
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "female"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "female"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Number Of Traveller</label>
                            ";
        // line 153
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "traveler"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "traveler"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class=\"well\">
                      <h3> Activities Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Activities</label>
                            ";
        // line 165
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "activity"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "activity"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Pick-Up Location</label>
                           ";
        // line 171
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "pick"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "pick"), 'errors');
        echo "
                            
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Drop-Off Location</label>
                            ";
        // line 178
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "down"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "down"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class=\"well\">
                      <h3> Other Details </h3>
                      <hr>
                    <div class =\"row-fluid\">
                      
                      <div class=\"span12\">
                        <div class=\"control-group\">
                          <label class=\"control-label\">If your group size is too small for an organizer to respond, are you open for us to combine your group with others who are looking for the exact or similar trips?  </label>
                          <div class=\"controls\">
                             ";
        // line 192
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "team"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "editForm"), "team"), 'errors');
        echo "
                          </div>
                        </div>
                        
                      </div>
                       </div>
                       <hr>
                       <div class =\"row-fluid\">
                      <div class=\"span12\">
                        <div class=\"control-group\">
                          <label class=\"input-block-level\"> Special Requirement: </label>
                          <div class=\"controls\">
                               ";
        // line 204
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "editForm"), 'rest');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                    
                    <div class=\"form-actions\">
                      <button class=\"btn btn-primary\" id=\"save\" type=\"submit\"> Submit</button>
                      <button type=\"button\" class=\"btn btn-default\">Cancel</button>
                    </div>
                  ";
        // line 215
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "editForm"), 'form_end');
        echo "
                </div>
              </div>
            </div>
            <!-- END ACCORDION PORTLET--> 
          </div>
        </div>
        
        <!-- END PAGE CONTENT--> 
      </div>
      
      <!-- END PAGE CONTENT--> 
      
    </div>
    
    <!-- END PAGE CONTAINER--> 
  </div>
  <!-- END PAGE --> 
</div>
<!-- END CONTAINER --> 
</div>
</body>
<!-- END BODY -->
";
    }

    // line 240
    public function block_javascripts($context, array $blocks = array())
    {
        // line 241
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
\t\t\$(document).ready(function() {
\t\t\$('.multipl').multiselect();
\t\t});
\t</script> 
    <script type=\"text/javascript\">
    \$('#example-single-selected').multiselect();
    </script> 
    <script>
    \$('.datepicker').datepicker({
        format: 'mm-dd-yyyy',
        startDate: '-3d'
    })
     function getValue(){
        var a = document.getElementById('userRequest_male').value;
        var b = document.getElementById('userRequest_female').value;
         
        var c = parseInt(a) + parseInt(b) ;
        document.getElementById('userRequest_traveler').value = c ;
    }
    </script> 

";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:User:editRequestUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  403 => 241,  400 => 240,  372 => 215,  358 => 204,  341 => 192,  322 => 178,  310 => 171,  299 => 165,  282 => 153,  271 => 147,  260 => 141,  242 => 128,  230 => 121,  219 => 115,  202 => 103,  194 => 100,  183 => 94,  172 => 88,  161 => 80,  151 => 72,  148 => 71,  141 => 67,  135 => 63,  132 => 62,  129 => 61,  126 => 60,  116 => 56,  105 => 51,  95 => 47,  85 => 42,  68 => 28,  49 => 11,  46 => 10,  40 => 7,  34 => 4,  31 => 3,);
    }
}
