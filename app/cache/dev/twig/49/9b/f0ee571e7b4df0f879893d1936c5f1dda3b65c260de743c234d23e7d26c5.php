<?php

/* AdventureBiddingBundle:Organiser:dashboardOrganiser.html.twig */
class __TwigTemplate_499bf0ee571e7b4df0f879893d1936c5f1dda3b65c260de743c234d23e7d26c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::organiserbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::organiserbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                    Organizer Dashboard
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           All Request
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
               <div class=\"row-fluid\">
                <div class=\"span12\">
                <!-- BEGIN EXAMPLE TABLE widget-->
                
                <div class=\"widget blue\">
                    <div class=\"widget-title\">
                        <h4><i class=\"icon-search\"></i> Filter Your Search</h4>
                         
                    </div>
                    <div class=\"widget-body\">
                         <form class=\"form-inline\" method=\"post\">
                         <div class=\"row-fluid\">
                         
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Filter Search</label>
                          <div class=\"controls\">
                                    <select class=\"span8 \" data-placeholder=\"Choose a Category\" tabindex=\"1\">
                                        <option value=\"\">Select Filter</option>
                                        <option value=\"Activities\">Activities</option>
                                        <option value=\"Zones\">Zones</option>
                                        <option value=\"Category 3\">Articles</option>
                                        <option value=\"Category 4\">Location</option>
                                    </select>
                                </div>
                          
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Search</label>
                           <div class=\"controls\">
                          <input type=\"text\" class=\"span8\"/>   
                          </div>
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          
                          <label class=\"control-label\"></label>
                          <div class=\"controls\">
                <button type=\"submit\" class=\"btn btn-primary\"><i class=\"icon-ok\"></i> Submit</button>
                         <button type=\"button\" class=\"btn\">Cancel</button>
                
               </div>
                          </div>
                         </div>
                         
                         
                         </div>
                         
                         </form>
                        
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
                <div class=\"row-fluid\">
                    <!--marquee start here-->
                    <div class=\"span12\">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class=\"widget blue\">
                            <div class=\"widget-title\">
                                <h4>Bidding Info</h4>
                            </div>
                            <div class=\"widget-body\">
                                <table class=\"table table-striped table-bordered  table-hover\" id=\"sample_1\">
                                    <thead>
                                    <tr>
                                    \t <th class=\"hidden-phone\">Sr. No.</th>
                                         <th>Your Request</th>
                                         <th>Request Title</th>
                                       \t <th>Status</th>
                                         <th>Received Bid</th>
                                         <th>User Response</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    ";
        // line 122
        $context["count"] = "1";
        // line 123
        echo "                                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "requestUser"));
        foreach ($context['_seq'] as $context["_key"] => $context["requestUser"]) {
            // line 124
            echo "                                    <tr>
                                    \t <td class=\"hidden-phone\">";
            // line 125
            echo twig_escape_filter($this->env, $this->getContext($context, "count"), "html", null, true);
            echo "</td>
                                         <td>";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "requestUser"), "trip"), "html", null, true);
            echo "</td>
                                         <td>";
            // line 127
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "requestUser"), "username"), "html", null, true);
            echo "</td>
                                         ";
            // line 128
            if (($this->getAttribute($this->getContext($context, "requestUser"), "status") == "0")) {
                // line 129
                echo "                                            <td><span class=\"label label-success label-mini\">close</span></td>
                                            <td>
                                                <a href=\"";
                // line 131
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_view_close_request", array("id" => $this->getAttribute($this->getContext($context, "requestUser"), "reqid"))), "html", null, true);
                echo "\" class=\"btn btn-success\">Click to See</a>
                                            </td>
                                        ";
            } else {
                // line 134
                echo "                                            <td><span class=\"label label-important label-mini\">open</span></td>
                                            <td>
                                                <a href=\"";
                // line 136
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_view_open_request", array("id" => $this->getAttribute($this->getContext($context, "requestUser"), "reqid"))), "html", null, true);
                echo "\" class=\"btn btn-success\">Click to See</a>
                                            </td> 
                                            
                                        ";
            }
            // line 140
            echo "                                               ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "requestUser"), "messagecount"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 141
                echo "                                            <td><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_view_open_request", array("id" => $this->getAttribute($this->getContext($context, "requestUser"), "reqid"))), "html", null, true);
                echo "\">
                                            <input type=\"button\" class=\"btn-danger\" value=\"";
                // line 142
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "message"), 1, array(), "array"), "html", null, true);
                echo "\">&nbsp;&nbsp;New message</a></td>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 144
            echo "                                    </tr>
                                    ";
            // line 145
            $context["count"] = ($this->getContext($context, "count") + "1");
            // line 146
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['requestUser'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
                         <!--marquee End here-->
                </div>
                
                
            </div>

            <!-- END PAGE CONTENT-->         

      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

    
</body>
<!-- END BODY -->
";
    }

    // line 171
    public function block_javascripts($context, array $blocks = array())
    {
        // line 172
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:dashboardOrganiser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 172,  265 => 171,  239 => 147,  233 => 146,  231 => 145,  228 => 144,  220 => 142,  215 => 141,  210 => 140,  203 => 136,  199 => 134,  193 => 131,  189 => 129,  187 => 128,  183 => 127,  179 => 126,  175 => 125,  172 => 124,  167 => 123,  165 => 122,  68 => 28,  49 => 11,  46 => 10,  40 => 7,  34 => 4,  31 => 3,);
    }
}
