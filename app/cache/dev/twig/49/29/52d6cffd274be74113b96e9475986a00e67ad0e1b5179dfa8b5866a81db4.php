<?php

/* AdventureBiddingBundle:Organiser:editEventOrganiser.html.twig */
class __TwigTemplate_492952d6cffd274be74113b96e9475986a00e67ad0e1b5179dfa8b5866a81db4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::organiserbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::organiserbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                    Edit Events
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           Edit Events
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
             <!-- Acknowledgement message start -->
\t\t\t";
        // line 40
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 45
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 54
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 59
            echo "                        ";
        } else {
            // line 60
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 61
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 69
            echo "                        ";
        }
        // line 70
        echo "                        <!-- Acknowledgement message end -->  
          <!-- BEGIN ACCORDION PORTLET-->
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
               <div class=\"row-fluid\">
                <div class=\"span12\">
                <!-- BEGIN EXAMPLE TABLE widget-->
                
                <div class=\"widget blue\">
                    <div class=\"widget-title\">
                        <h4><i class=\"icon-search\"></i> Filter Your Search</h4>
                         
                    </div>
                    <div class=\"widget-body\">
                         <form class=\"form-inline\" method=\"post\">
                         <div class=\"row-fluid\">
                         
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Filter Search</label>
                          <div class=\"controls\">
                                    <select class=\"span8 \" data-placeholder=\"Choose a Category\" tabindex=\"1\">
                                        <option value=\"\">Select...</option>
                                        <option value=\"Activities\">Activities</option>
                                        <option value=\"Zones\">Zones</option>
                                        <option value=\"Category 3\">Category 5</option>
                                        <option value=\"Category 4\">Category 4</option>
                                    </select>
                                </div>
                          
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Search</label>
                           <div class=\"controls\">
                          <input type=\"text\" class=\"span8\"/>   
                          </div>
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          
                          <label class=\"control-label\"></label>
                          <div class=\"controls\">
                <button type=\"submit\" class=\"btn btn-primary\"><i class=\"icon-ok\"></i> Submit</button>
                         <button type=\"button\" class=\"btn\">Cancel</button>
                
               </div>
                          </div>
                         </div>
                         
                         
                         </div>
                         
                         </form>
                        
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
              <div class=\"row-fluid\">
                    
                    <!--marquee start here-->
                    <div class=\"span12\">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class=\"widget blue\">
                            <div class=\"widget-title\">
                                <h4>Add Event</h4>
                            </div>
                            <div class=\"widget-body\">
                                <div class =\"row-fluid\">  
                               <div class=\"span12\">
                                
                                ";
        // line 146
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "form"), 'form_start', array("attr" => array("class" => "form-vertical")));
        echo "
                                    <div class=\"well\">
                      <h3>Event Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Full Name</label>
                            ";
        // line 154
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "ename"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                            
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Activities </label>
                            ";
        // line 161
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eActivity"), 'widget', array("attr" => array("class" => "input-block-level chzn-select")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eActivity"), 'errors');
        echo "
                          
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Travel Date</label>
                            ";
        // line 168
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDate"), 'widget', array("attr" => array("class" => "datepicker span5")));
        echo "  ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDate"), 'errors');
        echo "
                          
                          </div>
                        </div>
                        
                       
                      </div>
                      <div class=\"row-fluid\">
                      <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Phone No </label>
                            ";
        // line 179
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eContactDetail"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                <div class=\"control-group\">
                            <label class=\"control-label\" >Email </label>
                            ";
        // line 185
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "email"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                <div class=\"control-group\">
                            <label class=\"control-label\" >Duration </label>
                            ";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "duration"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                          </div>
                        </div>
                    
                        </div>
                       <div class=\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">continent</label>
                            ";
        // line 200
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "continent"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "country"), 'errors');
        echo "
                            
                          </div>
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Country</label>
                            ";
        // line 207
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "country"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "continent"), 'errors');
        echo "
                          </div>
                        </div>
                    <div class=\"span4\">
                        <div class=\"control-group\">
                          <label class=\"input-block-level\"> Description: </label>
                          <div class=\"controls\">
                            
                            ";
        // line 215
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eBrief"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eBrief"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                      </div>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >State</label>
                          ";
        // line 224
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eState"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4", "placeholder" => "Choose an option")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eState"), 'errors');
        echo "
                          </div>
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Region</label>
                            ";
        // line 230
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eRegion"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eRegion"), 'errors');
        echo "
                          </div>
                              
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Destination</label>
                            ";
        // line 237
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDestination"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDestination"), 'errors');
        echo "
                          </div>
                          </div>  
                        </div>
                           <div class =\"row-fluid\">
                          
                               <div class=\"span4\">
                        
                         <div class=\"control-group\">
                                    <label class=\"control-label\">Logo Upload</label>
                                    <div class=\"controls\">
                                        <div data-provides=\"fileupload\" class=\"fileupload fileupload-new\">
                                            <div style=\"width: 200px; height: 150px;\" class=\"fileupload-new thumbnail\">
                                                <img alt=\"\" src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\">
                                            </div>
                                            <div style=\"max-width: 200px; max-height: 150px; line-height: 20px;\" class=\"fileupload-preview fileupload-exists thumbnail\"></div>
                                            <div>
                                               <span class=\"btn btn-file\"><span class=\"fileupload-new\">Select image</span>
                                               <span class=\"fileupload-exists\">Change</span>
                                               ";
        // line 256
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "path"), 'widget', array("attr" => array("class" => "default")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "path"), 'errors');
        echo "</span>
                                                <a data-dismiss=\"fileupload\" class=\"btn fileupload-exists\" href=\"#\">Remove</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                        
                        </div>
                          ";
        // line 277
        echo "                      </div>
                         
                          <div class =\"row-fluid\">
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Difficulty</label>
                            ";
        // line 283
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDifficulty"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDifficulty"), 'errors');
        echo "
                          </div>
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Zone</label>
                            ";
        // line 289
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eZone"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eZone"), 'errors');
        echo "
                          </div>
                        </div>
                               <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Budget</label>
                            ";
        // line 295
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "budget"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "budget"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>             
                     ";
        // line 305
        echo "                            ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'rest');
        echo "
                    <div class=\"form-actions\">
                      <button class=\"btn btn-primary\" id=\"save\" type=\"submit\"> Submit</button>
                      <button type=\"button\" class=\"btn btn-default\">Cancel</button>
                    </div>
                  ";
        // line 310
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "form"), 'form_end');
        echo "      
                              
                   
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
                         <!--marquee End here-->
                </div>
                
                
            </div>

            <!-- END PAGE CONTENT-->         

      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
 <!--model for feedback-->  
      <div id=\"myModal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">User Feedback</h3>
            </div>
            <div class=\"modal-body\">
            <p>Phasellus fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo.
             Sed ut quam at magna porttitor hendrerit. Maecenas quis erat fringilla augue feugiat vulputate a eu 
            sem.Vivamus ut diam at turpis varius tempor. Aliquam dictum sagittis erat, vehicula adipiscing
             diam condimentum id. </p>
            </div>
            <div class=\"modal-footer\">
            <button class=\"btn btn-primary\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>
           
            </div>
      </div>
               
 <!--end model for feedback-->    
    
</body>
<!-- END BODY -->
";
    }

    // line 355
    public function block_javascripts($context, array $blocks = array())
    {
        // line 356
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:editEventOrganiser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  503 => 355,  455 => 310,  446 => 305,  436 => 295,  425 => 289,  414 => 283,  406 => 277,  392 => 256,  356 => 230,  345 => 224,  331 => 215,  363 => 178,  358 => 147,  350 => 26,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 164,  274 => 162,  251 => 146,  551 => 325,  548 => 324,  526 => 304,  506 => 356,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 219,  394 => 217,  378 => 204,  366 => 179,  353 => 190,  347 => 25,  340 => 182,  338 => 178,  334 => 177,  321 => 174,  317 => 172,  315 => 171,  307 => 169,  245 => 125,  242 => 124,  228 => 115,  206 => 107,  202 => 106,  194 => 104,  113 => 56,  218 => 110,  210 => 108,  186 => 94,  172 => 86,  81 => 34,  192 => 75,  174 => 66,  155 => 59,  23 => 1,  233 => 113,  184 => 76,  137 => 66,  58 => 11,  225 => 126,  213 => 78,  205 => 21,  198 => 105,  104 => 49,  237 => 170,  207 => 111,  195 => 145,  185 => 97,  344 => 190,  336 => 158,  333 => 157,  328 => 178,  325 => 25,  318 => 207,  316 => 189,  308 => 187,  304 => 186,  300 => 185,  296 => 184,  292 => 183,  288 => 182,  284 => 181,  272 => 178,  260 => 168,  256 => 174,  248 => 161,  216 => 79,  200 => 102,  190 => 103,  170 => 108,  126 => 63,  211 => 127,  181 => 96,  129 => 66,  279 => 148,  275 => 147,  265 => 143,  261 => 142,  257 => 141,  222 => 101,  188 => 93,  167 => 62,  178 => 73,  175 => 91,  161 => 91,  150 => 69,  114 => 54,  110 => 53,  90 => 30,  127 => 59,  124 => 58,  97 => 61,  290 => 166,  286 => 165,  280 => 146,  276 => 179,  270 => 161,  266 => 160,  262 => 134,  253 => 149,  249 => 137,  244 => 171,  236 => 133,  231 => 160,  197 => 103,  191 => 99,  180 => 69,  165 => 82,  146 => 69,  65 => 21,  53 => 18,  152 => 60,  148 => 78,  134 => 65,  76 => 33,  70 => 38,  34 => 4,  480 => 162,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 208,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 237,  365 => 111,  362 => 110,  360 => 194,  355 => 146,  341 => 189,  337 => 103,  322 => 174,  314 => 172,  312 => 170,  309 => 97,  305 => 168,  298 => 168,  294 => 191,  285 => 185,  283 => 88,  278 => 163,  268 => 177,  264 => 176,  258 => 140,  252 => 173,  247 => 78,  241 => 77,  229 => 112,  220 => 111,  214 => 109,  177 => 95,  169 => 93,  140 => 67,  132 => 50,  128 => 49,  107 => 47,  61 => 20,  273 => 96,  269 => 144,  254 => 92,  243 => 88,  240 => 171,  238 => 154,  235 => 74,  230 => 82,  227 => 146,  224 => 113,  221 => 109,  219 => 154,  217 => 75,  208 => 22,  204 => 109,  179 => 95,  159 => 77,  143 => 68,  135 => 63,  119 => 50,  102 => 46,  71 => 21,  67 => 20,  63 => 19,  59 => 18,  94 => 30,  89 => 28,  85 => 40,  75 => 22,  68 => 28,  56 => 18,  87 => 25,  28 => 3,  93 => 45,  88 => 38,  78 => 21,  27 => 7,  46 => 10,  44 => 11,  31 => 3,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 82,  171 => 69,  166 => 67,  163 => 61,  158 => 78,  156 => 76,  151 => 74,  142 => 67,  138 => 70,  136 => 70,  121 => 59,  117 => 43,  105 => 51,  91 => 43,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 23,  72 => 16,  69 => 29,  47 => 15,  40 => 7,  37 => 14,  22 => 1,  246 => 133,  157 => 63,  145 => 67,  139 => 65,  131 => 62,  123 => 58,  120 => 44,  115 => 49,  111 => 48,  108 => 57,  101 => 34,  98 => 33,  96 => 43,  83 => 40,  74 => 30,  66 => 37,  55 => 17,  52 => 17,  50 => 24,  43 => 14,  41 => 15,  35 => 12,  32 => 4,  29 => 5,  209 => 82,  203 => 147,  199 => 80,  193 => 98,  189 => 98,  187 => 84,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 65,  154 => 78,  149 => 70,  147 => 57,  144 => 70,  141 => 48,  133 => 61,  130 => 60,  125 => 60,  122 => 55,  116 => 54,  112 => 51,  109 => 43,  106 => 36,  103 => 49,  99 => 45,  95 => 42,  92 => 29,  86 => 28,  82 => 33,  80 => 30,  73 => 39,  64 => 10,  60 => 22,  57 => 19,  54 => 10,  51 => 16,  48 => 11,  45 => 10,  42 => 10,  39 => 13,  36 => 13,  33 => 6,  30 => 2,);
    }
}
