<?php

/* AdventureBiddingBundle:User:editProfileUser.html.twig */
class __TwigTemplate_f4b0c251c2c4b3efcecbc441334ebcc1300befb8422c045b2565e20cc16c6f09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::userbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::userbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Adventure Bidding System | User Edit Profile";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "   
<!-- BEGIN BODY -->
<body class=\"fixed-top\">
   
   <!-- BEGIN CONTAINER -->
   <div id=\"container\" class=\"row-fluid\">
      
      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                 
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                     
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"#\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"\">
                           My Profile <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           Edit Profile
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
      <div class=\"row-fluid\">
          
        <div class=\"span12\"> 
            <!-- Acknowledgement message start -->
\t\t\t";
        // line 53
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 58
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 67
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 72
            echo "                        ";
        } else {
            // line 73
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 74
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 78
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 82
            echo "                        ";
        }
        // line 83
        echo "                        <!-- Acknowledgement message end -->  
          <!-- BEGIN ACCORDION PORTLET-->
          <div class=\"widget blue\">
            <div class=\"widget-title\">
              <h4><i class=\"icon-user\"></i>Edit User Inforation</h4>
            </div>
            <div class=\"widget-body\">
              <div class =\"row-fluid\">
                <div class=\"span12\">
                    ";
        // line 92
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_start', array("attr" => array("class" => "form-vertical")));
        echo "
                    <div class=\"well\">
                      <h3>Personal Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Full Name</label>
                            ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "name"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                          </div>
                        </div>
                        
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Email </label>
                            ";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'errors');
        echo "
                          </div>
                        </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Phone No </label>
                            ";
        // line 113
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "phone"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "phone"), 'errors');
        echo "
                          </div>
                        </div>
                         
                        
                       
                      </div>
                      <h3>Location Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Country</label>
                            ";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'widget', array("attr" => array("class" => "input-block-level chzn-select", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'errors');
        echo "
                            
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Area Code</label>
                            ";
        // line 133
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zip"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zip"), 'errors');
        echo "
                          </div>
                        </div>
                        
                        <div class=\"span4\">
                        <div class=\"control-group\">
                          <label class=\"input-block-level\"> Address: </label>
                          <div class=\"controls\">
                            ";
        // line 141
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "address"), 'widget', array("attr" => array("class" => "span12")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "address"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                      </div>
                      <hr>
                      <div class=\"row-fluid\">
                      
                        <div class=\"span4\">
                        
                         <div class=\"control-group\">
                                    <label class=\"control-label\">Image Upload</label>
                                    <div class=\"controls\">
                                        <div data-provides=\"fileupload\" class=\"fileupload fileupload-new\">
                                            <div style=\"width: 200px; height: 150px;\" class=\"fileupload-new thumbnail\">
                                                <img alt=\"\" src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\">
                                            </div>
                                            <div style=\"max-width: 200px; max-height: 150px; line-height: 20px;\" class=\"fileupload-preview fileupload-exists thumbnail\"></div>
                                            <div>
                                               <span class=\"btn btn-file\"><span class=\"fileupload-new\">Select image</span>
                                               <span class=\"fileupload-exists\">Change</span>
                                               ";
        // line 162
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "applicantphoto"), 'widget', array("attr" => array("class" => "default")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "applicantphoto"), 'errors');
        echo "</span>
                                                <a data-dismiss=\"fileupload\" class=\"btn fileupload-exists\" href=\"#\">Remove</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                        
                        </div>
                      
                      </div>
                    </div>
                   
                    
                    ";
        // line 176
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "Form"), 'rest');
        echo "
                    <div class=\"form-actions\">
                        <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">
                      <a href=\"";
        // line 179
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_edit_profile");
        echo "\"><button type=\"button\" class=\"btn btn-default\">Cancel</button></a>
                    </div>
                  ";
        // line 181
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_end');
        echo "
                </div>
              </div>
              
              
             
              
            </div>
            <!-- END ACCORDION PORTLET--> 
          </div>
        </div>
        
        <!-- END PAGE CONTENT--> 
      </div>
      
      <!-- END PAGE CONTENT--> 
      
    </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

    
\t
";
    }

    // line 209
    public function block_javascripts($context, array $blocks = array())
    {
        // line 210
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
\t\t\$(document).ready(function() {
\t\t\$('.multipl').multiselect();
\t\t});
    </script>

";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:User:editProfileUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  335 => 210,  332 => 209,  301 => 181,  296 => 179,  290 => 176,  271 => 162,  245 => 141,  232 => 133,  220 => 126,  202 => 113,  191 => 107,  181 => 100,  170 => 92,  159 => 83,  156 => 82,  149 => 78,  143 => 74,  140 => 73,  137 => 72,  134 => 71,  124 => 67,  113 => 62,  103 => 58,  93 => 53,  49 => 11,  46 => 10,  40 => 7,  34 => 4,  31 => 3,);
    }
}
