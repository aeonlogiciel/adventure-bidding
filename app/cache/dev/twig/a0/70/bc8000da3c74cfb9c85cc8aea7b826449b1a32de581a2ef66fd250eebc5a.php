<?php

/* AdventureBiddingBundle:User:registerUser.html.twig */
class __TwigTemplate_a070bc8000da3c74cfb9c85cc8aea7b826449b1a32de581a2ef66fd250eebc5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Complete USER Registeration: 

";
        // line 3
        if (array_key_exists("message", $context)) {
            // line 4
            echo "      ";
            echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
            echo "
";
        }
        // line 6
        echo "

";
        // line 8
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_start');
        echo "
    <br>
    Name : ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "name"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "name"), 'errors');
        echo "<br>
    Email : ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'errors');
        echo "<br>
    Phone : ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "phone"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "phone"), 'errors');
        echo "<br>
    Address : ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "address"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "address"), 'errors');
        echo "<br>
    Zip : ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zip"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zip"), 'errors');
        echo "<br>
    Country : ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'errors');
        echo "<br>
    Image : ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "applicantphoto"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "applicantphoto"), 'errors');
        echo "
    ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "Form"), 'rest');
        echo "
    <button class=\"btn btn-success\" id=\"save\" type=\"submit\"> <i class=\"icon-ok\"></i>Submit</button>
";
        // line 19
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_end');
        echo "
    
";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:User:registerUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  504 => 354,  501 => 353,  434 => 293,  412 => 281,  404 => 275,  390 => 254,  354 => 228,  343 => 222,  329 => 213,  295 => 201,  118 => 72,  369 => 241,  313 => 187,  297 => 177,  293 => 176,  287 => 173,  234 => 153,  271 => 145,  259 => 138,  232 => 121,  473 => 333,  470 => 332,  349 => 220,  324 => 205,  160 => 86,  239 => 155,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 318,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 286,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 238,  408 => 232,  403 => 230,  382 => 218,  377 => 216,  372 => 242,  346 => 200,  342 => 198,  299 => 171,  263 => 155,  255 => 152,  250 => 150,  215 => 141,  153 => 82,  503 => 355,  455 => 310,  446 => 305,  436 => 251,  425 => 289,  414 => 283,  406 => 277,  392 => 222,  356 => 230,  345 => 224,  331 => 215,  363 => 178,  358 => 147,  350 => 201,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 171,  274 => 177,  251 => 146,  551 => 325,  548 => 324,  526 => 304,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 219,  394 => 217,  378 => 204,  366 => 235,  353 => 190,  347 => 25,  340 => 197,  338 => 178,  334 => 177,  321 => 174,  317 => 172,  315 => 171,  307 => 169,  245 => 125,  242 => 124,  228 => 144,  206 => 117,  202 => 106,  194 => 110,  113 => 59,  218 => 110,  210 => 140,  186 => 94,  172 => 124,  81 => 38,  192 => 114,  174 => 93,  155 => 59,  23 => 3,  233 => 146,  184 => 94,  137 => 69,  58 => 13,  225 => 144,  213 => 78,  205 => 21,  198 => 117,  104 => 49,  237 => 170,  207 => 120,  195 => 145,  185 => 104,  344 => 190,  336 => 212,  333 => 157,  328 => 188,  325 => 25,  318 => 207,  316 => 205,  308 => 175,  304 => 198,  300 => 185,  296 => 184,  292 => 189,  288 => 166,  284 => 165,  272 => 178,  260 => 163,  256 => 174,  248 => 149,  216 => 79,  200 => 102,  190 => 96,  170 => 108,  126 => 74,  211 => 127,  181 => 93,  129 => 63,  279 => 148,  275 => 147,  265 => 171,  261 => 157,  257 => 141,  222 => 128,  188 => 93,  167 => 123,  178 => 73,  175 => 91,  161 => 90,  150 => 85,  114 => 71,  110 => 53,  90 => 30,  127 => 59,  124 => 64,  97 => 57,  290 => 166,  286 => 165,  280 => 146,  276 => 168,  270 => 161,  266 => 160,  262 => 134,  253 => 149,  249 => 137,  244 => 156,  236 => 152,  231 => 145,  197 => 101,  191 => 99,  180 => 69,  165 => 91,  146 => 69,  65 => 21,  53 => 19,  152 => 83,  148 => 81,  134 => 76,  76 => 16,  70 => 15,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 308,  444 => 303,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 287,  413 => 234,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 220,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 237,  365 => 111,  362 => 110,  360 => 225,  355 => 223,  341 => 189,  337 => 103,  322 => 185,  314 => 172,  312 => 170,  309 => 175,  305 => 174,  298 => 168,  294 => 169,  285 => 171,  283 => 183,  278 => 149,  268 => 172,  264 => 164,  258 => 166,  252 => 160,  247 => 157,  241 => 77,  229 => 112,  220 => 142,  214 => 128,  177 => 95,  169 => 91,  140 => 70,  132 => 64,  128 => 58,  107 => 62,  61 => 20,  273 => 164,  269 => 166,  254 => 92,  243 => 127,  240 => 171,  238 => 154,  235 => 74,  230 => 82,  227 => 182,  224 => 181,  221 => 115,  219 => 123,  217 => 75,  208 => 107,  204 => 109,  179 => 126,  159 => 80,  143 => 71,  135 => 65,  119 => 59,  102 => 46,  71 => 21,  67 => 26,  63 => 24,  59 => 21,  94 => 30,  89 => 28,  85 => 33,  75 => 22,  68 => 28,  56 => 20,  87 => 19,  28 => 3,  93 => 50,  88 => 45,  78 => 18,  27 => 7,  46 => 11,  44 => 11,  31 => 6,  38 => 6,  26 => 5,  24 => 4,  25 => 4,  201 => 112,  196 => 90,  183 => 100,  171 => 69,  166 => 67,  163 => 82,  158 => 78,  156 => 79,  151 => 74,  142 => 78,  138 => 66,  136 => 70,  121 => 59,  117 => 66,  105 => 51,  91 => 43,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 32,  72 => 17,  69 => 33,  47 => 17,  40 => 10,  37 => 10,  22 => 1,  246 => 159,  157 => 63,  145 => 67,  139 => 65,  131 => 59,  123 => 58,  120 => 44,  115 => 49,  111 => 48,  108 => 54,  101 => 47,  98 => 50,  96 => 23,  83 => 40,  74 => 30,  66 => 26,  55 => 17,  52 => 12,  50 => 17,  43 => 16,  41 => 15,  35 => 8,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 134,  193 => 131,  189 => 129,  187 => 101,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 91,  154 => 75,  149 => 75,  147 => 68,  144 => 70,  141 => 76,  133 => 61,  130 => 75,  125 => 57,  122 => 73,  116 => 71,  112 => 52,  109 => 43,  106 => 66,  103 => 55,  99 => 45,  95 => 42,  92 => 29,  86 => 28,  82 => 17,  80 => 30,  73 => 29,  64 => 14,  60 => 15,  57 => 19,  54 => 18,  51 => 18,  48 => 10,  45 => 9,  42 => 12,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
