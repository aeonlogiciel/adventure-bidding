<?php

/* AdventureLoginBundle:Member:loginAdmin.html.twig */
class __TwigTemplate_6cf3969c0738d6470bce09e9d0160bd01b235aa21cbaa25d61024959a7f5ea96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 8]> <html lang=\"en\" class=\"ie8\"> <![endif]-->
<!--[if IE 9]> <html lang=\"en\" class=\"ie9\"> <![endif]-->
<!--[if !IE]><!--> <html lang=\"en\"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
\t<meta charset=\"utf-8\" />
\t<title>Metronic | Login Page</title>
\t<meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\" />
\t<meta content=\"\" name=\"description\" />
\t<meta content=\"\" name=\"author\" />
\t<!-- BEGIN GLOBAL MANDATORY STYLES -->
\t<link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/bootstrap/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/css/style-metro.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/css/style-responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" id=\"style_color\"/>
\t<link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/select2/select2_metro.css"), "html", null, true);
        echo "\" />
\t<!-- END GLOBAL MANDATORY STYLES -->
\t<!-- BEGIN PAGE LEVEL STYLES -->
\t<link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/css/pages/login.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<!-- END PAGE LEVEL STYLES -->
\t<link rel=\"shortcut icon\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/favicon.ico"), "html", null, true);
        echo "\" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class=\"login\">
\t<!-- BEGIN LOGO -->
\t<div class=\"logo\">
\t\t<!--<img src=\"assets1/img/logo-big.png\" alt=\"\" /> -->
\t</div>
\t<!-- END LOGO -->
\t<!-- BEGIN LOGIN -->
\t<div class=\"content\">
    <header class=\"headertop\">
        \t<h3 class=\"text-bold white\">Adventure Bidding System</h3>
\t\t\t<span class=\"title\"><strong>Admin Sign In</strong></span>
\t\t</header>
\t\t<!-- BEGIN LOGIN FORM -->
\t\t<form class=\"form-vertical login-form\" action=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
\t\t\t<div class=\"seprator\"></div>
                        <!-- Acknowledgement message start -->
\t\t\t";
        // line 46
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 51
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 60
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 65
            echo "                        ";
        } else {
            // line 66
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 67
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 71
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 75
            echo "                        ";
        }
        // line 76
        echo "                        <!-- Acknowledgement message end -->
\t\t\t";
        // line 81
        echo "\t\t\t<div class=\"control-group\">
\t\t\t\t<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
\t\t\t\t<label class=\"control-label visible-ie8 visible-ie9\">Username</label>
\t\t\t\t<div class=\"controls\">
\t\t\t\t\t<div class=\"input-icon left\">
\t\t\t\t\t\t<i class=\"icon-user\"></i>
                                                <input type=\"text\" name=\"_username\" class=\"m-wrap placeholder-no-fix\" autocomplete=\"off\" placeholder=\"Email\">
\t\t\t\t\t\t";
        // line 89
        echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"control-group\">
\t\t\t\t<label class=\"control-label visible-ie8 visible-ie9\">Password</label>
\t\t\t\t<div class=\"controls\">
\t\t\t\t\t<div class=\"input-icon left\">
\t\t\t\t\t\t<i class=\"icon-lock\"></i>
                                                <input type=\"password\" name=\"_password\" class=\"m-wrap placeholder-no-fix\" placeholder=\"Password\" autocomplete=\"off\">
\t\t\t\t\t\t";
        // line 99
        echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-actions\">
\t\t\t\t<a href=\"";
        // line 103
        echo $this->env->getExtension('routing')->getPath("adventure_login_index");
        echo "\" class=\"btn\">
\t\t\t\t<i class=\"m-icon-swapleft\"></i>  Back
\t\t\t\t</a>
                                <input type=\"submit\" class=\"btn green pull-right \" value=\"SIGN IN\">
\t\t\t\t";
        // line 109
        echo "           
\t\t\t</div>
\t\t\t
\t\t\t
\t\t</form>
\t\t<!-- END LOGIN FORM -->        
\t\t<div class=\"footsec black\">
\t\t\t
            2014 &copy; Adventure Bidding System.
            
\t\t</div>
\t\t
\t</div>
\t<!-- END LOGIN -->
\t<!-- BEGIN COPYRIGHT -->
\t<!--<div class=\"copyright\">
\t\t2014 &copy; Adventure Bidding System.
\t</div>-->
\t<!-- END COPYRIGHT -->
\t<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
\t<!-- BEGIN CORE PLUGINS -->   
\t<script src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/jquery-1.10.1.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/jquery-migrate-1.2.1.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
\t<script src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>      
\t<script src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
\t<!--[if lt IE 9]>
\t<script src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/plugins/excanvas.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/plugins/respond.min.js"), "html", null, true);
        echo "\"></script>  
\t<![endif]-->   
\t<script src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/jquery-slimscroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/jquery.blockui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>  
\t<script src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/jquery.cookie.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
\t<!-- END CORE PLUGINS -->
\t<!-- BEGIN PAGE LEVEL PLUGINS -->
\t<script src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/jquery-validation/dist/jquery.validate.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>\t
\t<script type=\"text/javascript\" src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/plugins/select2/select2.min.js"), "html", null, true);
        echo "\"></script>     
\t<!-- END PAGE LEVEL PLUGINS -->
\t<!-- BEGIN PAGE LEVEL SCRIPTS -->
\t<script src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/scripts/app.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets1/scripts/login.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script> 
\t<!-- END PAGE LEVEL SCRIPTS --> 
\t<script>
\t\tjQuery(document).ready(function() {     
\t\t  App.init();
\t\t  Login.init();
\t\t
\t\t 
\t\t
\t\t});
\t</script>
\t<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>";
    }

    public function getTemplateName()
    {
        return "AdventureLoginBundle:Member:loginAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  290 => 151,  286 => 150,  280 => 147,  276 => 146,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 135,  236 => 133,  231 => 131,  197 => 103,  191 => 99,  180 => 89,  165 => 75,  146 => 65,  65 => 21,  53 => 18,  152 => 67,  148 => 84,  134 => 73,  76 => 26,  70 => 33,  34 => 11,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 140,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 51,  128 => 49,  107 => 36,  61 => 20,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 134,  238 => 85,  235 => 74,  230 => 82,  227 => 130,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 109,  179 => 69,  159 => 61,  143 => 64,  135 => 53,  119 => 42,  102 => 46,  71 => 24,  67 => 15,  63 => 15,  59 => 14,  94 => 28,  89 => 20,  85 => 40,  75 => 17,  68 => 14,  56 => 9,  87 => 25,  28 => 5,  93 => 28,  88 => 6,  78 => 21,  27 => 7,  46 => 7,  44 => 12,  31 => 8,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 81,  166 => 71,  163 => 62,  158 => 71,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 56,  121 => 46,  117 => 44,  105 => 70,  91 => 27,  62 => 23,  49 => 17,  21 => 2,  19 => 1,  79 => 36,  72 => 16,  69 => 25,  47 => 9,  40 => 8,  37 => 14,  22 => 3,  246 => 90,  157 => 56,  145 => 46,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 57,  111 => 37,  108 => 36,  101 => 32,  98 => 46,  96 => 43,  83 => 25,  74 => 14,  66 => 34,  55 => 15,  52 => 27,  50 => 24,  43 => 6,  41 => 15,  35 => 5,  32 => 4,  29 => 6,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 76,  164 => 59,  162 => 57,  154 => 58,  149 => 66,  147 => 58,  144 => 49,  141 => 48,  133 => 60,  130 => 41,  125 => 44,  122 => 55,  116 => 41,  112 => 51,  109 => 54,  106 => 36,  103 => 32,  99 => 31,  95 => 45,  92 => 44,  86 => 28,  82 => 22,  80 => 19,  73 => 34,  64 => 14,  60 => 29,  57 => 19,  54 => 10,  51 => 14,  48 => 8,  45 => 16,  42 => 7,  39 => 9,  36 => 5,  33 => 13,  30 => 10,);
    }
}
