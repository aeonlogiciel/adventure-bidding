<?php

/* ::userbase1.html.twig */
class __TwigTemplate_54c590300c57b591ae4c66da92b90f029bb2a9cadf2f40cccefbfb7ed4fb9f89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/bootstrap/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/bootstrap/css/bootstrap-fileupload.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/font-awesome/css/font-awesome.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style-responsive.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style-default.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"), "html", null, true);
        echo "\" />
        
  
   
";
        // line 18
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "    </head>             
  <body class=\"fixed-top\">  
       <!-- BEGIN HEADER -->
   <div id=\"header\" class=\"navbar navbar-inverse navbar-fixed-top\">
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class=\"navbar-inner\">
           <div class=\"container-fluid\">
              
               <!-- BEGIN LOGO -->
               <a class=\"brand\" href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">
                   <h2 class=\"hreaderbrand\">Adventure Bidding System</h2>
               </a>
               <!-- END LOGO -->
               <!-- BEGIN RESPONSIVE MENU TOGGLER -->
               <a class=\"btn btn-navbar collapsed\" id=\"main_menu_trigger\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
                   <span class=\"icon-bar\"></span>
                   <span class=\"icon-bar\"></span>
                   <span class=\"icon-bar\"></span>
                   <span class=\"arrow\"></span>
               </a>
               <!-- END RESPONSIVE MENU TOGGLER -->
               
               <!-- END  NOTIFICATION -->
               <div class=\"top-nav \">
                   <ul class=\"nav pull-right top-menu\" >
                       <li class=\"dropdown\">
                           <a href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("adventure_login_index");
        echo "\" class=\"dropdown-toggle\">
                               <i class=\"icon-angle-up\"></i>
                               <span class=\"username\">Join Now</span>
                               <!--     <b class=\"caret\"></b>-->
                           </a>
                       </li>
                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       <li class=\"dropdown\">
                           <a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("adventure_login_index");
        echo "\" class=\"dropdown-toggle\">
                               <i class=\"icon-user\"></i>
                               <span class=\"username\">Login</span>
                               <b class=\"caret\"></b>
                           </a>
                           <!--<ul class=\"dropdown-menu extended logout\">
                               <li><a href=\"profile.html\"><i class=\"icon-user\"></i> My Profile</a></li>
                               <li><a href=\"#\"><i class=\"icon-cog\"></i> My Settings</a></li>
                               <li><a href=\"login.html\"><i class=\"icon-key\"></i> Log Out</a></li>
                           </ul>-->
                       </li>
                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                   <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->
      
";
        // line 76
        $this->displayBlock('body', $context, $blocks);
        // line 80
        echo " 
 <!-- BEGIN FOOTER -->
   <div id=\"footer\">
       2014 &copy; Adventure Bidding System
   </div>
   <!-- END FOOTER --> 
   
   
   
       
        <script src=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>  
        <script src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/common-scripts.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/marquee.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.marquee.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
       <script src=\" ";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"), "html", null, true);
        echo "\"></script>
       
        <script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/jquery-slimscroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        
       
       
";
        // line 103
        $this->displayBlock('javascripts', $context, $blocks);
        // line 107
        echo "</body>
</html>
";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 18
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 19
        echo " 
";
    }

    // line 76
    public function block_body($context, array $blocks = array())
    {
        // line 77
        echo "

";
    }

    // line 103
    public function block_javascripts($context, array $blocks = array())
    {
        // line 104
        echo "

";
    }

    public function getTemplateName()
    {
        return "::userbase1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1414 => 421,  1408 => 419,  1402 => 417,  1400 => 416,  1398 => 415,  1394 => 414,  1385 => 413,  1383 => 412,  1380 => 411,  1367 => 405,  1361 => 403,  1355 => 401,  1353 => 400,  1351 => 399,  1347 => 398,  1341 => 397,  1339 => 396,  1336 => 395,  1323 => 389,  1317 => 387,  1311 => 385,  1309 => 384,  1307 => 383,  1303 => 382,  1297 => 381,  1291 => 380,  1287 => 379,  1283 => 378,  1279 => 377,  1273 => 376,  1271 => 375,  1268 => 374,  1256 => 369,  1251 => 368,  1249 => 367,  1246 => 366,  1237 => 360,  1231 => 358,  1228 => 357,  1223 => 356,  1221 => 355,  1218 => 354,  1211 => 349,  1202 => 347,  1198 => 346,  1195 => 345,  1192 => 344,  1190 => 343,  1187 => 342,  1179 => 338,  1177 => 337,  1174 => 336,  1168 => 332,  1162 => 330,  1159 => 329,  1157 => 328,  1154 => 327,  1145 => 322,  1143 => 321,  1118 => 320,  1115 => 319,  1112 => 318,  1109 => 317,  1106 => 316,  1103 => 315,  1100 => 314,  1098 => 313,  1095 => 312,  1088 => 308,  1084 => 307,  1079 => 306,  1077 => 305,  1074 => 304,  1067 => 299,  1064 => 298,  1056 => 293,  1053 => 292,  1051 => 291,  1048 => 290,  1040 => 285,  1036 => 284,  1032 => 283,  1029 => 282,  1027 => 281,  1024 => 280,  1016 => 276,  1014 => 272,  1012 => 271,  1009 => 270,  1004 => 266,  982 => 261,  979 => 260,  976 => 259,  973 => 258,  970 => 257,  967 => 256,  964 => 255,  961 => 254,  958 => 253,  955 => 252,  952 => 251,  950 => 250,  947 => 249,  939 => 243,  936 => 242,  934 => 241,  931 => 240,  923 => 236,  920 => 235,  918 => 234,  915 => 233,  903 => 229,  900 => 228,  897 => 227,  894 => 226,  892 => 225,  889 => 224,  881 => 220,  878 => 219,  876 => 218,  873 => 217,  865 => 213,  862 => 212,  860 => 211,  857 => 210,  849 => 206,  846 => 205,  844 => 204,  841 => 203,  833 => 199,  830 => 198,  828 => 197,  825 => 196,  817 => 192,  814 => 191,  812 => 190,  809 => 189,  801 => 185,  798 => 184,  793 => 182,  785 => 178,  783 => 177,  780 => 176,  772 => 172,  769 => 171,  767 => 170,  764 => 169,  756 => 165,  753 => 164,  751 => 163,  739 => 156,  729 => 155,  724 => 154,  721 => 153,  715 => 151,  712 => 150,  707 => 148,  699 => 142,  697 => 141,  696 => 140,  695 => 139,  689 => 137,  683 => 135,  680 => 134,  678 => 133,  675 => 132,  666 => 126,  662 => 125,  658 => 124,  654 => 123,  643 => 120,  638 => 118,  635 => 117,  619 => 113,  617 => 112,  614 => 111,  598 => 107,  596 => 106,  576 => 101,  564 => 99,  555 => 95,  550 => 94,  547 => 93,  524 => 90,  512 => 84,  496 => 79,  478 => 74,  464 => 71,  450 => 64,  442 => 62,  433 => 60,  388 => 42,  385 => 41,  20 => 1,  462 => 202,  449 => 198,  441 => 196,  439 => 195,  415 => 180,  401 => 172,  348 => 140,  323 => 128,  267 => 101,  672 => 345,  668 => 344,  664 => 342,  651 => 337,  647 => 336,  644 => 335,  640 => 119,  631 => 327,  626 => 325,  613 => 320,  609 => 319,  591 => 309,  588 => 308,  585 => 307,  581 => 305,  577 => 303,  569 => 300,  563 => 298,  559 => 296,  552 => 293,  545 => 291,  541 => 290,  533 => 284,  531 => 283,  525 => 280,  519 => 278,  515 => 85,  509 => 83,  499 => 268,  489 => 262,  479 => 256,  471 => 253,  465 => 249,  459 => 69,  448 => 240,  428 => 59,  422 => 184,  418 => 224,  383 => 207,  327 => 114,  303 => 122,  291 => 102,  810 => 492,  807 => 491,  796 => 183,  792 => 488,  788 => 486,  775 => 485,  749 => 162,  746 => 161,  727 => 476,  710 => 149,  706 => 473,  702 => 472,  698 => 471,  694 => 138,  686 => 468,  682 => 467,  679 => 466,  677 => 465,  660 => 340,  649 => 122,  634 => 456,  629 => 326,  625 => 453,  622 => 323,  620 => 451,  606 => 318,  601 => 446,  567 => 414,  549 => 411,  532 => 410,  527 => 91,  522 => 279,  517 => 404,  389 => 160,  371 => 35,  361 => 146,  458 => 253,  375 => 201,  364 => 195,  332 => 20,  690 => 469,  687 => 609,  100 => 36,  653 => 349,  650 => 348,  627 => 327,  612 => 325,  593 => 105,  589 => 305,  575 => 294,  557 => 96,  546 => 274,  537 => 268,  520 => 257,  505 => 270,  486 => 228,  481 => 225,  467 => 72,  460 => 214,  429 => 188,  426 => 58,  411 => 194,  405 => 49,  399 => 190,  397 => 213,  380 => 160,  376 => 205,  212 => 279,  500 => 323,  497 => 267,  456 => 68,  351 => 141,  339 => 179,  335 => 21,  311 => 14,  226 => 84,  417 => 196,  386 => 159,  367 => 339,  320 => 127,  223 => 147,  373 => 156,  370 => 146,  357 => 123,  301 => 181,  289 => 113,  281 => 411,  277 => 154,  504 => 354,  501 => 80,  434 => 293,  412 => 222,  404 => 275,  390 => 43,  354 => 228,  343 => 146,  329 => 131,  295 => 178,  118 => 49,  369 => 241,  313 => 15,  297 => 179,  293 => 6,  287 => 173,  234 => 142,  271 => 374,  259 => 103,  232 => 88,  473 => 254,  470 => 73,  349 => 188,  324 => 113,  160 => 86,  239 => 155,  84 => 41,  77 => 25,  602 => 317,  599 => 349,  566 => 288,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 92,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 78,  490 => 77,  485 => 285,  477 => 283,  475 => 282,  438 => 236,  420 => 197,  408 => 50,  403 => 48,  382 => 183,  377 => 37,  372 => 215,  346 => 200,  342 => 23,  299 => 8,  263 => 365,  255 => 353,  250 => 341,  215 => 280,  153 => 56,  503 => 81,  455 => 252,  446 => 197,  436 => 235,  425 => 289,  414 => 52,  406 => 277,  392 => 222,  356 => 230,  345 => 147,  331 => 140,  363 => 32,  358 => 151,  350 => 26,  330 => 176,  326 => 138,  310 => 171,  306 => 286,  302 => 125,  282 => 179,  274 => 97,  251 => 136,  551 => 325,  548 => 292,  526 => 260,  506 => 293,  492 => 279,  483 => 258,  463 => 248,  454 => 244,  443 => 245,  431 => 189,  416 => 227,  410 => 221,  400 => 47,  394 => 168,  378 => 157,  366 => 33,  353 => 328,  347 => 191,  340 => 145,  338 => 135,  334 => 141,  321 => 112,  317 => 185,  315 => 125,  307 => 128,  245 => 335,  242 => 132,  228 => 104,  206 => 132,  202 => 4,  194 => 103,  113 => 40,  218 => 136,  210 => 270,  186 => 239,  172 => 62,  81 => 40,  192 => 114,  174 => 95,  155 => 47,  23 => 1,  233 => 304,  184 => 233,  137 => 72,  58 => 15,  225 => 103,  213 => 78,  205 => 108,  198 => 130,  104 => 90,  237 => 141,  207 => 269,  195 => 105,  185 => 66,  344 => 24,  336 => 181,  333 => 175,  328 => 139,  325 => 129,  318 => 111,  316 => 16,  308 => 13,  304 => 181,  300 => 121,  296 => 121,  292 => 159,  288 => 4,  284 => 165,  272 => 152,  260 => 363,  256 => 96,  248 => 336,  216 => 76,  200 => 72,  190 => 76,  170 => 94,  126 => 147,  211 => 19,  181 => 232,  129 => 148,  279 => 148,  275 => 105,  265 => 96,  261 => 157,  257 => 147,  222 => 297,  188 => 102,  167 => 124,  178 => 64,  175 => 65,  161 => 202,  150 => 55,  114 => 111,  110 => 38,  90 => 27,  127 => 35,  124 => 132,  97 => 41,  290 => 5,  286 => 112,  280 => 155,  276 => 395,  270 => 102,  266 => 366,  262 => 98,  253 => 342,  249 => 145,  244 => 133,  236 => 142,  231 => 83,  197 => 249,  191 => 246,  180 => 69,  165 => 60,  146 => 181,  65 => 14,  53 => 11,  152 => 46,  148 => 90,  134 => 161,  76 => 31,  70 => 19,  34 => 5,  480 => 75,  474 => 267,  469 => 158,  461 => 70,  457 => 245,  453 => 199,  444 => 238,  440 => 148,  437 => 61,  435 => 146,  430 => 144,  427 => 143,  423 => 57,  413 => 234,  409 => 132,  407 => 131,  402 => 215,  398 => 211,  393 => 211,  387 => 164,  384 => 121,  381 => 202,  379 => 182,  374 => 36,  368 => 34,  365 => 197,  362 => 26,  360 => 173,  355 => 27,  341 => 189,  337 => 22,  322 => 178,  314 => 172,  312 => 124,  309 => 108,  305 => 163,  298 => 120,  294 => 199,  285 => 3,  283 => 100,  278 => 410,  268 => 373,  264 => 164,  258 => 354,  252 => 146,  247 => 135,  241 => 90,  229 => 85,  220 => 290,  214 => 134,  177 => 124,  169 => 210,  140 => 76,  132 => 40,  128 => 73,  107 => 37,  61 => 13,  273 => 394,  269 => 107,  254 => 92,  243 => 327,  240 => 326,  238 => 312,  235 => 311,  230 => 303,  227 => 301,  224 => 81,  221 => 113,  219 => 77,  217 => 289,  208 => 18,  204 => 267,  179 => 97,  159 => 196,  143 => 51,  135 => 62,  119 => 117,  102 => 30,  71 => 19,  67 => 18,  63 => 21,  59 => 13,  94 => 57,  89 => 47,  85 => 30,  75 => 22,  68 => 20,  56 => 14,  87 => 26,  28 => 4,  93 => 28,  88 => 30,  78 => 24,  27 => 7,  46 => 10,  44 => 8,  31 => 4,  38 => 5,  26 => 3,  24 => 2,  25 => 35,  201 => 106,  196 => 107,  183 => 98,  171 => 216,  166 => 93,  163 => 82,  158 => 91,  156 => 195,  151 => 188,  142 => 80,  138 => 42,  136 => 168,  121 => 131,  117 => 39,  105 => 47,  91 => 29,  62 => 16,  49 => 10,  21 => 2,  19 => 1,  79 => 32,  72 => 18,  69 => 13,  47 => 10,  40 => 8,  37 => 7,  22 => 2,  246 => 93,  157 => 89,  145 => 52,  139 => 169,  131 => 160,  123 => 42,  120 => 31,  115 => 40,  111 => 110,  108 => 33,  101 => 33,  98 => 29,  96 => 67,  83 => 31,  74 => 21,  66 => 12,  55 => 12,  52 => 12,  50 => 10,  43 => 9,  41 => 8,  35 => 4,  32 => 4,  29 => 3,  209 => 82,  203 => 73,  199 => 265,  193 => 113,  189 => 240,  187 => 99,  182 => 87,  176 => 223,  173 => 85,  168 => 61,  164 => 203,  162 => 92,  154 => 90,  149 => 182,  147 => 54,  144 => 176,  141 => 175,  133 => 61,  130 => 46,  125 => 42,  122 => 41,  116 => 55,  112 => 39,  109 => 105,  106 => 104,  103 => 58,  99 => 68,  95 => 34,  92 => 31,  86 => 46,  82 => 25,  80 => 24,  73 => 23,  64 => 17,  60 => 20,  57 => 12,  54 => 15,  51 => 37,  48 => 10,  45 => 9,  42 => 7,  39 => 10,  36 => 5,  33 => 6,  30 => 3,);
    }
}
