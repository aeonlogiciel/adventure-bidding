<?php

/* AdventureBiddingBundle:Organiser:viewOpenRequestOrganiser.html.twig */
class __TwigTemplate_db20b1e7829eb1e2b4959550cddb1f8b50ab1b77e01248338bb3ec43adc57dca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::organiserbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::organiserbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "<!-- BEGIN BODY -->
<body class=\"fixed-top\">


        <!-- BEGIN PAGE -->  
        <div id=\"main-content\">
            <!-- BEGIN PAGE CONTAINER-->
            <div class=\"container-fluid\">
                <!-- BEGIN PAGE HEADER-->   
                <div class=\"row-fluid\">
                    <div class=\"span12\">

                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class=\"page-title\">
                            <!-- FAQ -->
                        </h3> 
                        <ul class=\"breadcrumb\">
                            <li>
                                <a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a>
                                <span class=\"divider\">/</span>
                            </li>
                            <li>
                                <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_dashboard");
        echo "\">All Request</a>
                                <span class=\"divider\">/</span>
                            </li>
                            <li class=\"active\">
                                Organiser Conversation
                            </li>

                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <!-- Acknowledgement message start -->
\t\t\t";
        // line 47
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 52
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 61
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 66
            echo "                        ";
        } else {
            // line 67
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 68
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 72
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 76
            echo "                        ";
        }
        // line 77
        echo "                        <!-- Acknowledgement message end -->  
                <div class=\"row-fluid\">
                    
                    <div class=\"span12\">
                        <!-- BEGIN BLANK PAGE PORTLET-->
                        <div class=\"widget blue\">
                            <div class=\"widget-title\">
                                <h4><i class=\"icon-question-sign\"></i> Request and Comments </h4>

                            </div>
                            <div class=\"widget-body\">
                                <div class=\"row-fluid\">
                                    <div class=\"span6\">
                                        <h4 class=\"title grey\" style=\"font-weight:bold; color:#000;\">Users Request Details</h4>

                                        <!-- BEGIN HORIZONTAL DESCRIPTION LISTS PORTLET-->
                                        <div class=\"widget blue\">
                                            <div class=\"widget-title\">
                                                <h4><i class=\"icon-reorder\"></i>Request Details By <b style=\"text-transform: uppercase;\"> ";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "user"), "html", null, true);
        echo " </b></h4>

                                            </div>
                                            <div class=\"widget-body\">
                                                <div class=\"row-fluid\">
                                                    <div class= \"span12\">
                                                        <h3>Trip Info:</h3>
                                                        <ul class=\" unstyled amounts\">
                                                            <li class=\"text-bold\">Trip Name\t: ";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "name"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">No of Males: ";
        // line 104
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "male"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">No Of Females\t: ";
        // line 105
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "female"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">No Of Travellers\t: ";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "traveler"), "html", null, true);
        echo " </li>
                                                            <li  class=\"text-bold\">Pick-Up Location\t: ";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "pick"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">Drop Location\t: ";
        // line 108
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "down"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">Trip Tentative (in days)\t: ";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "day"), "html", null, true);
        echo "</li>
\t\t\t\t\t\t\t";
        // line 110
        if (($this->getAttribute($this->getContext($context, "request"), "team") == 0)) {
            // line 111
            echo "                                                            <li  class=\"text-bold\">Combine Group\t: No</li>
                                                     ";
        } else {
            // line 113
            echo "                                                            <li  class=\"text-bold\">Combine Group\t: Yes</li>
                                                     ";
        }
        // line 115
        echo "
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class=\"row-fluid\">
                                                    <div class= \"span6\">
                                                        <h3>Activities:</h3>
                                                        <ul class=\" unstyled amounts\">
                                                            ";
        // line 123
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "request"), "activity"));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 124
            echo "                                                            
                                                            <li class=\"text-bold\">";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "activity"), "typeOfActivity"), "html", null, true);
            echo "</li>
                                                     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "
                                                        </ul>
                                                    </div>
                                                    <div class= \"span6\">
                                                        <h3>Location:</h3>
                                                        <ul class=\" unstyled amounts\">

                                                            <li  class=\"text-bold\">Zone\t: ";
        // line 134
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "zone"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">Country\t: ";
        // line 135
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "country"), "html", null, true);
        echo "</li>


                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class=\"row-fluid\">
                                                    <div class= \"span12\">
                                                        <h3>Special Requirements:</h3>
                                                        <ul class=\" unstyled amounts\">
                                                            <li class=\"text-bold\"><p> ";
        // line 146
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "description"), "html", null, true);
        echo " </p></li>


                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>   <!-- END row-fluid -->
                                        </div>
                                        <!-- END HORIZONTAL DESCRIPTION LISTS PORTLET-->
                                    </div>
                                    <div class=\"span6\">
                                        <h4 style=\"font-weight:bold; color:#000;\">Chat History</h4>

                                        <!-- BEGIN CHAT PORTLET-->
                                        <div class=\"widget red\">
                                            <div class=\"widget-title\">
                                                <h4><i class=\"icon-comments-alt\"></i> Conversation</h4>

                                            </div>

                                            <div class=\"widget-body  widgetoverflow\">
                                               
                                                ";
        // line 168
        $context["userid"] = "";
        // line 169
        echo "                                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "comment"));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 170
            echo "                                                <div class=\"timeline-messages\">
                                                ";
            // line 171
            if ((($this->getAttribute($this->getContext($context, "comment"), "user") != null) && ($this->getAttribute($this->getContext($context, "comment"), "role") == "ROLE_USER"))) {
                // line 172
                echo "                                                    <!-- Comment -->
                                                    <div class=\"msg-time-chat\">
                                                        <a class=\"message-img\" href=\"#\"><img alt=\"\" src=\"";
                // line 174
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getAttribute($this->getContext($context, "comment"), "user"), "webpath")), "html", null, true);
                echo "\" class=\"avatar\"></a>
                                                        <div class=\"message-body msg-in\">
                                                            <span class=\"arrow\"></span>
                                                            <div class=\"text\">
                                                                <p class=\"attribution\"><a href=\"#\">";
                // line 178
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "user"), "html", null, true);
                echo "</a> at ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "createdAt")), "html", null, true);
                echo "</p>
                                                                <p>";
                // line 179
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "description"), "html", null, true);
                echo "</p>
                                                                ";
                // line 180
                $context["userid"] = $this->getAttribute($this->getAttribute($this->getContext($context, "comment"), "user"), "id");
                // line 181
                echo "                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /comment -->
                                                                ";
            } elseif ((($this->getAttribute($this->getContext($context, "comment"), "organiser") != null) && ($this->getAttribute($this->getContext($context, "comment"), "role") == "ROLE_ORGANISER"))) {
                // line 186
                echo "                                                    
                                                     
                                                    <!-- Comment -->
                                                    <div class=\"msg-time-chat\">
                                                        <a class=\"message-img\" href=\"#\"><img alt=\"\" src=\"";
                // line 190
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getAttribute($this->getContext($context, "comment"), "organiser"), "webpath")), "html", null, true);
                echo "\" class=\"avatar\"></a>
                                                        <div class=\"message-body msg-out\">
                                                            <span class=\"arrow\"></span>
                                                            <div class=\"text\">
                                                                <p class=\"attribution\"> <a href=\"#\">";
                // line 194
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "organiser"), "html", null, true);
                echo "</a> at ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "createdAt")), "html", null, true);
                echo "</p>
                                                                <p>";
                // line 195
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "comment"), "description"), "html", null, true);
                echo "</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                    <!-- /comment -->
                                                    
                                  
                                                                ";
            }
            // line 204
            echo "

                                                </div>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 208
        echo "


                                            </div>
                                            <div class=\"form-actions\" style=\"margin-left:50px;\">
                                                ";
        // line 217
        echo "                                                <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_create_response", array("id" => $this->getAttribute($this->getContext($context, "request"), "id"), "userid" => $this->getContext($context, "userid"))), "html", null, true);
        echo "\" class=\"btn btn-small btn-danger\"><i class=\"icon-check\"></i>  New Quatation</a>
                                                <a href=\"\" class=\"btn btn-small btn-success\"><i class=\"icon-check\"></i>  Close Deal</a>
                                                <a href=\"";
        // line 219
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_dashboard");
        echo "\" class=\"btn btn-small btn-info\"><i class=\"icon-ban-circle\"></i> Cancel</a>
                                            </div>

                                        </div>
                                       
                                        <!-- END CHAT PORTLET-->
                                        <div class=\"form-actions\" style=\"margin-left:50px;\">
                                       ";
        // line 226
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "quatations"));
        foreach ($context['_seq'] as $context["_key"] => $context["quatations"]) {
            echo " <!-- Close form  organiser -->
                                        <form action=\"";
            // line 227
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_reply_open_request", array("id" => $this->getAttribute($this->getContext($context, "request"), "id"))), "html", null, true);
            echo "\" method=\"POST\" >       
                                        <div id=\"collapse_2\" class=\"accordion-body collapse in\">
                      <div class=\"\">
                          
                        <div class=\"well\">
                          <h3>Response Details:-</h3>
                          <hr> 
                                        
                                        
                          <div class =\"row-fluid\">
                            <div class=\"span8\">
                              <div class=\"form-group\">
                                  <input type=\"hidden\" name=\"userid\" value=\"";
            // line 239
            echo twig_escape_filter($this->env, $this->getContext($context, "userid"), "html", null, true);
            echo "\">
                                <label >Any particular Date or Month </label>
                                
                                <input type=\"text\" value=\"";
            // line 242
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "quatations"), "dateMin"), "d-m-Y"), "html", null, true);
            echo "\"  name='dateMin' class=\"datepicker1 span5\" data-date-format=\"mm-dd-yyyy\" >
                                To
                                
                                <input type=\"text\" value=\"";
            // line 245
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "quatations"), "dateMax"), "d-m-Y"), "html", null, true);
            echo "\" name='dateMax' class=\"datepicker2 span5\" data-date-format=\"mm-dd-yyyy\" >
                              </div>
                            </div>
                               </div>
                          <div class =\"row-fluid\">
                            <div class=\"span4\">
                              <div class=\"control-group\">
                                <label class=\"control-label\" >Pick-Up Point </label>
                               <input class='input-block-level' name='pick' type=\"text\" value=\"";
            // line 253
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "quatations"), "pick"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                            <div class=\"span4\">
                              <div class=\"control-group\">
                                <label class=\"control-label\" >Drop off point </label>
                               <input class='input-block-level'type=\"text\" name='down' value=\"";
            // line 259
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "quatations"), "down"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                          </div>
                          <div class=\"row-fluid\">
                            <div class=\"span4\">
                              <div class=\"control-group\">
                                  <label class=\"control-label\" >Total Estimate</label>
                                <input class='input-block-level' type=\"text\" name='totalCost'  value=\"";
            // line 267
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "quatations"), "totalCost"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                            <div class=\"span4\">
                              <div class=\"form-group\">
                                <label class=\"control-label\">Cost include</label>
                               <input class='input-block-level'type=\"text\" name='includeCost' value=\"";
            // line 273
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "quatations"), "includeCost"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                            <div class=\"span4\">
                              <div class=\"form-group\">
                                <label class=\"control-label\">Cost Exclude</label>
                               <input class='input-block-level' type=\"text\" name='excludeCost' value=\"";
            // line 279
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "quatations"), "excludeCost"), "html", null, true);
            echo "\" >
                              </div>
                            </div>
                                  </div>
                          <div class=\"row-fluid\">
                             
                                  
                            <div class=\"span5\">
                              <div class=\"control-group\">
                                <label class=\"input-block-level\"> Short itinerary and Details: </label>
                                <div class=\"controls\">
                                    <textarea name='description'> ";
            // line 290
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "quatations"), "description"), "html", null, true);
            echo "</textarea>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        </div>
                   
                        <div class=\"form-actions\">
                          <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">
                        </div>
</div></form>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quatations'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 304
        echo "                                        <!-- close form final deal -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END BLANK PAGE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
            </div>
            <!-- END PAGE -->  
        </div>
        <!-- END CONTAINER -->

</body>
<!-- END BODY -->
";
    }

    // line 324
    public function block_javascripts($context, array $blocks = array())
    {
        // line 325
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script type=\"text/javascript\">
\t\t\$(document).ready(function() {
\t\t\$('.multipl').multiselect();
\t\t});
\t</script> 
<script>
\t\t\$('.datepicker1').datepicker({
\t\t\tdateFormat: 'mm-dd-Y',
                        setDate :''
\t\t\t
                        
\t\t})
\t\t</script> 
<script>
\t\t\$('.datepicker2').datepicker({
\t\t\tdateFormat: 'mm-dd-Y',
                        setDate : '' 
                     
\t\t\t
                        
\t\t})
\t\t</script> 

";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:viewOpenRequestOrganiser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  551 => 325,  548 => 324,  526 => 304,  506 => 290,  492 => 279,  483 => 273,  474 => 267,  463 => 259,  454 => 253,  443 => 245,  437 => 242,  431 => 239,  416 => 227,  410 => 226,  400 => 219,  394 => 217,  387 => 208,  378 => 204,  366 => 195,  360 => 194,  353 => 190,  347 => 186,  340 => 181,  338 => 180,  334 => 179,  328 => 178,  321 => 174,  317 => 172,  315 => 171,  312 => 170,  307 => 169,  305 => 168,  280 => 146,  266 => 135,  262 => 134,  253 => 127,  245 => 125,  242 => 124,  238 => 123,  228 => 115,  224 => 113,  220 => 111,  218 => 110,  214 => 109,  210 => 108,  206 => 107,  202 => 106,  198 => 105,  194 => 104,  190 => 103,  179 => 95,  159 => 77,  156 => 76,  149 => 72,  143 => 68,  140 => 67,  137 => 66,  134 => 65,  124 => 61,  113 => 56,  103 => 52,  93 => 47,  76 => 33,  69 => 29,  49 => 11,  46 => 10,  40 => 7,  34 => 4,  31 => 3,);
    }
}
