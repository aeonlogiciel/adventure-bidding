<?php

/* AdventureBiddingBundle:User:payment.html.twig */
class __TwigTemplate_dbbf426214392b1812f0beee3c975c354cd0f4eda2ee18b831fcb7aa634a82b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::userbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::userbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System | User Profile";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

   <!-- BEGIN CONTAINER -->
   <div id=\"container\" class=\"row-fluid\">

      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                     Change Password
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"index.html\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           Change Password
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
             <div class=\"row-fluid\">
        <div class=\"span12\"> 
          <!-- BEGIN BLANK PAGE PORTLET-->
          <div class=\"widget red\">
            <div class=\"widget-title\">
              <h4><i class=\"icon-edit\"></i> Order Summary </h4>
            </div>
            <div class=\"widget-body\"> </div>
          </div>
          <!-- END BLANK PAGE PORTLET--> 
        </div>
      </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
                <div class=\"row-fluid\">
                <div class=\"span12\">
                    <!-- Acknowledgement message start -->
\t\t\t";
        // line 60
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 65
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 74
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 79
            echo "                        ";
        } else {
            // line 80
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 81
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 85
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 89
            echo "                        ";
        }
        // line 90
        echo "                        <!-- Acknowledgement message end -->  
                    <!-- BEGIN ACCORDION PORTLET-->
                    <div class=\"widget box purple\">
            <div class=\"widget-title\">
              <h4> <i class=\"icon-reorder\"></i> Payment Options </span> </h4>
            </div>
            <div class=\"widget-body\">
              <form class=\"form-horizontal\" action=\"#\">
              <div id=\"tabsleft\" class=\"tabbable tabs-left\">
              <ul>
                <li><a href=\"#tabsleft-tab1\" data-toggle=\"tab\"><span class=\"strong\">Debit Card</span> </a></li>
                <li><a href=\"#tabsleft-tab2\" data-toggle=\"tab\"><span class=\"strong\">Credit Card</span> </a></a></li>
                <li><a href=\"#tabsleft-tab3\" data-toggle=\"tab\"><span class=\"strong\">Net Banking</span> </a></li>
                <li><a href=\"#tabsleft-tab4\" data-toggle=\"tab\"><span class=\"strong\">Others</span> </a></li>
              </ul>
              <!--<div class=\"progress progress-info progress-striped\">
                                    <div class=\"bar\"></div>
                                </div>-->
              <div class=\"tab-content\">
              <div class=\"tab-pane\" id=\"tabsleft-tab1\">
               \t
                
                
                
                 <div class=\"row-fluid\">
                 
                        <div class=\"span8\">
                
                              <form class=\"form-vertical\" method=\"get\" action=\"#\">
                                <div class=\"row-fluid\">
                                    <div class=\"span5\">
                                        <div class=\"control-group\">
                                        <label class=\"control-label\" >Card Type</label>
                                           
                                        <select class=\"span6 \" data-placeholder=\"Choose a Card\" tabindex=\"1\">
                                        <option value=\"\">Select Card</option>
                                        <option value=\"Master\">Master</option>
                                        <option value=\"Visa\">Visa</option>
                                        <option value=\"Mastero\">Mastero</option>
                                        <option value=\"Category 4\">Category 4</option>
                                    </select>
                                       
                                        </div>
                                    </div>
                                    <div class=\"span7\">
                                        <div class=\"control-group\">
                                            <label class=\"control-label\" >Debit Card Number</label>
                                            
                                                <input type=\"text\" class=\"input-block-level\" placeholder=\"xxxxxxxxxxxxxxxx\"  name=\"cardnumber\">
                                            
                                        </div>
                                    </div>
                                    
                                 </div>
                                <h4>Expiry</h4>
                                 
                                <div class=\"row-fluid\">
                                 
                                    <div class=\"span4\">
                                    
                                        <div class=\"control-group\">
                                          <label class=\"control-label\" >Month</label>
                                        <select class=\"span6 \" data-placeholder=\"Choose a Month\" tabindex=\"1\">
                                        <option value=\"\">Month</option>
                                        <option value=\"Jan\">Jan</option>
                                        <option value=\"Feb\">Feb</option>
                                        <option value=\"Mar\">Mar</option>
                                        <option value=\"Apr\">Apr</option>
                                    </select>
                                        </div>
                                    </div>
                                    <div class=\"span4\">
                                        <div class=\"control-group\">
                                         <label class=\"control-label\" >Year</label>
                                        <select class=\"span6 \" data-placeholder=\"Choose a Year\" tabindex=\"1\">
                                        <option value=\"\">Year</option>
                                        <option value=\"2014\">2014</option>
                                        <option value=\"2015\">2015</option>
                                        <option value=\"2016\">2016</option>
                                        <option value=\"2017\">2017</option>
                                    </select>
                                        </div>
                                    </div>
                                    <div class=\"span4\">
                                        <div class=\"control-group\">
                                            <label class=\"control-label\" >CVV2/CVC2</label>
                                             
                                              <input type=\"text\" class=\"input-block-level\" placeholder=\"xxxxxxxxx\"  name=\"\">
                                           
                                        </div>
                                    </div>
                                  
                                </div>
                                <div class=\"row-fluid\">
                                    <div class=\"span8\">
                                        <div class=\"control-group\">
                                            <!--<label class=\"control-label\" >Name On Card</label>-->
                                           
                                                <input type=\"text\" class=\"input-block-level\" placeholder=\"Name On Card\"  name=\"\">
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                 
\t\t\t\t\t\t\t\t <div class=\"form-actions\">
                                <button type=\"submit\" class=\"btn btn-primary\">Submit</button>
                                <button type=\"button\" class=\"btn btn-default\">Cancel</button>
                                </div> 
                                
                                 
                            </form>
                            </div>
                        
                            
                                   <div class=\"span4\">
                                         <img src=\"img/pay.jpg\" class=\"img-circle\"/>
                                   </div>
                                   
                  
                 </div>
                
                
                
                
                
                
                
            </div>
            <div class=\"tab-pane\" id=\"tabsleft-tab2\">
              <div class=\"row-fluid\">
\t\t\t   <div class=\"span8\">
\t\t\t     <form class=\"form-vertical\" method=\"get\" action=\"#\">
\t\t\t\t   <div class=\"row-fluid\">
                          <div class=\"span6\">
                                        <div class=\"control-group\">
                                            <label class=\"control-label\" >Credit Card Number</label>
                                            
                                                <input type=\"text\" class=\"input-block-level\" placeholder=\"xxxxxxxxxxxxxxxx\"  name=\"cardnumber\">
                                            
                                        </div>
                                    </div>
                                    
                                 </div>
                                <h4>Expiry</h4>
\t\t\t\t\t\t\t\t<div class=\"row-fluid\">
                                 
                                    <div class=\"span4\">
                                    
                                        <div class=\"control-group\">
                                          <label class=\"control-label\" >Month</label>
                                        <select class=\"span6 \" data-placeholder=\"Choose a Month\" tabindex=\"1\">
                                        <option value=\"\">Month</option>
                                        <option value=\"Jan\">Jan</option>
                                        <option value=\"Feb\">Feb</option>
                                        <option value=\"Mar\">Mar</option>
                                        <option value=\"Apr\">Apr</option>
                                    </select>
                                        </div>
                                    </div>
                                    <div class=\"span4\">
                                        <div class=\"control-group\">
                                         <label class=\"control-label\" >Year</label>
                                        <select class=\"span6 \" data-placeholder=\"Choose a Year\" tabindex=\"1\">
                                        <option value=\"\">Year</option>
                                        <option value=\"2014\">2014</option>
                                        <option value=\"2015\">2015</option>
                                        <option value=\"2016\">2016</option>
                                        <option value=\"2017\">2017</option>
                                    </select>
                                        </div>
                                    </div>
                                    <div class=\"span4\">
                                        <div class=\"control-group\">
                                            <label class=\"control-label\" >CVV2/CVC2</label>
                                             
                                              <input type=\"text\" class=\"input-block-level\" placeholder=\"xxxxxxxxx\"  name=\"\">
                                           
                                        </div>
                                    </div>
                                  
                                </div>
\t\t\t\t\t\t\t\t<div class=\"row-fluid\">
                                    <div class=\"span8\">
                                        <div class=\"control-group\">
                                            <!--<label class=\"control-label\" >Name On Card</label>-->
                                           
                                                <input type=\"text\" class=\"input-block-level\" placeholder=\"Name On Card\"  name=\"\">
                                            
                                        </div>
                                    </div>
                                    
                                </div>
\t\t\t\t\t\t\t <div class=\"form-actions\">
                                <button type=\"submit\" class=\"btn btn-primary\">Submit</button>
                                <button type=\"button\" class=\"btn btn-default\">Cancel</button>
                                </div> 
\t\t\t\t  </form>
\t\t\t   
\t\t\t   </div>
\t\t\t   <div class=\"span4\">
\t\t\t             <img src=\"img/pay.jpg\" class=\"img-circle\"/>
\t\t\t   </div>
\t\t\t  </div>
              
            </div>
            <div class=\"tab-pane\" id=\"tabsleft-tab3\">
           
            <div class=\"row-fluid\">
\t\t\t   <div class=\"span8\">
\t\t\t    <form class=\"form-vertical\" method=\"get\" action=\"#\">
\t\t\t     <div class=\"row-fluid\">
                                 
                                    <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                    <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                     <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\"/> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                   <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
               <div class=\"row-fluid\">
                                 
                                    <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                    <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                     <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\"/> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                   <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                </div> 
                  
                    <!--  <form class=\"form-vertical\" method=\"get\" action=\"#\"> -->
\t\t\t\t   
                               
\t\t\t\t\t\t\t\t<div class=\"row-fluid\">
                                 
                                    <div class=\"span10\">
                                    
                                       <div class=\"control-group\">
                                        <label class=\"control-label\" >All Other Banks</label>
                                        <select class=\"span6 \" data-placeholder=\"Choose a Banks\" tabindex=\"1\">
                                        <option value=\"\">All Other Banks</option>
                                        <option value=\"State Bank Of India\">State Bank Of India</option>
                                        <option value=\"Central Bank Of India\">Central Bank Of India</option>
                                        <option value=\"Maharashtra Bank\">Maharashtra Bank</option>
                                        <option value=\"Axis Bank\">Axis Bank</option>
                                    </select>
                                        </div>
                                    </div>
                                    
                                </div>
\t\t\t\t\t\t\t\t
\t\t\t\t <!--  </form>        -->                  
                        <div class=\"form-actions\">
                                <button type=\"submit\" class=\"btn btn-primary\">Submit</button>
                                <button type=\"button\" class=\"btn btn-default\">Cancel</button>
              </div> 
              </form>        
\t\t\t   
\t\t\t   </div>
               
\t\t\t   <div class=\"span4\">
\t\t\t             <img src=\"img/pay.jpg\" class=\"img-circle\"/>
\t\t\t   </div>
\t\t\t  </div>
\t\t         
              
            </div>
            
            <div class=\"tab-pane\" id=\"tabsleft-tab4\">
              <div class=\"row-fluid\">
\t\t\t   <div class=\"span8\">
\t\t\t    <form class=\"form-vertical\" method=\"get\" action=\"#\">
\t\t\t     <div class=\"row-fluid\">
                                 
                                    <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                    <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                     <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\"/> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                   <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t
                                </div>
                                
                                
               <div class=\"row-fluid\">
                                 
                                    <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                    <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                     <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\"/> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                   <div class=\"span3\">
                                    
                                        <div class=\"well\">
                                         <div class=\"control-group\">
               
                <div class=\"controls bankradio\">
                  <label class=\"radio line\">
                    <input type=\"radio\" value=\"\" />
                      <img src=\"img/hdfc.jpg\" /> </label>
                </div>
              </div>
                                        </div>
                                    </div>
                                </div> 
                  <div class=\"form-actions\">
                                <button type=\"submit\" class=\"btn btn-primary\">Submit</button>
                                <button type=\"button\" class=\"btn btn-default\">Cancel</button>
              </div> 
              </form>       
\t\t\t   </div>
               
\t\t\t   <div class=\"span4\">
\t\t\t             <img src=\"img/pay.jpg\" class=\"img-circle\"/>
\t\t\t   </div>
\t\t\t  </div>
              
            </div>
            <!-- <ul class=\"pager wizard\">
              <li class=\"next\"><a href=\"javascript:;\">Finish</a></li>
            </ul> -->
          </div>
        </div>
        </form>
      </div>
    </div>
            </div>

            <!-- END PAGE CONTENT-->         
         </div>

            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   </div>
</body>
<!-- END BODY -->
";
    }

    // line 609
    public function block_javascripts($context, array $blocks = array())
    {
        // line 610
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:User:payment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  690 => 610,  687 => 609,  100 => 60,  653 => 349,  650 => 348,  627 => 327,  612 => 325,  593 => 307,  589 => 305,  575 => 294,  557 => 282,  546 => 274,  537 => 268,  520 => 257,  505 => 245,  486 => 228,  481 => 225,  467 => 218,  460 => 214,  429 => 200,  426 => 199,  411 => 194,  405 => 192,  399 => 190,  397 => 189,  380 => 183,  376 => 181,  212 => 108,  500 => 323,  497 => 322,  456 => 284,  351 => 182,  339 => 179,  335 => 178,  311 => 164,  226 => 119,  417 => 196,  386 => 219,  367 => 208,  320 => 176,  223 => 147,  373 => 147,  370 => 146,  357 => 186,  301 => 167,  289 => 158,  281 => 152,  277 => 154,  504 => 354,  501 => 353,  434 => 293,  412 => 281,  404 => 275,  390 => 254,  354 => 228,  343 => 180,  329 => 174,  295 => 160,  118 => 72,  369 => 241,  313 => 153,  297 => 166,  293 => 165,  287 => 173,  234 => 140,  271 => 147,  259 => 138,  232 => 128,  473 => 219,  470 => 332,  349 => 167,  324 => 205,  160 => 86,  239 => 155,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 288,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 232,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 197,  408 => 193,  403 => 241,  382 => 183,  377 => 216,  372 => 215,  346 => 200,  342 => 163,  299 => 162,  263 => 155,  255 => 152,  250 => 135,  215 => 141,  153 => 82,  503 => 355,  455 => 310,  446 => 305,  436 => 205,  425 => 289,  414 => 195,  406 => 277,  392 => 222,  356 => 230,  345 => 178,  331 => 177,  363 => 178,  358 => 204,  350 => 196,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 153,  274 => 153,  251 => 146,  551 => 325,  548 => 324,  526 => 260,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 240,  394 => 217,  378 => 214,  366 => 174,  353 => 169,  347 => 166,  340 => 197,  338 => 178,  334 => 177,  321 => 172,  317 => 171,  315 => 171,  307 => 169,  245 => 125,  242 => 132,  228 => 138,  206 => 117,  202 => 103,  194 => 100,  113 => 64,  218 => 114,  210 => 115,  186 => 94,  172 => 104,  81 => 38,  192 => 114,  174 => 126,  155 => 74,  23 => 3,  233 => 146,  184 => 94,  137 => 69,  58 => 13,  225 => 144,  213 => 144,  205 => 21,  198 => 117,  104 => 49,  237 => 141,  207 => 141,  195 => 103,  185 => 107,  344 => 190,  336 => 212,  333 => 175,  328 => 188,  325 => 162,  318 => 207,  316 => 205,  308 => 150,  304 => 198,  300 => 185,  296 => 184,  292 => 159,  288 => 166,  284 => 165,  272 => 152,  260 => 141,  256 => 137,  248 => 154,  216 => 79,  200 => 102,  190 => 96,  170 => 91,  126 => 60,  211 => 127,  181 => 100,  129 => 66,  279 => 148,  275 => 147,  265 => 140,  261 => 157,  257 => 147,  222 => 121,  188 => 93,  167 => 124,  178 => 73,  175 => 91,  161 => 80,  150 => 81,  114 => 71,  110 => 65,  90 => 30,  127 => 59,  124 => 72,  97 => 57,  290 => 166,  286 => 142,  280 => 155,  276 => 178,  270 => 146,  266 => 160,  262 => 139,  253 => 136,  249 => 145,  244 => 133,  236 => 150,  231 => 139,  197 => 114,  191 => 99,  180 => 69,  165 => 90,  146 => 78,  65 => 18,  53 => 19,  152 => 91,  148 => 90,  134 => 76,  76 => 33,  70 => 15,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 209,  444 => 303,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 198,  413 => 234,  409 => 132,  407 => 131,  402 => 191,  398 => 129,  393 => 188,  387 => 187,  384 => 121,  381 => 120,  379 => 182,  374 => 116,  368 => 237,  365 => 27,  362 => 26,  360 => 173,  355 => 182,  341 => 177,  337 => 176,  322 => 178,  314 => 172,  312 => 170,  309 => 170,  305 => 163,  298 => 149,  294 => 148,  285 => 163,  283 => 156,  278 => 149,  268 => 141,  264 => 164,  258 => 150,  252 => 146,  247 => 134,  241 => 77,  229 => 120,  220 => 142,  214 => 128,  177 => 93,  169 => 91,  140 => 76,  132 => 67,  128 => 73,  107 => 62,  61 => 20,  273 => 177,  269 => 151,  254 => 92,  243 => 143,  240 => 142,  238 => 154,  235 => 74,  230 => 121,  227 => 182,  224 => 181,  221 => 115,  219 => 115,  217 => 75,  208 => 107,  204 => 109,  179 => 94,  159 => 75,  143 => 71,  135 => 68,  119 => 62,  102 => 46,  71 => 31,  67 => 20,  63 => 19,  59 => 18,  94 => 30,  89 => 28,  85 => 42,  75 => 22,  68 => 28,  56 => 15,  87 => 25,  28 => 3,  93 => 29,  88 => 48,  78 => 18,  27 => 3,  46 => 10,  44 => 12,  31 => 3,  38 => 6,  26 => 5,  24 => 4,  25 => 4,  201 => 102,  196 => 90,  183 => 94,  171 => 69,  166 => 90,  163 => 89,  158 => 94,  156 => 85,  151 => 77,  142 => 78,  138 => 69,  136 => 75,  121 => 66,  117 => 65,  105 => 51,  91 => 26,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 24,  72 => 17,  69 => 29,  47 => 15,  40 => 7,  37 => 10,  22 => 1,  246 => 144,  157 => 63,  145 => 67,  139 => 65,  131 => 74,  123 => 58,  120 => 69,  115 => 49,  111 => 42,  108 => 57,  101 => 47,  98 => 53,  96 => 23,  83 => 24,  74 => 21,  66 => 26,  55 => 17,  52 => 14,  50 => 17,  43 => 14,  41 => 15,  35 => 12,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 100,  193 => 113,  189 => 132,  187 => 97,  182 => 95,  176 => 105,  173 => 92,  168 => 76,  164 => 77,  162 => 96,  154 => 78,  149 => 75,  147 => 80,  144 => 79,  141 => 78,  133 => 61,  130 => 60,  125 => 67,  122 => 57,  116 => 70,  112 => 52,  109 => 63,  106 => 66,  103 => 55,  99 => 32,  95 => 47,  92 => 29,  86 => 28,  82 => 17,  80 => 30,  73 => 29,  64 => 14,  60 => 16,  57 => 19,  54 => 18,  51 => 16,  48 => 13,  45 => 9,  42 => 12,  39 => 13,  36 => 10,  33 => 13,  30 => 2,);
    }
}
