<?php

/* AdventureLoginBundle:Member:index.html.twig */
class __TwigTemplate_26c55c5bc5cd55a84ca8b02af52cfad213a2329f73b1296130919ae961c027e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>

<head>

  <meta charset=\"UTF-8\">

  <title>Adventure Bidding System</title>

    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/login.css"), "html", null, true);
        echo "\" media=\"screen\" type=\"text/css\" />
    <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
</head>

<body>
\t<div id=\"flatix\">
\t\t<header class=\"headertop\">
        \t<h3>Adventure Bidding System</h3>
\t\t\t<span class=\"title\"><strong>Member Sign In</strong></span>
\t\t</header>
       
\t\t<section>
        
        \t<!-- Acknowledgement message start -->
                    ";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                        <div class=\"alert alert-success fade in\">
                              <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                              <h4 class=\"alert-heading\">Success!</h4>
                              <p>
                                  ";
            // line 29
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                              </p>
                          </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "                    ";
        if ((!array_key_exists("error", $context))) {
            // line 34
            echo "                    ";
        } else {
            // line 35
            echo "                    ";
            if ($this->getContext($context, "error")) {
                // line 36
                echo "                        <div class=\"alert alert-danger fade in\">
                              <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                              <h4 class=\"alert-heading\">Error!</h4>
                              <p>
                                  ";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                              </p>
                          </div>
                    ";
            }
            // line 44
            echo "                    ";
        }
        // line 45
        echo "                    <!-- Acknowledgement message end -->
\t\t\t<form class=\"loginform\" action=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
                            <input type=\"text\" name=\"_username\" class=\"maill\" placeholder=\"E-Mail Id\">
                            <input type=\"password\" name=\"_password\" class=\"passwordd\" placeholder=\"Password\">
                            <a href=\"#myModal\" role=\"button\" data-toggle=\"modal\" title=\"Lost your password?\">Lost your password?</a>
                       \t    <input type=\"submit\" class=\"login\" value=\"SIGN IN\">
\t\t\t</form>
\t\t\t<div class=\"or\">
\t\t\t\t<div class=\"facebook\">
\t\t\t\t\t<a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("adventure_login_user_register");
        echo "\" title=\"Sign up\">SignUp as User</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"twitter\">
\t\t\t\t\t<a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("adventure_login_organiser_register");
        echo "\" title=\"Sign up\">SignUp as Organizer</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</section>
\t\t<div class=\"footsec\">Copyright &copy; 2006 - 2015</div>
\t</div>
    

<!--model for forgate password-->
    <div id=\"myModal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-header\">
    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
    <h4 id=\"myModalLabel\">Enter your E-Mail</h4>
    </div>
    <div class=\"modal-body\">
        <form action=\"";
        // line 73
        echo $this->env->getExtension('routing')->getPath("forgot_password_mail");
        echo "\" method=\"post\">
            <input type=\"email\" class=\"span6\" id=\"email\" name=\"email\"  placeholder=\"Enter your Registered email id\" />
            <p>After click on submit, please check your mail</p>
    </div>
    <div class=\"modal-footer\">
    <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>
    <button class=\"btn btn-primary\">Submit</button>
    </div>
    </div>
    
 <!--End model for forgate password-->   
  <script src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

</body>

</html>";
    }

    public function getTemplateName()
    {
        return "AdventureLoginBundle:Member:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 85,  148 => 84,  134 => 73,  115 => 57,  109 => 54,  98 => 46,  95 => 45,  92 => 44,  85 => 40,  79 => 36,  76 => 35,  73 => 34,  70 => 33,  60 => 29,  50 => 24,  34 => 11,  30 => 10,  19 => 1,);
    }
}
