<?php

/* AdventureBiddingBundle:Organiser:viewEventOrganiser.html.twig */
class __TwigTemplate_46cd75f4821badc65ab9cc98f81a53b48103373ffd046e79364024c49536c1d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::organiserbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::organiserbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                    All Events
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           All Events
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
               <div class=\"row-fluid\">
                <div class=\"span12\">
                     <!-- Acknowledgement message start -->
\t\t\t";
        // line 45
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 50
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 59
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 64
            echo "                        ";
        } else {
            // line 65
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 66
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 74
            echo "                        ";
        }
        // line 75
        echo "                        <!-- Acknowledgement message end -->  
                <!-- BEGIN EXAMPLE TABLE widget-->
                
                <div class=\"widget blue\">
                    <div class=\"widget-title\">
                        <h4><i class=\"icon-search\"></i> Filter Your Search</h4>
                         
                    </div>
                    <div class=\"widget-body\">
                         <form class=\"form-inline\" method=\"post\">
                         <div class=\"row-fluid\">
                         
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Filter Search</label>
                          <div class=\"controls\">
                                    <select class=\"span8 \" data-placeholder=\"Choose a Category\" tabindex=\"1\">
                                        <option value=\"\">Select...</option>
                                        <option value=\"Activities\">Activities</option>
                                        <option value=\"Zones\">Zones</option>
                                        <option value=\"Category 3\">Category 5</option>
                                        <option value=\"Category 4\">Category 4</option>
                                    </select>
                                </div>
                          
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Search</label>
                           <div class=\"controls\">
                          <input type=\"text\" class=\"span8\"/>   
                          </div>
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          
                          <label class=\"control-label\"></label>
                          <div class=\"controls\">
                <button type=\"submit\" class=\"btn btn-primary\"><i class=\"icon-ok\"></i> Submit</button>
                         <button type=\"button\" class=\"btn\">Cancel</button>
                
               </div>
                          </div>
                         </div>
                         
                         
                         </div>
                         
                         </form>
                        
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
                <div class=\"row-fluid\">
                    
                    <!--marquee start here-->
                    <div class=\"span12\">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class=\"widget blue\">
                            <div class=\"widget-title\">
                                <h4>Add Event</h4>
                            </div>
                            <div class=\"widget-body\">
                                <div class =\"row-fluid\">  
                               <div class=\"span12\">
                                ";
        // line 144
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "form"), 'form_start', array("attr" => array("class" => "form-vertical")));
        echo "
                                    <div class=\"well\">
                      <h3>Event Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Full Name</label>
                            ";
        // line 152
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "ename"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                            
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Activities </label>
                            ";
        // line 159
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eActivity"), 'widget', array("attr" => array("class" => "input-block-level chzn-select")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eActivity"), 'errors');
        echo "
                          
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Travel Date</label>
                            ";
        // line 166
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDate"), 'widget', array("attr" => array("class" => "datepicker span5")));
        echo "  ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDate"), 'errors');
        echo "
                          
                          </div>
                        </div>
                        
                       
                      </div>
                      <div class=\"row-fluid\">
                      <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Phone No </label>
                            ";
        // line 177
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eContactDetail"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                <div class=\"control-group\">
                            <label class=\"control-label\" >Email </label>
                            ";
        // line 183
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "email"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                <div class=\"control-group\">
                            <label class=\"control-label\" >Duration </label>
                            ";
        // line 189
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "duration"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                          </div>
                        </div>
                    
                        </div>
                       <div class=\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">continent</label>
                            ";
        // line 198
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "continent"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "country"), 'errors');
        echo "
                            
                          </div>
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Country</label>
                            ";
        // line 205
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "country"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "continent"), 'errors');
        echo "
                          </div>
                        </div>
                    <div class=\"span4\">
                        <div class=\"control-group\">
                          <label class=\"input-block-level\"> Description: </label>
                          <div class=\"controls\">
                            
                            ";
        // line 213
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eBrief"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eBrief"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                      </div>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >State</label>
                          ";
        // line 222
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eState"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4", "placeholder" => "Choose an option")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eState"), 'errors');
        echo "
                          </div>
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Region</label>
                            ";
        // line 228
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eRegion"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eRegion"), 'errors');
        echo "
                          </div>
                              
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Destination</label>
                            ";
        // line 235
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDestination"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDestination"), 'errors');
        echo "
                          </div>
                          </div>  
                        </div>
                           <div class =\"row-fluid\">
                          
                               <div class=\"span4\">
                        
                         <div class=\"control-group\">
                                    <label class=\"control-label\">Logo Upload</label>
                                    <div class=\"controls\">
                                        <div data-provides=\"fileupload\" class=\"fileupload fileupload-new\">
                                            <div style=\"width: 200px; height: 150px;\" class=\"fileupload-new thumbnail\">
                                                <img alt=\"\" src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\">
                                            </div>
                                            <div style=\"max-width: 200px; max-height: 150px; line-height: 20px;\" class=\"fileupload-preview fileupload-exists thumbnail\"></div>
                                            <div>
                                               <span class=\"btn btn-file\"><span class=\"fileupload-new\">Select image</span>
                                               <span class=\"fileupload-exists\">Change</span>
                                               ";
        // line 254
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "path"), 'widget', array("attr" => array("class" => "default")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "path"), 'errors');
        echo "</span>
                                                <a data-dismiss=\"fileupload\" class=\"btn fileupload-exists\" href=\"#\">Remove</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                        
                        </div>
                          ";
        // line 275
        echo "                      </div>
                         
                          <div class =\"row-fluid\">
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Difficulty</label>
                            ";
        // line 281
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDifficulty"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eDifficulty"), 'errors');
        echo "
                          </div>
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Zone</label>
                            ";
        // line 287
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eZone"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "eZone"), 'errors');
        echo "
                          </div>
                        </div>
                               <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Budget</label>
                            ";
        // line 293
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "budget"), 'widget', array("attr" => array("class" => "input-block-level ", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "budget"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>             
                     ";
        // line 303
        echo "                            ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'rest');
        echo "
                    <div class=\"form-actions\">
                      <button class=\"btn btn-primary\" id=\"save\" type=\"submit\"> Submit</button>
                      <button type=\"button\" class=\"btn btn-default\">Cancel</button>
                    </div>
                  ";
        // line 308
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "form"), 'form_end');
        echo "      
                              
                   
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
                         <!--marquee End here-->
                </div>
                
                
            </div>

            <!-- END PAGE CONTENT-->         

      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
 <!--model for feedback-->  
      <div id=\"myModal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">Events</h3>
            </div>
            <div class=\"modal-body\">
            <p>Phasellus fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo.
             Sed ut quam at magna porttitor hendrerit. Maecenas quis erat fringilla augue feugiat vulputate a eu 
            sem.Vivamus ut diam at turpis varius tempor. Aliquam dictum sagittis erat, vehicula adipiscing
             diam condimentum id. </p>
            </div>
            <div class=\"modal-footer\">
            <button class=\"btn btn-primary\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>
           
            </div>
      </div>
               
 <!--end model for feedback-->    
    
</body>
<!-- END BODY -->
";
    }

    // line 353
    public function block_javascripts($context, array $blocks = array())
    {
        // line 354
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script>
\t\t\$(document).ready(function() {
\t\t\$('.multipl').multiselect();
\t\t})(jQuery);
                \$('#example-single-selected').multiselect()(jQuery);
    </script> 
    <script>
    \$('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        startDate: '-3d'
    })(jQuery);
    </script> 

";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:viewEventOrganiser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  504 => 354,  501 => 353,  434 => 293,  412 => 281,  404 => 275,  390 => 254,  354 => 228,  343 => 222,  329 => 213,  295 => 201,  118 => 72,  369 => 241,  313 => 187,  297 => 177,  293 => 176,  287 => 173,  234 => 153,  271 => 145,  259 => 138,  232 => 121,  473 => 333,  470 => 332,  349 => 220,  324 => 205,  160 => 86,  239 => 155,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 318,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 286,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 238,  408 => 232,  403 => 230,  382 => 218,  377 => 216,  372 => 242,  346 => 200,  342 => 198,  299 => 171,  263 => 155,  255 => 152,  250 => 150,  215 => 141,  153 => 82,  503 => 355,  455 => 310,  446 => 305,  436 => 251,  425 => 289,  414 => 283,  406 => 277,  392 => 222,  356 => 230,  345 => 224,  331 => 215,  363 => 178,  358 => 147,  350 => 201,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 171,  274 => 177,  251 => 146,  551 => 325,  548 => 324,  526 => 304,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 219,  394 => 217,  378 => 204,  366 => 235,  353 => 190,  347 => 25,  340 => 197,  338 => 178,  334 => 177,  321 => 174,  317 => 172,  315 => 171,  307 => 169,  245 => 125,  242 => 124,  228 => 144,  206 => 117,  202 => 106,  194 => 110,  113 => 59,  218 => 110,  210 => 140,  186 => 94,  172 => 124,  81 => 38,  192 => 114,  174 => 93,  155 => 59,  23 => 1,  233 => 146,  184 => 94,  137 => 69,  58 => 11,  225 => 144,  213 => 78,  205 => 21,  198 => 117,  104 => 49,  237 => 170,  207 => 120,  195 => 145,  185 => 104,  344 => 190,  336 => 212,  333 => 157,  328 => 188,  325 => 25,  318 => 207,  316 => 205,  308 => 175,  304 => 198,  300 => 185,  296 => 184,  292 => 189,  288 => 166,  284 => 165,  272 => 178,  260 => 163,  256 => 174,  248 => 149,  216 => 79,  200 => 102,  190 => 96,  170 => 108,  126 => 74,  211 => 127,  181 => 93,  129 => 63,  279 => 148,  275 => 147,  265 => 171,  261 => 157,  257 => 141,  222 => 128,  188 => 93,  167 => 123,  178 => 73,  175 => 91,  161 => 90,  150 => 85,  114 => 71,  110 => 53,  90 => 30,  127 => 59,  124 => 64,  97 => 57,  290 => 166,  286 => 165,  280 => 146,  276 => 168,  270 => 161,  266 => 160,  262 => 134,  253 => 149,  249 => 137,  244 => 156,  236 => 152,  231 => 145,  197 => 101,  191 => 99,  180 => 69,  165 => 91,  146 => 69,  65 => 21,  53 => 19,  152 => 83,  148 => 81,  134 => 76,  76 => 36,  70 => 30,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 308,  444 => 303,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 287,  413 => 234,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 220,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 237,  365 => 111,  362 => 110,  360 => 225,  355 => 223,  341 => 189,  337 => 103,  322 => 185,  314 => 172,  312 => 170,  309 => 175,  305 => 174,  298 => 168,  294 => 169,  285 => 171,  283 => 183,  278 => 149,  268 => 172,  264 => 164,  258 => 166,  252 => 160,  247 => 157,  241 => 77,  229 => 112,  220 => 142,  214 => 128,  177 => 95,  169 => 91,  140 => 70,  132 => 64,  128 => 58,  107 => 62,  61 => 20,  273 => 164,  269 => 166,  254 => 92,  243 => 127,  240 => 171,  238 => 154,  235 => 74,  230 => 82,  227 => 182,  224 => 181,  221 => 115,  219 => 123,  217 => 75,  208 => 107,  204 => 109,  179 => 126,  159 => 80,  143 => 71,  135 => 65,  119 => 59,  102 => 46,  71 => 21,  67 => 26,  63 => 24,  59 => 21,  94 => 30,  89 => 28,  85 => 33,  75 => 22,  68 => 28,  56 => 20,  87 => 25,  28 => 3,  93 => 50,  88 => 45,  78 => 18,  27 => 7,  46 => 10,  44 => 11,  31 => 3,  38 => 6,  26 => 5,  24 => 4,  25 => 3,  201 => 112,  196 => 90,  183 => 100,  171 => 69,  166 => 67,  163 => 82,  158 => 78,  156 => 79,  151 => 74,  142 => 78,  138 => 66,  136 => 70,  121 => 59,  117 => 66,  105 => 51,  91 => 43,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 32,  72 => 17,  69 => 33,  47 => 17,  40 => 7,  37 => 10,  22 => 1,  246 => 159,  157 => 63,  145 => 67,  139 => 65,  131 => 59,  123 => 58,  120 => 44,  115 => 49,  111 => 48,  108 => 54,  101 => 47,  98 => 50,  96 => 23,  83 => 40,  74 => 30,  66 => 26,  55 => 17,  52 => 17,  50 => 17,  43 => 16,  41 => 15,  35 => 14,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 134,  193 => 131,  189 => 129,  187 => 101,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 91,  154 => 75,  149 => 75,  147 => 68,  144 => 70,  141 => 76,  133 => 61,  130 => 75,  125 => 57,  122 => 73,  116 => 71,  112 => 52,  109 => 43,  106 => 66,  103 => 55,  99 => 45,  95 => 42,  92 => 29,  86 => 28,  82 => 33,  80 => 30,  73 => 29,  64 => 10,  60 => 15,  57 => 19,  54 => 18,  51 => 18,  48 => 10,  45 => 9,  42 => 12,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
