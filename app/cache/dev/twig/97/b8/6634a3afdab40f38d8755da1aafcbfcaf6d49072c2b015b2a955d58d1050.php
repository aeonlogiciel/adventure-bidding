<?php

/* AdventureBiddingBundle:Organiser:editProfileOrganiser.html.twig */
class __TwigTemplate_97b86634a3afdab40f38d8755da1aafcbfcaf6d49072c2b015b2a955d58d1050 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::organiserbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::organiserbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<style>
    .btn-circle.btn-xl {
  width: auto !important;
  height: auto !important;
  padding: 10px 16px !important;
  font-size: 24px ;
  line-height: 1.33 !important;
  border-radius: 35px !important;
}
    </style>
";
    }

    // line 17
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 20
    public function block_body($context, array $blocks = array())
    {
        // line 21
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">
      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                   
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                     
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"#\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"\">
                           My Profile <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           Edit Profile
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
      <div class=\"row-fluid\">
        <div class=\"span12\"> 
            <!-- Acknowledgement message start -->
\t\t\t";
        // line 57
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 62
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 71
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 76
            echo "                        ";
        } else {
            // line 77
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 78
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 82
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 86
            echo "                        ";
        }
        // line 87
        echo "                        <!-- Acknowledgement message end -->  
          <!-- BEGIN ACCORDION PORTLET-->
          <div class=\"widget blue\">
            <div class=\"widget-title\">
              <h4><i class=\"icon-user\"></i>Edit Organizer Inforation</h4>
            </div>
            <div class=\"widget-body\">
              <div class =\"row-fluid\">
                <div class=\"span12\">
                  ";
        // line 96
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_start', array("attr" => array("class" => "form-vertical")));
        echo "
                    <div class=\"well\">
                      <h3>Personal Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Full Name</label>
                            ";
        // line 104
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "name"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Email </label>
                            ";
        // line 110
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Zones</label>
                            <div class=\"controls\">
                                ";
        // line 117
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zone"), 'widget', array("attr" => array("class" => "input-block-level chzn-select", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zone"), 'errors');
        echo "
                            </div>
                          </div>
                        </div>
                        
                       
                      </div>
                      <div class=\"row-fluid\">
                      <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Phone No </label>
                            ";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "phone"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "phone"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                        
                         <div class=\"control-group\">
                                    <label class=\"control-label\">Image Upload</label>
                                    <div class=\"controls\">
                                        <div data-provides=\"fileupload\" class=\"fileupload fileupload-new\">
                                            <div style=\"width: 200px; height: 150px;\" class=\"fileupload-new thumbnail\">
                                                <img alt=\"\" src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\">
                                            </div>
                                            <div style=\"max-width: 200px; max-height: 150px; line-height: 20px;\" class=\"fileupload-preview fileupload-exists thumbnail\"></div>
                                            <div>
                                               <span class=\"btn btn-file\"><span class=\"fileupload-new\">Select image</span>
                                               <span class=\"fileupload-exists\">Change</span>
                                               ";
        // line 144
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "applicantphoto"), 'widget', array("attr" => array("class" => "default")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "applicantphoto"), 'errors');
        echo "</span>
                                                <a data-dismiss=\"fileupload\" class=\"btn fileupload-exists\" href=\"#\">Remove</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                        
                        </div>
                      
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Country</label>
                            ";
        // line 157
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'widget', array("attr" => array("class" => "input-block-level chzn-select", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'errors');
        echo "
                            
                          </div>
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Zip Code</label>
                            ";
        // line 164
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zip"), 'widget', array("attr" => array("class" => "input-block-level")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zip"), 'errors');
        echo "
                          </div>
                        </div>
                    <div class=\"span4\">
                        <div class=\"control-group\">
                          <label class=\"input-block-level\"> Address: </label>
                          <div class=\"controls\">
                            ";
        // line 171
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "address"), 'widget', array("attr" => array("class" => "span12")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "address"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                      </div>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >State</label>
                            <input type=\"text\" class=\"input-block-level\" placeholder=\"Enter State Name\"  name=\"state\">
                          </div>
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >City</label>
                            <input type=\"text\" class=\"input-block-level\" placeholder=\"Enter City Name\"  name=\"city\">
                          </div>
                        </div>
                          <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >About Me</label>
                            <textarea class=\"input-block-level\" row=\"4\" cols=\"10\"  name=\"about\"></textarea>
                          </div>
                        </div>
                          
                      </div>
                    </div>
                    <div class=\"well\">
                      <h3>Activities Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                           <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Activities</label>
                            ";
        // line 205
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "activity"), 'widget', array("attr" => array("class" => "input-block-level chzn-select", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "activity"), 'errors');
        echo "
                           
                          </div>
                        </div>
                       <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >State</label>
                            ";
        // line 212
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "state"), 'widget', array("attr" => array("class" => "input-block-level chzn-select", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "state"), 'errors');
        echo "
                          </div>
                        </div>
                        
                       
                      
                    </div>
                    
                    ";
        // line 220
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "Form"), 'rest');
        echo "
                    <div class=\"form-actions\">
                      <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">
                      <a href=\"";
        // line 223
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_edit_profile");
        echo "\"><button type=\"button\" class=\"btn btn-default\">Cancel</button></a>
                    </div>
                  ";
        // line 225
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_end');
        echo "
                </div>
                                <div class=\"well\">
                      <h3>User Trails </h3>
                      <hr>
                      <div class =\"row-fluid\">

<div role=\"tabpanel\">

  <!-- Nav tabs -->
  <div class=\"row-fluid\" role=\"tablist\">
    <button role=\"\" class=\"btn btn-success btn-circle btn-xl\"><a href=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Trails</a></button>
    <button role=\"\" class=\"btn btn-info btn-circle btn-xl\"><a href=\"#profile\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Travelogues</a></button>
    <button role=\"\" class=\"btn btn-warning btn-circle btn-xl\"><a href=\"#messages\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Photologues</a></button>
    
  </div>

  <!-- Tab panes -->
  <div class=\"tab-content\">
    <div role=\"tabpanel\" class=\"tab-pane fade in active\" id=\"home\">
                    <div class=\"form-actions\">
                    <div class=\"control-group\">
                      <label class=\"control-label labelmargin\" >Trail Comming Soon</label>
                    </div>
                    </div>
    </div>
    <div role=\"tabpanel\" class=\"tab-pane fade\" id=\"profile\">
                    <div class=\"form-actions\" >
                    <div class=\"control-group\">
                      <label class=\"control-label labelmargin\" >Travelogues Comming Soon</label>
                    </div>
                    </div>
    </div>
    <div role=\"tabpanel\" class=\"tab-pane fade\" id=\"messages\">
                    <div class=\"form-actions\" >
                    <div class=\"control-group\">
                      <label class=\"control-label labelmargin\" >Photologues Comming Soon</label>
                    </div>
                    </div>
    </div>
  
  </div>

</div>

                    
                      
                     
                        
                     
                        
                       
                      
                    </div>
                    <div class=\"form-actions\">
                      <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">
                      <a href=\"\"><button type=\"button\" class=\"btn btn-default\">Cancel</button></a>
                    </div>
                </div>
                </div>

                  <div class=\"well\">
                      <h3>Add You Trails/Trip </h3>
                      <hr>
                      <div class =\"row-fluid\">
                           <div class=\"span4\">
                          <div class=\"form-group\">
                            
                                                       
                              
                               
                        </div>
                        
                    </div>
                    <div class=\"form-actions\">
                      <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">
                      <a href=\"\"><button type=\"button\" class=\"btn btn-default\">Cancel</button></a>
                    </div>
                          
                </div>
                
                </div>
              
              
             
              
            </div>
            <!-- END ACCORDION PORTLET--> 
          </div>
        </div>
        
        <!-- END PAGE CONTENT--> 
      </div>
      
      <!-- END PAGE CONTENT--> 
      
    </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
  
</body>
<!-- END BODY -->
";
    }

    // line 332
    public function block_javascripts($context, array $blocks = array())
    {
        // line 333
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>
<script type=\"text/javascript\">
\t\t\$(document).ready(function() {
\t\t\$('.multipl').multiselect();
\t\t});
   
}    
</script>
<script>

\$('#myTab a').click(function (e) {
  e.preventDefault()
  \$(this).tab('show')
})

</script>
";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:editProfileOrganiser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  473 => 333,  470 => 332,  349 => 220,  324 => 205,  160 => 86,  239 => 147,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 318,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 286,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 238,  408 => 232,  403 => 230,  382 => 218,  377 => 216,  372 => 214,  346 => 200,  342 => 198,  299 => 171,  263 => 155,  255 => 152,  250 => 150,  215 => 141,  153 => 82,  503 => 355,  455 => 310,  446 => 305,  436 => 251,  425 => 289,  414 => 283,  406 => 277,  392 => 222,  356 => 230,  345 => 224,  331 => 215,  363 => 178,  358 => 147,  350 => 201,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 164,  274 => 161,  251 => 146,  551 => 325,  548 => 324,  526 => 304,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 219,  394 => 217,  378 => 204,  366 => 179,  353 => 190,  347 => 25,  340 => 197,  338 => 178,  334 => 177,  321 => 174,  317 => 172,  315 => 171,  307 => 169,  245 => 125,  242 => 124,  228 => 144,  206 => 117,  202 => 106,  194 => 110,  113 => 56,  218 => 110,  210 => 140,  186 => 94,  172 => 124,  81 => 34,  192 => 114,  174 => 96,  155 => 59,  23 => 1,  233 => 146,  184 => 76,  137 => 80,  58 => 11,  225 => 126,  213 => 78,  205 => 21,  198 => 117,  104 => 49,  237 => 170,  207 => 120,  195 => 145,  185 => 104,  344 => 190,  336 => 212,  333 => 157,  328 => 188,  325 => 25,  318 => 207,  316 => 189,  308 => 187,  304 => 173,  300 => 185,  296 => 184,  292 => 183,  288 => 166,  284 => 165,  272 => 178,  260 => 153,  256 => 174,  248 => 149,  216 => 79,  200 => 102,  190 => 103,  170 => 108,  126 => 75,  211 => 121,  181 => 96,  129 => 66,  279 => 148,  275 => 147,  265 => 171,  261 => 157,  257 => 141,  222 => 128,  188 => 93,  167 => 123,  178 => 73,  175 => 125,  161 => 91,  150 => 85,  114 => 54,  110 => 53,  90 => 30,  127 => 59,  124 => 58,  97 => 57,  290 => 166,  286 => 165,  280 => 146,  276 => 179,  270 => 161,  266 => 160,  262 => 134,  253 => 149,  249 => 137,  244 => 171,  236 => 133,  231 => 145,  197 => 103,  191 => 99,  180 => 69,  165 => 122,  146 => 69,  65 => 21,  53 => 19,  152 => 60,  148 => 78,  134 => 65,  76 => 33,  70 => 38,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 234,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 220,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 237,  365 => 111,  362 => 110,  360 => 225,  355 => 223,  341 => 189,  337 => 103,  322 => 185,  314 => 172,  312 => 170,  309 => 175,  305 => 168,  298 => 168,  294 => 169,  285 => 171,  283 => 88,  278 => 162,  268 => 172,  264 => 176,  258 => 140,  252 => 151,  247 => 78,  241 => 77,  229 => 112,  220 => 142,  214 => 109,  177 => 95,  169 => 95,  140 => 67,  132 => 50,  128 => 71,  107 => 62,  61 => 20,  273 => 164,  269 => 159,  254 => 92,  243 => 144,  240 => 171,  238 => 154,  235 => 74,  230 => 82,  227 => 182,  224 => 181,  221 => 109,  219 => 123,  217 => 75,  208 => 22,  204 => 109,  179 => 126,  159 => 77,  143 => 68,  135 => 63,  119 => 50,  102 => 46,  71 => 21,  67 => 26,  63 => 24,  59 => 21,  94 => 30,  89 => 28,  85 => 33,  75 => 22,  68 => 28,  56 => 20,  87 => 25,  28 => 3,  93 => 45,  88 => 38,  78 => 18,  27 => 7,  46 => 10,  44 => 11,  31 => 3,  38 => 6,  26 => 5,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 127,  171 => 69,  166 => 67,  163 => 87,  158 => 78,  156 => 87,  151 => 74,  142 => 67,  138 => 75,  136 => 70,  121 => 59,  117 => 66,  105 => 51,  91 => 21,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 32,  72 => 17,  69 => 33,  47 => 17,  40 => 7,  37 => 10,  22 => 1,  246 => 133,  157 => 63,  145 => 67,  139 => 65,  131 => 62,  123 => 58,  120 => 44,  115 => 49,  111 => 48,  108 => 57,  101 => 34,  98 => 33,  96 => 23,  83 => 40,  74 => 30,  66 => 16,  55 => 17,  52 => 17,  50 => 17,  43 => 16,  41 => 15,  35 => 14,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 134,  193 => 131,  189 => 129,  187 => 128,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 91,  154 => 78,  149 => 70,  147 => 78,  144 => 77,  141 => 76,  133 => 61,  130 => 60,  125 => 60,  122 => 55,  116 => 71,  112 => 51,  109 => 43,  106 => 66,  103 => 49,  99 => 45,  95 => 42,  92 => 29,  86 => 28,  82 => 33,  80 => 30,  73 => 29,  64 => 10,  60 => 15,  57 => 21,  54 => 14,  51 => 18,  48 => 13,  45 => 10,  42 => 12,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
