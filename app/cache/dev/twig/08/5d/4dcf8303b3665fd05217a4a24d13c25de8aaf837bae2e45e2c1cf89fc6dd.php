<?php

/* AdventureBiddingBundle:Organiser:viewCloseRequest.html.twig */
class __TwigTemplate_085d4dcf8303b3665fd05217a4a24d13c25de8aaf837bae2e45e2c1cf89fc6dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::organiserbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::organiserbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "<!-- BEGIN BODY -->
<body class=\"fixed-top\">

    <!-- BEGIN CONTAINER -->
    <div id=\"container\" class=\"row-fluid\">

        <!-- BEGIN PAGE -->  
        <div id=\"main-content\">
            <!-- BEGIN PAGE CONTAINER-->
            <div class=\"container-fluid\">
                <!-- BEGIN PAGE HEADER-->   
                <div class=\"row-fluid\">
                    <div class=\"span12\">

                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class=\"page-title\">
                            User-Organiser Conformation
                        </h3> 
                        <ul class=\"breadcrumb\">
                            <li>
                                <a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a>
                                <span class=\"divider\">/</span>
                            </li>
                            <li>
                                <a href=\"\">Request List</a>
                                <span class=\"divider\">/</span>
                            </li>
                            <li class=\"active\">
                                User-Organiser Conformation
                            </li>

                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class=\"row-fluid\">
                    <div class=\"span12\">
                        <!-- BEGIN BLANK PAGE PORTLET-->
                        <div class=\"widget blue\">
                            <div class=\"widget-title\">
                                <h4><i class=\"icon-question-sign\"></i> Conformation </h4>

                            </div>
                            <div class=\"widget-body\">
                                <div class=\"row-fluid\">
                                    <div class=\"span6\">
                                        <h4 class=\"title grey\" style=\"font-weight:bold; color:#000;\">User Request Details</h4>

                                        <!-- BEGIN HORIZONTAL DESCRIPTION LISTS PORTLET-->
                                        <div class=\"widget blue\">
                                            <div class=\"widget-title\">
                                                <h4><i class=\"icon-reorder\"></i> User’s Request Details </h4>

                                            </div>
                                            <div class=\"widget-body\">
                                                <div class=\"row-fluid\">
                                                    <div class= \"span12\">
                                                        <h3>Trip Info:</h3>
                                                        <ul class=\" unstyled amounts\">
                                                            <li class=\"text-bold\">Trip Name\t: ";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "name"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">No of Males         : ";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "male"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">No Of Females\t: ";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "female"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">No Of Travellers\t: ";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "traveler"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">Pick-Up Location\t: ";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "pick"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">Drop Location\t: ";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "down"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">Trip Tentative (in days)\t: ";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "day"), "html", null, true);
        echo "</li>
                                                     ";
        // line 78
        if (($this->getAttribute($this->getContext($context, "request"), "team") == 0)) {
            // line 79
            echo "                                                            <li  class=\"text-bold\">Combine Group\t: No</li>
                                                     ";
        } else {
            // line 81
            echo "                                                            <li  class=\"text-bold\">Combine Group\t: Yes</li>
                                                     ";
        }
        // line 83
        echo "                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class=\"row-fluid\">
                                                    <div class= \"span6\">
                                                        <h3>Activities:</h3>
                                                        <ul class=\" unstyled amounts\">
                                                     ";
        // line 90
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "request"));
        foreach ($context['_seq'] as $context["_key"] => $context["request"]) {
            // line 91
            echo "                                                            <li class=\"text-bold\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "typeOfActivity"), "html", null, true);
            echo "</li>
                                                     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['request'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "
                                                        </ul>
                                                    </div>
                                                    <div class= \"span6\">
                                                        <h3>Location:</h3>
                                                        <ul class=\" unstyled amounts\">

                                                            <li  class=\"text-bold\">Zone\t: ";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "zone"), "html", null, true);
        echo "</li>
                                                            <li  class=\"text-bold\">Country\t: ";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "country"), "html", null, true);
        echo "</li>


                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class=\"row-fluid\">
                                                    <div class= \"span12\">
                                                        <h3>Special Requirements:</h3>
                                                        <ul class=\" unstyled amounts\">
                                                            <li class=\"text-bold\"><p> ";
        // line 112
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "description"), "html", null, true);
        echo " </p></li>


                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>   <!-- END row-fluid -->
                                        </div>
                                        <!-- END HORIZONTAL DESCRIPTION LISTS PORTLET-->
                                    </div>
                                    <div class=\"span6\"> 
                                        <h4 class=\"title grey\" style=\"font-weight:bold; color:#000;\">Organizer Response Details</h4>
                                        <!-- BEGIN HORIZONTAL DESCRIPTION LISTS PORTLET-->
                                        <div class=\"widget blue\">
                                            <div class=\"widget-title\">
                                                <h4><i class=\"icon-reorder\"></i> Organizer Response Details </h4>
                                            </div>
                                            <div class=\"widget-body\">
                                                <div class=\"row-fluid\">
                                                    <div class= \"span12\">
                                                        <h3>Trip Response:</h3>
                                                        <ul class=\" unstyled amounts\">
                                                            <li class=\"text-bold\">AnyParticular Months\t: From: 02-10-12  &amp; To: 08-12-12</li>
                                                            <li  class=\"text-bold\">Cost Include: Rs 1500</li>
                                                            <li  class=\"text-bold\">Cost Exclude : Rs 500 </li>
                                                            <li  class=\"text-bold\">Quotations\t: Rs 15000 </li>
                                                            <li  class=\"text-bold\">Pick-Up Location\t: Pune</li>
                                                            <li  class=\"text-bold\">Drop Location\t: Shivaji Nagar</li>
                                                            <li  class=\"text-bold\">Short Details\t: 
                                                                <p>
                                                                    The trip is about Rafting.
                                                                </p>
                                                            </li>



                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class=\"row-fluid\">
                                                    <!-- <div class= \"span6\">
                                                    <h3>Activities:</h3>
                                                    <ul class=\" unstyled amounts\">
                                                            <li class=\"text-bold\">Land\t: Rock climbing, Camping</li>
                                                        <li  class=\"text-bold\">Water: Rafting,Sailing</li>
                                                        <li  class=\"text-bold\">Aero\t: Paramotoring,Hang Gliding</li>
                                                        <li  class=\"text-bold\">Off-Beat  : Cycling,Caving </li>
                                                        <li  class=\"text-bold\">Location  : Goa , Shimla,Srinagar.</li>
                                                        
                                                   </ul>
                                                   </div> -->
                                                    <!-- <div class= \"span6\">
                                                      <h3>Location:</h3>
                                                      <ul class=\" unstyled amounts\">
                                                             
                                                         <li  class=\"text-bold\">Zone\t: Shivaji Nagar</li>
                                                                                                              <li  class=\"text-bold\">State \t: Maharashtra</li>
                                                                                                              <li  class=\"text-bold\">Region\t: Baner </li>
                                                                                                               <li  class=\"text-bold\">Location\t: Baner </li>
                                                                                                                <li  class=\"text-bold\">Country\t: India</li>
                                                                                                                
                                                        
                             </ul>
                                                     </div> -->

                                                </div>
                                            </div>
                                            <!-- END row-fluid --> 
                                        </div>
                                        <!-- END HORIZONTAL DESCRIPTION LISTS PORTLET--> 
                                    </div>
                                </div>
                            </div>
                            <!-- END BLANK PAGE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
            </div>
            <!-- END PAGE -->  
        </div>
        <!-- END CONTAINER -->
 </div>
</body>
<!-- END BODY -->
";
    }

    // line 200
    public function block_javascripts($context, array $blocks = array())
    {
        // line 201
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:viewCloseRequest.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  295 => 201,  118 => 72,  369 => 241,  313 => 187,  297 => 177,  293 => 176,  287 => 173,  234 => 153,  271 => 145,  259 => 138,  232 => 121,  473 => 333,  470 => 332,  349 => 220,  324 => 205,  160 => 86,  239 => 155,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 318,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 286,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 238,  408 => 232,  403 => 230,  382 => 218,  377 => 216,  372 => 242,  346 => 200,  342 => 198,  299 => 171,  263 => 155,  255 => 152,  250 => 150,  215 => 141,  153 => 82,  503 => 355,  455 => 310,  446 => 305,  436 => 251,  425 => 289,  414 => 283,  406 => 277,  392 => 222,  356 => 230,  345 => 224,  331 => 215,  363 => 178,  358 => 147,  350 => 201,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 171,  274 => 161,  251 => 146,  551 => 325,  548 => 324,  526 => 304,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 219,  394 => 217,  378 => 204,  366 => 179,  353 => 190,  347 => 25,  340 => 197,  338 => 178,  334 => 177,  321 => 174,  317 => 172,  315 => 171,  307 => 169,  245 => 125,  242 => 124,  228 => 144,  206 => 117,  202 => 106,  194 => 110,  113 => 59,  218 => 110,  210 => 140,  186 => 94,  172 => 124,  81 => 38,  192 => 114,  174 => 93,  155 => 59,  23 => 1,  233 => 146,  184 => 94,  137 => 69,  58 => 11,  225 => 126,  213 => 78,  205 => 21,  198 => 117,  104 => 49,  237 => 170,  207 => 120,  195 => 145,  185 => 104,  344 => 190,  336 => 212,  333 => 157,  328 => 188,  325 => 25,  318 => 207,  316 => 189,  308 => 175,  304 => 173,  300 => 185,  296 => 184,  292 => 200,  288 => 166,  284 => 165,  272 => 178,  260 => 163,  256 => 174,  248 => 149,  216 => 79,  200 => 102,  190 => 96,  170 => 108,  126 => 74,  211 => 127,  181 => 93,  129 => 66,  279 => 148,  275 => 147,  265 => 171,  261 => 157,  257 => 141,  222 => 128,  188 => 93,  167 => 123,  178 => 73,  175 => 91,  161 => 90,  150 => 85,  114 => 71,  110 => 53,  90 => 30,  127 => 59,  124 => 64,  97 => 57,  290 => 166,  286 => 165,  280 => 146,  276 => 168,  270 => 161,  266 => 160,  262 => 134,  253 => 149,  249 => 137,  244 => 156,  236 => 154,  231 => 145,  197 => 101,  191 => 99,  180 => 69,  165 => 91,  146 => 69,  65 => 21,  53 => 19,  152 => 83,  148 => 81,  134 => 76,  76 => 36,  70 => 30,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 234,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 220,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 237,  365 => 111,  362 => 110,  360 => 225,  355 => 223,  341 => 189,  337 => 103,  322 => 185,  314 => 172,  312 => 170,  309 => 175,  305 => 174,  298 => 168,  294 => 169,  285 => 171,  283 => 88,  278 => 149,  268 => 172,  264 => 164,  258 => 140,  252 => 160,  247 => 157,  241 => 77,  229 => 112,  220 => 142,  214 => 128,  177 => 95,  169 => 91,  140 => 70,  132 => 67,  128 => 58,  107 => 62,  61 => 20,  273 => 164,  269 => 166,  254 => 92,  243 => 127,  240 => 171,  238 => 154,  235 => 74,  230 => 82,  227 => 182,  224 => 181,  221 => 115,  219 => 123,  217 => 75,  208 => 107,  204 => 109,  179 => 126,  159 => 80,  143 => 71,  135 => 68,  119 => 62,  102 => 46,  71 => 21,  67 => 26,  63 => 24,  59 => 21,  94 => 30,  89 => 28,  85 => 33,  75 => 22,  68 => 28,  56 => 20,  87 => 25,  28 => 3,  93 => 50,  88 => 48,  78 => 18,  27 => 7,  46 => 10,  44 => 11,  31 => 3,  38 => 6,  26 => 5,  24 => 4,  25 => 3,  201 => 112,  196 => 90,  183 => 100,  171 => 69,  166 => 67,  163 => 82,  158 => 78,  156 => 79,  151 => 77,  142 => 78,  138 => 77,  136 => 70,  121 => 59,  117 => 66,  105 => 51,  91 => 43,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 32,  72 => 17,  69 => 33,  47 => 17,  40 => 7,  37 => 10,  22 => 1,  246 => 133,  157 => 63,  145 => 67,  139 => 65,  131 => 59,  123 => 58,  120 => 44,  115 => 49,  111 => 48,  108 => 57,  101 => 47,  98 => 53,  96 => 23,  83 => 40,  74 => 30,  66 => 26,  55 => 17,  52 => 17,  50 => 17,  43 => 16,  41 => 15,  35 => 14,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 134,  193 => 131,  189 => 129,  187 => 101,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 91,  154 => 78,  149 => 75,  147 => 68,  144 => 79,  141 => 76,  133 => 61,  130 => 75,  125 => 57,  122 => 73,  116 => 71,  112 => 52,  109 => 43,  106 => 66,  103 => 55,  99 => 45,  95 => 42,  92 => 29,  86 => 28,  82 => 33,  80 => 30,  73 => 29,  64 => 10,  60 => 15,  57 => 19,  54 => 18,  51 => 18,  48 => 10,  45 => 9,  42 => 12,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
