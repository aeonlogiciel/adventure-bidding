<?php

/* ::userbase.html.twig */
class __TwigTemplate_fe5cdbe51fc846c1d6d08fce3c2eeebd8f0b18b0642b5dc710294a62392318e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 8]> <html lang=\"en\" class=\"ie8\"> <![endif]-->
<!--[if IE 9]> <html lang=\"en\" class=\"ie9\"> <![endif]-->
<!--[if !IE]><!--> <html lang=\"en\"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset=\"utf-8\" />
   <title>Welcome to Adventure Bidding System</title>
   <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\" />
   <meta content=\"\" name=\"description\" />
   <meta content=\"\" name=\"author\" />
   <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap-fileupload.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/font-awesome/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style-responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style-default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"style_color\" />
   <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/chosen-bootstrap/chosen/chosen.css"), "html", null, true);
        echo "\" />
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-tags-input/jquery.tagsinput.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/fancybox/source/jquery.fancybox.css"), "html", null, true);
        echo "\" />
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" />
   <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/data-tables/DT_bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap-datepicker/css/datepicker.css"), "html", null, true);
        echo "\" />
";
        // line 26
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 29
        echo "
</head>
";
        // line 31
        $context["userId"] = $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "id");
        // line 32
        echo "<div id=\"header\" class=\"navbar navbar-inverse navbar-fixed-top\">
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class=\"navbar-inner\">
           <div class=\"container-fluid\">
              <!--BEGIN SIDEBAR TOGGLE-->
               <div class=\"sidebar-toggle-box hidden-phone\">
                   <div class=\"icon-reorder\"></div>
               </div>
               <!--END SIDEBAR TOGGLE-->
               <!-- BEGIN LOGO -->
               <a class=\"brand\" href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">
                   <h2 class=\"hreaderbrand\">Adventure Bidding System</h2>
               </a>
               <!-- END LOGO -->
               
               <!-- END  NOTIFICATION -->
               <div class=\"top-nav \">
                   <ul class=\"nav pull-right top-menu\" >
                   \t\t";
        // line 57
        echo "                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       ";
        // line 58
        if ((!$this->getAttribute($this->getContext($context, "app"), "user"))) {
            // line 59
            echo "                       <li class=\"dropdown\">
                           <a href=\"";
            // line 60
            echo $this->env->getExtension('routing')->getPath("adventure_login_index");
            echo "\" class=\"dropdown-toggle\">
                               <i class=\"icon-user\"></i>
                               <span class=\"username\">Login</span>
                           </a>
                          
                       </li>
                       ";
        } else {
            // line 67
            echo "                       <li class=\"dropdown\">
                           <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                               <img src=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home"), "html", null, true);
            echo "/img/avatar-mini.png\" alt=\"\">
                               <span class=\"username\">";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "email"), "html", null, true);
            echo "</span>
                               <b class=\"caret\"></b>
                           </a>
                           <ul class=\"dropdown-menu extended logout\">
                               <li><a href=\"";
            // line 74
            echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_view_profile");
            echo "\"><i class=\"icon-user\"></i> My Profile</a></li>
                               <li><a href=\"";
            // line 75
            echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_edit_profile");
            echo "\"><i class=\"icon-cog\"></i> Edit Profile</a></li>
                               <li><a href=\"";
            // line 76
            echo $this->env->getExtension('routing')->getPath("log_out");
            echo "\"><i class=\"icon-key\"></i> Log Out</a></li>
                           </ul>
                       </li>
                       ";
        }
        // line 80
        echo "                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                   <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
<!-- BEGIN SIDEBAR -->
      <div class=\"sidebar-scroll\">
        <div id=\"sidebar\" class=\"nav-collapse collapse\">

         <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
         <!-- <div class=\"navbar-inverse\">
            <form class=\"navbar-search visible-phone\">
               <input type=\"text\" class=\"search-query\" placeholder=\"Search\" />
            </form>
         </div> -->
         <!-- END RESPONSIVE QUICK SEARCH FORM -->
         <!-- BEGIN SIDEBAR MENU -->
          <ul class=\"sidebar-menu\">
          
          \t\t<li class=\"sub-menu active\">
              \t <a class=\"\" href=\"";
        // line 103
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_list_request");
        echo "\">
              \t <i class=\"icon-list-alt\"></i>
                      <span>Request List</span>
                 </a>
              </li>
              <li class=\"sub-menu\">
                      <a href=\"javascript:;\" class=\"\">
                         <i class=\"icon-user\"></i>
                          <span>My Profile</span>
                          <span class=\"arrow\"></span>
                      </a>
                      <ul class=\"sub\">
                          <li><a class=\"\" href=\"";
        // line 115
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_view_profile");
        echo "\">Veiw Profile</a></li>
                          <li><a class=\"\" href=\"";
        // line 116
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_edit_profile");
        echo "\">Edit Profile</a></li>
\t\t\t\t\t  </ul>
              </li>
              
              <li class=\"sub-menu\">
              \t<a class=\"\" href=\"";
        // line 121
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_create_request");
        echo "\">
              \t <i class=\"icon-plus-sign-alt\"></i>
                      <span> Post Request</span>
                 </a>
                
              </li>
              <li class=\"sub-menu\">
              \t<a class=\"\" href=\"";
        // line 128
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_changepassword");
        echo "\">
              \t <i class=\"icon-edit\"></i>
                      <span>Change password</span>
                 </a>
                
              </li>
             
              <li>
                  <a class=\"\" href=\"";
        // line 136
        echo $this->env->getExtension('routing')->getPath("log_out");
        echo "\">
                    <i class=\"icon-key\"></i>
                    <span>Log Out</span>
                  </a>
              </li>
          </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
";
        // line 146
        $this->displayBlock('body', $context, $blocks);
        // line 150
        echo "<!-- START FOOTER -->
   <div id=\"footer\">
       2014 &copy; Adventure Bidding System
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   
   <script src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/marquee.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery.marquee.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery.nicescroll.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
   <script src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
   <script src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-slimscroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
   <script src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/common-scripts.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-tags-input/jquery.tagsinput.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/js/bootstrap-fileupload.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/form-component.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/data-tables/jquery.dataTables.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/data-tables/DT_bootstrap.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/dynamic-table.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script> 
   <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/form-wizard.js"), "html", null, true);
        echo "\"></script> 
   <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery.blockui.js"), "html", null, true);
        echo "\"></script> 
   <script  type=\"text/javascript\" src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"), "html", null, true);
        echo "\"></script>


";
        // line 182
        $this->displayBlock('javascripts', $context, $blocks);
        // line 186
        echo "    </body> 
</html>";
    }

    // line 26
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 27
        echo " 
";
    }

    // line 146
    public function block_body($context, array $blocks = array())
    {
        // line 147
        echo "

";
    }

    // line 182
    public function block_javascripts($context, array $blocks = array())
    {
        // line 183
        echo "

";
    }

    public function getTemplateName()
    {
        return "::userbase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  382 => 183,  379 => 182,  373 => 147,  370 => 146,  365 => 27,  362 => 26,  357 => 186,  355 => 182,  349 => 179,  345 => 178,  341 => 177,  337 => 176,  333 => 175,  329 => 174,  325 => 173,  321 => 172,  317 => 171,  313 => 170,  309 => 169,  305 => 168,  301 => 167,  297 => 166,  293 => 165,  289 => 164,  285 => 163,  281 => 162,  277 => 161,  269 => 159,  258 => 150,  256 => 146,  243 => 136,  232 => 128,  222 => 121,  214 => 116,  210 => 115,  195 => 103,  170 => 80,  163 => 76,  159 => 75,  155 => 74,  148 => 70,  144 => 69,  140 => 67,  130 => 60,  127 => 59,  125 => 58,  122 => 57,  111 => 42,  99 => 32,  97 => 31,  93 => 29,  91 => 26,  87 => 25,  83 => 24,  79 => 23,  75 => 22,  67 => 20,  63 => 19,  59 => 18,  55 => 17,  51 => 16,  47 => 15,  43 => 14,  39 => 13,  35 => 12,  22 => 1,  276 => 178,  273 => 160,  248 => 154,  242 => 153,  240 => 152,  236 => 150,  228 => 148,  223 => 147,  218 => 146,  213 => 144,  207 => 141,  201 => 139,  194 => 135,  189 => 132,  187 => 130,  185 => 129,  181 => 128,  177 => 127,  174 => 126,  169 => 125,  167 => 124,  71 => 21,  49 => 11,  46 => 10,  40 => 7,  34 => 4,  31 => 3,);
    }
}
