<?php

/* AdventureBiddingBundle:User:createRequestUser.html.twig */
class __TwigTemplate_0273a732ef63e031a4ac003f52883b95ff4cdd44086b7600a43fea9fa6d14c96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::userbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::userbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System | User Profile";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

<!-- BEGIN CONTAINER -->
<div id=\"container\" class=\"row-fluid\">
 
<!-- BEGIN PAGE -->
<div id=\"main-content\"> 
  <!-- BEGIN PAGE CONTAINER-->
  <div class=\"container-fluid\"> 
    <!-- BEGIN PAGE HEADER-->
    <div class=\"row-fluid\">
      <div class=\"span12\"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class=\"page-title\"> </h3>
        <ul class=\"breadcrumb\">
          <li> <a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a> <span class=\"divider\">/</span> </li>
          <li class=\"active\"> Post Request </li>
         
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER--> 
    <!-- BEGIN PAGE CONTENT-->
    <div id=\"page-wraper\">
      <div class=\"row-fluid\">
        <div class=\"span12\"> 
          <!-- BEGIN ACCORDION PORTLET-->
          <!-- Acknowledgement message start -->
\t\t\t";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 47
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 56
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 61
            echo "                        ";
        } else {
            // line 62
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 63
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 71
            echo "                        ";
        }
        // line 72
        echo "                        <!-- Acknowledgement message end -->  
          <div class=\"widget blue\">
            <div class=\"widget-title\">
              <h4><i class=\"icon-user\"></i>Create User Request</h4>
            </div>
            <div class=\"widget-body\">
              <div class =\"row-fluid\">
                <div class=\"span12\">
                    ";
        // line 80
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_start', array("attr" => array("class" => "form-vertical")));
        echo "
                    <div class=\"well\">
                      <h3>Trip Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Trip Name</label>
                            ";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "name"), 'widget', array("attr" => array("class" => "input-block-level", "placeholder" => "")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "name"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Duration Of Trip</label>
                            ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "day"), 'widget', array("attr" => array("class" => "span12 select2", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "day"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label >Journey Date </label>
                            ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "dateMin"), 'widget', array("attr" => array("class" => "datepicker span5")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "dateMin"), 'errors');
        echo "
                            To
                            ";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "dateMax"), 'widget', array("attr" => array("class" => "datepicker span5")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "dateMax"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class=\"well\">
                      <h3>Location Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Country</label>
                            ";
        // line 114
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'widget', array("attr" => array("class" => "span12 chzn-select", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'errors');
        echo "
                          </div>
                        </div>
                           <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">State</label>
                            ";
        // line 120
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "state"), 'widget', array("attr" => array("class" => "span12 chzn-select", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label labelmargin\" >Location</label>
                            ";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "location"), 'widget', array("attr" => array("class" => "input-block-level", "placeholder" => "Enter Location Name")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "location"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Zones</label>
                            <div class=\"controls\">
                                ";
        // line 133
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zone"), 'widget', array("attr" => array("class" => "input-block-level chzn-select")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zone"), 'errors');
        echo "
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class=\"well\">
                      <h3> Groups Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Number Of Males</label>
                            ";
        // line 146
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "male"), 'widget', array("attr" => array("class" => "span12 select2", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "male"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Number Of Females</label>
                            ";
        // line 152
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "female"), 'widget', array("attr" => array("class" => "span12 select2", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "female"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Number Of Traveller</label>
                            ";
        // line 158
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "traveler"), 'widget', array("attr" => array("class" => "span12 select2", "id" => "select2_sample4")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "traveler"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class=\"well\">
                      <h3> Activities Details </h3>
                      <hr>
                      <div class =\"row-fluid\">
                        <div class=\"span4\">
                          <div class=\"form-group\">
                            <label class=\"control-label\">Activities</label>
                            ";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "activity"), 'widget', array("attr" => array("class" => "input-block-level chzn-select")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "activity"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Pick-Up Location</label>
                            ";
        // line 176
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "pick"), 'widget', array("attr" => array("class" => "input-block-level", "placeholder" => "Pick-Up Location")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "pick"), 'errors');
        echo "
                          </div>
                        </div>
                        <div class=\"span4\">
                          <div class=\"control-group\">
                            <label class=\"control-label\" >Drop-Off Location</label>
                            ";
        // line 182
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "down"), 'widget', array("attr" => array("class" => "input-block-level", "placeholder" => "Drop Location")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "down"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class=\"well\">
                      <h3> Other Details </h3>
                      <hr>
                    <div class =\"row-fluid\">
                      
                      <div class=\"span12\">
                        <div class=\"control-group\">
                          <label class=\"control-label\">If your group size is too small for an organizer to respond, are you open for us to combine your group with others who are looking for the exact or similar trips?  </label>
                          <div class=\"controls\">
                              ";
        // line 196
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "team"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "team"), 'errors');
        echo "
                          </div>
                        </div>
                        
                      </div>
                       </div>
                       <hr>
                       <div class =\"row-fluid\">
                      <div class=\"span12\">
                        <div class=\"control-group\">
                          <label class=\"input-block-level\"> Special Requirement: </label>
                          <div class=\"controls\">
                              ";
        // line 208
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "description"), 'widget', array("attr" => array("class" => "span12")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "description"), 'errors');
        echo "
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                    ";
        // line 214
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "Form"), 'rest');
        echo "
                    <div class=\"form-actions\">
                      <button class=\"btn btn-primary\" id=\"save\" type=\"submit\"> Submit</button>
                      <button type=\"button\" class=\"btn btn-default\">Cancel</button>
                    </div>
                  ";
        // line 219
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_end');
        echo "
                </div>
              </div>
            </div>
            <!-- END ACCORDION PORTLET--> 
          </div>
        </div>
        
        <!-- END PAGE CONTENT--> 
      </div>
      
      <!-- END PAGE CONTENT--> 
      
    </div>
    
    <!-- END PAGE CONTAINER--> 
  </div>
  <!-- END PAGE --> 
</div>
<!-- END CONTAINER --> 
</div>
</body>
<!-- END BODY -->
";
    }

    // line 244
    public function block_javascripts($context, array $blocks = array())
    {
        // line 245
        echo "    <script>
   function getValue(){
       
        var a = document.getElementById('userRequest_male').value;
        var b = document.getElementById('userRequest_female').value;
       
        var c = parseInt(a) + parseInt(b) ;
        document.getElementById('userRequest_traveler').value = c ; 
    } 
    
    </script> 
    
     <script>
\t\t\$(document).ready(function() {
\t\t\$('.multipl').multiselect();
\t\t})(jQuery);
                \$('#example-single-selected').multiselect()(jQuery);
    </script> 
    <script>
    \$('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-3d'
    })(jQuery);
    </script> 
    


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:User:createRequestUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  417 => 245,  414 => 244,  386 => 219,  378 => 214,  367 => 208,  350 => 196,  331 => 182,  320 => 176,  309 => 170,  292 => 158,  281 => 152,  270 => 146,  252 => 133,  240 => 126,  229 => 120,  218 => 114,  201 => 102,  194 => 100,  183 => 94,  172 => 88,  161 => 80,  151 => 72,  148 => 71,  141 => 67,  135 => 63,  132 => 62,  129 => 61,  126 => 60,  116 => 56,  105 => 51,  95 => 47,  85 => 42,  68 => 28,  49 => 11,  46 => 10,  40 => 7,  34 => 4,  31 => 3,);
    }
}
