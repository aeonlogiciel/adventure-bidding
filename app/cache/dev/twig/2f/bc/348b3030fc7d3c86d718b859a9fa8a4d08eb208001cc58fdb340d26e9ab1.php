<?php

/* AdventureLoginBundle:Member:register2.html.twig */
class __TwigTemplate_2fbc348b3030fc7d3c86d718b859a9fa8a4d08eb208001cc58fdb340d26e9ab1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::registerationbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::registerationbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "         
 ";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "\t
    <!-- BEGIN LOGO -->
\t<div class=\"logo\">
\t\t<!-- <img src=\"assets/img/logo-big.png\" alt=\"\" />  -->
\t</div>
\t<!-- END LOGO -->
\t<!-- BEGIN LOGIN -->
\t<div class=\"content\">
\t\t<!-- BEGIN Registration FORM -->
\t\t<form class=\"form-vertical login-form\" action=\"#\" method=\"post\">
\t\t\t<h3 class=\"form-title text-center\"> User Sign Up </h3>
\t\t\t<div class=\"alert alert-success fade in\">
                              <button data-dismiss=\"alert\" class=\"close\" type=\"button\"></button>
                              <h4 class=\"alert-heading\">Successfully..!</h4>
                              <span> Registration Done </span>
                          </div>
\t\t\t<div class=\"control-group\">
\t\t\t\t<label class=\"control-label visible-ie8 visible-ie9\">Full Name</label>
\t\t\t\t<div class=\"controls\">
\t\t\t\t\t<div class=\"input-icon left\">
\t\t\t\t\t\t<i class=\"icon-user\"></i>
\t\t\t\t\t\t<input class=\"m-wrap placeholder-no-fix\" type=\"text\" autocomplete=\"off\" placeholder=\"Full Name\" name=\"username\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"control-group\">
\t\t\t\t<label class=\"control-label visible-ie8 visible-ie9\">Email</label>
\t\t\t\t<div class=\"controls\">
\t\t\t\t\t<div class=\"input-icon left\">
\t\t\t\t\t\t<i class=\"icon-envelope-alt\"></i>
\t\t\t\t\t\t<input class=\"m-wrap placeholder-no-fix\" type=\"text\" autocomplete=\"off\" placeholder=\"Email\" name=\"username\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"control-group\">
\t\t\t\t<label class=\"control-label visible-ie8 visible-ie9\">Password</label>
\t\t\t\t<div class=\"controls\">
\t\t\t\t\t<div class=\"input-icon left\">
\t\t\t\t\t\t<i class=\"icon-lock\"></i>
\t\t\t\t\t\t<input class=\"m-wrap placeholder-no-fix\" type=\"password\" autocomplete=\"off\" id=\"register_password\" placeholder=\"Password\" name=\"password\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"control-group\">
\t\t\t\t<label class=\"control-label visible-ie8 visible-ie9\">Re-type Your Password</label>
\t\t\t\t<div class=\"controls\">
\t\t\t\t\t<div class=\"input-icon left\">
\t\t\t\t\t\t<i class=\"icon-ok\"></i>
\t\t\t\t\t\t<input class=\"m-wrap placeholder-no-fix\" type=\"password\" autocomplete=\"off\" placeholder=\"Re-type Your Password\" name=\"rpassword\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-actions\">
\t\t\t      <a href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("login");
        echo "\" type=\"button\" class=\"btn\">
\t\t\t\t<i class=\"m-icon-swapleft\"></i>  Back
\t\t\t\t</a>
\t\t\t\t<button type=\"submit\" id=\"register-submit-btn\" class=\"btn green pull-right\">
\t\t\t\tSign Up <i class=\"m-icon-swapright m-icon-white\"></i>
\t\t\t\t</button>            
\t\t\t</div>
\t\t\t
\t  <div class=\"fbgmail\">
            \t<div class=\"btn blue btn-block \">
                \t<a href=\"#\">Facebook Sign up</a>
                </div>
                <div class=\"btn red btn-block\">
                \t<a href=\"#\">Gmail Sign up</a>
                </div>
            </div>
\t\t</form>
\t\t<!-- END Registration FORM -->        
\t\t
\t</div>
\t<!-- END LOGIN -->
    
";
    }

    // line 86
    public function block_javascripts($context, array $blocks = array())
    {
        // line 87
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  <script>
\t\tjQuery(document).ready(function() {     
\t\t  App.init();
\t\t  Login.init();
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "AdventureLoginBundle:Member:register2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 87,  124 => 86,  97 => 61,  290 => 151,  286 => 150,  280 => 147,  276 => 146,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 135,  236 => 133,  231 => 131,  197 => 103,  191 => 99,  180 => 89,  165 => 75,  146 => 65,  65 => 21,  53 => 18,  152 => 67,  148 => 84,  134 => 73,  76 => 26,  70 => 33,  34 => 11,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 140,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 51,  128 => 49,  107 => 36,  61 => 20,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 134,  238 => 85,  235 => 74,  230 => 82,  227 => 130,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 109,  179 => 69,  159 => 61,  143 => 64,  135 => 53,  119 => 42,  102 => 46,  71 => 24,  67 => 15,  63 => 15,  59 => 14,  94 => 28,  89 => 20,  85 => 40,  75 => 17,  68 => 14,  56 => 9,  87 => 25,  28 => 5,  93 => 28,  88 => 6,  78 => 21,  27 => 7,  46 => 7,  44 => 12,  31 => 8,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 81,  166 => 71,  163 => 62,  158 => 71,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 56,  121 => 46,  117 => 44,  105 => 70,  91 => 27,  62 => 23,  49 => 17,  21 => 2,  19 => 1,  79 => 36,  72 => 16,  69 => 25,  47 => 9,  40 => 8,  37 => 14,  22 => 3,  246 => 90,  157 => 56,  145 => 46,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 57,  111 => 37,  108 => 36,  101 => 32,  98 => 46,  96 => 43,  83 => 25,  74 => 14,  66 => 34,  55 => 15,  52 => 27,  50 => 24,  43 => 6,  41 => 7,  35 => 5,  32 => 4,  29 => 6,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 76,  164 => 59,  162 => 57,  154 => 58,  149 => 66,  147 => 58,  144 => 49,  141 => 48,  133 => 60,  130 => 41,  125 => 44,  122 => 55,  116 => 41,  112 => 51,  109 => 54,  106 => 36,  103 => 32,  99 => 31,  95 => 45,  92 => 44,  86 => 28,  82 => 22,  80 => 19,  73 => 34,  64 => 14,  60 => 29,  57 => 19,  54 => 10,  51 => 14,  48 => 8,  45 => 16,  42 => 7,  39 => 9,  36 => 5,  33 => 3,  30 => 2,);
    }
}
