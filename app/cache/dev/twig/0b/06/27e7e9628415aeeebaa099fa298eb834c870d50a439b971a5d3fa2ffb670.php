<?php

/* AdventureLoginBundle:Member:changePassword.html.twig */
class __TwigTemplate_0b0627e7e9628415aeeebaa099fa298eb834c870d50a439b971a5d3fa2ffb670 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::admin.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System | User Profile";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

   <!-- BEGIN CONTAINER -->
   <div id=\"container\" class=\"row-fluid\">

      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                     Change Password
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"index.html\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           Change Password
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
                <div class=\"row-fluid\">
                <div class=\"span12\">
                    <!-- Acknowledgement message start -->
\t\t\t";
        // line 48
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-success fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Success!</h4>
                                  <p>
                                      ";
            // line 53
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " 
                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
            // line 62
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo "
                                  </p>
                              </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                        ";
        if ((!array_key_exists("error", $context))) {
            // line 67
            echo "                        ";
        } else {
            // line 68
            echo "                        ";
            if ($this->getContext($context, "error")) {
                // line 69
                echo "                            <div class=\"alert alert-danger fade in\">
                                  <button data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                                  <h4 class=\"alert-heading\">Error!</h4>
                                  <p>
                                      ";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
                                  </p>
                              </div>
                            ";
            }
            // line 77
            echo "                        ";
        }
        // line 78
        echo "                        <!-- Acknowledgement message end -->  
                    <!-- BEGIN ACCORDION PORTLET-->
                    <div class=\"widget blue\">
                        <div class=\"widget-title\">
                            <h4><i class=\"icon-user\"></i>Change Password</h4>
\t\t\t\t\t\t</div>
                        <div class=\"widget-body\">
                            <div class=\"accordion\" id=\"accordion1\">
\t\t\t\t\t\t\t\t<div class=\"accordion-group\">
                                    <div id=\"collapse_4\" class=\"accordion-body collapse in\">
                                        <div class=\"accordion-inner\">
                                        \t<!--Submit massege-->
                                        \t
                                            <form  id=\"change_password\" method=\"POST\" action=\"";
        // line 91
        echo $this->env->getExtension('routing')->getPath("adventure_update_password");
        echo "\" name=\"changepassword\" enctype=\"multipart/form-data\">      
\t\t\t\t\t\t<label class=\"control-label\">Current Password</label>
\t\t\t\t\t\t<input type=\"password\" name=\"currentPassword\" class=\"m-wrap span8\" required/>
\t\t\t\t\t\t<label class=\"control-label\">New Password</label>
\t\t\t\t\t\t<input type=\"password\" class=\"m-wrap span8\" name=\"newPassword\" required/>
\t\t\t\t\t\t<label class=\"control-label\">Re-type New Password</label>
\t\t\t\t\t\t<input type=\"password\" class=\"m-wrap span8\" name=\"confirmPassword\" required/>
\t\t\t\t\t\t<div class=\"submit-btn\">
                                                    <button class=\"btn btn-primary\" type=\"submit\">Save</button>
                                                    <a href=\"";
        // line 100
        echo $this->env->getExtension('routing')->getPath("adventure_change_password");
        echo "\"><button class=\"btn btn-default\" type=\"button\">Cancel</button></a>
                                            </div>
\t\t\t\t\t</form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- END ACCORDION PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->         
         </div>

            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   </div>
</body>
<!-- END BODY -->
";
    }

    // line 127
    public function block_javascripts($context, array $blocks = array())
    {
        // line 128
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureLoginBundle:Member:changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 127,  181 => 100,  129 => 66,  279 => 148,  275 => 147,  265 => 143,  261 => 142,  257 => 141,  222 => 115,  188 => 93,  167 => 78,  178 => 82,  175 => 84,  161 => 31,  150 => 81,  114 => 54,  110 => 53,  90 => 41,  127 => 87,  124 => 86,  97 => 61,  290 => 151,  286 => 150,  280 => 147,  276 => 146,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 135,  236 => 133,  231 => 131,  197 => 103,  191 => 99,  180 => 89,  165 => 75,  146 => 65,  65 => 21,  53 => 18,  152 => 85,  148 => 68,  134 => 73,  76 => 26,  70 => 33,  34 => 4,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 140,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 128,  177 => 65,  169 => 91,  140 => 55,  132 => 67,  128 => 49,  107 => 36,  61 => 20,  273 => 96,  269 => 144,  254 => 92,  243 => 88,  240 => 134,  238 => 85,  235 => 74,  230 => 82,  227 => 117,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 109,  179 => 69,  159 => 61,  143 => 64,  135 => 68,  119 => 62,  102 => 46,  71 => 24,  67 => 15,  63 => 15,  59 => 14,  94 => 28,  89 => 20,  85 => 40,  75 => 17,  68 => 24,  56 => 18,  87 => 25,  28 => 5,  93 => 28,  88 => 48,  78 => 21,  27 => 7,  46 => 10,  44 => 15,  31 => 3,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 82,  171 => 81,  166 => 37,  163 => 62,  158 => 30,  156 => 66,  151 => 77,  142 => 66,  138 => 69,  136 => 70,  121 => 57,  117 => 44,  105 => 51,  91 => 27,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 36,  72 => 16,  69 => 25,  47 => 9,  40 => 7,  37 => 14,  22 => 1,  246 => 133,  157 => 73,  145 => 67,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 57,  111 => 53,  108 => 57,  101 => 48,  98 => 53,  96 => 43,  83 => 25,  74 => 14,  66 => 34,  55 => 15,  52 => 17,  50 => 24,  43 => 6,  41 => 15,  35 => 5,  32 => 4,  29 => 6,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 76,  164 => 77,  162 => 57,  154 => 78,  149 => 66,  147 => 58,  144 => 73,  141 => 48,  133 => 60,  130 => 41,  125 => 44,  122 => 55,  116 => 41,  112 => 51,  109 => 54,  106 => 36,  103 => 32,  99 => 31,  95 => 45,  92 => 44,  86 => 28,  82 => 33,  80 => 30,  73 => 26,  64 => 20,  60 => 19,  57 => 19,  54 => 10,  51 => 14,  48 => 16,  45 => 16,  42 => 7,  39 => 9,  36 => 13,  33 => 13,  30 => 2,);
    }
}
