<?php

/* ::organiserbase.html.twig */
class __TwigTemplate_7305acf2fce86b2193aaa2c8c6cb11cac343c5ba2ba0e7fd7cdbe1f837185f9e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 8]> <html lang=\"en\" class=\"ie8\"> <![endif]-->
<!--[if IE 9]> <html lang=\"en\" class=\"ie9\"> <![endif]-->
<!--[if !IE]><!--> <html lang=\"en\"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset=\"utf-8\" />
   <title>Welcome to Adventure Bidding System</title>
   <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\" />
   <meta content=\"\" name=\"description\" />
   <meta content=\"Mosaddek\" name=\"author\" />
   <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap-fileupload.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/font-awesome/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style-responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style-default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"style_color\" />
   <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/chosen-bootstrap/chosen/chosen.css"), "html", null, true);
        echo "\" />
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-tags-input/jquery.tagsinput.css"), "html", null, true);
        echo "\" />
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" />
   <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/data-tables/DT_bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap-datepicker/css/datepicker.css"), "html", null, true);
        echo "\" />
";
        // line 25
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 28
        echo "</head>
";
        // line 29
        $context["userId"] = $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "id");
        // line 30
        echo "<!-- END HEAD -->

   <!-- BEGIN HEADER -->
   <div id=\"header\" class=\"navbar navbar-inverse navbar-fixed-top\">
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class=\"navbar-inner\">
           <div class=\"container-fluid\">
               <!--BEGIN SIDEBAR TOGGLE-->
               <div class=\"sidebar-toggle-box hidden-phone\">
                   <div class=\"icon-reorder\"></div>
               </div>
               <!--END SIDEBAR TOGGLE-->
               <!-- BEGIN LOGO -->
               <a class=\"brand\" href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">
                   <h2 class=\"hreaderbrand\">Adventure Bidding System</h2>
               </a>
               <!-- END LOGO -->
               <!-- BEGIN RESPONSIVE MENU TOGGLER -->
               <a class=\"btn btn-navbar collapsed\" id=\"main_menu_trigger\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
                   <span class=\"icon-bar\"></span>
                   <span class=\"icon-bar\"></span>
                   <span class=\"icon-bar\"></span>
                   <span class=\"arrow\"></span>
               </a>
               <!-- END RESPONSIVE MENU TOGGLER -->
               
               <!-- END  NOTIFICATION -->
               <div class=\"top-nav \">
                   <ul class=\"nav pull-right top-menu\" >
                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       <li class=\"dropdown\">
                           <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                              <img src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home"), "html", null, true);
        echo "/img/avatar-mini.png\" alt=\"\">
                               <span class=\"username\">";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "username"), "html", null, true);
        echo "</span>
                               <b class=\"caret\"></b>
                           </a>
                           <ul class=\"dropdown-menu extended logout\">
                               <li><a href=\"";
        // line 67
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_view_profile");
        echo "\"><i class=\"icon-user\"></i> My Profile</a></li>
                               <li><a href=\"";
        // line 68
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_edit_profile");
        echo "\"><i class=\"icon-cog\"></i> Edit Profile</a></li>
                               <li><a href=\"";
        // line 69
        echo $this->env->getExtension('routing')->getPath("log_out");
        echo "\"><i class=\"icon-key\"></i> Log Out</a></li>
                           </ul>
                       </li>
                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                   <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id=\"container\" class=\"row-fluid\">
      <!-- BEGIN SIDEBAR -->
      <div class=\"sidebar-scroll\">
        <div id=\"sidebar\" class=\"nav-collapse collapse\">

         <!-- BEGIN SIDEBAR MENU -->
          <ul class=\"sidebar-menu\">
          
          \t\t<li class=\"sub-menu active\">
              \t <a class=\"\" href=\"";
        // line 91
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_dashboard");
        echo "\">
              \t <i class=\"icon-list-alt\"></i>
                      <span>All Request</span>
                 </a>
              </li>
              <li class=\"sub-menu\">
                      <a href=\"javascript:;\" class=\"\">
                         <i class=\"icon-user\"></i>
                          <span>My Profile</span>
                          <span class=\"arrow\"></span>
                      </a>
                      <ul class=\"sub\">
                          <li><a class=\"\" href=\"";
        // line 103
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_view_profile");
        echo "\">Veiw Profile</a></li>
                          <li><a class=\"\" href=\"";
        // line 104
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_view_event");
        echo "\">Add Event</a></li>
                          <li><a class=\"\" href=\"";
        // line 105
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_view_trails");
        echo "\">Add Trails</a></li>
                         
\t\t\t\t\t  </ul>
              </li>
              
              <li class=\"sub-menu\">
              \t<a class=\"\" href=\"";
        // line 111
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_feedback");
        echo "\">
              \t <i class=\"icon-plus-sign-alt\"></i>
                      <span>Feedback</span>
                 </a>
                
              </li>
              
              <li class=\"sub-menu\">
              \t<a class=\"\" href=\"\">
              \t <i class=\"icon-cloud\"></i>
                      <span>Closed Request</span>
                 </a>
                
              </li>
              <li class=\"sub-menu\">
              \t<a class=\"\" href=\"";
        // line 126
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_organiser_changepassword");
        echo "\">
              \t <i class=\"icon-edit\"></i>
                      <span>Change Password</span>
                 </a>
                
              </li>
              
              
             
              <li>
                  <a class=\"\" href=\"";
        // line 136
        echo $this->env->getExtension('routing')->getPath("log_out");
        echo "\">
                    <i class=\"icon-key\"></i>
                    <span>Log Out</span>
                  </a>
              </li>
          </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
";
        // line 146
        $this->displayBlock('body', $context, $blocks);
        // line 149
        echo "   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id=\"footer\">
       2014 &copy; Adventure Bidding System
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/marquee.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery.marquee.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery.nicescroll.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
   <script src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
   <script src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-slimscroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
   <script src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/common-scripts.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-tags-input/jquery.tagsinput.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/js/bootstrap-fileupload.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/form-component.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/data-tables/jquery.dataTables.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/data-tables/DT_bootstrap.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/dynamic-table.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script> 
";
        // line 178
        $this->displayBlock('javascripts', $context, $blocks);
        // line 182
        echo "   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>";
    }

    // line 25
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 26
        echo " 
";
    }

    // line 146
    public function block_body($context, array $blocks = array())
    {
        // line 147
        echo "
";
    }

    // line 178
    public function block_javascripts($context, array $blocks = array())
    {
        // line 179
        echo "

";
    }

    public function getTemplateName()
    {
        return "::organiserbase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  366 => 179,  363 => 178,  358 => 147,  355 => 146,  350 => 26,  347 => 25,  340 => 182,  338 => 178,  334 => 177,  330 => 176,  326 => 175,  322 => 174,  318 => 173,  314 => 172,  310 => 171,  306 => 170,  302 => 169,  298 => 168,  294 => 167,  290 => 166,  286 => 165,  282 => 164,  278 => 163,  274 => 162,  270 => 161,  266 => 160,  253 => 149,  251 => 146,  238 => 136,  225 => 126,  207 => 111,  198 => 105,  194 => 104,  190 => 103,  150 => 69,  146 => 68,  142 => 67,  135 => 63,  131 => 62,  109 => 43,  94 => 30,  92 => 29,  89 => 28,  87 => 25,  83 => 24,  79 => 23,  75 => 22,  71 => 21,  67 => 20,  63 => 19,  59 => 18,  55 => 17,  51 => 16,  47 => 15,  43 => 14,  39 => 13,  35 => 12,  22 => 1,  268 => 172,  265 => 171,  239 => 147,  233 => 146,  231 => 145,  228 => 144,  220 => 142,  215 => 141,  210 => 140,  203 => 136,  199 => 134,  193 => 131,  189 => 129,  187 => 128,  183 => 127,  179 => 126,  175 => 91,  172 => 124,  167 => 123,  165 => 122,  68 => 28,  49 => 11,  46 => 10,  40 => 7,  34 => 4,  31 => 3,);
    }
}
