<?php

/* AdventureAdminBundle:Admin:userList.html.twig */
class __TwigTemplate_48a810ea59a9f50dd212056bdb582e974171b894f37f7ac521e819b8f8a42d72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::admin.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome Admin to Adventure Bidding System";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "

    <!-- BEGIN BODY -->
<body class=\"fixed-top\">
";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "flashMessage"), "html", null, true);
            echo " ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                    Admin Dashboard
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           All Request
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
               <div class=\"row-fluid\">
                <div class=\"span12\">
                <!-- BEGIN EXAMPLE TABLE widget-->
                
                <div class=\"widget blue\">
                    <div class=\"widget-title\">
                        <h4><i class=\"icon-search\"></i> Filter Your Search</h4>
                         
                    </div>
                    <div class=\"widget-body\">
                         <form class=\"form-inline\" method=\"post\">
                         <div class=\"row-fluid\">
                         
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Filter Search</label>
                          <div class=\"controls\">
                                    <select class=\"span8 \" data-placeholder=\"Choose a Category\" tabindex=\"1\">
                                        <option value=\"\">Select...</option>
                                        <option value=\"Activities\">Activities</option>
                                        <option value=\"Zones\">Zones</option>
                                        <option value=\"Category 3\">Category 5</option>
                                        <option value=\"Category 4\">Category 4</option>
                                    </select>
                                </div>
                          
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Search</label>
                           <div class=\"controls\">
                          <input type=\"text\" class=\"span8\"/>   
                          </div>
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          
                          <label class=\"control-label\"></label>
                          <div class=\"controls\">
                <button type=\"submit\" class=\"btn btn-primary\"><i class=\"icon-ok\"></i> Submit</button>
                         <button type=\"button\" class=\"btn\">Cancel</button>
                
               </div>
                          </div>
                         </div>
                         
                         
                         </div>
                         
                         </form>
                        
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
                <div class=\"row-fluid\">
                    <!--marquee start here-->
                    <div class=\"span12\">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class=\"widget blue\">
                            <div class=\"widget-title\">
                                <h4>All User List</h4>
                            </div>
                            <div class=\"widget-body\">
<table class=\"table table-striped table-bordered  table-hover\" id=\"sample_1\">
    <thead>
        <tr>
            <th class=\"hidden-phone\">Sr. No.</th>
            <th >Username</th>
            <th >Contact</th>
            <th >Email</th>
            <th >Country</th>
            <th >Created ON</th>
            <th >Status</th>

        </tr>
    </thead>
    <tbody>
        ";
        // line 124
        $context["count"] = "1";
        // line 125
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "member"));
        foreach ($context['_seq'] as $context["_key"] => $context["member"]) {
            // line 126
            echo "        <tr>
            <td>";
            // line 127
            echo twig_escape_filter($this->env, $this->getContext($context, "count"), "html", null, true);
            echo "</td>
            <td>&nbsp;";
            // line 128
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "name"), "html", null, true);
            echo "</td>
            <td>&nbsp;";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "phone"), "html", null, true);
            echo "</td>
            <td>&nbsp;";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "email"), "html", null, true);
            echo "</td>
            <td>&nbsp;";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "country"), "html", null, true);
            echo "</td>
            <td>&nbsp;";
            // line 132
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "createdAt"), "Y-m-d"), "html", null, true);
            echo " </td>
             <td><a onclick=\"return confirm('Are you sure you want to delete this member ?')\" class=\"btn btn-danger\" href=\"";
            // line 133
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_admin_user_delete", array("id" => $this->getAttribute($this->getContext($context, "member"), "id"))), "html", null, true);
            echo "\"><i class=\"icon-trash \"></i></a> &nbsp;</a>
                    ";
            // line 134
            if (($this->getAttribute($this->getContext($context, "member"), "status") == "1")) {
                // line 135
                echo "                        <a href='javascript:void(0)'  onclick='return getval(0,";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "memberid"), "html", null, true);
                echo ",this)' > Active</a>
                    ";
            } else {
                // line 137
                echo "                        <a href='javascript:void(0)'  onclick='return getval(1,";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "member"), "memberid"), "html", null, true);
                echo ",this)' >InActive</a>
                    ";
            }
            // line 139
            echo "            </td>
        </tr>
        ";
            // line 141
            $context["count"] = ($this->getContext($context, "count") + "1");
            // line 142
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['member'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 143
        echo "    </tbody>
</table></div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
                         <!--marquee End here-->
                </div>
                
                
            </div>

            <!-- END PAGE CONTENT-->         

      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

    
</body>
<!-- END BODY -->
";
    }

    // line 166
    public function block_javascripts($context, array $blocks = array())
    {
        // line 167
        echo "

<script src=\"http://code.jquery.com/jquery-1.11.0.min.js\"></script>
<script type=\"text/javascript\"> 
         
            function getval(value1, ids, caller) {
                   
               var i = ids;
                var status = value1;
                //alert(i);
                \$(caller).html('Wait..');
                \$.post(\"";
        // line 178
        echo $this->env->getExtension('routing')->getPath("adventure_admin_member_status");
        echo "\", {userid: i, status: status}, function(data){
                    if (data == 'OK')
                    {
                        location.reload();
                    } else {
                        //Error occured dispay
                        alert(data);
                    }

                });
            }
          
        </script>
        
";
    }

    public function getTemplateName()
    {
        return "AdventureAdminBundle:Admin:userList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  332 => 209,  690 => 610,  687 => 609,  100 => 32,  653 => 349,  650 => 348,  627 => 327,  612 => 325,  593 => 307,  589 => 305,  575 => 294,  557 => 282,  546 => 274,  537 => 268,  520 => 257,  505 => 245,  486 => 228,  481 => 225,  467 => 218,  460 => 214,  429 => 200,  426 => 199,  411 => 194,  405 => 192,  399 => 190,  397 => 189,  380 => 183,  376 => 181,  212 => 108,  500 => 323,  497 => 322,  456 => 284,  351 => 182,  339 => 179,  335 => 210,  311 => 164,  226 => 119,  417 => 196,  386 => 219,  367 => 208,  320 => 176,  223 => 147,  373 => 147,  370 => 146,  357 => 186,  301 => 181,  289 => 158,  281 => 178,  277 => 154,  504 => 354,  501 => 353,  434 => 293,  412 => 281,  404 => 275,  390 => 254,  354 => 228,  343 => 180,  329 => 174,  295 => 160,  118 => 72,  369 => 241,  313 => 153,  297 => 166,  293 => 165,  287 => 173,  234 => 142,  271 => 162,  259 => 138,  232 => 141,  473 => 219,  470 => 332,  349 => 167,  324 => 205,  160 => 86,  239 => 155,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 288,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 232,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 197,  408 => 193,  403 => 241,  382 => 183,  377 => 216,  372 => 215,  346 => 200,  342 => 163,  299 => 162,  263 => 155,  255 => 152,  250 => 135,  215 => 141,  153 => 46,  503 => 355,  455 => 310,  446 => 305,  436 => 205,  425 => 289,  414 => 195,  406 => 277,  392 => 222,  356 => 230,  345 => 178,  331 => 177,  363 => 178,  358 => 204,  350 => 196,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 153,  274 => 153,  251 => 146,  551 => 325,  548 => 324,  526 => 260,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 240,  394 => 217,  378 => 214,  366 => 174,  353 => 169,  347 => 166,  340 => 197,  338 => 178,  334 => 177,  321 => 172,  317 => 171,  315 => 171,  307 => 169,  245 => 141,  242 => 132,  228 => 139,  206 => 132,  202 => 131,  194 => 129,  113 => 62,  218 => 114,  210 => 133,  186 => 127,  172 => 104,  81 => 38,  192 => 114,  174 => 126,  155 => 74,  23 => 3,  233 => 146,  184 => 101,  137 => 72,  58 => 13,  225 => 144,  213 => 144,  205 => 21,  198 => 130,  104 => 49,  237 => 141,  207 => 141,  195 => 103,  185 => 107,  344 => 190,  336 => 212,  333 => 175,  328 => 188,  325 => 162,  318 => 207,  316 => 205,  308 => 150,  304 => 198,  300 => 185,  296 => 179,  292 => 159,  288 => 166,  284 => 165,  272 => 152,  260 => 141,  256 => 137,  248 => 154,  216 => 135,  200 => 102,  190 => 128,  170 => 92,  126 => 60,  211 => 127,  181 => 100,  129 => 66,  279 => 148,  275 => 147,  265 => 166,  261 => 157,  257 => 147,  222 => 137,  188 => 102,  167 => 124,  178 => 125,  175 => 94,  161 => 80,  150 => 81,  114 => 71,  110 => 65,  90 => 30,  127 => 75,  124 => 67,  97 => 57,  290 => 176,  286 => 142,  280 => 155,  276 => 178,  270 => 178,  266 => 160,  262 => 139,  253 => 136,  249 => 145,  244 => 133,  236 => 150,  231 => 139,  197 => 114,  191 => 107,  180 => 69,  165 => 90,  146 => 78,  65 => 18,  53 => 19,  152 => 91,  148 => 90,  134 => 71,  76 => 33,  70 => 15,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 209,  444 => 303,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 198,  413 => 234,  409 => 132,  407 => 131,  402 => 191,  398 => 129,  393 => 188,  387 => 187,  384 => 121,  381 => 120,  379 => 182,  374 => 116,  368 => 237,  365 => 27,  362 => 26,  360 => 173,  355 => 182,  341 => 177,  337 => 176,  322 => 178,  314 => 172,  312 => 170,  309 => 170,  305 => 163,  298 => 149,  294 => 148,  285 => 163,  283 => 156,  278 => 149,  268 => 167,  264 => 164,  258 => 150,  252 => 146,  247 => 134,  241 => 77,  229 => 120,  220 => 126,  214 => 134,  177 => 93,  169 => 91,  140 => 73,  132 => 40,  128 => 73,  107 => 62,  61 => 20,  273 => 179,  269 => 151,  254 => 92,  243 => 143,  240 => 143,  238 => 154,  235 => 74,  230 => 121,  227 => 182,  224 => 181,  221 => 115,  219 => 115,  217 => 75,  208 => 107,  204 => 109,  179 => 94,  159 => 83,  143 => 74,  135 => 77,  119 => 73,  102 => 46,  71 => 31,  67 => 20,  63 => 19,  59 => 18,  94 => 30,  89 => 28,  85 => 42,  75 => 35,  68 => 28,  56 => 15,  87 => 29,  28 => 3,  93 => 53,  88 => 48,  78 => 29,  27 => 3,  46 => 10,  44 => 12,  31 => 4,  38 => 6,  26 => 5,  24 => 4,  25 => 4,  201 => 102,  196 => 90,  183 => 126,  171 => 69,  166 => 92,  163 => 89,  158 => 94,  156 => 47,  151 => 77,  142 => 78,  138 => 42,  136 => 75,  121 => 66,  117 => 65,  105 => 34,  91 => 30,  62 => 15,  49 => 11,  21 => 2,  19 => 1,  79 => 24,  72 => 17,  69 => 29,  47 => 15,  40 => 11,  37 => 10,  22 => 1,  246 => 144,  157 => 63,  145 => 80,  139 => 78,  131 => 76,  123 => 74,  120 => 69,  115 => 36,  111 => 42,  108 => 57,  101 => 47,  98 => 53,  96 => 31,  83 => 24,  74 => 21,  66 => 26,  55 => 17,  52 => 14,  50 => 17,  43 => 14,  41 => 15,  35 => 12,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 100,  193 => 113,  189 => 132,  187 => 97,  182 => 95,  176 => 124,  173 => 92,  168 => 76,  164 => 77,  162 => 91,  154 => 78,  149 => 45,  147 => 80,  144 => 43,  141 => 78,  133 => 61,  130 => 60,  125 => 38,  122 => 57,  116 => 70,  112 => 52,  109 => 63,  106 => 66,  103 => 33,  99 => 32,  95 => 47,  92 => 29,  86 => 28,  82 => 17,  80 => 30,  73 => 29,  64 => 14,  60 => 16,  57 => 19,  54 => 18,  51 => 14,  48 => 13,  45 => 10,  42 => 9,  39 => 13,  36 => 6,  33 => 13,  30 => 2,);
    }
}
