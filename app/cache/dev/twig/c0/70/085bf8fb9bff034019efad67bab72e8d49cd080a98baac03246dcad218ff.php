<?php

/* ::registerationbase.html.twig */
class __TwigTemplate_c070085bf8fb9bff034019efad67bab72e8d49cd080a98baac03246dcad218ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 8]> <html lang=\"en\" class=\"ie8\"> <![endif]-->
<!--[if IE 9]> <html lang=\"en\" class=\"ie9\"> <![endif]-->
<!--[if !IE]><!--> <html lang=\"en\"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
\t<meta charset=\"utf-8\" />
\t<title> Home | Registration Page</title>
\t<meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\" />
\t<meta content=\"\" name=\"description\" />
\t<meta content=\"\" name=\"author\" />
\t<!-- BEGIN GLOBAL MANDATORY STYLES -->
\t<link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/plugins/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/plugins/bootstrap/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/plugins/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/css/style-metro.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/css/style-responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" id=\"style_color\"/>
\t<link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/plugins/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t
\t<!-- END GLOBAL MANDATORY STYLES -->
\t<!-- BEGIN PAGE LEVEL STYLES -->
\t<link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/css/pages/login.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
\t<!-- END PAGE LEVEL STYLES -->
\t<link rel=\"shortcut icon\" href=\"favicon.ico\" />
\t

   
";
        // line 30
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 33
        echo "    </head>             
  <body class=\"login\">  
       <!-- BEGIN HEADER -->
   
";
        // line 37
        $this->displayBlock('body', $context, $blocks);
        // line 41
        echo " 
 <!-- BEGIN FOOTER -->
   <div class=\"copyright\">
\t\t2014&copy; Adventure Bidding.
   </div>
   <!-- END FOOTER --> 
   
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
\t<!-- BEGIN CORE PLUGINS -->   
        <script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/plugins/jquery-1.10.1.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/plugins/jquery-migrate-1.2.1.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
\t<script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>      
\t<script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/plugins/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<!-- <script src=\"assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js\" type=\"text/javascript\" ></script> -->
\t<!--[if lt IE 9]>
\t<script src=\"assets/plugins/excanvas.min.js\"></script>
\t<script src=\"assets/plugins/respond.min.js\"></script>  
\t<![endif]-->   
\t<!-- <script src=\"assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js\" type=\"text/javascript\"></script>
\t
\t<script src=\"assets/plugins/uniform/jquery.uniform.min.js\" type=\"text/javascript\" ></script> -->
\t<!-- END CORE PLUGINS -->
\t<!-- BEGIN PAGE LEVEL PLUGINS -->
\t<script src=\"assets/plugins/jquery-validation/dist/jquery.validate.min.js\" type=\"text/javascript\"></script>\t
\t<script type=\"text/javascript\" src=\"assets/plugins/select2/select2.min.js\"></script>     
\t<!-- END PAGE LEVEL PLUGINS -->
\t<!-- BEGIN PAGE LEVEL SCRIPTS -->
\t<script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/scripts/app.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets1/scripts/login.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script> 
\t<!-- END PAGE LEVEL SCRIPTS --> 
\t<script>
\t\tjQuery(document).ready(function() {     
\t\t  App.init();
\t\t  Login.init();
\t\t});
\t</script>
\t<!-- END JAVASCRIPTS -->
        
        
";
        // line 81
        $this->displayBlock('javascripts', $context, $blocks);
        // line 85
        echo "</body>
</html>
";
    }

    // line 30
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 31
        echo " 
";
    }

    // line 37
    public function block_body($context, array $blocks = array())
    {
        // line 38
        echo "

";
    }

    // line 81
    public function block_javascripts($context, array $blocks = array())
    {
        // line 82
        echo "

";
    }

    public function getTemplateName()
    {
        return "::registerationbase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 82,  175 => 81,  161 => 31,  150 => 81,  114 => 54,  110 => 53,  90 => 41,  127 => 87,  124 => 86,  97 => 61,  290 => 151,  286 => 150,  280 => 147,  276 => 146,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 135,  236 => 133,  231 => 131,  197 => 103,  191 => 99,  180 => 89,  165 => 75,  146 => 65,  65 => 21,  53 => 18,  152 => 85,  148 => 84,  134 => 73,  76 => 26,  70 => 33,  34 => 11,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 140,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 38,  140 => 55,  132 => 69,  128 => 49,  107 => 36,  61 => 20,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 134,  238 => 85,  235 => 74,  230 => 82,  227 => 130,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 109,  179 => 69,  159 => 61,  143 => 64,  135 => 53,  119 => 42,  102 => 46,  71 => 24,  67 => 15,  63 => 15,  59 => 14,  94 => 28,  89 => 20,  85 => 40,  75 => 17,  68 => 14,  56 => 18,  87 => 25,  28 => 5,  93 => 28,  88 => 37,  78 => 21,  27 => 7,  46 => 7,  44 => 15,  31 => 8,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 81,  166 => 37,  163 => 62,  158 => 30,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 70,  121 => 46,  117 => 44,  105 => 51,  91 => 27,  62 => 23,  49 => 17,  21 => 2,  19 => 1,  79 => 36,  72 => 16,  69 => 25,  47 => 9,  40 => 14,  37 => 14,  22 => 1,  246 => 90,  157 => 56,  145 => 46,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 57,  111 => 37,  108 => 36,  101 => 50,  98 => 46,  96 => 43,  83 => 25,  74 => 14,  66 => 34,  55 => 15,  52 => 17,  50 => 24,  43 => 6,  41 => 7,  35 => 5,  32 => 4,  29 => 6,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 76,  164 => 59,  162 => 57,  154 => 58,  149 => 66,  147 => 58,  144 => 49,  141 => 48,  133 => 60,  130 => 41,  125 => 44,  122 => 55,  116 => 41,  112 => 51,  109 => 54,  106 => 36,  103 => 32,  99 => 31,  95 => 45,  92 => 44,  86 => 28,  82 => 33,  80 => 30,  73 => 34,  64 => 20,  60 => 19,  57 => 19,  54 => 10,  51 => 14,  48 => 16,  45 => 16,  42 => 7,  39 => 9,  36 => 13,  33 => 3,  30 => 2,);
    }
}
