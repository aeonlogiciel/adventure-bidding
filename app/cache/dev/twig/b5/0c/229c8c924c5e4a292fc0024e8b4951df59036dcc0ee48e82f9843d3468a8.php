<?php

/* AdventureBiddingBundle:User:dashboardUser.html.twig */
class __TwigTemplate_b50c229c8c924c5e4a292fc0024e8b4951df59036dcc0ee48e82f9843d3468a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::userbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::userbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class=\"fixed-top\">
  
   <!-- BEGIN CONTAINER -->
   <div id=\"container\" class=\"row-fluid\">
      
      <!-- BEGIN PAGE -->  
      <div id=\"main-content\">
         <!-- BEGIN PAGE CONTAINER-->
         <div class=\"container-fluid\">
            <!-- BEGIN PAGE HEADER-->   
            <div class=\"row-fluid\">
               <div class=\"span12\">
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class=\"page-title\">
                    Recent Bidding Information
                   </h3>
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("adventure_home");
        echo "\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           Request List
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
           <div id=\"page-wraper\">
               <div class=\"row-fluid\">
                <div class=\"span12\">
                <!-- BEGIN EXAMPLE TABLE widget-->
                
                <div class=\"widget blue\">
                    <div class=\"widget-title\">
                        <h4><i class=\"icon-search\"></i> Filter Your Search</h4>
                         
                    </div>
                    <div class=\"widget-body\">
                         <form class=\"form-inline\" method=\"post\">
                         <div class=\"row-fluid\">
                         
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Filter Search</label>
                          <div class=\"controls\">
                                    <select class=\"span8 \" data-placeholder=\"Choose a Category\" tabindex=\"1\">
                                        <option value=\"\">Select...</option>
                                        <option value=\"Activities\">Activities</option>
                                        <option value=\"Zones\">Zones</option>
                                        <option value=\"Category 3\">Category 5</option>
                                        <option value=\"Category 4\">Category 4</option>
                                    </select>
                                </div>
                          
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          <label class=\"control-label\">Search</label>
                           <div class=\"controls\">
                          <input type=\"text\" class=\"span8\"/>   
                          </div>
                          </div>
                         </div>
                         <div class=\"span4\">
                          <div class=\"control-group\">
                          
                          <label class=\"control-label\"></label>
                          <div class=\"controls\">
                <button type=\"submit\" class=\"btn btn-primary\"><i class=\"icon-ok\"></i> Submit</button>
                         <button type=\"button\" class=\"btn\">Cancel</button>
                
               </div>
                          </div>
                         </div>
                         
                         
                         </div>
                         
                         </form>
                        
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
                <div class=\"row-fluid\">
                    <!--marquee start here-->
                    <div class=\"span12\">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class=\"widget blue\">
                            <div class=\"widget-title\">
                                <h4>Bidding Info</h4>
                            </div>
                            <div class=\"widget-body\">
                                 <table class=\"table table-striped table-bordered  table-hover\" id=\"sample_1\">
                                    <thead>
                                    <tr>
                                    \t <th class=\"hidden-phone\">Sr. No.</th>
                                         <th>Your Request</th>
                                         <th>Option</th>
                                       \t <th>Status</th>
                                         <th>Received Bid</th>
                                         <th>Organiser Response</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        ";
        // line 124
        $context["count"] = "1";
        // line 125
        echo "                                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "requestUser"));
        foreach ($context['_seq'] as $context["_key"] => $context["requestUser"]) {
            // line 126
            echo "                                    <tr>
                                    \t <td class=\"hidden-phone\">";
            // line 127
            echo twig_escape_filter($this->env, $this->getContext($context, "count"), "html", null, true);
            echo "</td>
                                         <td>";
            // line 128
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "requestUser"), "trip"), "html", null, true);
            echo "</td>
                                         ";
            // line 129
            if (($this->getAttribute($this->getContext($context, "requestUser"), "status") == "0")) {
                // line 130
                echo "                                         <td> ";
                // line 132
                echo "                                             <a title=\"Request Closed\" value=\"Request Closed\">Conformed</a>
                                        </td>
                                        <td><span class=\"label label-success label-mini\">Close</span></td>
                                            <td><a href=\"";
                // line 135
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_user_view_close_request", array("id" => $this->getAttribute($this->getContext($context, "requestUser"), "reqid"))), "html", null, true);
                echo "\" class=\"btn btn-success\" title='View Detail'>Click to See</a></td>
                                            
                                            
                                         ";
            } else {
                // line 139
                echo "                                            <td> <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_user_edit_request", array("id" => $this->getAttribute($this->getContext($context, "requestUser"), "reqid"))), "html", null, true);
                echo "\" title='Edit' class=\"btn btn-primary\"><i class=\"icon-pencil\"></i></a>
                                            
                                                <a onclick=\"return confirm('Are you sure you want to delete this member ?')\"href=\"";
                // line 141
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_user_delete_request", array("id" => $this->getAttribute($this->getContext($context, "requestUser"), "reqid"))), "html", null, true);
                echo "\" title='Delete'><button class=\"btn btn-danger\" onclick=\"return conformed('Are You sure you want to delete')\"><i class=\"icon-trash \"></i></button></a>
                                        </td>
                                        <td><span class=\"label label-important label-mini\">Open</span></td>
                                         <td><a href=\"";
                // line 144
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_user_view_open_request", array("id" => $this->getAttribute($this->getContext($context, "requestUser"), "reqid"))), "html", null, true);
                echo "\" class=\"btn btn-success\" title='View Detail'>Click to See</a></td>
                                         ";
            }
            // line 146
            echo "                                         ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "requestUser"), "messagecount"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 147
                echo "                                            <td><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("adventure_bidding_user_view_open_request", array("id" => $this->getAttribute($this->getContext($context, "requestUser"), "reqid"))), "html", null, true);
                echo "\">
                                            <input type=\"button\" class=\"btn-danger\" value=\"";
                // line 148
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "message"), 1, array(), "array"), "html", null, true);
                echo "\">&nbsp;&nbsp;New message</a></td>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 150
            echo "                                        
                                    </tr>
                                    ";
            // line 152
            $context["count"] = ($this->getContext($context, "count") + "1");
            // line 153
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['requestUser'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 154
        echo "                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
                         <!--marquee End here-->
                </div>
                
                
            </div>

            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
</body>
";
    }

    // line 177
    public function block_javascripts($context, array $blocks = array())
    {
        // line 178
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:User:dashboardUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  276 => 178,  273 => 177,  248 => 154,  242 => 153,  240 => 152,  236 => 150,  228 => 148,  223 => 147,  218 => 146,  213 => 144,  207 => 141,  201 => 139,  194 => 135,  189 => 132,  187 => 130,  185 => 129,  181 => 128,  177 => 127,  174 => 126,  169 => 125,  167 => 124,  71 => 31,  49 => 11,  46 => 10,  40 => 7,  34 => 4,  31 => 3,);
    }
}
