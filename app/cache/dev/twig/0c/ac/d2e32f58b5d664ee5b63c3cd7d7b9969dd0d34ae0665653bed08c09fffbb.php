<?php

/* ::admin.html.twig */
class __TwigTemplate_0cacd2e32f58b5d664ee5b63c3cd7d7b9969dd0d34ae0665653bed08c09fffbb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 8]> <html lang=\"en\" class=\"ie8\"> <![endif]-->
<!--[if IE 9]> <html lang=\"en\" class=\"ie9\"> <![endif]-->
<!--[if !IE]><!--> <html lang=\"en\"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset=\"utf-8\" />
   <title>Welcome to Adventure Bidding System</title>
   <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\" />
   <meta content=\"\" name=\"description\" />
   <meta content=\"Mosaddek\" name=\"author\" />
   <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/css/bootstrap-fileupload.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/font-awesome/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style-responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/css/style-default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"style_color\" />
   <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/chosen-bootstrap/chosen/chosen.css"), "html", null, true);
        echo "\" />
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-tags-input/jquery.tagsinput.css"), "html", null, true);
        echo "\" />
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" />
   <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/data-tables/DT_bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
   <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap-datepicker/css/datepicker.css"), "html", null, true);
        echo "\" />
   ";
        // line 25
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 28
        echo "</head>

<!-- END HEAD -->

   <!-- BEGIN HEADER -->
   <div id=\"header\" class=\"navbar navbar-inverse navbar-fixed-top\">
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class=\"navbar-inner\">
           <div class=\"container-fluid\">
               <!--BEGIN SIDEBAR TOGGLE-->
               <div class=\"sidebar-toggle-box hidden-phone\">
                   <div class=\"icon-reorder\"></div>
               </div>
               <!--END SIDEBAR TOGGLE-->
               <!-- BEGIN LOGO -->
               <a class=\"brand\" href=\"\">
                   <h2 class=\"hreaderbrand\">Adventure Bidding System</h2>
               </a>
               <!-- END LOGO -->
               <!-- BEGIN RESPONSIVE MENU TOGGLER -->
               <a class=\"btn btn-navbar collapsed\" id=\"main_menu_trigger\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
                   <span class=\"icon-bar\"></span>
                   <span class=\"icon-bar\"></span>
                   <span class=\"icon-bar\"></span>
                   <span class=\"arrow\"></span>
               </a>
               <!-- END RESPONSIVE MENU TOGGLER -->
               
               <!-- END  NOTIFICATION -->
               <div class=\"top-nav \">
                   <ul class=\"nav pull-right top-menu\" >
                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       <li class=\"dropdown\">
                           <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                               <img src=\"img/avatar1_small.jpg\" alt=\"userimg\">
                               <span class=\"username\">";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "username"), "html", null, true);
        echo "</span>
                               <b class=\"caret\"></b>
                           </a>
                           <ul class=\"dropdown-menu extended logout\">
                               ";
        // line 69
        echo "                               <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("log_out");
        echo "\"><i class=\"icon-key\"></i> Log Out</a></li>
                           </ul>
                       </li>
                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                   <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id=\"container\" class=\"row-fluid\">
      <!-- BEGIN SIDEBAR -->
      <div class=\"sidebar-scroll\">
        <div id=\"sidebar\" class=\"nav-collapse collapse\">

         <!-- BEGIN SIDEBAR MENU -->
          <ul class=\"sidebar-menu\">
          
          \t\t<li class=\"sub-menu active\">
              \t <a class=\"\" href=\"";
        // line 91
        echo $this->env->getExtension('routing')->getPath("adventure_admin_dashboard");
        echo "\">
              \t <i class=\"icon-list-alt\"></i>
                      <span>All Request</span>
                 </a>
              </li>
              ";
        // line 107
        echo "               <li class=\"sub-menu\">
              \t<a class=\"\" href=\"";
        // line 108
        echo $this->env->getExtension('routing')->getPath("adventure_admin_user_list");
        echo "\">
              \t <i class=\"icon-user\"></i>
                      <span>User List</span>
                 </a>
                
              </li>
               <li class=\"sub-menu\">
              \t<a class=\"\" href=\"";
        // line 115
        echo $this->env->getExtension('routing')->getPath("adventure_admin_organiser_list");
        echo "\">
              \t <i class=\"icon-user\"></i>
                      <span>Organiser list</span>
                 </a>
                
              </li>
              <li class=\"sub-menu\">
              \t<a class=\"\" href=\"";
        // line 122
        echo $this->env->getExtension('routing')->getPath("adventure_admin_feedback_list");
        echo "\" >
              \t <i class=\"icon-plus-sign-alt\"></i>
                      <span>Feedback</span>
                 </a>
                
              </li>
              
              ";
        // line 136
        echo "              <li class=\"sub-menu\">
              \t<a class=\"\" href=\"";
        // line 137
        echo $this->env->getExtension('routing')->getPath("adventure_change_password");
        echo "\">
              \t <i class=\"icon-edit\"></i>
                      <span>Change Password</span>
                 </a>
                
              </li>
              
              
             
              <li>
                  <a class=\"\" href=\"";
        // line 147
        echo $this->env->getExtension('routing')->getPath("log_out");
        echo "\">
                    <i class=\"icon-key\"></i>
                    <span>Log Out</span>
                  </a>
              </li>
          </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      ";
        // line 157
        $this->displayBlock('body', $context, $blocks);
        // line 160
        echo "   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id=\"footer\">
       2014 &copy; Adventure Bidding System
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/marquee.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery.marquee.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/jquery.nicescroll.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
   <script src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
   <script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-slimscroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" ></script>
   <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/common-scripts.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/jquery-tags-input/jquery.tagsinput.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap/js/bootstrap-fileupload.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/form-component.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/data-tables/jquery.dataTables.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/data-tables/DT_bootstrap.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/js/dynamic-table.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\" src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("home/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script> 
";
        // line 189
        $this->displayBlock('javascripts', $context, $blocks);
        // line 193
        echo "   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>";
    }

    // line 25
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 26
        echo " 
";
    }

    // line 157
    public function block_body($context, array $blocks = array())
    {
        // line 158
        echo "
";
    }

    // line 189
    public function block_javascripts($context, array $blocks = array())
    {
        // line 190
        echo "

";
    }

    public function getTemplateName()
    {
        return "::admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  344 => 190,  336 => 158,  333 => 157,  328 => 26,  325 => 25,  318 => 193,  316 => 189,  308 => 187,  304 => 186,  300 => 185,  296 => 184,  292 => 183,  288 => 182,  284 => 181,  272 => 178,  260 => 175,  256 => 174,  248 => 172,  216 => 147,  200 => 136,  190 => 122,  170 => 108,  126 => 63,  211 => 127,  181 => 100,  129 => 66,  279 => 148,  275 => 147,  265 => 143,  261 => 142,  257 => 141,  222 => 115,  188 => 93,  167 => 107,  178 => 82,  175 => 84,  161 => 31,  150 => 81,  114 => 54,  110 => 53,  90 => 41,  127 => 87,  124 => 86,  97 => 61,  290 => 151,  286 => 150,  280 => 180,  276 => 179,  270 => 143,  266 => 142,  262 => 141,  253 => 138,  249 => 137,  244 => 171,  236 => 133,  231 => 160,  197 => 103,  191 => 99,  180 => 115,  165 => 75,  146 => 65,  65 => 21,  53 => 18,  152 => 85,  148 => 68,  134 => 73,  76 => 26,  70 => 33,  34 => 4,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 189,  337 => 103,  322 => 101,  314 => 99,  312 => 188,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 177,  264 => 176,  258 => 140,  252 => 173,  247 => 78,  241 => 77,  229 => 157,  220 => 70,  214 => 128,  177 => 65,  169 => 91,  140 => 55,  132 => 67,  128 => 49,  107 => 36,  61 => 20,  273 => 96,  269 => 144,  254 => 92,  243 => 88,  240 => 134,  238 => 85,  235 => 74,  230 => 82,  227 => 117,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 109,  179 => 69,  159 => 91,  143 => 64,  135 => 68,  119 => 62,  102 => 46,  71 => 21,  67 => 20,  63 => 19,  59 => 18,  94 => 28,  89 => 28,  85 => 40,  75 => 22,  68 => 24,  56 => 18,  87 => 25,  28 => 5,  93 => 28,  88 => 48,  78 => 21,  27 => 7,  46 => 10,  44 => 15,  31 => 3,  38 => 6,  26 => 6,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 82,  171 => 81,  166 => 37,  163 => 62,  158 => 30,  156 => 66,  151 => 77,  142 => 66,  138 => 69,  136 => 70,  121 => 57,  117 => 44,  105 => 51,  91 => 27,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 23,  72 => 16,  69 => 25,  47 => 15,  40 => 7,  37 => 14,  22 => 1,  246 => 133,  157 => 73,  145 => 67,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 57,  111 => 53,  108 => 57,  101 => 48,  98 => 53,  96 => 43,  83 => 24,  74 => 14,  66 => 34,  55 => 17,  52 => 17,  50 => 24,  43 => 14,  41 => 15,  35 => 12,  32 => 4,  29 => 6,  209 => 82,  203 => 137,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 76,  164 => 77,  162 => 57,  154 => 78,  149 => 66,  147 => 58,  144 => 73,  141 => 48,  133 => 69,  130 => 41,  125 => 44,  122 => 55,  116 => 41,  112 => 51,  109 => 54,  106 => 36,  103 => 32,  99 => 31,  95 => 45,  92 => 44,  86 => 28,  82 => 33,  80 => 30,  73 => 26,  64 => 20,  60 => 19,  57 => 19,  54 => 10,  51 => 16,  48 => 16,  45 => 16,  42 => 7,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
