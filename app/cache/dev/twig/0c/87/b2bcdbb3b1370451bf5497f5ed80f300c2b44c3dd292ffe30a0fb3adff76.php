<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class __TwigTemplate_0c87b2bcdbb3b1370451bf5497f5ed80f300c2b44c3dd292ffe30a0fb3adff76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ".sf-reset .traces {
    padding-bottom: 14px;
}
.sf-reset .traces li {
    font-size: 12px;
    color: #868686;
    padding: 5px 4px;
    list-style-type: decimal;
    margin-left: 20px;
    white-space: break-word;
}
.sf-reset #logs .traces li.error {
    font-style: normal;
    color: #AA3333;
    background: #f9ecec;
}
.sf-reset #logs .traces li.warning {
    font-style: normal;
    background: #ffcc00;
}
/* fix for Opera not liking empty <li> */
.sf-reset .traces li:after {
    content: \"\\00A0\";
}
.sf-reset .trace {
    border: 1px solid #D3D3D3;
    padding: 10px;
    overflow: auto;
    margin: 10px 0 20px;
}
.sf-reset .block-exception {
    border-radius: 16px;
    margin-bottom: 20px;
    background-color: #f6f6f6;
    border: 1px solid #dfdfdf;
    padding: 30px 28px;
    word-wrap: break-word;
    overflow: hidden;
}
.sf-reset .block-exception div {
    color: #313131;
    font-size: 10px;
}
.sf-reset .block-exception-detected .illustration-exception,
.sf-reset .block-exception-detected .text-exception {
    float: left;
}
.sf-reset .block-exception-detected .illustration-exception {
    width: 152px;
}
.sf-reset .block-exception-detected .text-exception {
    width: 670px;
    padding: 30px 44px 24px 46px;
    position: relative;
}
.sf-reset .text-exception .open-quote,
.sf-reset .text-exception .close-quote {
    position: absolute;
}
.sf-reset .open-quote {
    top: 0;
    left: 0;
}
.sf-reset .close-quote {
    bottom: 0;
    right: 50px;
}
.sf-reset .block-exception p {
    font-family: Arial, Helvetica, sans-serif;
}
.sf-reset .block-exception p a,
.sf-reset .block-exception p a:hover {
    color: #565656;
}
.sf-reset .logs h2 {
    float: left;
    width: 654px;
}
.sf-reset .error-count {
    float: right;
    width: 170px;
    text-align: right;
}
.sf-reset .error-count span {
    display: inline-block;
    background-color: #aacd4e;
    border-radius: 6px;
    padding: 4px;
    color: white;
    margin-right: 2px;
    font-size: 11px;
    font-weight: bold;
}
.sf-reset .toggle {
    vertical-align: middle;
}
.sf-reset .linked ul,
.sf-reset .linked li {
    display: inline;
}
.sf-reset #output-content {
    color: #000;
    font-size: 12px;
}
";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function getDebugInfo()
    {
        return array (  672 => 345,  668 => 344,  664 => 342,  651 => 337,  647 => 336,  644 => 335,  640 => 334,  631 => 327,  626 => 325,  613 => 320,  609 => 319,  591 => 309,  588 => 308,  585 => 307,  581 => 305,  577 => 303,  569 => 300,  563 => 298,  559 => 296,  552 => 293,  545 => 291,  541 => 290,  533 => 284,  531 => 283,  525 => 280,  519 => 278,  515 => 276,  509 => 272,  499 => 268,  489 => 262,  479 => 256,  471 => 253,  465 => 249,  459 => 246,  448 => 240,  428 => 230,  422 => 226,  418 => 224,  383 => 207,  327 => 114,  303 => 106,  291 => 102,  810 => 492,  807 => 491,  796 => 489,  792 => 488,  788 => 486,  775 => 485,  749 => 479,  746 => 478,  727 => 476,  710 => 475,  706 => 473,  702 => 472,  698 => 471,  694 => 470,  686 => 468,  682 => 467,  679 => 466,  677 => 465,  660 => 340,  649 => 462,  634 => 456,  629 => 326,  625 => 453,  622 => 323,  620 => 451,  606 => 318,  601 => 446,  567 => 414,  549 => 411,  532 => 410,  527 => 281,  522 => 279,  517 => 404,  389 => 160,  371 => 156,  361 => 195,  458 => 253,  375 => 201,  364 => 195,  332 => 116,  690 => 469,  687 => 609,  100 => 39,  653 => 349,  650 => 348,  627 => 327,  612 => 325,  593 => 310,  589 => 305,  575 => 294,  557 => 295,  546 => 274,  537 => 268,  520 => 257,  505 => 270,  486 => 228,  481 => 225,  467 => 218,  460 => 214,  429 => 229,  426 => 199,  411 => 194,  405 => 192,  399 => 190,  397 => 213,  380 => 206,  376 => 205,  212 => 78,  500 => 323,  497 => 267,  456 => 284,  351 => 192,  339 => 179,  335 => 210,  311 => 164,  226 => 119,  417 => 196,  386 => 159,  367 => 198,  320 => 176,  223 => 147,  373 => 147,  370 => 146,  357 => 123,  301 => 181,  289 => 158,  281 => 114,  277 => 154,  504 => 354,  501 => 353,  434 => 293,  412 => 222,  404 => 275,  390 => 208,  354 => 228,  343 => 146,  329 => 188,  295 => 178,  118 => 49,  369 => 241,  313 => 183,  297 => 179,  293 => 120,  287 => 173,  234 => 142,  271 => 162,  259 => 103,  232 => 88,  473 => 254,  470 => 332,  349 => 188,  324 => 113,  160 => 86,  239 => 155,  84 => 40,  77 => 20,  602 => 317,  599 => 349,  566 => 288,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 409,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 232,  485 => 285,  477 => 283,  475 => 282,  438 => 236,  420 => 197,  408 => 193,  403 => 241,  382 => 183,  377 => 216,  372 => 215,  346 => 200,  342 => 184,  299 => 162,  263 => 95,  255 => 93,  250 => 135,  215 => 110,  153 => 77,  503 => 355,  455 => 252,  446 => 305,  436 => 235,  425 => 289,  414 => 195,  406 => 277,  392 => 222,  356 => 230,  345 => 147,  331 => 140,  363 => 126,  358 => 151,  350 => 196,  330 => 176,  326 => 138,  310 => 171,  306 => 107,  302 => 125,  282 => 179,  274 => 97,  251 => 136,  551 => 325,  548 => 292,  526 => 260,  506 => 293,  492 => 279,  483 => 258,  463 => 248,  454 => 244,  443 => 245,  431 => 230,  416 => 227,  410 => 221,  400 => 214,  394 => 217,  378 => 157,  366 => 174,  353 => 193,  347 => 191,  340 => 145,  338 => 182,  334 => 141,  321 => 112,  317 => 185,  315 => 110,  307 => 128,  245 => 141,  242 => 132,  228 => 140,  206 => 132,  202 => 77,  194 => 68,  113 => 48,  218 => 136,  210 => 77,  186 => 127,  172 => 64,  81 => 38,  192 => 114,  174 => 65,  155 => 47,  23 => 3,  233 => 145,  184 => 101,  137 => 72,  58 => 14,  225 => 115,  213 => 78,  205 => 108,  198 => 130,  104 => 32,  237 => 141,  207 => 108,  195 => 105,  185 => 74,  344 => 119,  336 => 181,  333 => 175,  328 => 139,  325 => 162,  318 => 111,  316 => 205,  308 => 150,  304 => 181,  300 => 180,  296 => 121,  292 => 159,  288 => 176,  284 => 165,  272 => 152,  260 => 141,  256 => 137,  248 => 97,  216 => 79,  200 => 130,  190 => 76,  170 => 96,  126 => 60,  211 => 109,  181 => 100,  129 => 66,  279 => 148,  275 => 147,  265 => 96,  261 => 157,  257 => 147,  222 => 83,  188 => 102,  167 => 124,  178 => 66,  175 => 65,  161 => 63,  150 => 81,  114 => 71,  110 => 22,  90 => 26,  127 => 35,  124 => 67,  97 => 41,  290 => 119,  286 => 142,  280 => 155,  276 => 111,  270 => 178,  266 => 167,  262 => 166,  253 => 100,  249 => 145,  244 => 133,  236 => 142,  231 => 83,  197 => 104,  191 => 67,  180 => 69,  165 => 83,  146 => 78,  65 => 18,  53 => 11,  152 => 46,  148 => 90,  134 => 54,  76 => 34,  70 => 15,  34 => 5,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 245,  453 => 209,  444 => 238,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 198,  413 => 234,  409 => 132,  407 => 131,  402 => 215,  398 => 211,  393 => 211,  387 => 187,  384 => 121,  381 => 202,  379 => 182,  374 => 116,  368 => 197,  365 => 197,  362 => 26,  360 => 173,  355 => 150,  341 => 189,  337 => 176,  322 => 178,  314 => 172,  312 => 109,  309 => 108,  305 => 163,  298 => 161,  294 => 199,  285 => 175,  283 => 100,  278 => 98,  268 => 167,  264 => 164,  258 => 94,  252 => 146,  247 => 135,  241 => 93,  229 => 87,  220 => 126,  214 => 134,  177 => 124,  169 => 91,  140 => 58,  132 => 40,  128 => 73,  107 => 62,  61 => 15,  273 => 174,  269 => 107,  254 => 92,  243 => 92,  240 => 143,  238 => 154,  235 => 85,  230 => 141,  227 => 86,  224 => 81,  221 => 113,  219 => 115,  217 => 111,  208 => 76,  204 => 109,  179 => 98,  159 => 90,  143 => 51,  135 => 62,  119 => 40,  102 => 40,  71 => 31,  67 => 20,  63 => 18,  59 => 16,  94 => 21,  89 => 28,  85 => 23,  75 => 19,  68 => 30,  56 => 11,  87 => 41,  28 => 3,  93 => 27,  88 => 25,  78 => 26,  27 => 3,  46 => 13,  44 => 10,  31 => 4,  38 => 7,  26 => 9,  24 => 4,  25 => 35,  201 => 106,  196 => 92,  183 => 126,  171 => 69,  166 => 95,  163 => 82,  158 => 80,  156 => 62,  151 => 59,  142 => 78,  138 => 42,  136 => 48,  121 => 50,  117 => 39,  105 => 25,  91 => 33,  62 => 12,  49 => 14,  21 => 2,  19 => 1,  79 => 21,  72 => 17,  69 => 16,  47 => 11,  40 => 6,  37 => 5,  22 => 1,  246 => 136,  157 => 89,  145 => 52,  139 => 49,  131 => 45,  123 => 61,  120 => 31,  115 => 69,  111 => 47,  108 => 19,  101 => 31,  98 => 30,  96 => 37,  83 => 33,  74 => 27,  66 => 26,  55 => 13,  52 => 12,  50 => 22,  43 => 12,  41 => 8,  35 => 6,  32 => 5,  29 => 3,  209 => 82,  203 => 73,  199 => 93,  193 => 113,  189 => 66,  187 => 75,  182 => 87,  176 => 86,  173 => 85,  168 => 76,  164 => 77,  162 => 88,  154 => 60,  149 => 79,  147 => 75,  144 => 42,  141 => 73,  133 => 61,  130 => 60,  125 => 42,  122 => 41,  116 => 57,  112 => 36,  109 => 35,  106 => 51,  103 => 58,  99 => 23,  95 => 47,  92 => 27,  86 => 28,  82 => 28,  80 => 27,  73 => 33,  64 => 13,  60 => 16,  57 => 12,  54 => 18,  51 => 13,  48 => 9,  45 => 9,  42 => 7,  39 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
