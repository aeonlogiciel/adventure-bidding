<?php

/* AdventureBiddingBundle:Organiser:registerOrganiser.html.twig */
class __TwigTemplate_cd5e278c1826f9432707e694f3be71bf6b943f2f9742fb068eeb85412bb89850 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Complete ORGANISER Registeration : 


";
        // line 4
        if (array_key_exists("message", $context)) {
            // line 5
            echo "      ";
            echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
            echo "
";
        }
        // line 7
        echo "


";
        // line 10
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_start');
        echo "
    <br>
    Name : ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "name"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "name"), 'errors');
        echo "<br>
    Email : ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "email"), 'errors');
        echo "<br>
    Phone : ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "phone"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "phone"), 'errors');
        echo "<br>
    Address : ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "address"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "address"), 'errors');
        echo "<br>
    Zip : ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zip"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zip"), 'errors');
        echo "<br>
    Country : ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "country"), 'errors');
        echo "<br>
    Activity : ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "activity"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "activity"), 'errors');
        echo "<br>
    Zone : ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zone"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "Form"), "zone"), 'errors');
        echo "<br>

    ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "Form"), 'rest');
        echo "
    <button class=\"btn btn-success\" id=\"save\" type=\"submit\"> <i class=\"icon-ok\"></i>Submit</button>
";
        // line 23
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "Form"), 'form_end');
        echo "
    
    ";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:Organiser:registerOrganiser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 318,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 286,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 238,  408 => 232,  403 => 230,  382 => 218,  377 => 216,  372 => 214,  346 => 200,  342 => 198,  299 => 171,  263 => 155,  255 => 152,  250 => 150,  215 => 122,  153 => 86,  503 => 355,  455 => 310,  446 => 305,  436 => 251,  425 => 289,  414 => 283,  406 => 277,  392 => 222,  356 => 230,  345 => 224,  331 => 215,  363 => 178,  358 => 147,  350 => 201,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 164,  274 => 161,  251 => 146,  551 => 325,  548 => 324,  526 => 304,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 219,  394 => 217,  378 => 204,  366 => 179,  353 => 190,  347 => 25,  340 => 197,  338 => 178,  334 => 177,  321 => 174,  317 => 172,  315 => 171,  307 => 169,  245 => 125,  242 => 124,  228 => 115,  206 => 107,  202 => 106,  194 => 104,  113 => 56,  218 => 110,  210 => 108,  186 => 94,  172 => 96,  81 => 34,  192 => 114,  174 => 66,  155 => 59,  23 => 1,  233 => 113,  184 => 76,  137 => 80,  58 => 11,  225 => 126,  213 => 78,  205 => 21,  198 => 117,  104 => 49,  237 => 170,  207 => 120,  195 => 145,  185 => 97,  344 => 190,  336 => 158,  333 => 157,  328 => 188,  325 => 25,  318 => 207,  316 => 189,  308 => 187,  304 => 173,  300 => 185,  296 => 184,  292 => 183,  288 => 166,  284 => 165,  272 => 178,  260 => 153,  256 => 174,  248 => 149,  216 => 79,  200 => 102,  190 => 103,  170 => 108,  126 => 75,  211 => 121,  181 => 96,  129 => 66,  279 => 148,  275 => 147,  265 => 143,  261 => 142,  257 => 141,  222 => 101,  188 => 93,  167 => 62,  178 => 73,  175 => 91,  161 => 91,  150 => 85,  114 => 54,  110 => 53,  90 => 30,  127 => 59,  124 => 58,  97 => 61,  290 => 166,  286 => 165,  280 => 146,  276 => 179,  270 => 161,  266 => 160,  262 => 134,  253 => 149,  249 => 137,  244 => 171,  236 => 133,  231 => 160,  197 => 103,  191 => 99,  180 => 69,  165 => 82,  146 => 69,  65 => 21,  53 => 19,  152 => 60,  148 => 78,  134 => 65,  76 => 33,  70 => 38,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 234,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 220,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 237,  365 => 111,  362 => 110,  360 => 204,  355 => 146,  341 => 189,  337 => 103,  322 => 185,  314 => 172,  312 => 170,  309 => 175,  305 => 168,  298 => 168,  294 => 169,  285 => 185,  283 => 88,  278 => 162,  268 => 177,  264 => 176,  258 => 140,  252 => 151,  247 => 78,  241 => 77,  229 => 112,  220 => 111,  214 => 109,  177 => 95,  169 => 95,  140 => 67,  132 => 50,  128 => 49,  107 => 47,  61 => 20,  273 => 96,  269 => 159,  254 => 92,  243 => 88,  240 => 171,  238 => 154,  235 => 74,  230 => 82,  227 => 182,  224 => 181,  221 => 109,  219 => 123,  217 => 75,  208 => 22,  204 => 109,  179 => 95,  159 => 77,  143 => 68,  135 => 63,  119 => 50,  102 => 46,  71 => 21,  67 => 26,  63 => 24,  59 => 18,  94 => 30,  89 => 28,  85 => 33,  75 => 22,  68 => 28,  56 => 18,  87 => 25,  28 => 3,  93 => 45,  88 => 38,  78 => 18,  27 => 7,  46 => 10,  44 => 11,  31 => 3,  38 => 6,  26 => 5,  24 => 4,  25 => 3,  201 => 102,  196 => 90,  183 => 82,  171 => 69,  166 => 67,  163 => 61,  158 => 78,  156 => 87,  151 => 74,  142 => 67,  138 => 70,  136 => 70,  121 => 59,  117 => 43,  105 => 51,  91 => 21,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 32,  72 => 17,  69 => 33,  47 => 17,  40 => 15,  37 => 10,  22 => 1,  246 => 133,  157 => 63,  145 => 67,  139 => 65,  131 => 62,  123 => 58,  120 => 44,  115 => 49,  111 => 48,  108 => 57,  101 => 34,  98 => 33,  96 => 23,  83 => 40,  74 => 30,  66 => 16,  55 => 17,  52 => 17,  50 => 24,  43 => 16,  41 => 15,  35 => 14,  32 => 7,  29 => 5,  209 => 82,  203 => 119,  199 => 80,  193 => 98,  189 => 98,  187 => 84,  182 => 66,  176 => 64,  173 => 94,  168 => 76,  164 => 77,  162 => 91,  154 => 78,  149 => 70,  147 => 84,  144 => 70,  141 => 48,  133 => 61,  130 => 60,  125 => 60,  122 => 55,  116 => 71,  112 => 51,  109 => 43,  106 => 66,  103 => 49,  99 => 45,  95 => 42,  92 => 29,  86 => 28,  82 => 33,  80 => 30,  73 => 29,  64 => 10,  60 => 15,  57 => 21,  54 => 14,  51 => 18,  48 => 13,  45 => 10,  42 => 12,  39 => 13,  36 => 13,  33 => 13,  30 => 2,);
    }
}
