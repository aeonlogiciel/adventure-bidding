<?php

/* AdventureBiddingBundle:User:confirmDealUser.html.twig */
class __TwigTemplate_50cca09f843abdbab39d650e74b172cfaf296f792844b2252ede5b2e05217f47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::userbase.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::userbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome to Adventure Bidding System";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <!-- BEGIN BODY -->
<body class=\"fixed-top\">

<!-- BEGIN CONTAINER -->
<div id=\"container\" class=\"row-fluid\"> 
  
  <!-- BEGIN PAGE -->
  <div id=\"main-content\"> 
    <!-- BEGIN PAGE CONTAINER-->
    <div class=\"container-fluid\"> 
      <!-- BEGIN PAGE HEADER-->
      <div class=\"row-fluid\">
        <div class=\"span12\"> 
          
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class=\"page-title\">
                    Confirm Payment Page
                   </h3> 
                   <ul class=\"breadcrumb\">
                       <li>
                           <a href=\"javascript::\">Home</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li>
                           <a href=\"javascript::\">Request List</a>
                           <span class=\"divider\">/</span>
                       </li>
                       <li>
                        <a href=\"javascript::\">User Request and Comments</a>
                          <span class=\"divider\">/</span>
                       </li>
                       <li class=\"active\">
                           Confirm Payment
                       </li>
                       
                   </ul>
          <!-- END PAGE TITLE & BREADCRUMB--> 
        </div>
      </div>
      <!-- END PAGE HEADER--> 
      <!-- BEGIN PAGE CONTENT-->
      <div class=\"row-fluid\">
        <div class=\"span12\"> 
          <!-- BEGIN BLANK PAGE PORTLET-->
          <div class=\"widget blue\">
            <div class=\"widget-title\">
              <h4><i class=\"icon-question-sign\"></i> Confirm Payment</h4>
            </div>
            <div class=\"widget-body\">
              <div class=\"row-fluid\">
                <div class=\"span12\">
                  <div class=\"span12\"> 
                    <!--BEGIN TABS-->
                    <div class=\"tabbable custom-tab\">
                      <ul class=\"nav nav-tabs\">
                        <li class=\"active\"><a href=\"#tab_1_1\" data-toggle=\"tab\">Conversations Details</a></li>
                        <li><a href=\"#tab_1_3\" data-toggle=\"tab\">Feedback</a></li>
                        <li><a href=\"#tab_1_4\" data-toggle=\"tab\">Payment Options</a></li>
                      </ul>
                      <div class=\"tab-content\">
                        <div class=\"tab-pane active\" id=\"tab_1_1\">
                          <div class=\"span6\"> 
                            
                            <!-- BEGIN HORIZONTAL DESCRIPTION LISTS PORTLET-->
                            <div class=\"widget blue\">
                              <div class=\"widget-title\">
                                <h4><i class=\"icon-reorder\"></i> User’s Request Details </h4>
                              </div>
                              <div class=\"widget-body\">
                                <div class=\"row-fluid\">
\t\t\t\t\t<div class= \"span12\">
\t\t\t\t\t<h3>Trip Info:</h3>
                       <ul class=\" unstyled amounts\">
                                                 \t <li class=\"text-bold\">Trip Name\t: ";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "name"), "html", null, true);
        echo "</li>
                                                     <li  class=\"text-bold\">No of Males: ";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "male"), "html", null, true);
        echo "</li>
                                                     <li  class=\"text-bold\">No Of Females\t: ";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "female"), "html", null, true);
        echo "</li>
                                                     <li  class=\"text-bold\">No Of Travellers\t: ";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "traveler"), "html", null, true);
        echo " </li>
                                                     <li  class=\"text-bold\">Pick-Up Location\t: ";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "pick"), "html", null, true);
        echo "</li>
                                                     <li  class=\"text-bold\">Drop Location\t: ";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "down"), "html", null, true);
        echo "</li>
\t\t\t\t\t\t\t<li  class=\"text-bold\">Trip Tentative (in days)\t: ";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "day"), "html", null, true);
        echo "</li>
\t\t\t\t\t\t\t";
        // line 91
        if (($this->getAttribute($this->getContext($context, "request"), "team") == 0)) {
            // line 92
            echo "                                                            <li  class=\"text-bold\">Combine Group\t: No</li>
                                                     ";
        } else {
            // line 94
            echo "                                                            <li  class=\"text-bold\">Combine Group\t: Yes</li>
                                                     ";
        }
        // line 96
        echo "                                                    
                         </ul>
\t\t\t\t\t\t </div>
\t\t\t\t\t\t </div>
\t\t\t\t\t\t <div class=\"row-fluid\">
\t\t\t\t\t\t <div class= \"span6\">
\t\t\t\t\t\t<h3>Activities:</h3>
                                                        <ul class=\" unstyled amounts\">
                                                     ";
        // line 104
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "request"));
        foreach ($context['_seq'] as $context["_key"] => $context["request"]) {
            // line 105
            echo "                                                            <li class=\"text-bold\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "typeOfActivity"), "html", null, true);
            echo "</li>
                                                     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['request'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class= \"span6\">
\t\t\t\t\t\t  <h3>Location:</h3>
\t\t\t\t\t\t  <ul class=\" unstyled amounts\">
                                                 \t 
                                                     <li  class=\"text-bold\">Zone\t: ";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "zone"), "html", null, true);
        echo "</li>
\t\t\t\t\t\t\t<li  class=\"text-bold\">Country\t: ";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "country"), "html", null, true);
        echo "</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t    
                                                    
                         </ul>
\t\t\t\t\t\t </div>
\t\t\t\t\t\t  
                    </div>
                     <div class=\"row-fluid\">
\t\t\t\t\t<div class= \"span12\">
\t\t\t\t\t<h3>Special Requirements:</h3>
                       <ul class=\" unstyled amounts\">
                           <li class=\"text-bold\"><p> ";
        // line 125
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "description"), "html", null, true);
        echo "</p></li>
                                                     
                                                    
                         </ul>
\t\t\t\t\t\t </div>
\t\t\t\t\t\t </div>
                              </div>
                              <!-- END row-fluid --> 
                            </div>
                            <!-- END HORIZONTAL DESCRIPTION LISTS PORTLET--> 
                          </div>
                           <div class=\"span6\"> 
                            ";
        // line 137
        $context["orgid"] = "";
        // line 138
        echo "                                        ";
        $context["desp"] = "";
        // line 139
        echo "                                        ";
        $context["pick"] = "";
        // line 140
        echo "                                        ";
        $context["down"] = "";
        // line 141
        echo "                                        ";
        $context["dateMin"] = "";
        // line 142
        echo "                                        ";
        $context["dateMax"] = "";
        // line 143
        echo "                                        ";
        $context["includeCost"] = "";
        // line 144
        echo "                                        ";
        $context["excludeCost"] = "";
        // line 145
        echo "                                        ";
        $context["totalCost"] = "";
        // line 146
        echo "                                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "listOrganiser"));
        foreach ($context['_seq'] as $context["_key"] => $context["listOrganiser"]) {
            // line 147
            echo "                               ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "comment"));
            foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
                // line 148
                echo "                                ";
                if ((($this->getAttribute($this->getContext($context, "comment"), "user") != null) && ($this->getAttribute($this->getContext($context, "comment"), "role") == "ROLE_USER"))) {
                    // line 149
                    echo "                                                             
                                ";
                } elseif ((($this->getAttribute($this->getContext($context, "comment"), "organiser") != null) && ($this->getAttribute($this->getContext($context, "comment"), "role") == "ROLE_ORGANISER"))) {
                    // line 151
                    echo "                              
                                ";
                    // line 152
                    $context["desp"] = $this->getAttribute($this->getContext($context, "comment"), "description");
                    // line 153
                    echo "                                ";
                    $context["down"] = $this->getAttribute($this->getContext($context, "comment"), "down");
                    // line 154
                    echo "                                ";
                    $context["pick"] = $this->getAttribute($this->getContext($context, "comment"), "pick");
                    // line 155
                    echo "                                 ";
                    $context["dateMin"] = $this->getAttribute($this->getContext($context, "comment"), "dateMin");
                    // line 156
                    echo "                                 ";
                    $context["dateMax"] = $this->getAttribute($this->getContext($context, "comment"), "dateMax");
                    // line 157
                    echo "                                 ";
                    $context["includeCost"] = $this->getAttribute($this->getContext($context, "comment"), "includeCost");
                    // line 158
                    echo "                                 ";
                    $context["excludeCost"] = $this->getAttribute($this->getContext($context, "comment"), "excludeCost");
                    // line 159
                    echo "                                 ";
                    $context["totalCost"] = $this->getAttribute($this->getContext($context, "comment"), "totalCost");
                    // line 160
                    echo "                                                 <!-- /comment -->
                                                                ";
                }
                // line 162
                echo "                               ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 163
            echo "                               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listOrganiser'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 164
        echo "                            <!-- BEGIN HORIZONTAL DESCRIPTION LISTS PORTLET-->
                          
                            <div class=\"widget blue\">
                              <div class=\"widget-title\">
                                  
                                <h4><i class=\"icon-reorder\"></i> Organizer Response Details </h4>
                              </div>
                              <div class=\"widget-body\">
                                  <div class=\"row-fluid\">
                                      <div class= \"span12\">
                                          <h3>Trip Response:</h3>
                                          <ul class=\" unstyled amounts\">
                                              <li class=\"text-bold\">AnyParticular Month: From:";
        // line 176
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getContext($context, "dateMin"), "m-d-Y"), "html", null, true);
        echo "  &amp; To: ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getContext($context, "dateMax"), "m-d-Y"), "html", null, true);
        echo "</li>
                                              <li  class=\"text-bold\">Cost Include: Rs ";
        // line 177
        echo twig_escape_filter($this->env, $this->getContext($context, "includeCost"), "html", null, true);
        echo "</li>
                                              <li  class=\"text-bold\">Cost Exclude : Rs ";
        // line 178
        echo twig_escape_filter($this->env, $this->getContext($context, "excludeCost"), "html", null, true);
        echo " </li>
                                              <li  class=\"text-bold\">Quotations\t: Rs ";
        // line 179
        echo twig_escape_filter($this->env, $this->getContext($context, "totalCost"), "html", null, true);
        echo " </li>
                                              <li  class=\"text-bold\">Pick-Up Location\t: ";
        // line 180
        echo twig_escape_filter($this->env, $this->getContext($context, "pick"), "html", null, true);
        echo "</li>
                                              <li  class=\"text-bold\">Drop Location\t: ";
        // line 181
        echo twig_escape_filter($this->env, $this->getContext($context, "down"), "html", null, true);
        echo "</li>
                                              <li  class=\"text-bold\">Short Details\t: ";
        // line 182
        echo twig_escape_filter($this->env, $this->getContext($context, "desp"), "html", null, true);
        echo "
                                                  
                                              </li>



                                          </ul>
                                      </div>
                                  </div>
\t\t\t\t\t\t <div class=\"row-fluid\">
\t\t\t\t\t\t 
                                                 </div>
                              </div>
                              <!-- END row-fluid --> 
                            </div>
                            <!-- END HORIZONTAL DESCRIPTION LISTS PORTLET--> 
                          </div>
                        </div>
                        
                        <div class=\"tab-pane\" id=\"tab_1_3\"> 
                          <!-- BEGIN PAGE CONTENT-->
                          <div class=\"row-fluid\">
                            <div class=\"span12\"> 
                              <!-- BEGIN BLANK PAGE PORTLET-->
                              <div class=\"widget blue\">
                                <div class=\"widget-title\">
                                  <h4><i class=\"icon-edit\"></i> Feedback </h4>
                                </div>
                                <div class=\"widget-body\">
                                
                                    <!-- BEGIN FORM-->
                            <form action=\"#\" class=\"form-horizontal\">
                            
                            
                            <div class=\"control-group\">
                                <label class=\"control-label\">Give Your Valuable Feedback</label>
                                <div class=\"controls\">
                                    <textarea class=\"span6 \" rows=\"3\"></textarea>
                                </div>
                            </div>
                            <div class=\"form-actions submit\">
                                <button type=\"submit\" class=\"btn btn-success\">Submit</button>
                                <button type=\"button\" class=\"btn\">Cancel</button>
                            </div>
                            </form>
                            <!-- END FORM-->
                                
                                 </div>
                              </div>
                              <!-- END BLANK PAGE PORTLET--> 
                            </div>
                          </div>
                          <!-- END PAGE CONTENT--> 
                        </div>
                        <div class=\"tab-pane\" id=\"tab_1_4\"> 
                        <!-- BEGIN PAGE CONTENT-->
                          <div class=\"row-fluid\">
                          
                            <div class=\"span12\"> 
                              <!-- BEGIN BLANK PAGE PORTLET-->
                              <div class=\"widget blue\">
                                <div class=\"widget-title\">
                                  <h4><i class=\"icon-edit\"></i> Payment Options </h4>
                                </div>
                                <div class=\"widget-body\">
                                <dl>
                           <div class=\"alert alert-block alert-info fade in\">
                             
                              <h4 class=\"alert-heading\">Offline Payement</h4>
                              <p>
                                 By selcting Offline  Payment options, You will be recieving mails in your inbox for further Transcation details .
                              </p>
                          </div>
                          
                           <div class=\"alert alert-block alert-info fade in\">
                             
                              <h4 class=\"alert-heading\">Online Payement</h4>
                              <p>
                                 By selcting Online Payment options, You will be redirected to payments page .
                              </p>
                          </div>
                          
                            
                        </dl>
                                    <!-- BEGIN FORM-->
                            <form action=\"\" method=\"POST\" class=\"form-horizontal\">
                            
                            
                            <div class=\"control-group\">
                                    <label class=\"control-label\">Deal Type</label>
                                    <div class=\"controls\">
                                        <label class=\"radio line\">
                                            <input type=\"radio\" name=\"optionsRadios2\" value=\"0\" />
                                           Offline Payment
                                        </label>
                                        <label class=\"radio line\">
                                            <input type=\"radio\" name=\"optionsRadios2\" value=\"1\" checked />
                                            Online Payment
                                        </label>
                                     </div>
                                </div>
                            <div class=\"form-actions submit\">
                                <a href=\"";
        // line 284
        echo $this->env->getExtension('routing')->getPath("adventure_bidding_user_request_conform_payment");
        echo "\" class=\"btn btn-success\">Submit</a>
                                <button type=\"button\" class=\"btn\">Cancel</button>
                            </div>
                            </form>
                            <!-- END FORM-->
                                
                                 </div>
                              </div>
                              <!-- END BLANK PAGE PORTLET--> 
                            </div>
                           
                          </div>
                          <!-- END PAGE CONTENT--> 
                        
                        </div>
                      </div>
                    </div>
                    <!--END TABS--> 
                  </div>
                  <div class=\"space10 visible-phone\"></div>
                </div>
              </div>
            </div>
          </div>
          <!-- END BLANK PAGE PORTLET--> 
        </div>
      </div>
      <!-- END PAGE CONTENT--> 
    </div>
    <!-- END PAGE CONTAINER--> 
  </div>
  <!-- END PAGE --> 
</div>
<!-- END CONTAINER --> 

</body>
";
    }

    // line 322
    public function block_javascripts($context, array $blocks = array())
    {
        // line 323
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


";
    }

    public function getTemplateName()
    {
        return "AdventureBiddingBundle:User:confirmDealUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  500 => 323,  497 => 322,  456 => 284,  351 => 182,  339 => 179,  335 => 178,  311 => 164,  226 => 137,  417 => 245,  386 => 219,  367 => 208,  320 => 176,  223 => 147,  373 => 147,  370 => 146,  357 => 186,  301 => 167,  289 => 158,  281 => 152,  277 => 154,  504 => 354,  501 => 353,  434 => 293,  412 => 281,  404 => 275,  390 => 254,  354 => 228,  343 => 180,  329 => 174,  295 => 160,  118 => 72,  369 => 241,  313 => 170,  297 => 166,  293 => 165,  287 => 173,  234 => 140,  271 => 147,  259 => 138,  232 => 128,  473 => 333,  470 => 332,  349 => 179,  324 => 205,  160 => 86,  239 => 155,  84 => 19,  77 => 31,  602 => 350,  599 => 349,  566 => 318,  560 => 317,  544 => 308,  540 => 307,  534 => 304,  529 => 302,  523 => 299,  516 => 297,  510 => 294,  498 => 290,  493 => 287,  490 => 286,  485 => 285,  477 => 283,  475 => 282,  438 => 252,  420 => 238,  408 => 232,  403 => 241,  382 => 183,  377 => 216,  372 => 215,  346 => 200,  342 => 198,  299 => 162,  263 => 155,  255 => 152,  250 => 150,  215 => 141,  153 => 82,  503 => 355,  455 => 310,  446 => 305,  436 => 251,  425 => 289,  414 => 244,  406 => 277,  392 => 222,  356 => 230,  345 => 178,  331 => 177,  363 => 178,  358 => 204,  350 => 196,  330 => 176,  326 => 175,  310 => 171,  306 => 200,  302 => 169,  282 => 153,  274 => 153,  251 => 146,  551 => 325,  548 => 324,  526 => 304,  506 => 293,  492 => 279,  483 => 273,  463 => 259,  454 => 253,  443 => 245,  431 => 239,  416 => 227,  410 => 226,  400 => 240,  394 => 217,  378 => 214,  366 => 235,  353 => 190,  347 => 181,  340 => 197,  338 => 178,  334 => 177,  321 => 172,  317 => 171,  315 => 171,  307 => 169,  245 => 125,  242 => 153,  228 => 138,  206 => 117,  202 => 103,  194 => 100,  113 => 64,  218 => 114,  210 => 115,  186 => 94,  172 => 104,  81 => 38,  192 => 114,  174 => 126,  155 => 74,  23 => 3,  233 => 146,  184 => 94,  137 => 69,  58 => 13,  225 => 144,  213 => 144,  205 => 21,  198 => 117,  104 => 49,  237 => 141,  207 => 141,  195 => 103,  185 => 107,  344 => 190,  336 => 212,  333 => 175,  328 => 188,  325 => 176,  318 => 207,  316 => 205,  308 => 175,  304 => 198,  300 => 185,  296 => 184,  292 => 159,  288 => 166,  284 => 165,  272 => 152,  260 => 141,  256 => 146,  248 => 154,  216 => 79,  200 => 102,  190 => 96,  170 => 80,  126 => 60,  211 => 125,  181 => 128,  129 => 61,  279 => 148,  275 => 147,  265 => 149,  261 => 157,  257 => 147,  222 => 121,  188 => 93,  167 => 124,  178 => 73,  175 => 91,  161 => 80,  150 => 85,  114 => 71,  110 => 53,  90 => 30,  127 => 59,  124 => 84,  97 => 57,  290 => 166,  286 => 157,  280 => 155,  276 => 178,  270 => 146,  266 => 160,  262 => 148,  253 => 149,  249 => 145,  244 => 156,  236 => 150,  231 => 139,  197 => 114,  191 => 99,  180 => 69,  165 => 91,  146 => 69,  65 => 18,  53 => 19,  152 => 91,  148 => 90,  134 => 76,  76 => 16,  70 => 15,  34 => 4,  480 => 284,  474 => 267,  469 => 158,  461 => 155,  457 => 153,  453 => 308,  444 => 303,  440 => 148,  437 => 242,  435 => 146,  430 => 144,  427 => 143,  423 => 287,  413 => 234,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 220,  384 => 121,  381 => 120,  379 => 182,  374 => 116,  368 => 237,  365 => 27,  362 => 26,  360 => 225,  355 => 182,  341 => 177,  337 => 176,  322 => 178,  314 => 172,  312 => 170,  309 => 170,  305 => 163,  298 => 168,  294 => 169,  285 => 163,  283 => 156,  278 => 149,  268 => 172,  264 => 164,  258 => 150,  252 => 146,  247 => 157,  241 => 77,  229 => 120,  220 => 142,  214 => 116,  177 => 127,  169 => 125,  140 => 88,  132 => 86,  128 => 85,  107 => 62,  61 => 20,  273 => 177,  269 => 151,  254 => 92,  243 => 143,  240 => 142,  238 => 154,  235 => 74,  230 => 121,  227 => 182,  224 => 181,  221 => 115,  219 => 115,  217 => 75,  208 => 107,  204 => 109,  179 => 126,  159 => 75,  143 => 71,  135 => 63,  119 => 59,  102 => 46,  71 => 31,  67 => 20,  63 => 19,  59 => 18,  94 => 30,  89 => 28,  85 => 42,  75 => 22,  68 => 28,  56 => 15,  87 => 25,  28 => 3,  93 => 29,  88 => 45,  78 => 18,  27 => 3,  46 => 10,  44 => 12,  31 => 3,  38 => 6,  26 => 5,  24 => 4,  25 => 4,  201 => 102,  196 => 90,  183 => 94,  171 => 69,  166 => 67,  163 => 76,  158 => 94,  156 => 79,  151 => 72,  142 => 78,  138 => 66,  136 => 87,  121 => 66,  117 => 65,  105 => 51,  91 => 26,  62 => 23,  49 => 11,  21 => 2,  19 => 1,  79 => 24,  72 => 17,  69 => 33,  47 => 15,  40 => 7,  37 => 10,  22 => 1,  246 => 144,  157 => 63,  145 => 67,  139 => 65,  131 => 59,  123 => 58,  120 => 44,  115 => 49,  111 => 42,  108 => 54,  101 => 47,  98 => 50,  96 => 23,  83 => 24,  74 => 21,  66 => 26,  55 => 17,  52 => 14,  50 => 17,  43 => 14,  41 => 15,  35 => 12,  32 => 7,  29 => 5,  209 => 82,  203 => 136,  199 => 134,  193 => 113,  189 => 132,  187 => 130,  182 => 66,  176 => 105,  173 => 94,  168 => 76,  164 => 77,  162 => 96,  154 => 92,  149 => 75,  147 => 68,  144 => 89,  141 => 67,  133 => 61,  130 => 60,  125 => 67,  122 => 57,  116 => 56,  112 => 52,  109 => 63,  106 => 66,  103 => 55,  99 => 32,  95 => 47,  92 => 29,  86 => 28,  82 => 17,  80 => 30,  73 => 29,  64 => 14,  60 => 16,  57 => 19,  54 => 18,  51 => 16,  48 => 13,  45 => 9,  42 => 12,  39 => 13,  36 => 10,  33 => 13,  30 => 2,);
    }
}
