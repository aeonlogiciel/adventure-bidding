<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                if (0 === strpos($pathinfo, '/_profiler/i')) {
                    // _profiler_info
                    if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                    }

                    // _profiler_import
                    if ($pathinfo === '/_profiler/import') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:importAction',  '_route' => '_profiler_import',);
                    }

                }

                // _profiler_export
                if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]++)\\.txt$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_export')), array (  '_controller' => 'web_profiler.controller.profiler:exportAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/hello')) {
            // adventure_bridge_homepage
            if (preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bridge_homepage')), array (  '_controller' => 'Adventure\\BridgeBundle\\Controller\\DefaultController::indexAction',));
            }

            // adventure_admin_homepage
            if (preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_admin_homepage')), array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\DefaultController::indexAction',));
            }

        }

        if (0 === strpos($pathinfo, '/bid/admin')) {
            // adventure_admin_dashboard
            if ($pathinfo === '/bid/admin/dashboard') {
                return array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::dashboardAction',  '_route' => 'adventure_admin_dashboard',);
            }

            // adventure_admin_user_list
            if ($pathinfo === '/bid/admin/user') {
                return array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::listUserAction',  '_route' => 'adventure_admin_user_list',);
            }

            // adventure_admin_organiser_list
            if ($pathinfo === '/bid/admin/organiser') {
                return array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::listOrganiserAction',  '_route' => 'adventure_admin_organiser_list',);
            }

            // adventure_admin_member_status
            if ($pathinfo === '/bid/admin/member-status') {
                return array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::changeStatusAction',  '_route' => 'adventure_admin_member_status',);
            }

            // adventure_admin_user_delete
            if ($pathinfo === '/bid/admin/user-delete') {
                return array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::deleteUserAction',  '_route' => 'adventure_admin_user_delete',);
            }

            // adventure_admin_organiser_delete
            if ($pathinfo === '/bid/admin/organiser-delete') {
                return array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::deleteOrganiserAction',  '_route' => 'adventure_admin_organiser_delete',);
            }

            // adventure_admin_view_close_request
            if (preg_match('#^/bid/admin/(?P<id>[^/]++)/request/view\\-close$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_admin_view_close_request')), array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::viewCloseRequestAction',));
            }

            // adventure_admin_view_open_request
            if (preg_match('#^/bid/admin/(?P<id>[^/]++)/request/view\\-open$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_admin_view_open_request')), array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::viewOpenRequestAction',));
            }

            // adventure_admin_list_request
            if ($pathinfo === '/bid/admin/request/list') {
                return array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::listRequestAction',  '_route' => 'adventure_admin_list_request',);
            }

            // adventure_admin_feedback_list
            if ($pathinfo === '/bid/admin/feedback') {
                return array (  '_controller' => 'Adventure\\AdminBundle\\Controller\\AdminController::feedbackAction',  '_route' => 'adventure_admin_feedback_list',);
            }

        }

        // adventure_bidding_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_homepage')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\DefaultController::indexAction',));
        }

        // adventure_bidding_user_dashboard
        if ($pathinfo === '/bid/user/dashboard') {
            return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::dashboardAction',  '_route' => 'adventure_bidding_user_dashboard',);
        }

        // adventure_bidding_user_register
        if (preg_match('#^/(?P<id>[^/]++)/user/register$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_user_register')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::registerAction',));
        }

        if (0 === strpos($pathinfo, '/bid/user')) {
            // adventure_bidding_user_view_profile
            if ($pathinfo === '/bid/user/view-profile') {
                return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::viewProfileAction',  '_route' => 'adventure_bidding_user_view_profile',);
            }

            // adventure_bidding_user_edit_profile
            if ($pathinfo === '/bid/user/edit-profile') {
                return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::editProfileAction',  '_route' => 'adventure_bidding_user_edit_profile',);
            }

            if (0 === strpos($pathinfo, '/bid/user/request')) {
                // adventure_bidding_user_list_request
                if ($pathinfo === '/bid/user/request/list') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::listRequestAction',  '_route' => 'adventure_bidding_user_list_request',);
                }

                // adventure_bidding_user_create_request
                if ($pathinfo === '/bid/user/request/create') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::createRequestAction',  '_route' => 'adventure_bidding_user_create_request',);
                }

            }

            // adventure_bidding_user_edit_request
            if (preg_match('#^/bid/user/(?P<id>[^/]++)/request/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_user_edit_request')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::editRequestAction',));
            }

            // adventure_bidding_user_delete_request
            if ($pathinfo === '/bid/user/request/delete') {
                return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::deleteRequestAction',  '_route' => 'adventure_bidding_user_delete_request',);
            }

            // adventure_bidding_user_view_close_request
            if (preg_match('#^/bid/user/(?P<id>[^/]++)/request/view\\-close$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_user_view_close_request')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::viewCloseRequestAction',));
            }

            // adventure_bidding_user_view_open_request
            if (preg_match('#^/bid/user/(?P<id>[^/]++)/request/view\\-open$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_user_view_open_request')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::viewOpenRequestAction',));
            }

            // adventure_bidding_user_view_open_message
            if (preg_match('#^/bid/user/(?P<id>[^/]++)/request/view\\-open\\-msg$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_user_view_open_message')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::updatemessageAction',));
            }

            // adventure_bidding_user_request_confirm_deal
            if (preg_match('#^/bid/user/(?P<reqId>[^/]++)/request/confirm\\-deal$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_user_request_confirm_deal')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::confirmDealAction',));
            }

            // adventure_bidding_user_request_conform_payment
            if ($pathinfo === '/bid/user/payment') {
                return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::conformPaymentAction',  '_route' => 'adventure_bidding_user_request_conform_payment',);
            }

        }

        // adventure_bidding_organiser_register
        if ($pathinfo === '/organiser/register') {
            return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::registerAction',  '_route' => 'adventure_bidding_organiser_register',);
        }

        if (0 === strpos($pathinfo, '/bid')) {
            if (0 === strpos($pathinfo, '/bid/organiser')) {
                // adventure_bidding_organiser_dashboard
                if ($pathinfo === '/bid/organiser/dashboard') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::dashboardAction',  '_route' => 'adventure_bidding_organiser_dashboard',);
                }

                // adventure_bidding_organiser_view_profile
                if ($pathinfo === '/bid/organiser/view-profile') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::viewProfileAction',  '_route' => 'adventure_bidding_organiser_view_profile',);
                }

                // adventure_bidding_organiser_edit_profile
                if ($pathinfo === '/bid/organiser/edit-profile') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::editProfileAction',  '_route' => 'adventure_bidding_organiser_edit_profile',);
                }

                // adventure_bidding_organiser_view_event
                if ($pathinfo === '/bid/organiser/view-event') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::createEventAction',  '_route' => 'adventure_bidding_organiser_view_event',);
                }

                // adventure_bidding_organiser_edit_event
                if (preg_match('#^/bid/organiser/(?P<id>[^/]++)/edit\\-event$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_organiser_edit_event')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::editEventAction',));
                }

                // adventure_bidding_organiser_event_update
                if (preg_match('#^/bid/organiser/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_adventure_bidding_organiser_event_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_organiser_event_update')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::updateEventAction',));
                }
                not_adventure_bidding_organiser_event_update:

                // adventure_bidding_organiser_event_delete
                if (preg_match('#^/bid/organiser/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_organiser_event_delete')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::deleteEventAction',));
                }

                // adventure_bidding_organiser_view_trails
                if ($pathinfo === '/bid/organiser/view-trails') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::viewTrailsAction',  '_route' => 'adventure_bidding_organiser_view_trails',);
                }

                // adventure_bidding_organiser_edit_trails
                if ($pathinfo === '/bid/organiser/edit-trails') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::editTrailsAction',  '_route' => 'adventure_bidding_organiser_edit_trails',);
                }

                // adventure_bidding_organiser_add_trails
                if (preg_match('#^/bid/organiser/(?P<id>[^/]++)/add\\-trails$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_organiser_add_trails')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::addTrailsAction',));
                }

                // adventure_bidding_organiser_remove_trails
                if (preg_match('#^/bid/organiser/(?P<id>[^/]++)/remove\\-trails$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_organiser_remove_trails')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::removeTrailAction',));
                }

                // adventure_bidding_organiser_create_response
                if (preg_match('#^/bid/organiser/(?P<id>[^/]++)/response/create$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_organiser_create_response')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::createResponseAction',));
                }

                // adventure_bidding_organiser_list_request
                if ($pathinfo === '/bid/organiser/list/request') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::listRequestAction',  '_route' => 'adventure_bidding_organiser_list_request',);
                }

                // adventure_bidding_organiser_view_close_request
                if (preg_match('#^/bid/organiser/(?P<id>[^/]++)/request/view\\-close$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_organiser_view_close_request')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::viewCloseRequestAction',));
                }

                // adventure_bidding_organiser_view_open_request
                if (preg_match('#^/bid/organiser/(?P<id>[^/]++)/request/view\\-open$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_organiser_view_open_request')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::viewOpenRequestAction',));
                }

                // adventure_bidding_organiser_reply_open_request
                if (preg_match('#^/bid/organiser/(?P<id>[^/]++)/request/reply$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_bidding_organiser_reply_open_request')), array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::orgRequestAction',));
                }

                // adventure_bidding_organiser_changepassword
                if ($pathinfo === '/bid/organiser/changepassword') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::changePasswordAction',  '_route' => 'adventure_bidding_organiser_changepassword',);
                }

            }

            if (0 === strpos($pathinfo, '/bid/u')) {
                // adventure_update_organiser_password
                if ($pathinfo === '/bid/updatePassword') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::updatePasswordAction',  '_route' => 'adventure_update_organiser_password',);
                }

                // adventure_bidding_user_changepassword
                if ($pathinfo === '/bid/user/changepassword') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::changePasswordAction',  '_route' => 'adventure_bidding_user_changepassword',);
                }

                // adventure_update_user_password
                if ($pathinfo === '/bid/updatePassword') {
                    return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\UserController::updatePasswordAction',  '_route' => 'adventure_update_user_password',);
                }

            }

            // adventure_bidding_organiser_feedback
            if ($pathinfo === '/bid/organiser/feedback') {
                return array (  '_controller' => 'Adventure\\BiddingBundle\\Controller\\OrganiserController::feedbackAction',  '_route' => 'adventure_bidding_organiser_feedback',);
            }

        }

        if (0 === strpos($pathinfo, '/h')) {
            // adventure_login_homepage
            if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adventure_login_homepage')), array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\DefaultController::indexAction',));
            }

            // adventure_home
            if ($pathinfo === '/home') {
                return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\MemberController::homeAction',  '_route' => 'adventure_home',);
            }

        }

        // adventure_login_index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'adventure_login_index');
            }

            return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\MemberController::indexAction',  '_route' => 'adventure_login_index',);
        }

        if (0 === strpos($pathinfo, '/bid/log')) {
            if (0 === strpos($pathinfo, '/bid/login')) {
                // login
                if ($pathinfo === '/bid/login') {
                    return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\LoginController::loginAction',  '_route' => 'login',);
                }

                // login_check
                if ($pathinfo === '/bid/login_check') {
                    return array('_route' => 'login_check');
                }

            }

            // log_out
            if ($pathinfo === '/bid/logout') {
                return array('_route' => 'log_out');
            }

            // default_security_target
            if ($pathinfo === '/bid/login-redirect') {
                return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\LoginController::loginRedirectAction',  '_route' => 'default_security_target',);
            }

        }

        // adventure_login_admin
        if ($pathinfo === '/admin') {
            return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\LoginController::loginAdminAction',  '_route' => 'adventure_login_admin',);
        }

        // adventure_login_user_register
        if ($pathinfo === '/user-register') {
            return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\MemberController::userRegisterAction',  '_route' => 'adventure_login_user_register',);
        }

        // adventure_login_organiser_register
        if ($pathinfo === '/organiser-register') {
            return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\MemberController::organiserRegisterAction',  '_route' => 'adventure_login_organiser_register',);
        }

        if (0 === strpos($pathinfo, '/forgot-password')) {
            // forgot_password
            if ($pathinfo === '/forgot-password') {
                return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\LoginController::forgotPasswordAction',  '_route' => 'forgot_password',);
            }

            // forgot_password_mail
            if ($pathinfo === '/forgot-password-mail') {
                return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\LoginController::forgotPasswordMailAction',  '_route' => 'forgot_password_mail',);
            }

        }

        // adventure_login_verify_email
        if ($pathinfo === '/verify-email') {
            return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\MemberController::verifyEmailAction',  '_route' => 'adventure_login_verify_email',);
        }

        if (0 === strpos($pathinfo, '/bid')) {
            // adventure_change_password
            if ($pathinfo === '/bid/changePassword') {
                return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\MemberController::changePasswordAction',  '_route' => 'adventure_change_password',);
            }

            // adventure_update_password
            if ($pathinfo === '/bid/updatePassword') {
                return array (  '_controller' => 'Adventure\\LoginBundle\\Controller\\MemberController::updatePasswordAction',  '_route' => 'adventure_update_password',);
            }

        }

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
