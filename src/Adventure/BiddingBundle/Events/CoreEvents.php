<?php

namespace Adventure\BiddingBundle\Events;

/**
 * Description of CoreEvents
 *
 * @author Jhuma Mandal
 * Written : 08-Jan-2015
 */
final class CoreEvents {
    //event for matching organiser list
    const REQUEST_MATCH = 'request.match';
}
