<?php

namespace Adventure\BiddingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
class eventType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    { $countries = Intl::getRegionBundle()->getCountryNames();
       // $continent =$em->getRepository('AdventureBiddingBundle:continents')->findAll();
        
        $builder
            ->add('ename')
            ->add('eActivity', 'entity', array(
                    'class' => 'AdventureBiddingBundle:BidActivity',
                    'property' => 'typeOfActivity',
                    'empty_value' => 'Select',
                    'multiple'  => true
                ),array('required'=> true))
            //->add('eOrganizer', 'hidden',array('data'=> 'organiser'))
            ->add('budget')
            ->add('eContactDetail')
            ->add('duration')
            ->add('eDate','date',array('widget' => 'single_text','format'=>'dd/MM/yyyy'))
            ->add('eDifficulty','choice', array(
                'empty_value' => 'Select Difficulty',
                'choices' => array(
                            'Basic' => 'Basic',
                            'Moderate' => 'Moderate',
                            'Difficult' => 'Difficult',
                            'Challenging' => 'Challenging')),array('required'=> true))
           
            ->add('eZone', 'choice', array(
                'empty_value' => 'Select Zone',
                      'choices' => array(
                            'east' => 'East',
                            'west' => 'West',
                            'north' => 'North',
                            'south' => 'South')),array('required'=> true))
            ->add('continent', 'choice', array(
                'empty_value' => 'Select Continent',
                                      'choices' => array(
                            '1' => 'Asia',
                            '2' => 'Africa',
                            '3' => 'North America',
                            '4' => 'South America',
                            '5' => 'Antartica',
                            '6' => 'Europe',
                            '7' => 'Australia')),array('required'=> true))
         
            ->add('country','choice', array(
                'empty_value' => 'Select Country',
                    'choices' => array(
                        $countries
                )),array('required'=> false))
                
            ->add('eState','choice', array(
                'empty_value' => 'Select State',
                    'choices' => array(
                        'Madhya Pradesh'=>'Madhya Pradesh',
                        'Maharastra'=>'Maharastra',
                        'Rajasthan'=>'Rajasthan'
                )),array('required'=> false))
            ->add('eRegion')
            ->add('eDestination')
            ->add('eBrief')
            ->add('path','file', array('label' => 'Image', 'data_class' => null,'required'=> false))
            ->add('email')
            ->add('status','hidden',array('data'=> '1'))
            ->add('flag','hidden',array('data'=> '1'))
            ->add('eMonth','hidden',array('data'=> 'NULL'))
            ->add('eYear','hidden',array('data'=> 'NULL'))
            ->add('uniquee','hidden',array('data'=> 'Null'))
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Adventure\BiddingBundle\Entity\event'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adventure_biddingbundle_event';
    }
}
