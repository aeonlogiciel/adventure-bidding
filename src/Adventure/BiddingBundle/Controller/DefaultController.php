<?php

namespace Adventure\BiddingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AdventureBiddingBundle:Default:index.html.twig', array('name' => $name));
    }
}
