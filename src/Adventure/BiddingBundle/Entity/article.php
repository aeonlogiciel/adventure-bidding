<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * article
 */
class article
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $coverImage;

    /**
     * @var string
     */
    private $autherEmail;

    /**
     * @var string
     */
    private $articleTitle;

    /**
     * @var string
     */
    private $discription;

    /**
     * @var string
     */
    private $catForFilter;

    /**
     * @var string
     */
    private $typeOfActivity;

    /**
     * @var string
     */
    private $durationMin;

    /**
     * @var string
     */
    private $durationMax;

    /**
     * @var string
     */
    private $difficulty;

    /**
     * @var string
     */
    private $zone;

    /**
     * @var integer
     */
    private $continent;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $destination;

    /**
     * @var string
     */
    private $gataways;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $season;

    /**
     * @var string
     */
    private $bucketList;

    /**
     * @var integer
     */
    private $vote;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var string
     */
    private $details;

    /**
     * @var integer
     */
    private $publish;

    /**
     * @var integer
     */
    private $showdetails;

    /**
     * @var string
     */
    private $folderName;

    /**
     * @var string
     */
    private $views;

    /**
     * @var string
     */
    private $uniquee;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coverImage
     *
     * @param string $coverImage
     * @return article
     */
    public function setCoverImage($coverImage)
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    /**
     * Get coverImage
     *
     * @return string 
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * Set autherEmail
     *
     * @param string $autherEmail
     * @return article
     */
    public function setAutherEmail($autherEmail)
    {
        $this->autherEmail = $autherEmail;

        return $this;
    }

    /**
     * Get autherEmail
     *
     * @return string 
     */
    public function getAutherEmail()
    {
        return $this->autherEmail;
    }

    /**
     * Set articleTitle
     *
     * @param string $articleTitle
     * @return article
     */
    public function setArticleTitle($articleTitle)
    {
        $this->articleTitle = $articleTitle;

        return $this;
    }

    /**
     * Get articleTitle
     *
     * @return string 
     */
    public function getArticleTitle()
    {
        return $this->articleTitle;
    }

    /**
     * Set discription
     *
     * @param string $discription
     * @return article
     */
    public function setDiscription($discription)
    {
        $this->discription = $discription;

        return $this;
    }

    /**
     * Get discription
     *
     * @return string 
     */
    public function getDiscription()
    {
        return $this->discription;
    }

    /**
     * Set catForFilter
     *
     * @param string $catForFilter
     * @return article
     */
    public function setCatForFilter($catForFilter)
    {
        $this->catForFilter = $catForFilter;

        return $this;
    }

    /**
     * Get catForFilter
     *
     * @return string 
     */
    public function getCatForFilter()
    {
        return $this->catForFilter;
    }

    /**
     * Set typeOfActivity
     *
     * @param string $typeOfActivity
     * @return article
     */
    public function setTypeOfActivity($typeOfActivity)
    {
        $this->typeOfActivity = $typeOfActivity;

        return $this;
    }

    /**
     * Get typeOfActivity
     *
     * @return string 
     */
    public function getTypeOfActivity()
    {
        return $this->typeOfActivity;
    }

    /**
     * Set durationMin
     *
     * @param string $durationMin
     * @return article
     */
    public function setDurationMin($durationMin)
    {
        $this->durationMin = $durationMin;

        return $this;
    }

    /**
     * Get durationMin
     *
     * @return string 
     */
    public function getDurationMin()
    {
        return $this->durationMin;
    }

    /**
     * Set durationMax
     *
     * @param string $durationMax
     * @return article
     */
    public function setDurationMax($durationMax)
    {
        $this->durationMax = $durationMax;

        return $this;
    }

    /**
     * Get durationMax
     *
     * @return string 
     */
    public function getDurationMax()
    {
        return $this->durationMax;
    }

    /**
     * Set difficulty
     *
     * @param string $difficulty
     * @return article
     */
    public function setDifficulty($difficulty)
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * Get difficulty
     *
     * @return string 
     */
    public function getDifficulty()
    {
        return $this->difficulty;
    }

    /**
     * Set zone
     *
     * @param string $zone
     * @return article
     */
    public function setZone($zone)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return string 
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set continent
     *
     * @param integer $continent
     * @return article
     */
    public function setContinent($continent)
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * Get continent
     *
     * @return integer 
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return article
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return article
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return article
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set destination
     *
     * @param string $destination
     * @return article
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set gataways
     *
     * @param string $gataways
     * @return article
     */
    public function setGataways($gataways)
    {
        $this->gataways = $gataways;

        return $this;
    }

    /**
     * Get gataways
     *
     * @return string 
     */
    public function getGataways()
    {
        return $this->gataways;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return article
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set season
     *
     * @param string $season
     * @return article
     */
    public function setSeason($season)
    {
        $this->season = $season;

        return $this;
    }

    /**
     * Get season
     *
     * @return string 
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set bucketList
     *
     * @param string $bucketList
     * @return article
     */
    public function setBucketList($bucketList)
    {
        $this->bucketList = $bucketList;

        return $this;
    }

    /**
     * Get bucketList
     *
     * @return string 
     */
    public function getBucketList()
    {
        return $this->bucketList;
    }

    /**
     * Set vote
     *
     * @param integer $vote
     * @return article
     */
    public function setVote($vote)
    {
        $this->vote = $vote;

        return $this;
    }

    /**
     * Get vote
     *
     * @return integer 
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return article
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return article
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set publish
     *
     * @param integer $publish
     * @return article
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return integer 
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set showdetails
     *
     * @param integer $showdetails
     * @return article
     */
    public function setShowdetails($showdetails)
    {
        $this->showdetails = $showdetails;

        return $this;
    }

    /**
     * Get showdetails
     *
     * @return integer 
     */
    public function getShowdetails()
    {
        return $this->showdetails;
    }

    /**
     * Set folderName
     *
     * @param string $folderName
     * @return article
     */
    public function setFolderName($folderName)
    {
        $this->folderName = $folderName;

        return $this;
    }

    /**
     * Get folderName
     *
     * @return string 
     */
    public function getFolderName()
    {
        return $this->folderName;
    }

    /**
     * Set views
     *
     * @param string $views
     * @return article
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return string 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set uniquee
     *
     * @param string $uniquee
     * @return article
     */
    public function setUniquee($uniquee)
    {
        $this->uniquee = $uniquee;

        return $this;
    }

    /**
     * Get uniquee
     *
     * @return string 
     */
    public function getUniquee()
    {
        return $this->uniquee;
    }
    /**
     * @var \Adventure\BiddingBundle\Entity\orgtrails
     */
    private $orgtrails;


    /**
     * Set orgtrails
     *
     * @param \Adventure\BiddingBundle\Entity\orgtrails $orgtrails
     * @return article
     */
    public function setOrgtrails(\Adventure\BiddingBundle\Entity\orgtrails $orgtrails = null)
    {
        $this->orgtrails = $orgtrails;

        return $this;
    }

    /**
     * Get orgtrails
     *
     * @return \Adventure\BiddingBundle\Entity\orgtrails 
     */
    public function getOrgtrails()
    {
        return $this->orgtrails;
    }
}
