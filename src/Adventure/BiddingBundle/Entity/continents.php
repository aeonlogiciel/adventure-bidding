<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * continents
 */
class continents
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return continents
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \Adventure\BiddingBundle\Entity\countries
     */
    private $countries;


    /**
     * Set countries
     *
     * @param \Adventure\BiddingBundle\Entity\countries $countries
     * @return continents
     */
    public function setCountries(\Adventure\BiddingBundle\Entity\countries $countries = null)
    {
        $this->countries = $countries;

        return $this;
    }

    /**
     * Get countries
     *
     * @return \Adventure\BiddingBundle\Entity\countries 
     */
    public function getCountries()
    {
        return $this->countries;
    }
}
