<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * event
 */
class event
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ename;

    /**
     * @var string
     */
    private $eActivity;

    /**
     * @var string
     */
    private $eOrganizer;

    /**
     * @var string
     */
    private $budget;

    /**
     * @var string
     */
    private $eContactDetail;

    /**
     * @var string
     */
    private $duration;

    /**
     * @var \DateTime
     */
    private $eDate;

    /**
     * @var string
     */
    private $eMonth;

    /**
     * @var string
     */
    private $eYear;

    /**
     * @var string
     */
    private $eDifficulty;

    /**
     * @var string
     */
    private $eZone;

    /**
     * @var string
     */
    private $continent;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $eState;

    /**
     * @var string
     */
    private $eRegion;

    /**
     * @var string
     */
    private $eDestination;

    /**
     * @var string
     */
    private $eBrief;

    
    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $eRegistrationDate;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $uniquee;

    
    
     /**
     * @var string
     */
    protected $path;
    
    public function getAbsolutePath() {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath() {
        return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'image/event';
    }

    
    private $applicantphoto;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ename
     *
     * @param string $ename
     * @return event
     */
    public function setEname($ename)
    {
        $this->ename = $ename;

        return $this;
    }

    /**
     * Get ename
     *
     * @return string 
     */
    public function getEname()
    {
        return $this->ename;
    }

    /**
     * Set eActivity
     *
     * @param string $eActivity
     * @return event
     */
    public function setEActivity($eActivity)
    {
        $this->eActivity = $eActivity;

      
        return $this;
    }

    /**
     * Get eActivity
     *
     * @return string 
     */
    public function getEActivity()
    {
        return $this->eActivity;
    }

    /**
     * Set eOrganizer
     *
     * @param string $eOrganizer
     * @return event
     */
    public function setEOrganizer($eOrganizer)
    {
        $this->eOrganizer = $eOrganizer;

        return $this;
    }

    /**
     * Get eOrganizer
     *
     * @return string 
     */
    public function getEOrganizer()
    {
        return $this->eOrganizer;
    }

    /**
     * Set budget
     *
     * @param string $budget
     * @return event
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return string 
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set eContactDetail
     *
     * @param string $eContactDetail
     * @return event
     */
    public function setEContactDetail($eContactDetail)
    {
        $this->eContactDetail = $eContactDetail;

        return $this;
    }

    /**
     * Get eContactDetail
     *
     * @return string 
     */
    public function getEContactDetail()
    {
        return $this->eContactDetail;
    }

    /**
     * Set duration
     *
     * @param string $duration
     * @return event
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set eDate
     *
     * @param \DateTime $eDate
     * @return event
     */
    public function setEDate($eDate)
    {
        $this->eDate = $eDate;

        return $this;
    }

    /**
     * Get eDate
     * 
     * @return DateTime 
     */
    public function getEDate()
    {
        return $this->eDate;
    }

    /**
     * Set eMonth
     *
     * @param string $eMonth
     * @return event
     */
    public function setEMonth($eMonth)
    {
        $this->eMonth = $eMonth;

        return $this;
    }

    /**
     * Get eMonth
     *
     * @return string 
     */
    public function getEMonth()
    {
        return $this->eMonth;
    }

    /**
     * Set eYear
     *
     * @param string $eYear
     * @return event
     */
    public function setEYear($eYear)
    {
        $this->eYear = $eYear;

        return $this;
    }

    /**
     * Get eYear
     *
     * @return string 
     */
    public function getEYear()
    {
        return $this->eYear;
    }

    /**
     * Set eDifficulty
     *
     * @param string $eDifficulty
     * @return event
     */
    public function setEDifficulty($eDifficulty)
    {
        $this->eDifficulty = $eDifficulty;

        return $this;
    }

    /**
     * Get eDifficulty
     *
     * @return string 
     */
    public function getEDifficulty()
    {
        return $this->eDifficulty;
    }

    /**
     * Set eZone
     *
     * @param string $eZone
     * @return event
     */
    public function setEZone($eZone)
    {
        $this->eZone = $eZone;

        return $this;
    }

    /**
     * Get eZone
     *
     * @return string 
     */
    public function getEZone()
    {
        return $this->eZone;
    }

    /**
     * Set continent
     *
     * @param integer $continent
     * @return event
     */
    public function setContinent($continent)
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * Get continent
     *
     * @return integer 
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * Set country
     *
     * @param integer $country
     * @return event
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return integer 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set eState
     *
     * @param string $eState
     * @return event
     */
    public function setEState($eState)
    {
        $this->eState = $eState;

        return $this;
    }

    /**
     * Get eState
     *
     * @return string 
     */
    public function getEState()
    {
        return $this->eState;
    }

    /**
     * Set eRegion
     *
     * @param string $eRegion
     * @return event
     */
    public function setERegion($eRegion)
    {
        $this->eRegion = $eRegion;

        return $this;
    }

    /**
     * Get eRegion
     *
     * @return string 
     */
    public function getERegion()
    {
        return $this->eRegion;
    }

    /**
     * Set eDestination
     *
     * @param string $eDestination
     * @return event
     */
    public function setEDestination($eDestination)
    {
        $this->eDestination = $eDestination;

        return $this;
    }

    /**
     * Get eDestination
     *
     * @return string 
     */
    public function getEDestination()
    {
        return $this->eDestination;
    }

    /**
     * Set eBrief
     *
     * @param string $eBrief
     * @return event
     */
    public function setEBrief($eBrief)
    {
        $this->eBrief = $eBrief;

        return $this;
    }

    /**
     * Get eBrief
     *
     * @return string 
     */
    public function getEBrief()
    {
        return $this->eBrief;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return event
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set applicantphoto
     *
     * @param string $applicantphoto
     * @param UploadedFile $applicantphoto
     * @return event 
     */
    public function setApplicantphoto(UploadedFile $applicantphoto = null)
    {
        $this->applicantphoto = $applicantphoto;

        return $this;
    }

    /**
     * Get applicantphoto
     *@return UploadedFile
     * @return string 
     */
    public function getApplicantphoto()
    {
        return $this->applicantphoto;
    }

    
    /**
     * Set status
     *
     * @param string $status
     * @return event
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set eRegistrationDate
     *
     * @param \DateTime $eRegistrationDate
     * @return event
     */
    public function setERegistrationDate($eRegistrationDate)
    {
        $this->eRegistrationDate = $eRegistrationDate;

        return $this;
    }

    /**
     * Get eRegistrationDate
     *
     * @return \DateTime 
     */
    public function getERegistrationDate()
    {
        return $this->eRegistrationDate;
    }
  /**
     * @ORM\PreUpdate
     */
    public function setERegistrationDateValue()
    {
       $this->eRegistrationDate = new \DateTime();
    }
    
    /**
     * Set flag
     *
     * @param integer $flag
     * @return event
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer 
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return event
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set uniquee
     *
     * @param string $uniquee
     * @return event
     */
    public function setUniquee($uniquee)
    {
        $this->uniquee = $uniquee;

        return $this;
    }

    /**
     * Get uniquee
     *
     * @return string 
     */
    public function getUniquee()
    {
        return $this->uniquee;
    }
     
  
    public function __toString()
    {
        return $this->eActivity;
    }
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $activity;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->eActivity= new \Doctrine\Common\Collections\ArrayCollection();
        $this->activity = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add eActivity
     *
     * @param \Adventure\BiddingBundle\Entity\event $eActivity
     * @return event
     */
    public function addEActivity($eActivity)
    {
        $this->eActivity = $eActivity;

        return $this;
    }

    /**
     * Remove eActivity
     *
     * @param \Adventure\BiddingBundle\Entity\event $eActivity
     */
    public function removeEActivity($eActivity)
    {
        $this->eActivity=$eActivity;
    }
    /**
     * Add activity
     *
     * @param \Adventure\BiddingBundle\Entity\BidActivity $activity
     * @return event
     */
    public function addActivity(\Adventure\BiddingBundle\Entity\BidActivity $activity)
    {
        $this->activity[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param \Adventure\BiddingBundle\Entity\BidActivity $activity
     */
    public function removeActivity(\Adventure\BiddingBundle\Entity\BidActivity $eActivity)
    {
        $this->activity->removeElement($activity);
    }

    /**
     * Add country
     *
     * @param \Adventure\BiddingBundle\Entity\BidCountry $country
     * @return event
     */
    public function addCountry(\Adventure\BiddingBundle\Entity\BidCountry $country)
    {
        $this->country[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \Adventure\BiddingBundle\Entity\BidCountry $country
     */
    public function removeCountry(\Adventure\BiddingBundle\Entity\BidCountry $country)
    {
        $this->country->removeElement($country);
    }

    /**
     * @ORM\PrePersist
     */
    public function eRegistrationDateValue()
    {
        $this->eRegistrationDate = new \DateTime();
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        // the file property can be empty if the field is not required
        if (null === $this->getApplicantphoto()) {
            return;
        }


        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        $this->getApplicantphoto()->move(
                $this->getUploadRootDir(), $this->path
        );

        // set the path property to the filename where you've saved the file
        // $this->path = $this->getImage()->getClientOriginalName();
        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function preUpload() {
        if (null !== $this->getApplicantphoto()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename . '.' . $this->getApplicantphoto()->guessExtension();
        }
    }
    
   
}
