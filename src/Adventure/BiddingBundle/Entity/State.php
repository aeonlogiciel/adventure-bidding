<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * State
 */
class State
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $countryId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return State
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set countryId
     *
     * @param integer $countryId
     * @return State
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->countryId;
    }
    /**
     * @var \Adventure\BiddingBundle\Entity\countries
     */
    private $countries;


    /**
     * Set countries
     *
     * @param \Adventure\BiddingBundle\Entity\countries $countries
     * @return State
     */
    public function setCountries(\Adventure\BiddingBundle\Entity\countries $countries = null)
    {
        $this->countries = $countries;

        return $this;
    }

    /**
     * Get countries
     *
     * @return \Adventure\BiddingBundle\Entity\countries 
     */
    public function getCountries()
    {
        return $this->countries;
    }
}
