<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * orgtrails
 */
class orgtrails
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $orgid;

    /**
     * @var integer
     */
    private $trailid;

    /**
     * @var \DateTime
     */
    private $createdon;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orgid
     *
     * @param integer $orgid
     * @return orgtrails
     */
    public function setOrgid($orgid)
    {
        $this->orgid = $orgid;

        return $this;
    }

    /**
     * Get orgid
     *
     * @return integer 
     */
    public function getOrgid()
    {
        return $this->orgid;
    }

    /**
     * Set trailid
     *
     * @param integer $trailid
     * @return orgtrails
     */
    public function setTrailid($trailid)
    {
        $this->trailid = $trailid;

        return $this;
    }

    /**
     * Get trailid
     *
     * @return integer 
     */
    public function getTrailid()
    {
        return $this->trailid;
    }

    /**
     * Set createdon
     *
     * @param \DateTime $createdon
     * @return orgtrails
     */
    public function setCreatedon($createdon)
    {
        $this->createdon = $createdon;

        return $this;
    }

    /**
     * Get createdon
     *
     * @return \DateTime 
     */
    public function getCreatedon()
    {
        return $this->createdon;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return orgtrails
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var \Adventure\BiddingBundle\Entity\article
     */
    private $article;


    /**
     * Set article
     *
     * @param \Adventure\BiddingBundle\Entity\article $article
     * @return orgtrails
     */
    public function setArticle(\Adventure\BiddingBundle\Entity\article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Adventure\BiddingBundle\Entity\article 
     */
    public function getArticle()
    {
        return $this->article;
    }
    
    /**
     * @ORM\PreUpdate
     */
    public function setCreatedonValue()
    {
       $this->createdon = new \DateTime();
    }
    
    

    /**
     * @var \Adventure\BiddingBundle\Entity\orgtrails
     */
//    private $customer;
//
//
//    /**
//     * Set customer
//     *
//     * @param \Adventure\BiddingBundle\Entity\orgtrails $customer
//     * @return orgtrails
//     */
//    public function setCustomer(\Adventure\BiddingBundle\Entity\orgtrails $customer = null)
//    {
//        $this->customer = $customer;
//
//        return $this;
//    }
//
//    /**
//     * Get customer
//     *
//     * @return \Adventure\BiddingBundle\Entity\orgtrails 
//     */
//    public function getCustomer()
//    {
//        return $this->customer;
//    }

    /**
     * @ORM\PrePersist
     */
    public function createdon()
    {
        $this->createdon = new \DateTime();
    }

    /**
     * @ORM\PostPersist
     */
    public function upload()
    {
        // Add your code here
    }
}
