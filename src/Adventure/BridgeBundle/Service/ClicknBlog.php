<?php

namespace Adventure\BridgeBundle\Service;
use \PDO;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClicknBlog
 *
 * @author al-05
 */
class ClicknBlog {

    private $container;
    private $db_username;
    private $db_name;
    private $db_password;
    private $db_host;
    private $db_port;
    private $connection;
    public function __construct($container) {
        $this->container = $container;
        $this->db_host = $this->container->getParameter('database_host');
        $this->db_name = $this->container->getParameter('database_name');
        $this->db_username = $this->container->getParameter('database_user');
        $this->db_password = $this->container->getParameter('database_password');
        $this->db_port = $this->container->getParameter('database_port');
        
        try {
           $this->connection = new PDO("mysql:host=$this->db_host;dbname=$this->db_name", $this->db_username, $this->db_password);
            // set the PDO error mode to exception
             $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();die();
        }
    }
  
   function getData($id) {
   $stmt = $this->connection->query("SELECT * FROM event WHERE id=$id");
   print_r( $stmt->fetchAll(PDO::FETCH_ASSOC));
  
   }
   
   public function getTrails(){
       $stmt = $this->connection->query("SELECT id,coverImage,articleTitle,typeOfActivity,durationMax,durationMin,difficulty,zone,state,uniquee FROM article ");
       $trails = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $trails;
   }
   
    public function getEvents($id){
       $stmt = $this->connection->query("SELECT id,path,ename,budget,duration,eDifficulty,country,eRegion,eDestination,eActivity,eDate,eZone,eState,uniquee,eBrief FROM event where eOrganizer =$id ");
       $event= $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $event;
   }
   
    public function removeEvent($orgid,$trailid) {
       // echo "WELCOME RAHUL" ;exit;
       $sql = "DELETE FROM event where eOrganizer= :orgid AND id = :trailid";
       $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':orgid', $orgid, PDO::PARAM_INT);  
        $stmt->bindParam(':trailid', $trailid, PDO::PARAM_INT);  
        $stmt->execute();
    }
   
   public function matchtrails($id) {
       $stmt =  $this->connection->query("SELECT trailid FROM orgtrails");
       $trails = $stmt->fetchAll(PDO::FETCH_ASSOC);
       
          $trails1 = array();
          foreach ($trails as $item) {
              foreach ($item as $key => $value) {
                  $trails1[]= $value;
              } 
              
          }
         
         $trails = array();
foreach ($trails1 as $key => $value) {
       $stmt = $this->connection->query("SELECT id,coverImage,articleTitle,typeOfActivity,durationMax,durationMin,difficulty,zone,state,uniquee FROM article where id =$value");
       $trails[]= $stmt->fetchAll(PDO::FETCH_ASSOC);
   
}
return $trails;
      
   }
   
   public function removeTrails($orgid,$trailid) {
       $sql = "DELETE FROM orgtrails where orgid= :orgid AND trailid = :trailid";
       $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':orgid', $orgid, PDO::PARAM_INT);  
        $stmt->bindParam(':trailid', $trailid, PDO::PARAM_INT);  
        $stmt->execute();
              
   }
   
   public function getCountries() {
       $stmt = $this->connection->query("SELECT * FROM countries ");
       $countries = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $countries;
   }
   
   public function getContinents() {
       $stmt = $this->connection->query("SELECT * FROM continents ");
       $continents = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $continents;
   }
   
   public function checktrails($orgid,$trailid) {
        $stmt = $this->connection->query("SELECT COUNT(*) as total FROM orgtrails where orgid=$orgid AND trailid =$trailid");
       $counttrail = $stmt->fetch(PDO::FETCH_NUM);
       return $counttrail ;
       
   }
//   public function addTrails($orgid,$trailid) {
//       $stmt = $this->connection->query("");
//       $trails = $stmt->fetchAll(PDO::FETCH_ASSOC);
//       return $trails;
//   }
   
   

}
