<?php

namespace Adventure\LoginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ChangePassword
 */
class ChangePassword
{
   /**
     * @var string
     * @Assert\NotBlank(message="Please enter new and confirm password")
     * @Assert\Length(
     *      min = "3",
     *      max = "100",
     *      minMessage = "Your Password  must be at least {{ limit }} characters length",
     *      maxMessage = "Your Password cannot be longer than {{ limit }} characters length"
     * )
     */
   private $newpassword;
    /**
     * Set newpassword
     *
     * @param string $newpassword
     * @return ChangePassword
     */
    public function setNewpassword($newpassword)
    {
        $this->newpassword = $newpassword;

        return $this;
    }

    /**
     * Get newpassword
     *
     * @return string 
     */
    public function getNewpassword()
    {
        return $this->newpassword;
    }
}
