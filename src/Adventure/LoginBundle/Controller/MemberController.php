<?php

namespace Adventure\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Adventure\LoginBundle\Form\RegisterType;
use Adventure\LoginBundle\Form\ChangePasswordType;
use Adventure\LoginBundle\Entity\BidMember;
use Adventure\BiddingBundle\Entity\BidUser;
use Adventure\BiddingBundle\Entity\BidOrganiser;
use Adventure\LoginBundle\Entity\ChangePassword;


/**
 * Description of MemberController
 *
 * @author Jhuma Mandal
 * Written : 10-Dec-2014
 * Description : Controller to control member functions.
 */
class MemberController extends Controller
{
    /*
     * Function : homeAction
     * Description: open ups home page.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 23-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function homeAction() {
        
        $em = $this->getDoctrine()->getManager();
        
        $request = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->findAll();
        
        //return the home page.
        return $this->render('AdventureLoginBundle:Member:home.html.twig',array(
            'request' => $request
        ));
    }
    
    /*
     * Function : indexAction
     * Description: open ups two buttons for registeration.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function indexAction() {
        
        //return the default page with two buttons for registeration.
        return $this->render('AdventureLoginBundle:Member:index.html.twig');
    }
    
    /*
     * Function : userRegisterAction
     * Description: open ups the registeration form with "ROLE_USER" for user.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function userRegisterAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $memberForm = new BidMember();
        $userForm = new BidUser();
        
        //Create registration form for user
        $registerForm = $this->createForm(new RegisterType(), $memberForm, array(
                'action' => $this->generateUrl('adventure_login_user_register'),
                'method' => 'POST',
            ));
        
        //validating form before saving to database
        if ($request->getMethod() == 'POST') {
            $registerForm->handleRequest($request);
            if ($registerForm->isValid()) {
                
                $password = $memberForm->getPassword();
                $confPassword= $memberForm->getConfirmpassword();
                if($password!=$confPassword){
                    //Displaying message after getting error while submitting form.
                    $this->get('session')->getFlashBag()->add(
                        'warning', 'Password and confirm password doesnot matched.');
                    
                    //returning the form if password and confirm password do not match.
                    return $this->render('AdventureLoginBundle:Member:register.html.twig', array(
                                'Form' => $registerForm->createView(),
                    ));
                }
                //setting data to store in database.
                $activationCode = rand();
                $memberName = $memberForm->getUsername();
                $memberEmail= $memberForm->getEmail();
                
                $memberForm->setStatus(1);
                $memberForm->setRole('ROLE_USER');
                $memberForm->setVerify('yes');
                $memberForm->setActivation($activationCode);
                //storing data to member table.
                $em->persist($memberForm);
                $em->flush();
                $memberId = $memberForm->getId();
                $userForm->setName($memberName);
                $userForm->setEmail($memberEmail);
                $userForm->setMemberId($memberId);
                $em->persist($userForm);
                //storing data to user table.
                $em->flush();
                
                $role = 'USER';
                
                $message = \Swift_Message::newInstance()
                        ->setSubject('Email Verification')
                        ->setFrom('Adventure@aeon.com')
                        ->setTo($memberEmail)
                        ->setBody(
                        $this->renderView(
                                'AdventureLoginBundle:Member:verifyEmailMessage.html.twig', array(
                                    'code' => $activationCode,
                                    'role' => $role,
                                    'memberId' => $memberId
                    )));

            // sending mail for verifying email id.
            $result = $this->get('mailer')->send($message);
            //echo $result;exit;
            if ($result != 1) {
                //Displaying message after getting error while submitting form.
                $this->get('session')->getFlashBag()->add(
                        'notice', 'An Error occured while processing, Please try again.');
            }
                //redirecting to page with proper message after sending mail.
                return new RedirectResponse($this->generateUrl('adventure_login_verify_email'));
            }
            
            //returning the form after getting error while submitting form
            return $this->render('AdventureLoginBundle:Member:register.html.twig', array(
                        'Form' => $registerForm->createView(),
            ));
        }
        
        //redirecting to the member registeration form.
        return $this->render('AdventureLoginBundle:Member:register.html.twig', array(
                    'Form' => $registerForm->createView()
                ));
    }
    
    /*
     * Function : organiserRegisterAction
     * Description: open ups the registeration form with "ROLE_ORGANISER" for organiser.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function organiserRegisterAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $memberForm = new BidMember();
        $userForm = new BidOrganiser();
         
        //Create registration form for user
        $registerForm = $this->createForm(new RegisterType(), $memberForm, array(
                'action' => $this->generateUrl('adventure_login_organiser_register'),
                'method' => 'POST',
            ));
        //validating form before saving to database
        if ($request->getMethod() == 'POST') {
            $registerForm->handleRequest($request);
            if ($registerForm->isValid()) {
                
                $password = $memberForm->getPassword();
                $confPassword= $memberForm->getConfirmpassword();
                if($password!=$confPassword){
                    //Displaying message after getting error while submitting form.
                    $this->get('session')->getFlashBag()->add(
                            'warning', 'Password and confirm password not matched.');
                    
                    //returning the form if password and confirm password do not match.
                    return $this->render('AdventureLoginBundle:Member:register.html.twig', array(
                                'Form' => $registerForm->createView(),
                    ));
                }
                
                //setting data to store in database.
                $activationCode = rand();
                $memberForm->setActivation($activationCode);
                $memberName = $memberForm->getUsername();
                $memberEmail= $memberForm->getEmail();
                
                
                $memberForm->setStatus(1);
                $memberForm->setRole('ROLE_ORGANISER');
                $memberForm->setVerify('yes');
                $email = $memberForm->getEmail('email');
                //storing data to member table.
                $em->persist($memberForm);
                $em->flush();
                $memberId = $memberForm->getId();
                $userForm->setName($memberName);
                $userForm->setEmail($memberEmail);
                $userForm->setMemberId($memberId);
                $em->persist($userForm);
                //storing data to organiser table.
                $em->flush();
                $role = 'ORGANISER';
                
                $message = \Swift_Message::newInstance()
                        ->setSubject('Email Verification')
                        ->setFrom('Adventure@aeon.com')
                        ->setTo($memberEmail)
                        ->setBody(
                        $this->renderView(
                                'AdventureLoginBundle:Member:verifyEmailMessage.html.twig', array(
                                    'code' => $activationCode,
                                    'role' => $role,
                                    'memberId' => $memberId
                    )));

            // sending mail for verifying email id.
            $result = $this->get('mailer')->send($message);
            if ($result == null) {
                //Displaying message after getting error while submitting form.
                $this->get('session')->getFlashBag()->add(
                        'warning', 'An Error occured while processing, Please try again.');
            } 
                //redirecting to page with proper message after sending mail.
                return new RedirectResponse($this->generateUrl('adventure_login_verify_email'));
            }
            
            //returning the form after getting error while submitting form.
            return $this->render('AdventureLoginBundle:Member:register.html.twig', array(
                        'Form' => $registerForm->createView(),
            ));
        }
        
        //redirecting to the member registeration form.
        return $this->render('AdventureLoginBundle:Member:register.html.twig', array(
                    'Form' => $registerForm->createView()
                ));
    }
    
    /*
     * Function : verifyEmailAction
     * Description: verify email of a member for activation.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 12-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function verifyEmailAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        if(!isset($_GET['memId'])){
                //Displaying message to inform about successfull sending of email for email verification.
                $this->get('session')->getFlashBag()->add(
                        'notice', 'You are register successfully ! please login to access.');
            
            //Redirecting with a message to inform about successfull sending of email for email verification.
            return $this->render('AdventureLoginBundle:Member:index.html.twig');
        }
        $memId = $_GET['memId'];
        $role = $_GET['role'];
        $activation = $_GET['activation'];
        //Checks the activation link provided by user is correct or not.
        $emailCheck = $em->getRepository('AdventureLoginBundle:BidMember')->getVerifyEmail($memId);
        
        if($activation==$emailCheck){
            //setting verify value to "yes" after verifying email id.
            $queryUpdate = $em->createQuery(
                        'UPDATE AdventureLoginBundle:BidMember u SET u.verify =:verifyYes WHERE u.id =:memId'
                )
                ->setParameter('verifyYes', 'yes')
                ->setParameter('memId', $memId)
                ;
            $update = $queryUpdate->getResult();
            
            //message after successfu; verification of email address.
            $this->get('session')->getFlashBag()->add(
                        'notice', 'Your email address is successfully verified. ');
            
            if($role=='USER'){
                
                //Redirect to user profile completion page with a message to inform that email is verified successfully.
                return $this->redirect($this->generateUrl('adventure_bidding_user_register',array(
                    'id' => $memId
            )));
            }elseif($role=='ORGANISER'){
                
                //Redirect to organiser profile completion page with a message to inform that email is verified successfully.
            return $this->redirect($this->generateUrl('adventure_bidding_organiser_register',array(
                'id' => $memId
            )));
            }else{
            
            //message with unknown role.
            $this->get('session')->getFlashBag()->add(
                        'warning', 'NO such role defined.');
            
            //Redirect with a message to inform that role is not matched.
            return $this->render('AdventureLoginBundle:Member:verify.html.twig');
            }
        }else{
            
            //message if email address is not verified.
            $this->get('session')->getFlashBag()->add(
                        'warning', 'Your Email address is not verified. Please check your mail box to verify it.');
            
            //Redirect with a message to inform that email is not verified yet.
            return $this->render('AdventureLoginBundle:Member:verify.html.twig');
        }
        
    }
    
    /*
     * Function : changePasswordAction
     * Description: update the current password
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 23-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function changePasswordAction(Request $request) {
        
        //return to the change password page for password updation
        return $this->render('AdventureLoginBundle:Member:changePassword.html.twig');
    }
    
    /*
     * Function : updatePasswordAction
     * Description: submit update the current password
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 23-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
        public function updatePasswordAction() {
            
             
        $request = $this->getRequest();
        if($request->getMethod()=='POST'){
        $retval = true;
        $user = $this->getUser();
        if (!$user) {
            //redirect to login page
            $this->redirect($this->generateUrl('adventure_login_index'));
        }

       
        $username = $user->getUsername();
        
        
         
        $password = $user->getPassword();
        $currentPassword = $request->get('currentPassword');
        $newPassword = $request->get('newPassword');
        $confirmPassword = $request->get('confirmPassword');
        // echo $newPassword;exit;
         
        if($newPassword !==$confirmPassword){
            
          $this->get('session')->getFlashBag()->add(
                        'warning', 'New password and Confirm passowrd must be same.');
            
          $retval = false;   
            
        }

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $encodedPassword = $encoder->encodePassword($currentPassword, $user->getSalt());
        if ($password !== $encodedPassword) {

           /*
            $this->get('session')->getFlashBag()->add(
                        'error', 'Entered password didn\'t matched. Try Again');
            */
            $retval = false;
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AdventureLoginBundle:BidMember')->findOneBy(array('username' => $username,'password'=>$encodedPassword));


        if (!$user) {
           
            
            $this->get('session')->getFlashBag()->add(
                        'warning', 'Enter your Current Password correctly.');
            $retval = false;
        }

        if ($retval){
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $encodedPassword = $encoder->encodePassword($newPassword, $user->getSalt());
            $user->setPassword($encodedPassword);
            $em->flush();
            $em->clear();
            $this->get('session')->getFlashBag()->add(
                        'notice', 'Password changed successfully.');
            
        }

        $referer = $this->getRequest()->headers->get('referer');
       return $this->redirect($referer);
    }
    }
    
}
