<?php

namespace Adventure\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Adventure\BiddingBundle\Entity\BidUserRequest;
use Adventure\LoginBundle\Entity\BidMember;
use Adventure\BiddingBundle\Entity\BidOrganiserResponse;
use Adventure\BiddingBundle\Form\RegisterUserType;
use Adventure\BiddingBundle\Form\RequestUserType;
use Adventure\BiddingBundle\Form\ReplyRequestType;
use Adventure\BiddingBundle\Entity\BidOrganiser;

/**
 * Description of AdminController
 *
 * @author Jhuma Mandal
 * written : 18-Dec-2014
 * Description : Controller to control admin functions.
 */
class AdminController extends Controller
{
    
    /*
     * Function : dashboardAction
     * Description: open ups the admin dashboard.
     * ###########################################
     * Author: Rahul Rajoria
     * Written: 09-feb-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function dashboardAction(Request $request) {
        
         $user = $this->getUser();
        $memberId = $user->getId();
        //redirect to the admin dashboard.
        $em = $this->getDoctrine()->getManager();

         $requestList = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->findBy( array(), array('id' => 'DESC'));
        
            if (!$requestList) {
                        throw $this->createNotFoundException('No such user found.');
                    }
       
        return $this->render('AdventureAdminBundle:Admin:dashboardAdmin.html.twig',array(
            'requestUser' => $requestList
        ));
    }
           public function feedbackAction() {
               
               
        return $this->render('AdventureAdminBundle:Admin:feedback.html.twig');
               
           }
    
        /*
     * Function : viewCloseRequestAction
     * Description: Display close request in details.
     * ###########################################
     * Written: 08-01-2015
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * @return : updated list
     * 
     */

    public function viewCloseRequestAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $requestId = $id;   
        $user = $this->getUser();
        $memberId = $user->getId();
        //Getting request details to view
        $entityRequest = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->find($id);
       //$entityRequest2 = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->findBy($requestId,$memberId);
        if (!$entityRequest) {
            throw $this->createNotFoundException('Unable to find');
        }        
        
        //redirecting to view page with request details.
        return $this->render('AdventureAdminBundle:Admin:viewCloseRequest.html.twig',array(
            'request' => $entityRequest,
           // 'matched' => $entityRequest2
            
        ));
    }


    
        /*
     * Function : listUserAction
     * Description: open ups the list of all registered users.
     * ###########################################
     * Author: Rahul Rajoria
     * Written: 09-feb-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function listUserAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        
        $role = 'ROLE_USER';
        //Getting all records to display the user list
        $allUser =  $em->getRepository('AdventureBiddingBundle:BidUser')->findBy( array(), array('id' => 'DESC'));
        
       // $status  =   $em->getRepository('AdventureLoginBundle:BidMember')->find();
        
        if (!$allUser) {
            throw $this->createNotFoundException('No such user found.');
        }
        
        //redirect to the list of all registered members.
        return $this->render('AdventureAdminBundle:Admin:userList.html.twig', array(
                    'member' => $allUser
                ));
    }
    
    /*
     * Function : listOrganiserAction
     * Description: open ups the list of all registered organiser.
     * ###########################################
     * Author: Rahul Rajoria
     * Written: 09-feb-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function listOrganiserAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
       
        $role = 'ROLE_ORGANISER';
        //Getting all records to display the organiser list]
        $allOrganiser = $em->getRepository('AdventureBiddingBundle:BidOrganiser')->findBy( array(), array('id' => 'DESC'));
        
         //array that store data to be randered on twig  arr_name['name']
         //loop through allOrganizer
         //arr_name['name']=allOrga->getNAme();
         //orgid=AllOrg->getId()
         //member=$em->getRepository('AdventureLoginBundle:Member')->findOneById(orgid);
         //arr_name['usernaem']=member->getUserNAme();
         //loopend
      // $organiser = $em->getRepository('AdventureBiddingBundle:BidOrganiser')->find($id);
        if (!$allOrganiser) {
            throw $this->createNotFoundException('No such user found.');
        }
        
        //redirect to the list of all registered members.
        return $this->render('AdventureAdminBundle:Admin:organiserList.html.twig', array(
                    'member' => $allOrganiser
                    
                ));
    }
    
    /*
     * Function : changeStatusAction
     * Description: open ups the list of all registered members to change status.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 19-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function changeStatusAction() {

          
        $session = $this->get('session');
        if (!$session->start()) {
            $session->start();
        }
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            
            $id = $request->get('userid');
           // echo $id;exit;
            $status = $request->get('status');
             
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdventureLoginBundle:BidMember')->find($id);

            if (!$entity) {
                return new \Symfony\Component\HttpFoundation\JsonResponse('Sorry Data not found!');
            } else {
                $entity->setStatus($status);
                $em->flush();
                $em->clear();
                return new \Symfony\Component\HttpFoundation\JsonResponse('OK');
            }
        } else {
            throw $this->createNotFoundException(
                    'No Allowed ! '
            );
        }

        //returns to the member list page
        return $this->redirect($this->generateUrl('adventure_admin_user_list'));
    }
    

    
    /*
     * Function : deleteUserAction
     * Description: delete single user by id.
     * ###########################################
     * Author: Rahul Rajoria
     * Written: 09-feb-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * @return : updated list
     * 
     */
    public function deleteUserAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');

        //Getting record for deletion from member.
        $entity = $em->getRepository('AdventureLoginBundle:BidMember')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find');
        }

        $em->remove($entity);
        $em->flush();
        
        $role = "BidUser";
        
        //Getting record for deletion from user.
        $entity = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($id,$role);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find');
        }

        $em->remove($entity);
        $em->flush();

       //Displaying message after successful deletion of user by id.
            $this->get('session')->getFlashBag()->add(
                    'notice', $this->get('translator')->trans('message.success.delete')
            );
            
        //redirecting to user list page
        return $this->redirect($this->generateUrl("adventure_admin_user_list"));
    }
    
    /*
     * Function : deleteOrganiserAction
     * Description: delete single organiser by id.
     * ###########################################
     * Written: 22-Dec-2014
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * @return : updated list
     * 
     */
    public function deleteOrganiserAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');

        //Getting record for deletion
        $entity = $em->getRepository('AdventureLoginBundle:BidMember')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find');
        }

        $em->remove($entity);
        $em->flush();

        $role = "BidOrganiser";
        
        //Getting record for deletion from organiser.
        $entity = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($id,$role);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find');
        }

        $em->remove($entity);
        $em->flush();
        
        //Displaying message after successful deletion of organiser by id.
            $this->get('session')->getFlashBag()->add(
                    'notice', $this->get('translator')->trans('message.success.delete')
            );
            
        //redirecting to organiser list page
        return $this->redirect($this->generateUrl("adventure_admin_organiser_list"));
    }
    

      public function viewOpenRequestAction(Request $request, $id) {

          
        $em = $this->getDoctrine()->getManager();
        $requestId = $id;
        //Getting request details to view
        $entityRequest = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->find($requestId);
        if (!$entityRequest) {
            throw $this->createNotFoundException('Unable to find');
        }        
        
        //getting logged in organiser details
        $organiser = $this->getUser();
        $memberId = $organiser->getId();
         $listOrganiser = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getMatchingOrganiser($requestId,$memberId);
        //Getting organiser details according to the logged in member Id.
        $organiserDetail = $em->getRepository('AdventureBiddingBundle:BidOrganiser')->getOrganiserByMemberId($memberId);
        //Getting all responses details of logged in organiser to this user request. 
        $responseDetail = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getBidComment($requestId,$memberId);
        
        //redirecting to view page with request details.
        return $this->render('AdventureAdminBundle:Admin:viewOpenRequestUser.html.twig',array(
            'request' => $entityRequest,
            'organiser' => $organiserDetail,
             'listOrganiser' => $listOrganiser,
            'comment' => $responseDetail
        ));
    }
    
       
    public function OrgRequestAction() {
        
$isAjax = $this->get('Request')->isXMLHttpRequest();
    if ($isAjax) {
        //...
        return new Response('This is ajax response');
    }
    return new Response('This is not ajax!', 400);
        
    }
}
