<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * BidUserRequest
 */
class BidUserRequest
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $activity;
    
    /**
     * @var string
     */
    private $zone;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $traveler;

    /**
     * @var \DateTime
     */
    private $dateMin;

    /**
     * @var \DateTime
     */
    private $dateMax;

    
    /**
     * @var string
     */
    private $male;

    /**
     * @var string
     */
    private $female;

    /**
     * @var string
     */
    private $day;

    /**
     * @var string
     */
    private $pick;

    /**
     * @var string
     */
    private $down;

    /**
     * @var string
     */
    private $team;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->activity = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BidUserRequest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set zone
     *
     * @param string $zone
     * @return BidUserRequest
     */
    public function setZone($zone)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return string 
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return BidUserRequest
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return BidUserRequest
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return BidUserRequest
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return BidUserRequest
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set traveler
     *
     * @param string $traveler
     * @return BidUserRequest
     */
    public function setTraveler($traveler)
    {
        $this->traveler = $traveler;

        return $this;
    }

    /**
     * Get traveler
     *
     * @return string 
     */
    public function getTraveler()
    {
        return $this->traveler;
    }

    /**
     * Set dateMin
     *
     * @param \DateTime $dateMin
     * @return BidUserRequest
     */
    public function setDateMin($dateMin)
    {
        $this->dateMin = $dateMin;

        return $this;
    }

    /**
     * Get dateMin
     *
     * @return \DateTime 
     */
    public function getDateMin()
    {
        return $this->dateMin;
    }

    /**
     * Set dateMax
     *
     * @param \DateTime $dateMax
     * @return BidUserRequest
     */
    public function setDateMax($dateMax)
    {
        $this->dateMax = $dateMax;

        return $this;
    }

    /**
     * Get dateMax
     *
     * @return \DateTime 
     */
    public function getDateMax()
    {
        return $this->dateMax;
    }
    
    /**
     * Set male
     *
     * @param string $male
     * @return BidUserRequest
     */
    public function setMale($male)
    {
        $this->male = $male;

        return $this;
    }

    /**
     * Get male
     *
     * @return string 
     */
    public function getMale()
    {
        return $this->male;
    }

    /**
     * Set female
     *
     * @param string $female
     * @return BidUserRequest
     */
    public function setFemale($female)
    {
        $this->female = $female;

        return $this;
    }

    /**
     * Get female
     *
     * @return string 
     */
    public function getFemale()
    {
        return $this->female;
    }

     /**
     * Set activity
     *
     * @param string $activity
     * @return BidUserRequest
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string 
     */
    public function getActivity()
    {
        return $this->activity;
    }
    
    /**
     * Set day
     *
     * @param string $day
     * @return BidUserRequest
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return string 
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set pick
     *
     * @param string $pick
     * @return BidUserRequest
     */
    public function setPick($pick)
    {
        $this->pick = $pick;

        return $this;
    }

    /**
     * Get pick
     *
     * @return string 
     */
    public function getPick()
    {
        return $this->pick;
    }

    /**
     * Set down
     *
     * @param string $down
     * @return BidUserRequest
     */
    public function setDown($down)
    {
        $this->down = $down;

        return $this;
    }

    /**
     * Get down
     *
     * @return string 
     */
    public function getDown()
    {
        return $this->down;
    }

    /**
     * Set team
     *
     * @param string $team
     * @return BidUserRequest
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return string 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return BidUserRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BidUserRequest
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return BidUserRequest
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedOnValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedOnValue()
    {
        $this->updatedAt = new \DateTime();
    }  
    
    public function __toString()
    {
        return $this->name;
    }
    

    /**
     * Add activity
     *
     * @param \Adventure\BiddingBundle\Entity\BidActivity $activity
     * @return BidUserRequest
     */
    public function addActivity(\Adventure\BiddingBundle\Entity\BidActivity $activity)
    {
        $this->activity[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param \Adventure\BiddingBundle\Entity\BidActivity $activity
     */
    public function removeActivity(\Adventure\BiddingBundle\Entity\BidActivity $activity)
    {
        $this->activity->removeElement($activity);
    }

    /**
     * Add country
     *
     * @param \Adventure\BiddingBundle\Entity\BidCountry $country
     * @return BidUserRequest
     */
    public function addCountry(\Adventure\BiddingBundle\Entity\BidCountry $country)
    {
        $this->country[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \Adventure\BiddingBundle\Entity\BidCountry $country
     */
    public function removeCountry(\Adventure\BiddingBundle\Entity\BidCountry $country)
    {
        $this->country->removeElement($country);
    }
    /**
     * @var string
     */
    private $description;


    /**
     * Set description
     *
     * @param string $description
     * @return BidUserRequest
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @var \Adventure\BiddingBundle\Entity\BidUser
     */
    private $user;


    /**
     * Set user
     *
     * @param \Adventure\BiddingBundle\Entity\BidUser $user
     * @return BidUserRequest
     */
    public function setUser(\Adventure\BiddingBundle\Entity\BidUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Adventure\BiddingBundle\Entity\BidUser 
     */
    public function getUser()
    {
        return $this->user;
    }
}
