<?php

namespace Adventure\BiddingBundle\Events;
 
use Symfony\Component\EventDispatcher\Event;
use Adventure\BiddingBundle\Entity\BidUserRequest;

/**
 * Description of RequestMatchEvent
 *
 * @author Jhuma Mandal
 * written: 08-Jan-2015
 */
class RequestMatchEvent extends Event{
    
    private $requestMatch;
    
    
    public function __construct(BidUserRequest $requestMatch)
    {
        $this->requestMatch = $requestMatch;
    }
 
    /*
     * Function : getRequestMatch
     * Description: execute this function to sent mail after getting organiser list matching with user request activities.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-Jan-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     */
    public function getRequestMatch()
    {
        return $this->requestMatch;
    }
    
   
}