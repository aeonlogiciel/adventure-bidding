<?php

namespace Adventure\BiddingBundle\Model;

/**
 * Description of activityModel
 *
 * @author Jhuma Mandal
 */
class activityModel {
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $typeOfActivity;

    /**
     * @var string
     */
    private $catImg;

    /**
     * @var integer
     */
    private $catId;

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeOfActivity
     *
     * @param string $typeOfActivity
     * @return BidActivity
     */
    public function setTypeOfActivity($typeOfActivity)
    {
        $this->typeOfActivity = $typeOfActivity;

        return $this;
    }

    /**
     * Get typeOfActivity
     *
     * @return string 
     */
    public function getTypeOfActivity()
    {
        return $this->typeOfActivity;
    }

    /**
     * Set catImg
     *
     * @param string $catImg
     * @return BidActivity
     */
    public function setCatImg($catImg)
    {
        $this->catImg = $catImg;

        return $this;
    }

    /**
     * Get catImg
     *
     * @return string 
     */
    public function getCatImg()
    {
        return $this->catImg;
    }

    /**
     * Set catId
     *
     * @param integer $catId
     * @return BidActivity
     */
    public function setCatId($catId)
    {
        $this->catId = $catId;

        return $this;
    }

    /**
     * Get catId
     *
     * @return integer 
     */
    public function getCatId()
    {
        return $this->catId;
    }
}
