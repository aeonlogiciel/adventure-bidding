<?php

namespace Adventure\BiddingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of RequestUserType
 *
 * @author Jhuma Mandal
 * Written: 14-01-2015
 */
class ReplyRequestType extends AbstractType{
    /*
     * Function : buildForm
     * Description: Creates user reply form for a request
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 14-01-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * 
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('description', 'choice', array(
                      'choices' => array(
                          'empty_value' => 'Select to reply',
                            'Price is high' => 'Price is high',
                            'Changed my mind' => 'Changed my mind',
                            'Proposed date is suitable' => 'Proposed date is suitable',
                            'Got a better deal somewhere else' => 'Got a better deal somewhere else',
                            'We do not need an organizer for it' => 'We do not need an organizer for it'
                          )),array('required'=> true));
                
    }
        
    public function getName() {
        return 'userRequestReply';
    }
    
     public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Adventure\BiddingBundle\Entity\BidOrganiserResponse'
        ));
    }
}
