<?php

namespace Adventure\BiddingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
/**
 * Description of RegisterUserType
 *
 * @author Jhuma Mandal
 * Written: 08-12-2014
 */
class RegisterUserType extends AbstractType{
    
    /*
     * Function : buildForm
     * Description: Creates user registration form
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-12-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * 
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $countries = Intl::getRegionBundle()->getCountryNames();
        $builder->add('name','text', array('required'=> true))
                ->add('email','email', array('required'=> true,'disabled'=> true))
                ->add('phone','text', array('required'=> true))
                ->add('address','textarea',array('required'=> true))
                ->add('zip','integer',array('required'=> false))
                ->add('country','choice', array(
                    'choices' => array(
                        $countries
                )),array('required'=> false))
                ->add('applicantphoto','file', array('label' => 'Image', 'data_class' => null,'required'=> false))
               ;
    }
        
    public function getName() {
        return 'userRegister';
    }
    
     public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Adventure\BiddingBundle\Entity\BidUser'
        ));
    }
}
