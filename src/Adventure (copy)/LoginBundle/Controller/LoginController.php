<?php

namespace Adventure\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;

use Adventure\LoginBundle\Entity\BidMember;
use Adventure\BiddingBundle\Entity\BidUser;

/**
 * Description of LoginController
 *
 * @author Jhuma Mandal
 * Written : 10-Dec-2014
 * Description : Controller to control login functions.
 */
class LoginController extends Controller {
    
    /*
     * Function : loginAdminAction
     * Description: Login page and check login credentials for Admin.
     * ###########################################
     * Written: 11-Dec-2014
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @return : login page
     * 
     */
    public function loginAdminAction() {
        $request = $this->getRequest();
        $session = $request->getSession();
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('AdventureLoginBundle:Member:loginAdmin.html.twig', array('last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error));
    }
    
    public function logincheckAction() {
        throw new \RuntimeException('Please Configure the check_path in your security setting.');
    }

    public function logoutAction() {
        throw new \RuntimeException('Please Configure the logout  in your security setting.');
    }
    
    /*
     * Function : loginAction
     * Description: Login page and check login credentials.
     * ###########################################
     * Written: 11-Dec-2014
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @return : login page
     * 
     */
    public function loginAction() {
        $request = $this->getRequest();
        $session = $request->getSession();
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('AdventureLoginBundle:Member:index.html.twig', array('last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error));
    }
   

    /*
     * Function : loginRedirectAction
     * Description: redirect the user according to the role.
     * ###########################################
     * Written: 11-Dec-2014
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @return : open correct page with valid user.
     * 
     */
    public function loginRedirectAction(Request $request) {

        try {
            $user = $this->getUser();
           
            // user is logged in then rediret to there crossponding dashboard 
            if ($user) {
                $role = $user->getRole();
                $memberVerify = $user->getVerify();
                $memberStatus = $user->getStatus();
                
                $session = $this->get('session');
                if (!$session->start()) {
                    $session->start();
                }
                
                if($memberVerify=="no"){
                    $this->get('session')->getFlashBag()->add(
                        'warning', 'Your Email is not verified yet. Please verify it.');
                    //redirect to error page with message if member email is not verified.
                    return $this->render('AdventureLoginBundle:Member:verify.html.twig');
                }
                elseif($memberStatus=="inactive"){
                    $this->get('session')->getFlashBag()->add(
                        'warning', 'Your account is not activated by the Admin.');
                    //redirect to error page with message if member is not activated by admin.
                    return $this->render('AdventureLoginBundle:Member:verify.html.twig');
                }else{
                //redirecting to the dashboard according to the login member role.
                switch ($role) {
                    case "ROLE_ADMIN":
                        //Redirect to admin dashboard
                        
                        return $this->redirect($this->generateUrl('adventure_admin_dashboard'));
                        break;
                    case "ROLE_USER":
                        //Redirect to user dashboard
                        //query to member tabel
                        //get id
                        //set to session
                        
                        
                        return $this->redirect($this->generateUrl('adventure_bidding_user_dashboard'));
                        break;
                    case "ROLE_ORGANISER":
                        //Redirect to organiser dashboard
                        return $this->redirect($this->generateUrl('adventure_bidding_organiser_dashboard'));
                        break;
                    default:
                        //Redirect to login page if role of member doesn't match with any.
                        return $this->redirect($this->generateUrl('adventure_login_index'));
                }
                }
            }
            else {
                //redirect to login page with invalid credentials
                return $this->redirect($this->generateUrl('adventure_login_index'));
            }
        } catch (Exception $ex) {
            throw new $this->createNotFoundException("Sorry You are not authorize ");
        }
    }
    
    /*
     * Function : forgotPasswordAction
     * Description: redirect the user to forgot password.
     * ###########################################
     * Written: 11-12-2014
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @return : open forgot password page
     * 
     */
    public function forgotPasswordAction() {

        //return form to get email id for recovery of password.
        return $this->render('AdventureLoginBundle:Login:forgotPassword.html.twig');
    }

    /*
     * Function : forgotPasswordMailAction
     * Description: sending mail to the user on request of forgot password.
     * ###########################################
     * Written: 11-12-2014
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @return : mail sent with username and password.
     * 
     */
    public function forgotPasswordMailAction(Request $request) {

        $email = $request->request->get('email');
        $em = $this->getDoctrine()->getManager();
        
        //get username and password after matching the member email id with database.
        $emailCheck = $em->getRepository('AdventureLoginBundle:BidMember')->getPasswordQuery($email);
        $username = $em->getRepository('AdventureLoginBundle:BidMember')->getUserNameQuery($email);

        if ($emailCheck == null) {
            //Displaying message if provided email id is not registered.
                $this->get('session')->getFlashBag()->add(
                        'warning', 'This Email Address is not registered with us.');
            //return form to get email id for recovery of password if the email id is not valid.
            return $this->render('AdventureLoginBundle:Login:forgotPassword.html.twig', array(
                        'message' => $message
            ));
        } else {
            $message = \Swift_Message::newInstance()
                    ->setSubject('Password Recovery')
                    ->setFrom('Adventure@aeon.com')
                    ->setTo($email)
                    ->setBody(
                    $this->renderView(
                            //get email body for recovery of password.
                            'AdventureLoginBundle:Login:emailForgotPassword.txt.twig', array(
                                'emailCheck' => $emailCheck,
                                'email' => $email
                    )));
            $this->get('mailer')->send($message);

            // Send the mail.
            $result = $this->get('mailer')->send($message);
            if ($result == null) {
                //Displaying message after getting any processing error while sending mail.
                $this->get('session')->getFlashBag()->add(
                        'warning', 'An Error occured while processing, Please try again.');
                
            } else {
                //Displaying message after getting error while submitting form.
                $this->get('session')->getFlashBag()->add(
                        'notice', 'An Email with your username and password is successfully send at your mail box.');
                
            }
            //return login page after sending email successfully.
            return $this->render('AdventureLoginBundle:Member:index.html.twig');
        }
        //return to forgot password form page.
        return $this->render('AdventureLoginBundle:Member:index.html.twig');
    }

}
