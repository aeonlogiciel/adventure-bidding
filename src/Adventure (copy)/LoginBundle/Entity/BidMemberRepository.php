<?php

namespace Adventure\LoginBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * BidMemberRepository
 *
 * Author Jhuma Mandal
 * Written : 12-Dec-2014
 * Description : Repository to define functions.
 */
class BidMemberRepository extends EntityRepository
{
    
    /*
     * Function : getPasswordQuery
     * Description: Get password of the registered member.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 12-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function getPasswordQuery($email) {
        
        //getting records from member table
        $queryUser = $this->getEntityManager()->createQuery(
                        'SELECT u.password FROM AdventureLoginBundle:BidMember u WHERE u.email = :email '
                )
                ->setParameter('email', $email);
        $password = $queryUser->getResult();
        
        if(empty($password)){
            $userPassword = null;
        }
        else{
            foreach($password as $val){
                $userPassword = $val['password'];
            }
        }
        //returns the password .
        return $userPassword;
    }
    
    /*
     * Function : getUserNameQuery
     * Description: Get username of the registered member.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 12-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function getUserNameQuery($email) {
        //getting records from member table
        $queryUser = $this->getEntityManager()->createQuery(
                        'SELECT u.username FROM AdventureLoginBundle:BidMember u WHERE u.email = :email '
                )
                ->setParameter('email', $email);
        $username = $queryUser->getResult();
        
        if(empty($username)){
            $userUsername = null;
        }
        else{
            foreach($username as $val){
                $userUsername = $val['username'];
            }
        }
        //returns the username .
        return $userUsername;
    }
    
    /*
     * Function : getVerifyEmail
     * Description: checks the activation code is same as the code provided by member.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 17-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function getVerifyEmail($memId) {
        
        $queryUser = $this->getEntityManager()->createQuery(
                        'SELECT u.activation FROM AdventureLoginBundle:BidMember u WHERE u.id = :memId '
                )
                ->setParameter('memId', $memId);
        $verify = $queryUser->getResult();
        foreach($verify as $val){
                $verifyCode = $val['activation'];
        }
        return $verifyCode;
    }
    
    /*
     * Function : getDetailByMemberId
     * Description: Get deail of user/organiser with reference to member id.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 18-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function getDetailByMemberId($id,$role) {
        
        $query = $this->getEntityManager()->createQuery(
                        'SELECT u FROM AdventureBiddingBundle:'.$role.' u WHERE u.memberId = :memId '
                )
                ->setParameter('memId', $id)
                ;
        $result = $query->getSingleResult();
        return $result;
    }
    
    /*
     * Function : getRecordsByRole
     * Description: Get all user/organiser with reference to role.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 22-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function getRecordsByRole($role) {
        
        $query = $this->getEntityManager()->createQuery(
                        'SELECT u FROM AdventureLoginBundle:BidMember u WHERE u.role = :roleValue ORDER by u.id DESC'
                )
                ->setParameter('roleValue', $role)
                
                ;
        $result = $query->getResult();
        return $result;
    }
    
}
