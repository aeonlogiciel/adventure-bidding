<?php

namespace Adventure\BridgeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AdventureBridgeBundle:Default:index.html.twig', array('name' => $name));
    }
}
