<?php

namespace Adventure\LoginBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChangePasswordType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('newpassword', 'repeated', array(
                    'type' => 'password',
                    'invalid_message' => 'Password and Confirm Password Must Be Same.',
                    'required' => true,
                    'first_options' => array('label' => 'Password *', 'attr' => array(
                            'title' => 'Please Enter Your Password',
                            'placeholder' => 'Password (more than 3 Charcters) '
                        )
                    ),
                    'second_options' => array('label' => 'Confirm Password *',
                        'attr' => array(
                            'title' => 'Please Re-Enter Your Password',
                            'placeholder' => 'Re-Enter Password '
                        )
                    )
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Adventure\LoginBundle\Entity\ChangePassword'
        ));
    }

    public function getName() {
        return 'change_passwd';
    }

}

