<?php

namespace Adventure\LoginBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of RegisterType
 *
 * @author Jhuma Mandal
 * written: 10-Dec-14
 * Description : Common registeraion form for login.
 */
class RegisterType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('username','text', array('required'=> true))
                ->add('password','password', array('required'=> true))
                ->add('confirmpassword','password', array('required'=> true))
                ->add('email','email', array('required'=> true))
                
               ;
    }
        
    public function getName() {
        return 'register';
    }
    
     public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Adventure\LoginBundle\Entity\BidMember'
        ));
    }
}
