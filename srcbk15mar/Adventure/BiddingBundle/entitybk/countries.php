<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * countries
 */
class countries
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $continent;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return countries
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set continent
     *
     * @param integer $continent
     * @return countries
     */
    public function setContinent($continent)
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * Get continent
     *
     * @return integer 
     */
    public function getContinent()
    {
        return $this->continent;
    }
    /**
     * @var \Adventure\BiddingBundle\Entity\continents
     */
    private $continents;


    /**
     * Set continents
     *
     * @param \Adventure\BiddingBundle\Entity\continents $continents
     * @return countries
     */
    public function setContinents(\Adventure\BiddingBundle\Entity\continents $continents = null)
    {
        $this->continents = $continents;

        return $this;
    }

    /**
     * Get continents
     *
     * @return \Adventure\BiddingBundle\Entity\continents 
     */
    public function getContinents()
    {
        return $this->continents;
    }
}
