<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BidActivity
 */
class BidActivity
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $typeOfActivity;

    /**
     * @var string
     */
    private $catImg;

    /**
     * @var integer
     */
    private $catId;

    /**
     * @var integer
     */
    private $photologuesValue;

    /**
     * @var integer
     */
    private $traveloguesValue;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeOfActivity
     *
     * @param string $typeOfActivity
     * @return BidActivity
     */
    public function setTypeOfActivity($typeOfActivity)
    {
        $this->typeOfActivity = $typeOfActivity;

        return $this;
    }

    /**
     * Get typeOfActivity
     *
     * @return string 
     */
    public function getTypeOfActivity()
    {
        return $this->typeOfActivity;
    }

    /**
     * Set catImg
     *
     * @param string $catImg
     * @return BidActivity
     */
    public function setCatImg($catImg)
    {
        $this->catImg = $catImg;

        return $this;
    }

    /**
     * Get catImg
     *
     * @return string 
     */
    public function getCatImg()
    {
        return $this->catImg;
    }

    /**
     * Set catId
     *
     * @param integer $catId
     * @return BidActivity
     */
    public function setCatId($catId)
    {
        $this->catId = $catId;

        return $this;
    }

    /**
     * Get catId
     *
     * @return integer 
     */
    public function getCatId()
    {
        return $this->catId;
    }

    /**
     * Set photologuesValue
     *
     * @param integer $photologuesValue
     * @return BidActivity
     */
    public function setPhotologuesValue($photologuesValue)
    {
        $this->photologuesValue = $photologuesValue;

        return $this;
    }

    /**
     * Get photologuesValue
     *
     * @return integer 
     */
    public function getPhotologuesValue()
    {
        return $this->photologuesValue;
    }

    /**
     * Set traveloguesValue
     *
     * @param integer $traveloguesValue
     * @return BidActivity
     */
    public function setTraveloguesValue($traveloguesValue)
    {
        $this->traveloguesValue = $traveloguesValue;

        return $this;
    }

    /**
     * Get traveloguesValue
     *
     * @return integer 
     */
    public function getTraveloguesValue()
    {
        return $this->traveloguesValue;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BidActivity
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return BidActivity
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return BidActivity
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedOnValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedOnValue()
    {
       $this->updatedAt = new \DateTime();
    }
    
    public function __toString()
    {
        return $this->typeOfActivity;
    }
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $organiser;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->organiser = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add organiser
     *
     * @param \Adventure\BiddingBundle\Entity\BidOrganiser $organiser
     * @return BidActivity
     */
    public function addOrganiser(\Adventure\BiddingBundle\Entity\BidOrganiser $organiser)
    {
        $this->organiser[] = $organiser;

        return $this;
    }

    /**
     * Remove organiser
     *
     * @param \Adventure\BiddingBundle\Entity\BidOrganiser $organiser
     */
    public function removeOrganiser(\Adventure\BiddingBundle\Entity\BidOrganiser $organiser)
    {
        $this->organiser->removeElement($organiser);
    }

    /**
     * Get organiser
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrganiser()
    {
        return $this->organiser;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $request;


    /**
     * Add request
     *
     * @param \Adventure\BiddingBundle\Entity\BidUserRequest $request
     * @return BidActivity
     */
    public function addRequest(\Adventure\BiddingBundle\Entity\BidUserRequest $request)
    {
        $this->request[] = $request;

        return $this;
    }

    /**
     * Remove request
     *
     * @param \Adventure\BiddingBundle\Entity\BidUserRequest $request
     */
    public function removeRequest(\Adventure\BiddingBundle\Entity\BidUserRequest $request)
    {
        $this->request->removeElement($request);
    }

    /**
     * Get request
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRequest()
    {
        return $this->request;
    }
    /**
     * @var \Adventure\BiddingBundle\Entity\bidCategory
     */
    private $bidCategory;


    /**
     * Set bidCategory
     *
     * @param \Adventure\BiddingBundle\Entity\bidCategory $bidCategory
     * @return BidActivity
     */
    public function setBidCategory(\Adventure\BiddingBundle\Entity\bidCategory $bidCategory = null)
    {
        $this->bidCategory = $bidCategory;

        return $this;
    }

    /**
     * Get bidCategory
     *
     * @return \Adventure\BiddingBundle\Entity\bidCategory 
     */
    public function getBidCategory()
    {
        return $this->bidCategory;
    }
}
