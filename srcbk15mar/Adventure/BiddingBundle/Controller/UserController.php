<?php

namespace Adventure\BiddingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

use Adventure\BiddingBundle\Events\CoreEvents;
use Adventure\BiddingBundle\Events\RequestMatchEvent;
use Adventure\BiddingBundle\Entity\BidUserRequest;
use Adventure\LoginBundle\Entity\BidMember;
use Adventure\BiddingBundle\Entity\BidOrganiserResponse;
use Adventure\BiddingBundle\Form\RegisterUserType;
use Adventure\BiddingBundle\Form\RequestUserType;
use Adventure\BiddingBundle\Form\ReplyRequestType;

/**
 * Description of UserController
 *
 * Author Jhuma Mandal
 * Written : 05-Dec-2014
 * Description : Controller to control user functions.
 */
class UserController extends Controller{
    
    /*
     * Function : dashboardAction
     * Description: open ups the user dashboard.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 05-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
        public function dashboardAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
       $memberId = $user->getId();
        
       // $id = $session->get('id');  
       
         
        //Getting all requests of logged in user to display
        $requestList = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getRequestByMemberId($memberId);
         $response = array();
        foreach ($requestList as $reqid ) {
            $returnObject = array();
            $returnObject['reqid'] = $reqid->getId(); 
            $returnObject['trip'] = $reqid->getName();
            $returnObject['status'] = $reqid->getStatus();
            $reqidu = $returnObject['reqid'];
            $totalrows = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getMessageByMemberId($reqidu);
            $returnObject['messagecount'] = $totalrows;
            $response[]=$returnObject;
        }
       
       // echo '<pre>';print_r($response); exit;
        
        //redirecting to the request list
        return $this->render('AdventureBiddingBundle:User:dashboardUser.html.twig',array(
            'requestUser' => $response
        ));
        
        
    }
    
    
    /*
     * Function : registerAction
     * Description: open ups the registration form to complete profile of an user.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function registerAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();
        $role = "BidUser";
        
        //getting record from member with id
        $userDetails = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($id,$role);
        if (!$userDetails) {
            throw $this->createNotFoundException('No such user found.');
        }
        
        //Create registration form for user
        $registerForm = $this->createForm(new RegisterUserType(), $userDetails, array(
                'action' => $this->generateUrl('adventure_bidding_user_register', array('id' => $id)),
                'method' => 'POST',
            ));
        
        //validating form before saving to database
        if ($request->getMethod() == 'POST') {
            $registerForm->handleRequest($request);
            if ($registerForm->isValid()) {
                
                //storing data to user request table.
                $userDetails->setUpdatedAt(new \DateTime());
                $em->flush();
                
                //Displaying message after getting error while submitting form.
                $this->get('session')->getFlashBag()->add(
                        'notice', $this->get('translator')->trans('form.message.success.active')
                );
                
                //redirecting to user dashboard after successful submission of form.
                return new RedirectResponse($this->generateUrl('adventure_login_index'));
            }
            
            //Displaying message after getting error while submitting form.
            $this->get('session')->getFlashBag()->add(
                    'notice', $this->get('translator')->trans('form.message.mail.verificationLink')
            );
            
            //returning the form after getting error while submitting form.
            return $this->render('AdventureBiddingBundle:User:registerUser.html.twig', array(
                        'Form' => $registerForm->createView(),
            ));
        }
        
        //redirecting to the user registreration form
        return $this->render('AdventureBiddingBundle:User:registerUser.html.twig', array(
                    'Form' => $registerForm->createView()
                ));
    }
    
    /*
     * Function : viewProfileAction
     * Description: open ups the user profile in view mode.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 23-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function viewProfileAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $memberId = $user->getId();
        $role = 'BidUser';
        
        $userDetails = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($memberId,$role);
        
        //echo '<pre>';print_r($userDetails);exit;
        
        //redirecting to the user profile in view mode
        return $this->render('AdventureBiddingBundle:User:viewProfileUser.html.twig',array(
            'member' => $userDetails
        ));
    }
    

        /*
     * Function : editProfileAction
     * Description: open ups the user profile in edit mode.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 23-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function editProfileAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $role = "BidUser";
        $user = $this->getUser();
        $id = $user->getId();
        //getting record from member with id
        $userDetails = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($id,$role);
        if (!$userDetails) {
            throw $this->createNotFoundException('No such user found.');
        }
        
        //Create registration form for user
        $registerForm = $this->createForm(new RegisterUserType(), $userDetails, array(
                'action' => $this->generateUrl('adventure_bidding_user_edit_profile'),
                'method' => 'POST',
            ));
        
        //validating form before saving to database
        if ($request->getMethod() == 'POST') {
            $registerForm->handleRequest($request);
            if ($registerForm->isValid()) {
                
                //storing data to user request table.
                $userDetails->setUpdatedAt(new \DateTime());
                
                $em->flush();
                //Displaying message after getting error while submitting form.
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Your profile is updated successfully.');
            }
            //returning the form after getting error while submitting form.
            return $this->render('AdventureBiddingBundle:User:editProfileUser.html.twig', array(
                        'Form' => $registerForm->createView(),
            ));
        }
        
        //redirecting to the user registreration form
        return $this->render('AdventureBiddingBundle:User:editProfileUser.html.twig', array(
                    'Form' => $registerForm->createView()
                ));
    }
    
    /*
     * Function : createRequestAction
     * Description: open up form to create request for user.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 26-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function createRequestAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $userForm = new BidUserRequest();
        
        //getting logged in user details
        $user = $this->getUser();
        $memberId = $user->getId();
        $table = 'BidUser';
        //Get user detail by logged in member.
        $userDetail = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($memberId,$table);
        
        //Create request form for user
        $requestForm = $this->createForm(new RequestUserType(), $userForm, array(
                'action' => $this->generateUrl('adventure_bidding_user_create_request'),
                'method' => 'POST',
            ));
        
        if ($request->getMethod() == 'POST') {
            $requestForm->handleRequest($request);
            if ($requestForm->isValid()) {
                
                $userForm->setUser($userDetail);
                if($memberId!=""){
                    
                //storing data to user-request table.
                $em->persist($userForm);
                $em->flush();
                $requestId = $userForm->getId();
                
                $location = $userForm->getLocation();
                
                //Finding matching organiser according to the user request 
                $listOrganiser = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getMatchingOrganiser($requestId,$memberId);
                
                // Dispatch event after getting organisers email list.
                $requestMatchEvent = new RequestMatchEvent($userForm);
                $requestMatchEvent = $this->container->get('event_dispatcher')->dispatch(
                        CoreEvents::REQUEST_MATCH, $requestMatchEvent);
                
                //Displaying message after successful submition of request
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Request is submitted successfully.');
                
                //redirecting to user dashboard
                return new RedirectResponse($this->generateUrl('adventure_bidding_user_create_request'));
                }else{
                    //redirecting to login page
                    return $this->redirect($this->generateUrl('adventure_login_index'));
                }
            }
            
            //returning the form after getting error while submitting form
            return $this->render('AdventureBiddingBundle:User:createRequestUser.html.twig', array(
                        'Form' => $registerForm->createView(),
            ));
        }
        
        //redirecting to the user dashboard
        return $this->render('AdventureBiddingBundle:User:createRequestUser.html.twig', array(
                    'Form' => $requestForm->createView()
                ));
    }
    
    /*
     * Function : editRequestAction
     * Description: opens up the form with values to update a request from user side.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 29-12-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */

    public function editRequestAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');

        //getting record from user rquest table with id 
        $requestUser = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->find($id);
        if (!$requestUser) {
            throw $this->createNotFoundException('Unable to find');
        }
        //echo "<pre>"; print_r($requestUser);exit;
        
        //Create registration form for user
        $editForm = $this->createForm(new RequestUserType(), $requestUser, array(
                'action' => $this->generateUrl('adventure_bidding_user_edit_request', array('id' => $id)),
                'method' => 'POST',
            ));
        //echo "hg";exit;
        
        //validation of form
        if ($request->getMethod() == 'POST') {
            
            
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                
                //updating user request table
                $requestUser->setUpdatedAt(new \DateTime());
                $em->flush();

                //redirecting to list page
                return new RedirectResponse($this->generateUrl('adventure_bidding_user_dashboard'));
            }
            
            //returning to edit form page
            return $this->render('AdventureBiddingBundle:User:editRequestUser.html.twig', array(
                        'editForm' => $editForm->createView(),
            ));
        }
        
        //returning to edit form page
        return $this->render('AdventureBiddingBundle:User:editRequestUser.html.twig', array(
                    'editForm' => $editForm->createView(),
        ));
    }
    
    
    
    /*
     * Function : deleteRequestAction
     * Description: Deletes logged in user request record
     * ###########################################
     * Written: 29-12-2014
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * @return : updated list
     * 
     */

    public function deleteRequestAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');

        //Getting record for deletion
        $entity = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find');
        }
        
        $em->remove($entity);
        $em->flush();

        //redirecting to list page
        return $this->redirect($this->generateUrl("adventure_bidding_user_list_request"));
    }
    
    //Dashboard view
    /*
     * Function : listRequestAction
     * Description: open ups the request list 
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 05-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function listRequestAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
        $memberId = $user->getId();
        
        //Getting all requests of logged in user to display
        $requestList = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getRequestByMemberId($memberId);
       $response = array();
        foreach ($requestList as $reqid ) {
            $returnObject = array();
            $returnObject['reqid'] = $reqid->getId(); 
            $returnObject['trip'] = $reqid->getName();
            $returnObject['status'] = $reqid->getStatus();
            $reqidu = $returnObject['reqid'];
            $totalrows = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getMessageByMemberId($reqidu);
            $returnObject['messagecount'] = $totalrows;
            $response[]=$returnObject;
        }
       
        //redirecting to the request list
        return $this->render('AdventureBiddingBundle:User:dashboardUser.html.twig',array(
            'requestUser' => $response
        ));
    }
    
    /*
     * Function : viewCloseRequestAction
     * Description: Display close request in details.
     * ###########################################
     * Written: 08-01-2015
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * @return : updated list
     * 
     */

    public function viewCloseRequestAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();

        //Getting request details to view
        $entityRequest = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->find($id);
        if (!$entityRequest) {
            throw $this->createNotFoundException('Unable to find');
        }        
        
        //redirecting to view page with request details.
        return $this->render('AdventureBiddingBundle:User:viewCloseRequest.html.twig',array(
            'request' => $entityRequest
        ));
    }
    
    /*
     * Function : viewOpenRequestAction
     * Description: Display open request in details.
     * ###########################################
     * Written: 12-01-2015
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * @return : updated list
     * 
     */

    public function viewOpenRequestAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $requestId = $id;
        
        $userForm = new BidOrganiserResponse();
        
        //Getting request details to view
        $entityRequest = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->find($requestId);
     
        if (!$entityRequest) {
            throw $this->createNotFoundException('Unable to find');
        }   
        
        //getting logged in user details
        $user = $this->getUser();
        $memberId = $user->getId();
        
        $table = 'BidUser';
        //Get user detail by logged in member.
        $userDetail = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($memberId,$table);
        // echo "<pre>"; print_r($userDetail); exit;
        //get request details by request id
        $requestDetail = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getDetailByRequestId($requestId);
        //Getting all organiser details with whom bidding is in progress.
        
        $listOrganiser = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getMatchingOrganiser($requestId,$memberId);
     
        
        //Getting all responses details of logged in organiser to this user request. 
        $comment = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getBidComment($requestId,$memberId);
         $a = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->updateMessageByMemberId($requestId);
        //Create request form for user
        $requestForm = $this->createForm(new ReplyRequestType(), $userForm, array(
                'action' => $this->generateUrl('adventure_bidding_user_view_open_request',array(
                    'id'=> $id
                )),
                'method' => 'POST',
            ));
        
        if ($request->getMethod() == 'POST') {
            $requestForm->handleRequest($request);
            if ($requestForm->isValid()) {
             
               $ORGANISER = $em->getRepository('AdventureBiddingBundle:BidOrganiser')->find($_POST['orgid']);
          
               
                $userForm->setUser($userDetail);
                $userForm->setFlag('0');
                $userForm->setOrganiser($ORGANISER);
                $userForm->setRole($this->getUser()->getRole());
                $userForm->setRequest($requestDetail);    
                //storing data to user-request table.
                $em->persist($userForm);
                $em->flush();
                
                
                //redirecting to view page with request details.
                return $this->render('AdventureBiddingBundle:User:viewOpenRequestUser.html.twig',array(
                    'request' => $entityRequest,
                    'listOrganiser' => $listOrganiser,
                    'Form' => $requestForm->createView(),
                    'comment' => $comment,
                  //  'activity' => $activities,
                    //'quatations' => $lastresponseDetail
                ));
            }
        }
        
        //redirecting to view page with request details.
        return $this->render('AdventureBiddingBundle:User:viewOpenRequestUser.html.twig',array(
            'request' => $entityRequest,
            'listOrganiser' => $listOrganiser,
            'Form' => $requestForm->createView(),
            'comment' => $comment,
           // 'activity' => $activities,
            //'quatations' => $lastresponseDetail
        ));
    }
    
     /*
     * Function : confirmDealAction
     * Description: open ups the confirm deal page
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 19-Jan-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function confirmDealAction(Request $request, $reqId) {
         $em = $this->getDoctrine()->getManager();
            $requestId = $reqId;
        
        $userForm = new BidOrganiserResponse();
        
        //Getting request details to view
        $entityRequest = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->find($requestId);
     
        if (!$entityRequest) {
            throw $this->createNotFoundException('Unable to find');
        }   
        
        //getting logged in user details
        $user = $this->getUser();
        $memberId = $user->getId();
        
        $table = 'BidUser';
        //Get user detail by logged in member.
        $userDetail = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($memberId,$table);
        // echo "<pre>"; print_r($userDetail); exit;
        //get request details by request id
        $requestDetail = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getDetailByRequestId($requestId);
        //Getting all organiser details with whom bidding is in progress.
        
        $listOrganiser = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getMatchingOrganiser($requestId,$memberId);
     
        
        //Getting all responses details of logged in organiser to this user request. 
        $comment = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getBidComment($requestId,$memberId);
        
        //redirecting to the user dashboard with all request list
        return $this->render('AdventureBiddingBundle:User:confirmDealUser.html.twig',array(
            'request' => $entityRequest,
            'listOrganiser' => $listOrganiser,
            'comment' => $comment,
            ));
    }
    
    public function conformPaymentAction(){
        return $this->render('AdventureBiddingBundle:User:payment.html.twig');
        
    }

    public function changePasswordAction(Request $request) {
        
        //return to the change password page for password updation
        return $this->render('AdventureBiddingBundle:User:changePassword.html.twig');
    }
    /*
     * Function : updatePasswordAction
     * Description: submit update the current password
     * ###########################################
     * Author: Rahl Rajoria
     * Written: 23-Fb-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
        public function updatePasswordAction() {
            
             
        $request = $this->getRequest();
        if($request->getMethod()=='POST'){
        $retval = true;
        $user = $this->getUser();
        if (!$user) {
            //redirect to login page
            $this->redirect($this->generateUrl('adventure_login_index'));
        }

       
        $username = $user->getUsername();
        
        
         
        $password = $user->getPassword();
        $currentPassword = $request->get('currentPassword');
        $newPassword = $request->get('newPassword');
        $confirmPassword = $request->get('confirmPassword');
        // echo $newPassword;exit;
         
        if($newPassword !==$confirmPassword){
            
          $this->get('session')->getFlashBag()->add(
                        'warning', 'New password and Confirm passowrd must be same.');
            
          $retval = false;   
            
        }

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $encodedPassword = $encoder->encodePassword($currentPassword, $user->getSalt());
        if ($password !== $encodedPassword) {

           /*
            $this->get('session')->getFlashBag()->add(
                        'error', 'Entered password didn\'t matched. Try Again');
            */
            $retval = false;
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AdventureLoginBundle:BidMember')->findOneBy(array('username' => $username,'password'=>$encodedPassword));


        if (!$user) {
           
            
            $this->get('session')->getFlashBag()->add(
                        'warning', 'Enter your Current Password correctly.');
            $retval = false;
        }

        if ($retval){
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $encodedPassword = $encoder->encodePassword($newPassword, $user->getSalt());
            $user->setPassword($encodedPassword);
            $em->flush();
            $em->clear();
            $this->get('session')->getFlashBag()->add(
                        'notice', 'Password changed successfully.');
            
        }

        $referer = $this->getRequest()->headers->get('referer');
       return $this->redirect($referer);
    }
    }
}

?>