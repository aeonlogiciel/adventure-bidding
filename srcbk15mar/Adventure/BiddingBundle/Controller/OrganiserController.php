<?php

namespace Adventure\BiddingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

use Adventure\BiddingBundle\Form\RegisterOrganiserType;
use Adventure\BiddingBundle\Form\ResponseOrganiserType;
use Adventure\BiddingBundle\Entity\OrganiserResponseReply;
use Adventure\BiddingBundle\Entity\BidOrganiser;
use Adventure\BiddingBundle\Entity\BidOrganiserResponse;
use Adventure\BiddingBundle\Entity\event;
use Adventure\BiddingBundle\Form\eventType;



/**
 * Description of OrganiserController
 *
 * Author Jhuma Mandal
 * Written : 05-Dec-2014
 * Description : Controller to control user functions.
 */
class OrganiserController extends Controller{
    
    /*
     * Function : dashboardAction
     * Description: open ups the organiser dashboard.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 05-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function dashboardAction(Request $request) {
      // $clicknblog=$this->get("adventure_bridge.clicknblog");
       // $clicknblog->getData();exit;
        $em = $this->getDoctrine()->getManager();
        
        //getting logged in organiser details
        $organiser = $this->getUser();
       $memberId = $organiser->getId();
      
        
     
        //Getting organiser details according to the logged in member Id.
        $organiserDetail = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getOrganiserByMemberId($memberId);
        $actId = array();
        $zone = "";
        $country = "";
        foreach ($organiserDetail as $valueAct) {
            $actId[] = $valueAct['id']; 
            $zone = $valueAct['zone'];
            $country = $valueAct['country'];
        }      
        
        //Getting all requests match with logged in organiser.
        $listRequest = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getMatchingRequest($actId,$country,$zone);
       $response = array();
        foreach($listRequest as $reqid ){
             $returnObject = array();
             $returnObject['reqid'] = $reqid->getId(); // request id
             $returnObject['userid'] = $reqid->getUser()->getId(); //request user id
             $returnObject['trip'] = $reqid->getUser()->getName(); //request trip name
             $returnObject['username'] = $reqid->getName(); // request user name
             $returnObject['status'] = $reqid->getStatus();
             $reqidu = $returnObject['reqid'];
             $userid = $returnObject['userid'];
             $totalrows = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getMessageByMemberId($reqidu,$userid,$memberId);
             $returnObject['messagecount'] = $totalrows;
             $response[]=$returnObject;
          
            
             
           }                           

           
 
        //redirecting to the organiser dashboard
        return $this->render('AdventureBiddingBundle:Organiser:dashboardOrganiser.html.twig',array(
            'requestUser' => $response,
            //'unread' => $totalrows  
        ));
    }
    /*
     * Function: getmessage
     * description : get unread message from user 
     * authore: Rahul Rajoria
     * written:02-03-2015
     * 
     */
//    public function updatemessageAction(Request $request){
//         $em = $this->getDoctrine()->getManager();
//        $userMessage = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getMessageByMemberId($id,$role);
//        return $this->render('AdventureBiddingBundle:Organiser:dashboardOrganiser.html.twig',array(
//            'message' => $userMessage
//        ));
//        
//    }
//    
    /*
     * Function : registerAction
     * Description: open ups the registration form to complete profile of an organiser.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function registerAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $role = "BidOrganiser";
        
        //getting record from batch with id
         $organiserDetails = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($id,$role);
         if (!$organiserDetails) {
            throw $this->createNotFoundException('No such organiser found.');
         }
        
        //Create registration form for Organiser
        $registerForm = $this->createForm(new RegisterOrganiserType(), $organiserDetails, array(
                'action' => $this->generateUrl('adventure_bidding_organiser_register', array('id' => $id)),
                'method' => 'POST',
            ));
        
        //validating form before saving to database
        if ($request->getMethod() == 'POST') {
            
            $registerForm->handleRequest($request);
            if ($registerForm->isValid()) {
               
                
                //storing data to organiser request table.
                $organiserDetails->setUpdatedAt(new \DateTime());
                $em->flush();
                
                //message after successfull completion of profile.
                $this->get('session')->getFlashBag()->add(
                            'notice', $this->get('translator')->trans('form.message.success.active')
                    );

                //redirecting to user dashboard after successful submission of form.
                return new RedirectResponse($this->generateUrl('adventure_login_index'));
            }
            
            //returning the form after getting error while submitting form
            return $this->render('AdventureBiddingBundle:Organiser:registerOrganiser.html.twig', array(
                        'Form' => $registerForm->createView(),
            ));
        }
        
        //redirecting to the organiser registreration form
        return $this->render('AdventureBiddingBundle:Organiser:registerOrganiser.html.twig', array(
                    'Form' => $registerForm->createView()
                ));
    }
    
    /*
     * Function : viewProfileAction
     * Description: open ups the organiser profile in view mode.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 23-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function viewProfileAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $memberId = $user->getId();
        $role = 'BidOrganiser';
        
        $organiserDetails = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($memberId,$role);
        
        //redirecting to the user profile in view mode
        return $this->render('AdventureBiddingBundle:Organiser:viewProfileOrganiser.html.twig',array(
            'member' => $organiserDetails
        ));
    }
    

        /*
     * Function : editProfileAction
     * Description: open ups the organiser profile in edit mode.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 23-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function editProfileAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $role = "BidOrganiser";
        $user = $this->getUser();
        $id = $user->getId();
        //getting record from member with id
        $organiserDetails = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($id,$role);
        if (!$organiserDetails) {
            throw $this->createNotFoundException('No such user found.');
        }
       
        //Create registration form for organiser
        $registerForm = $this->createForm(new RegisterOrganiserType(), $organiserDetails, array(
                'action' => $this->generateUrl('adventure_bidding_organiser_edit_profile'),
                'method' => 'POST',
            ));
     
          $entity = new event();
          $event = $this->createForm(new eventType(), $entity, array(
            'action' => $this->generateUrl('adventure_bidding_organiser_edit_profile'),
            'method' => 'POST',
        ));

        $event->add('submit', 'submit', array('label' => 'Create'));
       
        
        //validating form before saving to database
        if ($request->getMethod() == 'POST') {
            $registerForm->handleRequest($request);
            if ($registerForm->isValid()) {
                
                //storing data to user request table.
                $organiserDetails->setUpdatedAt(new \DateTime());
                
                $em->flush();
                
                ///Displaying message after getting error while submitting form.
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Your profile is updated successfully.');
            }
            
            //returning the form after getting error while submitting form.
            return $this->render('AdventureBiddingBundle:Organiser:editProfileOrganiser.html.twig', array(
                        'Form' => $registerForm->createView(),
                       
            ));
        }
        
        //redirecting to the user registreration form
        return $this->render('AdventureBiddingBundle:Organiser:editProfileOrganiser.html.twig', array(
                    'Form' => $registerForm->createView(),
                    'event'   => $event->createView(),
                ));
    }
    
    
    /*
     * Function : createResponseAction
     * Description: open ups the request form for user.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-Jan-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function createResponseAction(Request $request, $id) {
        
        $idReq = $id;
        $em = $this->getDoctrine()->getManager();
        $userForm = new BidOrganiserResponse();
        
        //get request details by request id
        $requestDetail = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getDetailByRequestId($id);
        $userid = $requestDetail->getUser()->getId();
        
        //getting logged in user details
        $user = $this->getUser();
        $memberId = $user->getId(); 
        $table = 'BidOrganiser';
        //Get user detail by logged in member.
        $organiserDetail = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($memberId,$table);
        //$a =   $memberId ;
        //Create registration form for user
        $requestForm = $this->createForm(new ResponseOrganiserType(), $userForm, array(
                'action' => $this->generateUrl('adventure_bidding_organiser_create_response',array('id'=> $id,'userid'=>$userid)),
                'method' => 'POST',
            ));
            
      $USER = $em->getRepository('AdventureBiddingBundle:BidUser')->find($userid);  
             
      
        if ($request->getMethod() == 'POST') {
          
        
            $requestForm->handleRequest($request);
            
         
            if ($requestForm->isValid()) {
            
                $userForm->setRole('ROLE_ORGANISER');
                $userForm->setflag('1');
                $userForm->setUser($USER);
                $userForm->setRequest($requestDetail);
                $userForm->setOrganiser($organiserDetail);
            //  storing data to user table.
         
                $em->persist($userForm);
                $em->flush();
                
                ///Displaying message after getting error while submitting form.
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Your response is added successfully.');
                
                //redirecting to list page
                return new RedirectResponse($this->generateUrl('adventure_bidding_organiser_view_open_request',array('id'=>$idReq)));
            }
            
            //returning the form after getting error while submitting form
            return $this->render('AdventureBiddingBundle:Organiser:responseOrganiser.html.twig', array(
                        'Form' => $requestForm->createView(),
            ));
        }
        
        //redirecting to the user dashboard
        return $this->render('AdventureBiddingBundle:Organiser:responseOrganiser.html.twig', array(
                    'Form' => $requestForm->createView()
                ));
    }
    
    /*
     * Function : listRequestAction
     * Description: open ups the request list for organiser 
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 09-Jan-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function listRequestAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        
        //getting logged in organiser details
        $organiser = $this->getUser();
        $memberId = $organiser->getId();
        
        //Getting organiser details according to the logged in member Id.
        $organiserDetail = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getOrganiserByMemberId($memberId);
        $actId = array();
        $zone = "";
        $country = "";
        foreach ($organiserDetail as $valueAct) {
            $actId[] = $valueAct['id'];
            $zone = $valueAct['zone'];
            $country = $valueAct['country'];
        }      
        
        //Getting all requests match with logged in organiser.
        $listRequest = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getMatchingRequest($actId,$country,$zone);
                
        
        
        
        //redirecting to the request list
        return $this->render('AdventureBiddingBundle:Organiser:listRequestOrganiser.html.twig',array(
            'requestUser' => $listRequest
        ));
    }
    
    /*
     * Function : viewCloseRequestAction
     * Description: Display close request in details.
     * ###########################################
     * Written: 12-01-2015
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * @return : updated list
     * 
     */

    public function viewCloseRequestAction(Request $request, $id) {
$em = $this->getDoctrine()->getManager();

        //Getting request details to view
        $entityRequest = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->find($id);
        if (!$entityRequest) {
            throw $this->createNotFoundException('Unable to find');
        }        
        
        $close = "close";
        
        //getting logged in organiser details
        $organiser = $this->getUser();
        $memberId = $organiser->getId();
        
        //Getting organiser details according to the logged in member Id.
        $organiserDetail = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getOrganiserByMemberId($memberId);
        
        $registerForm = $this->createForm(new RegisterOrganiserType(), $organiserDetails, array(
                'action' => $this->generateUrl('adventure_bidding_organiser_register', array('id' => $id)),
                'method' => 'POST',
            ));
        
          
        //validating form before saving to database
       if ($request->getMethod() == 'POST') {
          
            $requestForm->handleRequest($request);
           
            if ($requestForm->isValid()) {
                $userForm->setRequest($requestDetail);
                $userForm->setOrganiser($organiserDetail);
            //  storing data to user table.
         
               
                $em->flush();
                
                ///Displaying message after getting error while submitting form.
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Your response is added successfully.');
                
                //redirecting to list page
                return new RedirectResponse($this->generateUrl('adventure_bidding_organiser_view_open_request',array('id'=>$idReq)));
       }
            }
            
//Getting organiser response details to view with whom deal is closed.
//        $entityOrganiser = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getOrganiserDetail($memberId,$close);
//        if ($entityOrganiser==0) {
//            //Displaying message if the deal is open.
//                $this->get('session')->getFlashBag()->add(
//                        'notice', $this->get('translator')->trans('Deal is not closed yet.')
//                );
//        }  
        
        //redirecting to view page with request details.
        return $this->render('AdventureBiddingBundle:Organiser:viewCloseRequest.html.twig',array(
            'request' => $entityRequest,
            
        ));
    }
    
    /*
     * Function : viewOpenRequestAction
     * Description: Display open request in details.
     * ###########################################
     * Written: 12-01-2015
     * Author: Jhuma Mandal
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * @return : updated list
     * 
     */

    public function viewOpenRequestAction(Request $request, $id) {
       
        $em = $this->getDoctrine()->getManager();
        $requestId = $id;
        // form start here 
         //get request details by request id
        $requestDetail = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getDetailByRequestId($id);
        //getting logged in user details
        $user = $this->getUser();
        $memberId = $user->getId();
        $table = 'BidOrganiser';
        //Get user detail by logged in member.
        $organiserDetail = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($memberId,$table);
            
        //  forms ends here       // 
        //Getting request details to view
        $entityRequest = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->find($requestId);
        $userid= $entityRequest->getUser()->getId();
     
        if (!$entityRequest) {
            throw $this->createNotFoundException('Unable to find');
        }        
        
        //getting logged in organiser details
        $organiser = $this->getUser();
        $memberId = $organiser->getId();
        
        //Getting organiser details according to the logged in member Id.
        $organiserDetail = $em->getRepository('AdventureBiddingBundle:BidOrganiser')->getOrganiserByMemberId($memberId);
        $lastresponseDetail = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getLastBidComment($requestId,$memberId);
        //redirecting to view page with request details.
      
        //Getting all responses details of logged in organiser to this user request. 
        $responseDetail = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->getBidComment($requestId,$memberId);
        
        $a = $em->getRepository('AdventureBiddingBundle:BidOrganiserResponse')->updateMessageByMemberId($requestId,$userid,$memberId);
        
        return $this->render('AdventureBiddingBundle:Organiser:viewOpenRequestOrganiser.html.twig',array(
            'request' => $entityRequest,
            'organiser' => $organiserDetail,
            'comment' => $responseDetail,
            'quatations' => $lastresponseDetail
                
              
            
        ));
       
    }
  
    public function orgRequestAction(Request $request, $id) {
         $userForm = new BidOrganiserResponse();
         $idReq = $id;
         $em = $this->getDoctrine()->getManager();
         
         $USER = $em->getRepository('AdventureBiddingBundle:BidUser')->find($_POST['userid']);
     
        $dateMin = $request->get('dateMin');
        $dateMax = $request->get('dateMax');
        $pick = $request->get('pick');
        $role = $request->get('role');
        $flag = $request->get('flag');
        $down = $request->get('down');
        $estimate = $request->get('totalCost');
        $costIn = $request->get('includeCost');
        $costEx = $request->get('excludeCost');
        $desp = $request->get('description');
        
   
        
        $userForm->setdateMin(new \DateTime($dateMin." 12:00:00"));
        $userForm->setdateMax(new \DateTime($dateMax." 12:00:00"));
        $userForm->setPick($pick);
        $userForm->setRole('ROLE_ORGANISER');
        $userForm->setflag('0');
        $userForm->setDown($down);
        $userForm->setTotalCost($estimate);
        $userForm->setIncludeCost($costIn);
        $userForm->setExcludeCost($costEx);
        $userForm->setDescription($desp);
        $userForm->setUser($USER);
       // $userForm->set($this->getUser()->getId());
    
        
        //get request details by request id
        $requestDetail = $em->getRepository('AdventureBiddingBundle:BidUserRequest')->getDetailByRequestId($idReq);
        //getting logged in user details
       
       
       $user = $this->getUser();
       $memberId = $user->getId();
        $table = 'BidOrganiser';
        //Get user detail by logged in member.
       $organiserDetail = $em->getRepository('AdventureLoginBundle:BidMember')->getDetailByMemberId($memberId,$table);
            
   
     
           if ($request->getMethod() == 'POST') {
          
          //  $requestForm->handleRequest($request);
           
         //   if ($requestForm->isValid()) {
                $userForm->setRequest($requestDetail);
                $userForm->setOrganiser($organiserDetail);
            //  storing data to user table.
         
                $em->persist($userForm);
                $em->flush();
                
                //ends here
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Your response is added successfully.');
                
                //redirecting to list page
                return new RedirectResponse($this->generateUrl('adventure_bidding_organiser_view_open_request',array('id'=>$idReq)));
            }
            
            
            
    }
    
    public function changePasswordAction(Request $request) {
        
        //return to the change password page for password updation
        return $this->render('AdventureBiddingBundle:Organiser:changePassword.html.twig');
    }
    
    /*
     * Function : updatePasswordAction
     * Description: submit update the current password
     * ###########################################
     * Author: Rahl Rajoria
     * Written: 23-Fb-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
        public function updatePasswordAction() {
            
             
        $request = $this->getRequest();
        if($request->getMethod()=='POST'){
        $retval = true;
        $user = $this->getUser();
        if (!$user) {
            //redirect to login page
            $this->redirect($this->generateUrl('adventure_login_index'));
        }

       
        $username = $user->getUsername();
        
        
         
        $password = $user->getPassword();
        $currentPassword = $request->get('currentPassword');
        $newPassword = $request->get('newPassword');
        $confirmPassword = $request->get('confirmPassword');
       
         
        if($newPassword !==$confirmPassword){
            
          $this->get('session')->getFlashBag()->add(
                        'warning', 'New password and Confirm passowrd must be same.');
            
          $retval = false;   
            
        }

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $encodedPassword = $encoder->encodePassword($currentPassword, $user->getSalt());
        if ($password !== $encodedPassword) {

           /*
            $this->get('session')->getFlashBag()->add(
                        'error', 'Entered password didn\'t matched. Try Again');
            */
            $retval = false;
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AdventureLoginBundle:BidMember')->findOneBy(array('username' => $username,'password'=>$encodedPassword));


        if (!$user) {
           
            
            $this->get('session')->getFlashBag()->add(
                        'warning', 'Enter your Current Password correctly.');
            $retval = false;
        }

        if ($retval){
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $encodedPassword = $encoder->encodePassword($newPassword, $user->getSalt());
            $user->setPassword($encodedPassword);
            $em->flush();
            $em->clear();
            $this->get('session')->getFlashBag()->add(
                        'notice', 'Password changed successfully.');
            
        }

        $referer = $this->getRequest()->headers->get('referer');
       return $this->redirect($referer);
    }
    }
    public function feedbackAction(){
      
       return $this->render('AdventureBiddingBundle:Organiser:feedback.html.twig');
    }
    
    
    ////////////////////////////////////////////
    /**
     * Displays a form to edit an existing event entity.
     *
     */
    public function editEventAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $form = $this->createForm(new eventType(), $entity, array(
            'action' => $this->generateUrl('adventure_bidding_organiser_event_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        
        $entity = $em->getRepository('AdventureBiddingBundle:event')->find($id);

        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find event entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('adventure_bidding_organiser_view_profile', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

  /**
     * Edits an existing event entity.
     *
     */
    public function updateEventAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdventureBiddingBundle:event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find event entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('adventure_bidding_organiser_edit_event', array('id' => $id)));
        }

        return $this->render('AdventureBiddingBundle:Organiser:editEventOrganiser.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
     * Deletes a event entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        //$form = $this->createDeleteForm($id);
         /**
     * Creates a form to delete a event entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adventure_bidding_organiser_event_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
        
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdventureBiddingBundle:event')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find event entity.');
            }

            $em->remove($entity);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('adventure_bidding_organiser_edit_profile'));
        //return $this->redirect($this->generateUrl('event'));
    }

    /**
     * Creates a form to delete a event entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
  
    /////////////////////////////
    
            /**
    * Creates a form to create a event entity.
    *
    * @param event $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    

    public function createEventAction(Request $request){
         $em = $this->getDoctrine()->getManager();
         
        $entity = new event();
       
        $requestform = $this->createForm(new eventType(), $entity, array(
            'action' => $this->generateUrl('adventure_bidding_organiser_view_event'),
            'method' => 'POST',
        ));
        
          if ($request->getMethod() == 'POST') {
                            
          
        //echo"<pre>"; print_r($_POST); exit;
        $eDate = $request->get('eDate');
        
        $entity->setedate(new \DateTime($eDate." 12:00:00"));
    
        $requestform->handleRequest($request);
      
         $eActivity= $entity->getEActivity();
          $a="";
         foreach ($eActivity as $key => $value) {
          $a.=$value.'***';
         }
         $a = rtrim($a, '***');
         $entity->seteactivity($a);
           if ($requestform->isValid()) {
            $em->persist($entity);
            $em->flush();
              $this->get('session')->getFlashBag()->add(
                        'notice', 'Your Event is added successfully.');
            return new RedirectResponse($this->generateUrl('adventure_bidding_organiser_view_event'));
        }
        else{
        $this->get('session')->getFlashBag()->add(
                        'warning', 'Something went worng please check the fields.');
            $retval = false;
          }
          }
       return $this->render('AdventureBiddingBundle:Organiser:viewEventOrganiser.html.twig', array(
            'entity' => $entity,
            'form'   => $requestform->createView(),
        ));
  //   return $this->render('AdventureBiddingBundle:Organiser:viewEventOrganiser.html.twig');
            }
       
    
            
            
    public function editTrailsAction(){
        
        
        
        return $this->render('AdventureBiddingBundle:Organiser:editTrailsOrganiser.html.twig');
    }
    
    
    
    public function viewTrailsAction(){
        
        return $this->render('AdventureBiddingBundle:Organiser:viewTrailsOrganiser.html.twig');
    }
    
}
 ?>   