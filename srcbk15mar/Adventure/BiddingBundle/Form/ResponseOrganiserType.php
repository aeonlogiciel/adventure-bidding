<?php

namespace Adventure\BiddingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of ResponseOrganiserType
 *
 * @author Jhuma Mandal
 * Written: 08-01-2015
 */
class ResponseOrganiserType extends AbstractType{
    
    /*
     * Function : buildForm
     * Description: Creates organiser registration form
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-01-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * 
     */
      
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('dateMin','date',array('widget' => 'single_text','format'=>'dd-MM-yyyy'))
                ->add('dateMax','date',array('widget' => 'single_text','format'=>'dd-MM-yyyy'))
                ->add('pick','text',array('required'=> true))
                ->add('down','text',array('required'=> true))
                ->add('includeCost','integer',array('required'=> true))
                ->add('includeCost', 'choice', array(
                      'choices' => array(
                            '' => 'Select',
                            '15000' => '15000',
                            '20000' => '20000',
                            '25000' => '250000',
                            '30000' => '300000',
                          )))
                ->add('excludeCost', 'choice', array(
                      'choices' => array(
                            '' => 'Select',
                            '30000' => '3000',
                            '5000' => '5000',
                            '7000' => '7000',
                            '10000' => '10000',
                          )))
                ->add('totalCost','text',array('required'=> true))
                ->add('description','textarea',array('required'=> true))
                ->add('status','hidden',array('data'=> '1'))
                
               ;
    }
        
    public function getName() {
        return 'organiserResponse';
    }
    
     public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Adventure\BiddingBundle\Entity\BidOrganiserResponse'
        ));
    }
}

