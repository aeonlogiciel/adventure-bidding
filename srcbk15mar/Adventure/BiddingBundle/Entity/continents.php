<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * continents
 */
class continents
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return continents
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \Adventure\BiddingBundle\Entity\countries
     */
    private $countries;


    /**
     * Set countries
     *
     * @param \Adventure\BiddingBundle\Entity\countries $countries
     * @return continents
     */
    public function setCountries(\Adventure\BiddingBundle\Entity\countries $countries = null)
    {
        $this->countries = $countries;

        return $this;
    }

    /**
     * Get countries
     *
     * @return \Adventure\BiddingBundle\Entity\countries 
     */
    public function getCountries()
    {
        return $this->countries;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $features;

    /**
     * @var \Adventure\BiddingBundle\Entity\continent
     */
    private $continent;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->features = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add features
     *
     * @param \Adventure\BiddingBundle\Entity\countries $features
     * @return continents
     */
    public function addFeature(\Adventure\BiddingBundle\Entity\countries $features)
    {
        $this->features[] = $features;

        return $this;
    }

    /**
     * Remove features
     *
     * @param \Adventure\BiddingBundle\Entity\countries $features
     */
    public function removeFeature(\Adventure\BiddingBundle\Entity\countries $features)
    {
        $this->features->removeElement($features);
    }

    /**
     * Get features
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Set continent
     *
     * @param \Adventure\BiddingBundle\Entity\continent $continent
     * @return continents
     */
    public function setContinent(\Adventure\BiddingBundle\Entity\continent $continent = null)
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * Get continent
     *
     * @return \Adventure\BiddingBundle\Entity\continent 
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * Add countries
     *
     * @param \Adventure\BiddingBundle\Entity\countries $countries
     * @return continents
     */
    public function addCountry(\Adventure\BiddingBundle\Entity\countries $countries)
    {
        $this->countries[] = $countries;

        return $this;
    }

    /**
     * Remove countries
     *
     * @param \Adventure\BiddingBundle\Entity\countries $countries
     */
    public function removeCountry(\Adventure\BiddingBundle\Entity\countries $countries)
    {
        $this->countries->removeElement($countries);
    }
}
