<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bidcategory
 */
class Bidcategory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $img;

    /**
     * @var string
     */
    private $lifecyclecallbacks;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Bidcategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set img
     *
     * @param string $img
     * @return Bidcategory
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string 
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set lifecyclecallbacks
     *
     * @param string $lifecyclecallbacks
     * @return Bidcategory
     */
    public function setLifecyclecallbacks($lifecyclecallbacks)
    {
        $this->lifecyclecallbacks = $lifecyclecallbacks;

        return $this;
    }

    /**
     * Get lifecyclecallbacks
     *
     * @return string 
     */
    public function getLifecyclecallbacks()
    {
        return $this->lifecyclecallbacks;
    }
}
