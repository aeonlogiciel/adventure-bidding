<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 */
class Event
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ename;

    /**
     * @var string
     */
    private $eactivity;

    /**
     * @var string
     */
    private $eorganizer;

    /**
     * @var string
     */
    private $budget;

    /**
     * @var string
     */
    private $econtactdetail;

    /**
     * @var string
     */
    private $duration;

    /**
     * @var \DateTime
     */
    private $edate;

    /**
     * @var string
     */
    private $emonth;

    /**
     * @var string
     */
    private $eyear;

    /**
     * @var string
     */
    private $edifficulty;

    /**
     * @var string
     */
    private $ezone;

    /**
     * @var integer
     */
    private $continent;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $estate;

    /**
     * @var string
     */
    private $eregion;

    /**
     * @var string
     */
    private $edestination;

    /**
     * @var string
     */
    private $ebrief;

    /**
     * @var string
     */
    private $logo;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $eregistrationdate;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $uniquee;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ename
     *
     * @param string $ename
     * @return Event
     */
    public function setEname($ename)
    {
        $this->ename = $ename;

        return $this;
    }

    /**
     * Get ename
     *
     * @return string 
     */
    public function getEname()
    {
        return $this->ename;
    }

    /**
     * Set eactivity
     *
     * @param string $eactivity
     * @return Event
     */
    public function setEactivity($eactivity)
    {
        $this->eactivity = $eactivity;

        return $this;
    }

    /**
     * Get eactivity
     *
     * @return string 
     */
    public function getEactivity()
    {
        return $this->eactivity;
    }

    /**
     * Set eorganizer
     *
     * @param string $eorganizer
     * @return Event
     */
    public function setEorganizer($eorganizer)
    {
        $this->eorganizer = $eorganizer;

        return $this;
    }

    /**
     * Get eorganizer
     *
     * @return string 
     */
    public function getEorganizer()
    {
        return $this->eorganizer;
    }

    /**
     * Set budget
     *
     * @param string $budget
     * @return Event
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return string 
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set econtactdetail
     *
     * @param string $econtactdetail
     * @return Event
     */
    public function setEcontactdetail($econtactdetail)
    {
        $this->econtactdetail = $econtactdetail;

        return $this;
    }

    /**
     * Get econtactdetail
     *
     * @return string 
     */
    public function getEcontactdetail()
    {
        return $this->econtactdetail;
    }

    /**
     * Set duration
     *
     * @param string $duration
     * @return Event
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set edate
     *
     * @param \DateTime $edate
     * @return Event
     */
    public function setEdate($edate)
    {
        $this->edate = $edate;

        return $this;
    }

    /**
     * Get edate
     *
     * @return \DateTime 
     */
    public function getEdate()
    {
        return $this->edate;
    }

    /**
     * Set emonth
     *
     * @param string $emonth
     * @return Event
     */
    public function setEmonth($emonth)
    {
        $this->emonth = $emonth;

        return $this;
    }

    /**
     * Get emonth
     *
     * @return string 
     */
    public function getEmonth()
    {
        return $this->emonth;
    }

    /**
     * Set eyear
     *
     * @param string $eyear
     * @return Event
     */
    public function setEyear($eyear)
    {
        $this->eyear = $eyear;

        return $this;
    }

    /**
     * Get eyear
     *
     * @return string 
     */
    public function getEyear()
    {
        return $this->eyear;
    }

    /**
     * Set edifficulty
     *
     * @param string $edifficulty
     * @return Event
     */
    public function setEdifficulty($edifficulty)
    {
        $this->edifficulty = $edifficulty;

        return $this;
    }

    /**
     * Get edifficulty
     *
     * @return string 
     */
    public function getEdifficulty()
    {
        return $this->edifficulty;
    }

    /**
     * Set ezone
     *
     * @param string $ezone
     * @return Event
     */
    public function setEzone($ezone)
    {
        $this->ezone = $ezone;

        return $this;
    }

    /**
     * Get ezone
     *
     * @return string 
     */
    public function getEzone()
    {
        return $this->ezone;
    }

    /**
     * Set continent
     *
     * @param integer $continent
     * @return Event
     */
    public function setContinent($continent)
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * Get continent
     *
     * @return integer 
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Event
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set estate
     *
     * @param string $estate
     * @return Event
     */
    public function setEstate($estate)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return string 
     */
    public function getEstate()
    {
        return $this->estate;
    }

    /**
     * Set eregion
     *
     * @param string $eregion
     * @return Event
     */
    public function setEregion($eregion)
    {
        $this->eregion = $eregion;

        return $this;
    }

    /**
     * Get eregion
     *
     * @return string 
     */
    public function getEregion()
    {
        return $this->eregion;
    }

    /**
     * Set edestination
     *
     * @param string $edestination
     * @return Event
     */
    public function setEdestination($edestination)
    {
        $this->edestination = $edestination;

        return $this;
    }

    /**
     * Get edestination
     *
     * @return string 
     */
    public function getEdestination()
    {
        return $this->edestination;
    }

    /**
     * Set ebrief
     *
     * @param string $ebrief
     * @return Event
     */
    public function setEbrief($ebrief)
    {
        $this->ebrief = $ebrief;

        return $this;
    }

    /**
     * Get ebrief
     *
     * @return string 
     */
    public function getEbrief()
    {
        return $this->ebrief;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Event
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Event
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set eregistrationdate
     *
     * @param \DateTime $eregistrationdate
     * @return Event
     */
    public function setEregistrationdate($eregistrationdate)
    {
        $this->eregistrationdate = $eregistrationdate;

        return $this;
    }

    /**
     * Get eregistrationdate
     *
     * @return \DateTime 
     */
    public function getEregistrationdate()
    {
        return $this->eregistrationdate;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return Event
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer 
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Event
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set uniquee
     *
     * @param string $uniquee
     * @return Event
     */
    public function setUniquee($uniquee)
    {
        $this->uniquee = $uniquee;

        return $this;
    }

    /**
     * Get uniquee
     *
     * @return string 
     */
    public function getUniquee()
    {
        return $this->uniquee;
    }
}
