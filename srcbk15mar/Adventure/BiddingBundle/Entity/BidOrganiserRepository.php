<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * BidOrganiserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class BidOrganiserRepository extends EntityRepository
{
    /*
     * Function : getOrganiserByMemberId
     * Description: Get organiser with reference to member id.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 14-Jan-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    
    // open request organiser detail 
    public function getOrganiserByMemberId($id) {
        
        $query = $this->getEntityManager()->createQuery(
                        'SELECT u FROM AdventureBiddingBundle:BidOrganiser u WHERE u.memberId = :memId '
                )
                ->setParameter('memId', $id)
                ;
        $result = $query->getResult();
        
        return $result;
    }
    
}
