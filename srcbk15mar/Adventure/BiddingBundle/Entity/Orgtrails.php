<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orgtrails
 */
class Orgtrails
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $orgid;

    /**
     * @var integer
     */
    private $trailis;

    /**
     * @var \DateTime
     */
    private $createdon;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orgid
     *
     * @param integer $orgid
     * @return Orgtrails
     */
    public function setOrgid($orgid)
    {
        $this->orgid = $orgid;

        return $this;
    }

    /**
     * Get orgid
     *
     * @return integer 
     */
    public function getOrgid()
    {
        return $this->orgid;
    }

    /**
     * Set trailis
     *
     * @param integer $trailis
     * @return Orgtrails
     */
    public function setTrailis($trailis)
    {
        $this->trailis = $trailis;

        return $this;
    }

    /**
     * Get trailis
     *
     * @return integer 
     */
    public function getTrailis()
    {
        return $this->trailis;
    }

    /**
     * Set createdon
     *
     * @param \DateTime $createdon
     * @return Orgtrails
     */
    public function setCreatedon($createdon)
    {
        $this->createdon = $createdon;

        return $this;
    }

    /**
     * Get createdon
     *
     * @return \DateTime 
     */
    public function getCreatedon()
    {
        return $this->createdon;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Orgtrails
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
