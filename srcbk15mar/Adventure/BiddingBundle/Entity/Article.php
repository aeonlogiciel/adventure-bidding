<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 */
class Article
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $coverimage;

    /**
     * @var string
     */
    private $autheremail;

    /**
     * @var string
     */
    private $articletitle;

    /**
     * @var string
     */
    private $discription;

    /**
     * @var string
     */
    private $catforfilter;

    /**
     * @var string
     */
    private $typeofactivity;

    /**
     * @var string
     */
    private $duratonmin;

    /**
     * @var string
     */
    private $durationmax;

    /**
     * @var string
     */
    private $difficulty;

    /**
     * @var string
     */
    private $zone;

    /**
     * @var string
     */
    private $continent;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $destination;

    /**
     * @var string
     */
    private $getaways;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $season;

    /**
     * @var string
     */
    private $bucketlist;

    /**
     * @var integer
     */
    private $vote;

    /**
     * @var string
     */
    private $datecreated;

    /**
     * @var string
     */
    private $details;

    /**
     * @var integer
     */
    private $publish;

    /**
     * @var integer
     */
    private $showdetails;

    /**
     * @var string
     */
    private $foldername;

    /**
     * @var integer
     */
    private $views;

    /**
     * @var string
     */
    private $unique;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coverimage
     *
     * @param string $coverimage
     * @return Article
     */
    public function setCoverimage($coverimage)
    {
        $this->coverimage = $coverimage;

        return $this;
    }

    /**
     * Get coverimage
     *
     * @return string 
     */
    public function getCoverimage()
    {
        return $this->coverimage;
    }

    /**
     * Set autheremail
     *
     * @param string $autheremail
     * @return Article
     */
    public function setAutheremail($autheremail)
    {
        $this->autheremail = $autheremail;

        return $this;
    }

    /**
     * Get autheremail
     *
     * @return string 
     */
    public function getAutheremail()
    {
        return $this->autheremail;
    }

    /**
     * Set articletitle
     *
     * @param string $articletitle
     * @return Article
     */
    public function setArticletitle($articletitle)
    {
        $this->articletitle = $articletitle;

        return $this;
    }

    /**
     * Get articletitle
     *
     * @return string 
     */
    public function getArticletitle()
    {
        return $this->articletitle;
    }

    /**
     * Set discription
     *
     * @param string $discription
     * @return Article
     */
    public function setDiscription($discription)
    {
        $this->discription = $discription;

        return $this;
    }

    /**
     * Get discription
     *
     * @return string 
     */
    public function getDiscription()
    {
        return $this->discription;
    }

    /**
     * Set catforfilter
     *
     * @param string $catforfilter
     * @return Article
     */
    public function setCatforfilter($catforfilter)
    {
        $this->catforfilter = $catforfilter;

        return $this;
    }

    /**
     * Get catforfilter
     *
     * @return string 
     */
    public function getCatforfilter()
    {
        return $this->catforfilter;
    }

    /**
     * Set typeofactivity
     *
     * @param string $typeofactivity
     * @return Article
     */
    public function setTypeofactivity($typeofactivity)
    {
        $this->typeofactivity = $typeofactivity;

        return $this;
    }

    /**
     * Get typeofactivity
     *
     * @return string 
     */
    public function getTypeofactivity()
    {
        return $this->typeofactivity;
    }

    /**
     * Set duratonmin
     *
     * @param string $duratonmin
     * @return Article
     */
    public function setDuratonmin($duratonmin)
    {
        $this->duratonmin = $duratonmin;

        return $this;
    }

    /**
     * Get duratonmin
     *
     * @return string 
     */
    public function getDuratonmin()
    {
        return $this->duratonmin;
    }

    /**
     * Set durationmax
     *
     * @param string $durationmax
     * @return Article
     */
    public function setDurationmax($durationmax)
    {
        $this->durationmax = $durationmax;

        return $this;
    }

    /**
     * Get durationmax
     *
     * @return string 
     */
    public function getDurationmax()
    {
        return $this->durationmax;
    }

    /**
     * Set difficulty
     *
     * @param string $difficulty
     * @return Article
     */
    public function setDifficulty($difficulty)
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * Get difficulty
     *
     * @return string 
     */
    public function getDifficulty()
    {
        return $this->difficulty;
    }

    /**
     * Set zone
     *
     * @param string $zone
     * @return Article
     */
    public function setZone($zone)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return string 
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set continent
     *
     * @param string $continent
     * @return Article
     */
    public function setContinent($continent)
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * Get continent
     *
     * @return string 
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Article
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Article
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Article
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set destination
     *
     * @param string $destination
     * @return Article
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set getaways
     *
     * @param string $getaways
     * @return Article
     */
    public function setGetaways($getaways)
    {
        $this->getaways = $getaways;

        return $this;
    }

    /**
     * Get getaways
     *
     * @return string 
     */
    public function getGetaways()
    {
        return $this->getaways;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Article
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set season
     *
     * @param string $season
     * @return Article
     */
    public function setSeason($season)
    {
        $this->season = $season;

        return $this;
    }

    /**
     * Get season
     *
     * @return string 
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set bucketlist
     *
     * @param string $bucketlist
     * @return Article
     */
    public function setBucketlist($bucketlist)
    {
        $this->bucketlist = $bucketlist;

        return $this;
    }

    /**
     * Get bucketlist
     *
     * @return string 
     */
    public function getBucketlist()
    {
        return $this->bucketlist;
    }

    /**
     * Set vote
     *
     * @param integer $vote
     * @return Article
     */
    public function setVote($vote)
    {
        $this->vote = $vote;

        return $this;
    }

    /**
     * Get vote
     *
     * @return integer 
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Set datecreated
     *
     * @param string $datecreated
     * @return Article
     */
    public function setDatecreated($datecreated)
    {
        $this->datecreated = $datecreated;

        return $this;
    }

    /**
     * Get datecreated
     *
     * @return string 
     */
    public function getDatecreated()
    {
        return $this->datecreated;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Article
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set publish
     *
     * @param integer $publish
     * @return Article
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return integer 
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set showdetails
     *
     * @param integer $showdetails
     * @return Article
     */
    public function setShowdetails($showdetails)
    {
        $this->showdetails = $showdetails;

        return $this;
    }

    /**
     * Get showdetails
     *
     * @return integer 
     */
    public function getShowdetails()
    {
        return $this->showdetails;
    }

    /**
     * Set foldername
     *
     * @param string $foldername
     * @return Article
     */
    public function setFoldername($foldername)
    {
        $this->foldername = $foldername;

        return $this;
    }

    /**
     * Get foldername
     *
     * @return string 
     */
    public function getFoldername()
    {
        return $this->foldername;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return Article
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set unique
     *
     * @param string $unique
     * @return Article
     */
    public function setUnique($unique)
    {
        $this->unique = $unique;

        return $this;
    }

    /**
     * Get unique
     *
     * @return string 
     */
    public function getUnique()
    {
        return $this->unique;
    }
}
