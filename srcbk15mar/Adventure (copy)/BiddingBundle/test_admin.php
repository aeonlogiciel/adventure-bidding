<?php 
error_reporting(0);
if(session_id()==null || session_id()==""){session_start();}

if(!isset($_SESSION['id'])){
  echo "<script>location.href='login.php'</script>";
}

 require("../config/config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Dashboard - SB Admin</title>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Add custom CSS here -->
<link href="css/sb-admin.css" rel="stylesheet">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<!-- Page Specific CSS -->
<link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
</head>
<body>
<div id="wrapper">
<!-- Sidebar -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    <a class="navbar-brand" href="index.php">SB Admin</a> </div>
  <?php 

		$users=mysql_query("select * from users where  status ='active' and user_type='0' and flag='0'");

		$organiser=mysql_query("select * from organiser where flag='0'");
		$club=mysql_query("select * from club where flag='0'");
		
		$expert=mysql_query("select * from users Where  status ='active' and user_type='1'");
                $newarticle=mysql_fetch_array( mysql_query("SELECT count(id) FROM `article` where  `publish` = '0'"));
                $newalbum = mysql_fetch_array( mysql_query("SELECT COUNT( id ) FROM  `contributeimage` WHERE  `category` =  'album' AND  `status` =  'inactive'"));
		$newvideo = mysql_fetch_array( mysql_query("SELECT COUNT( id ) FROM  `contributevideo` WHERE  `category` =  'video' AND  `status` =  'inactive'"));
        $newurl = mysql_fetch_array( mysql_query("SELECT COUNT( Id ) FROM  `mail` WHERE  `status` =  '0' "));
		 ?>
  <!-- Collect the nav links, forms, and other content for toggling 
. All Post 19. All Pages 20. Add Pages 21.New Post(flipboard) 22. Old Server Backup
  -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
      <li class="active"><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class=""><a href="displayHome.php"><i class="fa fa-globe"></i> Display</a></li>
      <li><a href="listView.php"><i class="fa fa-user"></i> Users(<span style="color:#9AFF03"><?php echo mysql_num_rows($users); ?></span>)</a></li>
      <li class=""><a href="articleViewAll.php"><i class="fa fa-list"></i> Articles(<span style="color:#9AFF03"><?php echo $newarticle['0'];?></span>)</a></li>
      <li class=""><a href="albumViewAll.php"><i class="fa fa-camera"></i> Albums(<span style="color:#9AFF03"><?php echo $newalbum['0'];?></span>)</a></li>
      <li class=""><a href="videoViewAll.php"><i class="fa fa-play-circle"></i> Videos(<span style="color:#9AFF03"><?php echo $newvideo['0'];?></span>)</a></li>
      <li class=""><a href="urlViewAll.php"><i class="fa fa-link"></i> Urls(<span style="color:#9AFF03"><?php echo $newurl['0'];?></span>)</a></li>
      <li><a href="expert.php"><i class="fa fa-user"></i> Experts(<span style="color:#9AFF03"><?php echo mysql_num_rows($expert); ?></span>)</a></li>
      <li><a href="clubInfo.php"><i class="fa fa-user"></i> Clubs(<span style="color:#9AFF03"><?php echo mysql_num_rows($club); ?></span>)</a></li>
      <li><a href="adminEvent.php"><i class="fa fa-bullhorn"></i> Events(<span style="color:#9AFF03"><?php echo mysql_num_rows(mysql_query("select * from event where flag='0'")); ?></span>)</a></li>
      <li><a href="organiserInfo.php"><i class="fa fa-user"></i> Organizers(<span style="color:#9AFF03"><?php echo mysql_num_rows($organiser); ?></span>)</a></li>
      <li><a href="allStore.php"><i class="fa fa-bar-chart-o"></i> Stores(<span style="color:#9AFF03"><?php echo mysql_num_rows(mysql_query("select * from store where flag='0'")); ?></span>)</a></li>
      <li><a href="allCompetition.php"><i class="fa fa-trophy"></i> Competition Posts</a></li>
      <li><a href="competitionRegistration.php"><i class="fa fa-bar-chart-o"></i> Create New Competition</a></li>
      <li class=""><a href="allActivities.php"><i class="fa fa-pencil"></i> Activities</a></li>
      <li class=""><a href="allProvince.php"><i class="fa fa-map-marker"></i> Province</a></li>
      <li class=""><a href="addProvince.php"><i class="fa fa-map-marker"></i> Add Province</a></li>
      <li class=""><a href="allLocations.php"><i class="fa fa-road"></i> Locations</a></li>
      <li><a href="allGetaway.php"><i class="fa fa-bar-chart-o"></i> Getaways</a></li>  
      <li><a href="allCatalogue.php"><i class="fa fa-shopping-cart"></i> Catalogues</a></li>
      <li><a href="cataloguerequest.php"><i class="fa fa-bar-chart-o"></i> Catalogue Requests(<span style="color:#9AFF03"><?php echo mysql_num_rows(mysql_query("SELECT * FROM reimbursement WHERE flag='0'")); ?></span>)</a></li>
      <li><a href="allPosts.php"><i class="fa fa-check"></i> Posts</a></li>
      <li><a href="allPages.php"><i class="fa fa-bar-chart-o"></i> Pages</a></li>
      <li><a href="pages.php"><i class="fa fa-bar-chart-o"></i> Add Page</a></li>
      <li><a href="insert.php"><i class="fa fa-user"></i> New Post(Flipboard)</a></li>
      <li><a href="adminContributer.php"><i class="fa fa-bar-chart-o"></i> Contributer(<span style="color:#9AFF03"><?php echo mysql_num_rows(mysql_query("select * from contribute where flag='0'")); ?></span>)</a></li>
      <li><a href="serverData.php"><i class="fa fa-suitcase"></i> Old Server Backup</a></li>
      

    </ul>
    <ul class="nav navbar-nav navbar-right navbar-user">
  
      <li class="dropdown user-dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
              <?php if(isset($_SESSION['id'])){ echo "ADMIN" ; }  ?> <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="pwdChange.php"><i class="fa fa-user"></i> Profile</a></li>
          <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
          <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
          <li class="divider"></li>
          <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <!-- /.navbar-collapse -->
</nav>