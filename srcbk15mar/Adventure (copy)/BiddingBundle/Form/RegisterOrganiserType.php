<?php

namespace Adventure\BiddingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;


/**
 * Description of RegisterOrganiserType
 *
 * @author Jhuma Mandal
 * Written: 08-12-2014
 */
class RegisterOrganiserType extends AbstractType{
    
    /*
     * Function : buildForm
     * Description: Creates organiser registration form
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-12-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * 
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $countries = Intl::getRegionBundle()->getCountryNames();
        $state = getBridgeBundle()->getStateNames();
        
        $builder->add('name','text', array('required'=> true))
                ->add('email','email', array('required'=> true,'disabled'=> true))
                ->add('phone','text', array('required'=> true))
                ->add('address','textarea',array('required'=> true))
                ->add('zip','integer',array('required'=> false))
                ->add('activity', 'entity', array(
                    'class' => 'AdventureBiddingBundle:BidActivity',
                    'property' => 'typeOfActivity',
                    'empty_value' => 'Select',
                    'multiple'      => true
                ))
                ->add('zone', 'choice', array(
                      'choices' => array(
                            'east' => 'East',
                            'west' => 'West',
                            'north' => 'North',
                            'south' => 'South')),array('required'=> true))
                
                ->add('country','choice', array(
                    'choices' => array(
                        $countries
                )),array('required'=> false))
                ->add('applicantphoto','file', array('label' => 'Image', 'data_class' => null,'required'=> false))
               ;
    }
        
    public function getName() {
        return 'organiserRegister';
    }
    
     public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Adventure\BiddingBundle\Entity\BidOrganiser'
        ));
    }
}

