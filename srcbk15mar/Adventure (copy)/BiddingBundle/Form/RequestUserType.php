<?php

namespace Adventure\BiddingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;

/**
 * Description of RequestUserType
 *
 * @author Jhuma Mandal
 * Written: 08-12-2014
 */
class RequestUserType extends AbstractType{
    
    /*
     * Function : buildForm
     * Description: Creates user request form
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 26-12-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * 
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $countries = Intl::getRegionBundle()->getCountryNames();
        $builder->add('name','text', array('required'=> true))
                ->add('activity', 'entity', array(
                    'class' => 'AdventureBiddingBundle:BidActivity',
                    'property' => 'typeOfActivity',
                    'empty_value' => 'Select',
                    'multiple'  => true
                ),array('required'=> true))
                ->add('zone', 'choice', array(
                      'choices' => array(
                            'east' => 'East',
                            'west' => 'West',
                            'north' => 'North',
                            'south' => 'South')),array('required'=> true))
                
                ->add('country','choice', array(
                    'choices' => array(
                        $countries
                )),array('required'=> false))
                
                ->add('state','choice', array(
                    'choices' => array(
                        'MP'=>'Madhya Pradesh',
                        'MH'=>'Maharastra',
                        'RJ'=>'Rajasthan'
                )),array('required'=> false))
                ->add('location','text', array('required'=> false))
                ->add('traveler', 'text', array('required'=> true ,
                        
                     'attr' =>array(
                         'readonly ' =>'readonly'
                     )))
                ->add('male', 'choice', array(
                      'choices' => array(
                            '0' => '0',
                            '1' => '1',
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',
                            '6' => '6',
                            '7' => '7',
                            '8' => '8',
                            '9' => '9',
                            '10' => '10',
                             '11' =>'11',
                            '12' => '12',
                            '13' => '13',
                            '14' => '14',
                            '15' => '15',
                            '16' => '16',
                            '17' => '17',
                            '18' => '18',
                            '19' => '19',
                            '20' => '20',
                             '21' =>'21',
                            '22' => '22',
                            '23' => '23',
                            '24' => '24',
                            '25' => '25',
                            '26' => '26',
                            '27' => '27',
                            '28' => '28',
                            '29' => '29',
                            '30' => '30',
                             '31' =>'31',
                            '32' => '32',
                            '33' => '33',
                            '34' => '34',
                            '35' => '35',
                            '36' => '36',
                            '37' => '37',
                            '38' => '38',
                            '39' => '39',
                            '40' => '40',
                            '41' =>'41',
                            '42' => '42',
                            '43' => '43',
                            '44' => '44',
                            '45' => '45',
                            '46' => '46',
                            '47' => '47',
                            '48' => '48',
                            '49' => '49',
                            '50' => '50'
                          ),
                     'attr' =>array(
                         'onchange ' =>'getValue()'
                     )
                    ))
                ->add('female', 'choice', array(
                      'choices' => array(
                            '0' => '0',
                            '1' => '1',
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',
                            '6' => '6',
                            '7' => '7',
                            '8' => '8',
                            '9' => '9',
                            '10' => '10',
                             '11' =>'11',
                            '12' => '12',
                            '13' => '13',
                            '14' => '14',
                            '15' => '15',
                            '16' => '16',
                            '17' => '17',
                            '18' => '18',
                            '19' => '19',
                            '20' => '20',
                             '21' =>'21',
                            '22' => '22',
                            '23' => '23',
                            '24' => '24',
                            '25' => '25',
                            '26' => '26',
                            '27' => '27',
                            '28' => '28',
                            '29' => '29',
                            '30' => '30',
                             '31' =>'31',
                            '32' => '32',
                            '33' => '33',
                            '34' => '34',
                            '35' => '35',
                            '36' => '36',
                            '37' => '37',
                            '38' => '38',
                            '39' => '39',
                            '40' => '40',
                            '41' =>'41',
                            '42' => '42',
                            '43' => '43',
                            '44' => '44',
                            '45' => '45',
                            '46' => '46',
                            '47' => '47',
                            '48' => '48',
                            '49' => '49',
                            '50' => '50'
                          ),
                     'attr' =>array(
                         'onchange ' =>'getValue()',
                         
                     )))

                ->add('dateMin','date',array('widget' => 'single_text','format'=>'dd-MM-yyyy'))
                ->add('dateMax','date',array('widget' => 'single_text','format'=>'dd-MM-yyyy'))
                ->add('day', 'choice', array(
                      'choices' => array(
                            '' => '0',
                            '1' => '1',
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',
                            '6' => '6',
                            '7' => '7',
                            '8' => '8',
                            '9' => '9',
                            '10' => '10'
                          )))
                ->add('pick','text',array('required'=> false))
                ->add('down','text',array('required'=> false))
                ->add('description','textarea',array('required'=> false))
                ->add('team', 'choice', array(
                      'choices' => array(
                            '1' => 'Yes',
                            '0' => 'No')),array('required'=> false))
                ->add('status','hidden',array('data'=> '1'))
               ;
    }
        
    public function getName() {
        return 'userRequest';
    }
    
     public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Adventure\BiddingBundle\Entity\BidUserRequest'
        ));
    }
}

