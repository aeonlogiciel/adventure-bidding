<?php

namespace Adventure\BiddingBundle\EventHandler;
 
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Adventure\BiddingBundle\Events\RequestMatchEvent;

/**
 * Description of RequestMatchSubscriber
 *
 * @author Jhuma Mandal
 * written: 08-Jan-2015
 */
class RequestMatchSubscriber implements EventSubscriberInterface{
    //Call events for the subscriber
    public static function getSubscribedEvents() {
         
         return array(
         \Adventure\BiddingBundle\Events\CoreEvents::REQUEST_MATCH=>'onRequestMatch',
        );
    }
    
    /*
     * Function : onRequestMatch
     * Description: execute this function when event is fired
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 08-Jan-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * 
     */
    public function onRequestMatch(RequestMatchEvent $match)
    {
        $requestMatch = $match->getRequestMatch();
        
        //echo "Jhuma";exit;
    }
    
    
}
