<?php

namespace Adventure\BiddingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Description of GeneralController
 *
 * @author Jhuma Mandal
 * written : -Dec-2014
 * Description : Controller to control common functions of user and organiser.
 */
class GeneralController extends Controller
{
    //put your code here
}
