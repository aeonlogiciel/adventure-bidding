<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * BidUserRequestRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class BidUserRequestRepository extends EntityRepository
{
    /*
     * Function : getRequestByMemberId
     * Description: Get deails of user request with reference to member id.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 29-Dec-2014
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function getRequestByMemberId($memberId) {
        
        $query = $this->getEntityManager()->createQuery(
                'SELECT u.id FROM AdventureBiddingBundle:BidUser u WHERE u.memberId = :memId'
                )
                ->setParameter('memId', $memberId)
                ;
            $result = $query->getResult();
           
          $ID =  $result[0]['id'];
                             
            $query = $this->getEntityManager()->createQuery(
                        'SELECT r FROM AdventureBiddingBundle:BidUserRequest r JOIN r.user u WHERE u.id = :memId ORDER by r.id DESC'
                )
                ->setParameter('memId', $ID)
                ;
        $result = $query->getResult();
        
        return $result;
    }
    
    /*
     * Function : getMatchingOrganiser
     * Description: Get list of organiser matching to the user request details.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 07-Jan-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function getMatchingOrganiser($requestId,$memberId) {
     
        //Query to get activity id from user generated request id.
         $queryRequest = $this->getEntityManager()->createQuery(
                        'SELECT a.id FROM AdventureBiddingBundle:BidUserRequest r JOIN r.activity a WHERE r.id = :reqId '
                )
                ->setParameter('reqId', $requestId)
                ;
        $resultRequest = $queryRequest->getResult();
        $actId = array();
        $orgEmail = array();
        foreach ($resultRequest as $valueAct) {
            $actId[] = $valueAct['id'];
            
        }
     
        $totalAct = sizeof($actId);
            for($i=0;$i<$totalAct;$i++){
            //Query to get all organiser email id according to the activity id.
            $queryOrganiser = $this->getEntityManager()->createQuery(
                        'SELECT o.name FROM AdventureBiddingBundle:BidOrganiser o JOIN o.activity a WHERE a.id = :activityId '
                )
                ->setParameter('activityId', $actId[$i])
                ;
            $resultOrganiser = $queryOrganiser->getResult();
            foreach ($resultOrganiser as $valueOrg) {
                $orgEmail[] = $valueOrg['name'];
            }
           }
    
           //Getting unique email id of organisers. 
           $list = array_unique($orgEmail);
        
        //Returning organiser list conducting the request activity.
        return $list;
        
    }
    
    /*
     * Function : getDetailByRequestId
     * Description: Get detail of request with reference to request id.
     * ###########################################
     * Author: Jhuma Mandal
     * Written: 12-Jan-2015
     * ------------------------------------------
     * Update Log
     * ------------------------------------------
     * ###########################################
     * @access public 
     * @param Symfony\Component\HttpFoundation\Request
     * 
     */
    public function getDetailByRequestId($id) {
        
        $query = $this->getEntityManager()->createQuery(
                        'SELECT u FROM AdventureBiddingBundle:BidUserRequest u WHERE u.id = :reqId '
                )
                ->setParameter('reqId', $id)
                ;
        $result = $query->getSingleResult();
        return $result;
    }
    
}
