<?php

namespace Adventure\BiddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BidOrganiserResponse
 */
class BidOrganiserResponse
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateMin;

    /**
     * @var \DateTime
     */
    private $dateMax;

    /**
     * @var string
     */
    private $pick;

    /**
     * @var string
     */
    private $down;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var integer
     */
    private $includeCost;

    /**
     * @var integer
     */
    private $excludeCost;

    /**
     * @var integer
     */
    private $totalCost;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;
    /**
     * @var string
     */
    private $role;
    
    /**
     * @var integer
     */
    private $flag;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateMin
     *
     * @param \DateTime $dateMin
     * @return BidOrganiserResponse
     */
    public function setDateMin($dateMin)
    {
        $this->dateMin = $dateMin;

        return $this;
    }

    /**
     * Get dateMin
     *
     * @return \DateTime 
     */
    public function getDateMin()
    {
        return $this->dateMin;
    }

    /**
     * Set dateMax
     *
     * @param \DateTime $dateMax
     * @return BidOrganiserResponse
     */
    public function setDateMax($dateMax)
    {
        $this->dateMax = $dateMax;

        return $this;
    }

    /**
     * Get dateMax
     *
     * @return \DateTime 
     */
    public function getDateMax()
    {
        return $this->dateMax;
    }

    /**
     * Set pick
     *
     * @param string $pick
     * @return BidOrganiserResponse
     */
    public function setPick($pick)
    {
        $this->pick = $pick;

        return $this;
    }

    /**
     * Get pick
     *
     * @return string 
     */
    public function getPick()
    {
        return $this->pick;
    }

    /**
     * Set down
     *
     * @param string $down
     * @return BidOrganiserResponse
     */
    public function setDown($down)
    {
        $this->down = $down;

        return $this;
    }

    /**
     * Get down
     *
     * @return string 
     */
    public function getDown()
    {
        return $this->down;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return BidOrganiserResponse
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return BidOrganiserResponse
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }
    /**
     * Set status
     *
     * @param integer $status
     * @return BidOrganiserResponse
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set includeCost
     *
     * @param integer $includeCost
     * @return BidOrganiserResponse
     */
    public function setIncludeCost($includeCost)
    {
        $this->includeCost = $includeCost;

        return $this;
    }

    /**
     * Get includeCost
     *
     * @return integer
     */
    public function getIncludeCost()
    {
        return $this->includeCost;
    }

    /**
     * Set excludeCost
     *
     * @param integer $excludeCost
     * @return BidOrganiserResponse
     */
    public function setExcludeCost($excludeCost)
    {
        $this->excludeCost = $excludeCost;

        return $this;
    }

    /**
     * Get excludeCost
     *
     * @return integer 
     */
    public function getExcludeCost()
    {
        return $this->excludeCost;
    }

    /**
     * Set totalCost
     *
     * @param integer $totalCost
     * @return BidOrganiserResponse
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    /**
     * Get totalCost
     *
     * @return integer 
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return BidOrganiserResponse
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer 
     */
    public function getFlag()
    {
        return $this->flag;
    }
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BidOrganiserResponse
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return BidOrganiserResponse
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedOnValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedOnValue()
    {
        $this->updatedAt = new \DateTime();
    }
  
    /**
     * @var \Adventure\BiddingBundle\Entity\BidUserRequest
     */
    private $request;


    /**
     * Set request
     *
     * @param \Adventure\BiddingBundle\Entity\BidUserRequest $request
     * @return BidOrganiserResponse
     */
    public function setRequest(\Adventure\BiddingBundle\Entity\BidUserRequest $request )
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return \Adventure\BiddingBundle\Entity\BidUserRequest 
     */
    public function getRequest()
    {
        return $this->request;
    }
    /**
     * @var \Adventure\BiddingBundle\Entity\BidOrganiser
     */
    private $organiser;


    /**
     * Set organiser
     *
     * @param \Adventure\BiddingBundle\Entity\BidOrganiser $organiser
     * @return BidOrganiserResponse
     */
    public function setOrganiser(\Adventure\BiddingBundle\Entity\BidOrganiser $organiser )
    {
        $this->organiser = $organiser;

        return $this;
    }

    /**
     * Get organiser
     *
     * @return \Adventure\BiddingBundle\Entity\BidOrganiser 
     */
    public function getOrganiser()
    {
        return $this->organiser;
    }
    /**
     * @var \Adventure\BiddingBundle\Entity\BidUser
     */
    private $user;


    /**
     * Set user
     *
     * @param \Adventure\BiddingBundle\Entity\BidUser $user
     * @return BidOrganiserResponse
     */
    public function setUser(\Adventure\BiddingBundle\Entity\BidUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Adventure\BiddingBundle\Entity\BidUser 
     */
    public function getUser()
    {
        return $this->user;
    }
}
